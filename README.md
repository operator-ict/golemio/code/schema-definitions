<div align="center">
<p>
    <a href="https://operatorict.cz">
        <img src="https://gitlab.com/operator-ict/golemio/code/modules/core/-/raw/development/.assets/oict_logo.png" alt="oict" width="100px" height="100px" />
    </a>
    <a href="https://golemio.cz">
        <img src="https://gitlab.com/operator-ict/golemio/code/modules/core/-/raw/development/.assets/golemio_logo.png" alt="golemio" width="100px" height="100px" />
    </a>
</p>

<h1>@golemio/schema-definitions</h1>

<p>
    <a href="https://gitlab.com/operator-ict/golemio/code/modules/schema-definitions/commits/master">
        <img src="https://gitlab.com/operator-ict/golemio/code/modules/schema-definitions/badges/master/pipeline.svg" alt="pipeline">
    </a>
    <a href="./LICENSE">
        <img src="https://img.shields.io/npm/l/@golemio/schema-definitions" alt="license">
    </a>
</p>

<p>
    <a href="#installation">Installation</a> · <a href="./docs">Documentation</a>
</p>
</div>

> :warning: This module has been archived and is **no longer maintained**.

This module is intended for use with Golemio services. It only contains shared database migrations.

## Installation

We recommend to install this module as an exact version.

```bash
# Production (Golem) migrations
npm install --save-exact @golemio/schema-definitions@latest

# Development (Rabin) migrations
npm install --save-exact @golemio/schema-definitions@dev
```

## Description

Shared database Migrations of the Golemio Data Platform System.

## Local installation

### Prerequisites

-   node.js
-   npm
-   PostgreSQL

### Install dependencies

Install all dependencies using command

```
npm install
```

from the application's root directory.

## Database Migrations

For database migrations set the env variables `POSTGRES_CONN` or create .env file (copy .env.template) and set the configuration variables and then run:

```
npm run migrate-db
```

or

for Postgre:

```
npm run migrate-postgres-db
```

For Postgre the database has to exist.

For more informations read [db-migrate docs](https://db-migrate.readthedocs.io/en/latest/) and _migration manual_ in `docs/migrations.md` file.

## Contribution guidelines

Please read [CONTRIBUTING.md](CONTRIBUTING.md).
