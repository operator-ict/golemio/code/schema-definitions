CREATE TABLE public.tsk_std (
    "id" varchar(50) NOT NULL,
    "latitude" double precision,
    "longitude" double precision,
    "street" varchar(100),
    "segment" varchar(100),
    "direction" varchar(100),
    "note" varchar(100) NOT NULL,

    CONSTRAINT "tsk_std_detectors_pkey" PRIMARY KEY ("id")
) WITH (oids = false);


--DETECTOR MEASUREMENTS
CREATE TABLE public.tsk_std_measurements (
    "detector_id" varchar(50) NOT NULL, --
    "measured_from" bigint NOT NULL, --
    "measured_to" bigint NOT NULL, --

    "measurement_type" varchar(50) NOT NULL, --
    "class_id" integer NOT NULL, --
    "value" integer NOT NULL, --

    "create_batch_id" bigint,
    "created_at" timestamp with time zone,
    "created_by" character varying(150),
    "update_batch_id" bigint,
    "updated_at" timestamp with time zone,
    "updated_by" character varying(150),
    CONSTRAINT tsk_std_measurements_pkey PRIMARY KEY (detector_id, measured_from, measured_to, measurement_type, class_id)
);
CREATE INDEX tsk_std_measurements_detector_id
    ON public.tsk_std_measurements USING btree
    (detector_id COLLATE pg_catalog."default" ASC NULLS LAST)
    TABLESPACE pg_default;
CREATE INDEX tsk_std_measurements_measurement_type
    ON public.tsk_std_measurements USING btree
    (measurement_type COLLATE pg_catalog."default" ASC NULLS LAST)
    TABLESPACE pg_default;


--DETECTOR ERRORS
CREATE TABLE public.tsk_std_errors (
    "detector_id" varchar(50) NOT NULL, --
    "error_desc" TEXT NOT NULL, --
    "error_id" varchar(50) NOT NULL, --

    "measured_at" bigint NOT NULL, --
    "measured_at_iso" timestamp with time zone,

    "create_batch_id" bigint,
    "created_at" timestamp with time zone,
    "created_by" character varying(150),
    "update_batch_id" bigint,
    "updated_at" timestamp with time zone,
    "updated_by" character varying(150),
    CONSTRAINT tsk_std_errors_pkey PRIMARY KEY (detector_id, error_id, measured_at)
);
CREATE INDEX tsk_std_errors_detector_id
    ON public.tsk_std_errors USING btree
    (detector_id COLLATE pg_catalog."default" ASC NULLS LAST)
    TABLESPACE pg_default;


--DETECTOR TYPES
CREATE TABLE public.tsk_std_detector_types (
    "id" integer NOT NULL, -- airqualitystations.id, e.g. CZ_A_AKALA
    "detector_type" varchar(50) NOT NULL, -- States[].DateFromUTC, e.g. 159242949294
    "description_cs" varchar(200) NOT NULL, -- States[].DateToUTC, e.g. 159244949294
    "description_en" varchar(200), -- airqualitystations_metadata_indexes.index_code, e.g. 1A

    CONSTRAINT "tsk_std_detector_types_pkey" PRIMARY KEY ("id")
);
INSERT INTO public.tsk_std_detector_types
    ("id", "detector_type", "description_cs", "description_en")
VALUES
    (1, 'SCALA', 'Dopravní data z dopravní ústředny SCALA', ''),
    (2, 'CollectR', 'Strategické detektory II. generace', ''),
    (3, 'Traficam', 'Strategické detektory I. generace', ''),
    (4, 'SDDU', 'Strategický dopravní detektor úsekový (Camea)', ''),
    (5, 'MUR', 'Měření úsekové rychlosti (Camea)', ''),
    (6, 'DK', 'Dohledové kamery (Camea)', ''),
    (7, 'INFO', 'Info (Camea)', ''),
    (8, 'MP', 'Měřící profil (Camea)', ''),
    (9, 'RL', 'Detekce jízdy na červenou (Camea)', ''),
    (10, 'WIM', 'Měření hmotnosti (Camea)', '');


--MEASUREMENT CLASSES
CREATE TABLE public.tsk_std_class_types (
    "id" integer NOT NULL, --
    "detector_type_id" integer NOT NULL, --
    "class_type" varchar(20) NOT NULL, --
    "description_cs" varchar(200) NOT NULL, --
    "description_en" varchar(200), --

    CONSTRAINT "tsk_std_class_types_pkey" PRIMARY KEY ("id")
);
INSERT INTO public.tsk_std_class_types
    ("id", "detector_type_id", "class_type", "description_cs", "description_en")
VALUES
    (1, 2, 'All', 'Všechny kategorie dohromady (počet vozidel, rychlost, obsazenost)', ''),
    (2, 2, '1', 'Osobní vozidla (počet vozidel, rychlost)', ''),
    (3, 2, '2', 'Nákladní vozidla (počet vozidel, rychlost)', ''),
    (4, 2, 'All', 'Všechny kategorie dohromady (počet vozidel, rychlost, obsazenost)', ''),
    (5, 2, 'All', 'Všechny kategorie dohromady (počet vozidel, rychlost, obsazenost)', ''),
    (6, 2, 'All', 'Všechny kategorie dohromady (počet vozidel, rychlost, obsazenost)', ''),
    (7, 2, 'All', 'Všechny kategorie dohromady (počet vozidel, rychlost, obsazenost)', ''),

    (8, 3, 'All', 'Všechny kategorie dohromady (počet vozidel, rychlost, obsazenost)', ''),

    (9, 1, 'All', 'Všechny kategorie dohromady (počet vozidel, obsazenost)', '');


--STATUS TYPES
CREATE TABLE public.tsk_std_status_types (
    "id" integer NOT NULL,
    "detector_type_id" integer NOT NULL, --
    "status_type" varchar(50) NOT NULL, --
    "description_cs" varchar(50), --
    "description_en" varchar(50), --

    CONSTRAINT "tsk_std_status_types_pkey" PRIMARY KEY ("id")
);
INSERT INTO public.tsk_std_status_types
    ("id", "detector_type_id", "status_type", "description_cs", "description_en")
VALUES
    (1, 2, '0', 'Síťové napětí nepřítomno', ''),
    (2, 2, '1', 'Jistič nenahozen', ''),
    (3, 2, '2', 'Přepěťovka nenahozena', ''),
    (4, 2, '3', 'Dveře otevřeny', ''),
    (5, 2, '4', 'Porucha kamery 1', ''),
    (6, 2, '5', 'Porucha kamery 2', ''),
    (7, 2, '6', 'Porucha kamery 1', ''),
    (8, 2, '7', 'Porucha kamery 2', ''),
    (9, 2, '8', 'Napětí na baterii', ''),
    (10, 2, '9', 'Teplota v rozvaděči', ''),

    (11, 3, '0_INT', 'Intenzita: Data jsou v pořádku', ''),
    (12, 3, '0_OCP', 'Obsazenost: Data jsou v pořádku', ''),
    (13, 3, '0_SPD', 'Rychlost: Data jsou v pořádku', ''),
    (14, 3, '1_INT', 'Intenzita: Data jsou nevěrohodná', ''),
    (15, 3, '1_OCP', 'Obsazenost: Data jsou nevěrohodná', ''),
    (16, 3, '1_SPD', 'Rychlost: Data jsou nevěrohodná', ''),
    (17, 3, '2_INT', 'Intenzita: Data jsou neplatná', ''),
    (18, 3, '2_OCP', 'Obsazenost: Data jsou neplatná', ''),
    (19, 3, '2_SPD', 'Rychlost: Data jsou neplatná', '');


--FAILURES TYPES
CREATE TABLE public.tsk_std_failure_types (
    "id" integer NOT NULL,
    "detector_type_id" integer NOT NULL, --
    "failure_type" integer NOT NULL, --
    "description_cs" varchar(50), --
    "description_en" varchar(50), --

    CONSTRAINT "tsk_std_failure_types_pkey" PRIMARY KEY ("id")
);
INSERT INTO public.tsk_std_failure_types
    ("id", "detector_type_id", "failure_type", "description_cs", "description_en")
VALUES
    (1, 2, '2', 'Není spojení s radiovou sítí', ''),
    (2, 2, '3', 'Není spojení s detektorem', ''),
    (3, 2, '131072', 'Data definitivně ztracena', ''),

    (4, 1, '2', 'Není spojení s radiovou sítí', ''),
    (5, 1, '3', 'Není spojení s detektorem', ''),
    (6, 1, '4', 'Detektor neodpovídá na výzvu serveru', ''),
    (7, 1, '5', 'Detektoru není synchronizován se serverem', '');
