--DETECTORS
DROP TABLE public.tsk_std;
--DETECTOR MEASUREMENTS
DROP INDEX tsk_std_measurements_detector_id;
DROP INDEX tsk_std_measurements_measurement_type;
DROP TABLE public.tsk_std_measurements;
--DETECTOR ERRORS
DROP INDEX tsk_std_errors_detector_id;
DROP TABLE public.tsk_std_errors;
--DETECTOR TYPES
DROP TABLE public.tsk_std_detector_types;
--MEASUREMENT CLASSES
DROP TABLE public.tsk_std_class_types;
--STATUS TYPES
DROP TABLE public.tsk_std_status_types;
--FAILURES TYPES
DROP TABLE public.tsk_std_failure_types;