drop VIEW analytic.v_public_tenders_internal_frequent_suppliers;

CREATE OR REPLACE VIEW analytic.v_public_tenders_internal_frequent_suppliers
AS SELECT count(DISTINCT pt.id_zakazky) AS pocet_zakazek,
    pt.organizacni_jednotka_spravy,
    pt.varianta_druhu_rizeni,
        CASE
            WHEN ptpi.ico IS NOT NULL THEN 'Ano'::text
            ELSE 'Ne'::text
        END AS inhouse,
    pt.datum_uzavreni_smlouvy,
    pt.datum_vytvoreni_zakazky,
    pt.faze_zakazky,
        CASE
            WHEN pto.odbor IS NOT NULL THEN 'MHMP'::text
            ELSE 'Příspěvková organizace'::text
        END AS mhmp_po,
        CASE
            WHEN pt.varianta_druhu_rizeni = 'Neveřejná výzva k podání nabídky'::text THEN s.spolecnosti_oslovene_k_podani_nabidky_nazev
            ELSE s2.nazev_smluvniho_partnera
        END AS spolecnost,
    sum(
        CASE
            WHEN s.spolecnosti_oslovene_k_podani_nabidky_nazev = s2.nazev_smluvniho_partnera OR s.spolecnosti_oslovene_k_podani_nabidky_nazev IS NULL THEN 1
            ELSE 0
        END) AS pocet_vyhranych_zakazek,
        CASE
            WHEN pt.datum_uzavreni_smlouvy < '2020-09-01'::date THEN 'Uzavření smlouvy před datem vytvoření zakázky'::text
            WHEN pt.datum_uzavreni_smlouvy > pt.updated_at THEN 'Uzavření smlouvy v budoucnosti'::text
            ELSE 'Září 2020 - nyní'::text
        END AS uzavreni_smlouvy_filtr
   FROM python.public_tenders pt
     LEFT JOIN LATERAL unnest(string_to_array(pt.spolecnosti_oslovene_k_podani_nabidky_nazev, '; '::text)) s(spolecnosti_oslovene_k_podani_nabidky_nazev) ON true
     LEFT JOIN LATERAL unnest(string_to_array(pt.nazev_smluvniho_partnera, '; '::text)) s2(nazev_smluvniho_partnera) ON true
     LEFT JOIN LATERAL unnest(string_to_array(pt.ico_smluvniho_partnera, '; '::text)) s3(ico_smluvniho_partnera) ON true
     LEFT JOIN python.public_tenders_odbory pto ON pt.organizacni_jednotka_spravy ~~ (('%'::text || pto.odbor::text) || '%'::text)
     LEFT JOIN python.public_tenders_partners_ico ptpi ON ptpi.ico::text = pt.ico_smluvniho_partnera
  WHERE (pt.faze_zakazky = ANY (ARRAY['Ukončeno plnění smlouvy na základě veřejné zakázky'::text, 'Zadána'::text])) AND (pt.varianta_druhu_rizeni = ANY (ARRAY['Neveřejná výzva k podání nabídky'::text, 'Drobná objednávka'::text, 'Přímo objednáno u jednoho dodavatele'::text, 'Zakázka zadaná na základě výjimky'::text])) AND (pt.vysledkem_je_ramcova_dohoda = 'Ne'::text OR (pt.varianta_druhu_rizeni = ANY (ARRAY['Výzva k plnění rámcové smlouvy'::text, 'Minitendr'::text]))) AND (pt.datum_vytvoreni_zakazky >= '2020-09-01 01:00:00+02'::timestamp with time zone OR pt.datum_vytvoreni_zakazky >= '2021-01-01 00:00:00+01'::timestamp with time zone AND pto.odbor IS NOT NULL)
  GROUP BY pt.organizacni_jednotka_spravy, pt.varianta_druhu_rizeni, (
        CASE
            WHEN ptpi.ico IS NOT NULL THEN 'Ano'::text
            ELSE 'Ne'::text
        END), pt.datum_uzavreni_smlouvy, pt.datum_vytvoreni_zakazky, pt.faze_zakazky, (
        CASE
            WHEN pto.odbor IS NOT NULL THEN 'MHMP'::text
            ELSE 'Příspěvková organizace'::text
        END), (
        CASE
            WHEN pt.varianta_druhu_rizeni = 'Neveřejná výzva k podání nabídky'::text THEN s.spolecnosti_oslovene_k_podani_nabidky_nazev
            ELSE s2.nazev_smluvniho_partnera
        END), (
        CASE
            WHEN pt.datum_uzavreni_smlouvy < '2020-09-01'::date THEN 'Uzavření smlouvy před datem vytvoření zakázky'::text
            WHEN pt.datum_uzavreni_smlouvy > pt.updated_at THEN 'Uzavření smlouvy v budoucnosti'::text
            ELSE 'Září 2020 - nyní'::text
        END);


drop VIEW python.v_public_tenders_opendata;

alter table python.public_tenders 
drop column skutecne_uhrazena_cena_s_dph;
