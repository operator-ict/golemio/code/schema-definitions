-- analytic.v_containers_containers source

CREATE OR REPLACE VIEW analytic.v_containers_containers
AS WITH prep AS (
         SELECT COALESCE(containers_containers.sensoneo_code, containers_containers.code) AS container_code,
            containers_containers.id,
            containers_containers.code,
            containers_containers.knsko_id,
            containers_containers.knsko_code,
            containers_containers.station_code,
            containers_containers.total_volume,
            containers_containers.prediction,
            containers_containers.bin_type,
            containers_containers.installed_at,
            containers_containers.network,
            containers_containers.cleaning_frequency_interval,
            containers_containers.cleaning_frequency_frequency,
            containers_containers.company,
            containers_containers.container_type,
            containers_containers.trash_type,
            containers_containers.source,
            containers_containers.create_batch_id,
            containers_containers.created_at,
            containers_containers.created_by,
            containers_containers.update_batch_id,
            containers_containers.updated_at,
            containers_containers.updated_by,
            containers_containers.sensor_id,
            containers_containers.pick_days,
            containers_containers.sensoneo_code
           FROM containers_containers
          WHERE containers_containers.sensoneo_code IS NOT NULL OR (containers_containers.code::text IN ( SELECT DISTINCT m.container_code
                   FROM containers_measurement m
                     LEFT JOIN ( SELECT DISTINCT containers_containers_1.sensoneo_code
                           FROM containers_containers containers_containers_1) aa_1 ON m.container_code::text = aa_1.sensoneo_code::text
                  WHERE aa_1.sensoneo_code IS NULL))
        ), unn AS (
         SELECT prep.container_code AS code,
            unnest(string_to_array(prep.pick_days::text, ','::text))::integer AS day_num
           FROM prep
        ), translate AS (
         SELECT unn.code,
            unn.day_num,
                CASE
                    WHEN unn.day_num = 1 THEN 'Po'::text
                    WHEN unn.day_num = 2 THEN 'Út'::text
                    WHEN unn.day_num = 3 THEN 'St'::text
                    WHEN unn.day_num = 4 THEN 'Čt'::text
                    WHEN unn.day_num = 5 THEN 'Pá'::text
                    WHEN unn.day_num = 6 THEN 'So'::text
                    WHEN unn.day_num = 7 THEN 'Ne'::text
                    ELSE NULL::text
                END AS day_name
           FROM unn
        ), aa AS (
         SELECT translate.code,
            string_agg(translate.day_name, ', '::text ORDER BY translate.day_num) AS pick_days
           FROM translate
          GROUP BY translate.code
        )
 SELECT cc.container_code AS container_id,
    cc.total_volume,
    cc.network,
    cc.container_type,
    cc.bin_type,
        CASE
            WHEN cc.trash_type = 1 THEN 'glass coloured'::text
            WHEN cc.trash_type = 2 THEN 'electronics'::text
            WHEN cc.trash_type = 3 THEN 'metal'::text
            WHEN cc.trash_type = 4 THEN 'bevarage cartons'::text
            WHEN cc.trash_type = 5 THEN 'paper'::text
            WHEN cc.trash_type = 6 THEN 'plastic'::text
            WHEN cc.trash_type = 7 THEN 'glass clear'::text
            WHEN cc.trash_type = 8 THEN 'textile'::text
            ELSE NULL::text
        END AS trash_type,
        CASE
            WHEN cc.trash_type = 1 THEN 'Sklo barevné'::text
            WHEN cc.trash_type = 2 THEN 'Elektronika'::text
            WHEN cc.trash_type = 3 THEN 'Kovy'::text
            WHEN cc.trash_type = 4 THEN 'Nápojové kartony'::text
            WHEN cc.trash_type = 5 THEN 'Papír'::text
            WHEN cc.trash_type = 6 THEN 'Plast'::text
            WHEN cc.trash_type = 7 THEN 'Sklo čiré'::text
            WHEN cc.trash_type = 8 THEN 'Textil'::text
            ELSE NULL::text
        END AS typ_odpadu,
        CASE
            WHEN cc.trash_type = 1 THEN 411
            WHEN cc.trash_type = 3 THEN 120
            WHEN cc.trash_type = 4 THEN 55
            WHEN cc.trash_type = 5 THEN 76
            WHEN cc.trash_type = 6 THEN 39
            WHEN cc.trash_type = 7 THEN 411
            ELSE NULL::integer
        END AS koeficient,
    cc.cleaning_frequency_frequency,
    cc.cleaning_frequency_interval,
    cs.latitude,
    cs.longitude,
        CASE
            WHEN cs.address::text = ' Francouzská  240/76'::text THEN "substring"(cs.address::text, 2)::character varying
            ELSE cs.address
        END AS address,
    cd.district_cz,
    concat(1, "right"(split_part(cs.district::text, '-'::text, 2), 1), '000') AS psc,
    cc.sensor_id,
    cc.updated_at,
    aa.pick_days,
    row_number() OVER (PARTITION BY cs.address, cc.trash_type) AS row_number
   FROM prep cc
     LEFT JOIN containers_stations cs ON cc.station_code::text = cs.code::text
     LEFT JOIN analytic.containers_districts cd ON cs.district::text = cd.district::text
     LEFT JOIN aa ON cc.container_code::text = aa.code::text;