CREATE VIEW analytic.v_park_and_ride_capacities
AS SELECT DISTINCT ON (p.source_id) p.source_id,
    p.name,
    m.total_spot_number AS total_spot_number_measurements,
    to_timestamp((m.date_modified / 1000)::double precision) AS last_date
   FROM parkings p
     LEFT JOIN parkings_measurements_part m ON p.source_id::text = m.source_id::text
  WHERE p.category::text = 'park_and_ride'::text
  ORDER BY p.source_id, p.name, m.date_modified DESC;

CREATE VIEW analytic.v_park_and_ride
AS SELECT DISTINCT ON (pr.name, pr.updated_at_15_min) pr.name,
    pr.x,
    pr.y,
    pr.updated_at_15_min,
    pr.total_spot_number_measurements,
    pr.occupied_spot_number,
    pr.occupied_spot_number::double precision / NULLIF(pr.total_spot_number_measurements, 0)::double precision AS ocuupied_proportion,
    date_part('year'::text, pr.updated_at_15_min) AS year,
    date_part('month'::text, pr.updated_at_15_min) AS month,
    date_part('day'::text, pr.updated_at_15_min) AS day,
    date_part('dow'::text, pr.updated_at_15_min) AS dow,
    date_part('hour'::text, pr.updated_at_15_min) AS hour,
    date_part('minute'::text, pr.updated_at_15_min) AS minute,
    date_part('hour'::text, pr.updated_at_15_min) + date_part('minute'::text, pr.updated_at_15_min) / 60::double precision AS hour_quarter
   FROM ( SELECT p.source_id,
            p.name,
            st_x(p.location) AS x,
            st_y(p.location) AS y,
            p.total_spot_number AS total_spot_number_parking,
            m.available_spot_number,
            m.occupied_spot_number,
            m.total_spot_number AS total_spot_number_measurements,
            to_timestamp((m.date_modified / 1000)::double precision) AS updated_at_measurements,
            date_trunc('hour'::text, to_timestamp((m.date_modified / 1000)::double precision)) + ((date_part('minute'::text, to_timestamp((m.date_modified / 1000)::double precision)) + 7.5::double precision)::integer / 15)::double precision * '00:15:00'::interval AS updated_at_15_min
           FROM parkings p
             LEFT JOIN parkings_measurements_part m ON p.source_id::text = m.source_id::text
          WHERE p.category::text = 'park_and_ride'::text) pr
  WHERE pr.updated_at_15_min < date_trunc('month'::text, CURRENT_DATE::timestamp with time zone)
  ORDER BY pr.name, pr.updated_at_15_min;
