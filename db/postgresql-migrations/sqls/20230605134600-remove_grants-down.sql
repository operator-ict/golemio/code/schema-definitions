CREATE TABLE python.grants_applicants (
	nazev text NULL,
	ico text NULL,
	pravni_forma text NULL,
	ulice text NULL,
	cislo_popisne text NULL,
	cislo_orientacni text NULL,
	mestska_cast text NULL,
	psc text NULL,
	obec text NULL,
	id_zadatel text NOT NULL,
	updated_at timestamptz NULL,
	CONSTRAINT grants_applicants_pkey PRIMARY KEY (id_zadatel)
);

CREATE TABLE python.grants_projects (
	cislo_projektu text NULL,
	nazev_projektu text NULL,
	popis text NULL,
	stav text NULL,
	nazev_programu text NULL,
	typ_dotace text NULL,
	nazev_oblasti text NULL,
	ucel_dotace text NULL,
	rok_od int4 NULL,
	rok_do int4 NULL,
	castka_naklady float4 NULL,
	castka_pozadovana float4 NULL,
	castka_pridelena float4 NULL,
	castka_vycerpana float4 NULL,
	castka_vyuctovana float4 NULL,
	id_projekt text NOT NULL,
	id_zadatel text NULL,
	updated_at timestamptz NULL,
	CONSTRAINT grants_projects_pkey PRIMARY KEY (id_projekt)
);

CREATE TABLE python.grants_resolutions (
	cislo_usneseni text NULL,
	datum_schvaleni date NULL,
	schvalil text NULL,
	cislo_smlouvy text NULL,
	castka_pridelena float4 NULL,
	id_projekt text NULL,
	id_zadatel text NULL,
	updated_at timestamptz NULL
);

CREATE OR REPLACE VIEW analytic.v_grants_data_table
AS SELECT gr.cislo_usneseni,
    gr.datum_schvaleni,
    gr.schvalil,
    gr.castka_pridelena,
    gr.cislo_smlouvy,
    gp.id_projekt,
    gp.id_zadatel,
    gp.cislo_projektu,
    gp.nazev_projektu,
    gp.nazev_programu,
    gp.nazev_oblasti,
    gp.castka_pozadovana,
    gp.castka_vycerpana,
    gp.castka_vyuctovana,
    gp.stav,
    gp.rok_od,
    gp.rok_do,
    ga.nazev,
    ga.pravni_forma
   FROM python.grants_resolutions gr
     LEFT JOIN python.grants_projects gp ON gr.id_projekt = gp.id_projekt
     LEFT JOIN python.grants_applicants ga ON gp.id_zadatel = ga.id_zadatel;

CREATE OR REPLACE VIEW analytic.v_grants_last_update
AS SELECT gp.updated_at::date AS last_update
   FROM python.grants_projects gp
  ORDER BY gp.updated_at DESC
 LIMIT 1;

CREATE OR REPLACE VIEW analytic.v_grants_projects_applicants
AS SELECT gp.cislo_projektu,
    gp.nazev_projektu,
    gp.stav,
    gp.nazev_programu,
    gp.typ_dotace,
    gp.nazev_oblasti,
    gp.ucel_dotace,
    gp.rok_od,
    gp.rok_do,
    gp.castka_naklady,
    gp.castka_pozadovana,
    gp.castka_pridelena,
    gp.castka_vycerpana,
    gp.castka_vyuctovana,
    gp.id_projekt,
    gp.id_zadatel,
    ga.nazev,
    ga.pravni_forma
   FROM python.grants_projects gp
     LEFT JOIN python.grants_applicants ga ON gp.id_zadatel = ga.id_zadatel;
