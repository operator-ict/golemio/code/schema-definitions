/* Replace with your SQL commands */

drop view if exists analytic.v_ropidbi_ticket_zones;

-- analytic.v_ropidbi_ticket_zones source

CREATE OR REPLACE VIEW analytic.v_ropidbi_ticket_zones
AS SELECT c.created::date AS valid_from,
    c.customer_profile_name AS customer,
    c.tariff_profile_name AS tarif,
        CASE
            WHEN z.zone_name::text = ANY (ARRAY['Praha'::character varying, '0+B'::character varying]::text[]) THEN 'P+0+B'::character varying
            ELSE z.zone_name
        END AS zona,
        CASE
            WHEN z.zone_name::text = ANY (ARRAY['Praha'::character varying, '0+B'::character varying]::text[]) THEN '0'::character varying
            ELSE z.zone_name
        END AS zone_order,
    count(*) AS count
   FROM mos_be_coupons c
     LEFT JOIN mos_be_zones z ON z.coupon_id = c.coupon_id
  WHERE c.created::date IS NOT NULL AND z.zone_name IS NOT NULL
  GROUP BY (c.created::date), c.customer_profile_name, c.tariff_profile_name, (
        CASE
            WHEN z.zone_name::text = ANY (ARRAY['Praha'::character varying, '0+B'::character varying]::text[]) THEN 'P+0+B'::character varying
            ELSE z.zone_name
        END), (
        CASE
            WHEN z.zone_name::text = ANY (ARRAY['Praha'::character varying, '0+B'::character varying]::text[]) THEN '0'::character varying
            ELSE z.zone_name
        END)
  ORDER BY (c.created::date);