
-- vehiclepositions_positions

ALTER TABLE vehiclepositions_positions ADD create_batch_id bigint;
ALTER TABLE vehiclepositions_positions RENAME COLUMN created TO created_at;
ALTER TABLE vehiclepositions_positions ADD created_by character varying(150);
ALTER TABLE vehiclepositions_positions ADD update_batch_id bigint;
ALTER TABLE vehiclepositions_positions ADD updated_at timestamp with time zone;
ALTER TABLE vehiclepositions_positions ADD updated_by character varying(150);

-- vehiclepositions_stops

ALTER TABLE vehiclepositions_stops ADD create_batch_id bigint;
ALTER TABLE vehiclepositions_stops RENAME COLUMN created TO created_at;
ALTER TABLE vehiclepositions_stops ADD created_by character varying(150);
ALTER TABLE vehiclepositions_stops ADD update_batch_id bigint;
ALTER TABLE vehiclepositions_stops RENAME COLUMN modified TO updated_at;
ALTER TABLE vehiclepositions_stops ADD updated_by character varying(150);

-- vehiclepositions_trips

ALTER TABLE vehiclepositions_trips ADD create_batch_id bigint;
ALTER TABLE vehiclepositions_trips RENAME COLUMN created TO created_at;
ALTER TABLE vehiclepositions_trips ADD created_by character varying(150);
ALTER TABLE vehiclepositions_trips ADD update_batch_id bigint;
ALTER TABLE vehiclepositions_trips RENAME COLUMN modified TO updated_at;
ALTER TABLE vehiclepositions_trips ADD updated_by character varying(150);

-- merakiaccesspoints_observations

ALTER TABLE merakiaccesspoints_observations ADD create_batch_id bigint;
ALTER TABLE merakiaccesspoints_observations ADD created_at timestamp with time zone;
ALTER TABLE merakiaccesspoints_observations ADD created_by character varying(150);
ALTER TABLE merakiaccesspoints_observations ADD update_batch_id bigint;
ALTER TABLE merakiaccesspoints_observations ADD updated_at timestamp with time zone;
ALTER TABLE merakiaccesspoints_observations ADD updated_by character varying(150);

-- merakiaccesspoints_tags

ALTER TABLE merakiaccesspoints_tags ADD create_batch_id bigint;
ALTER TABLE merakiaccesspoints_tags ADD created_at timestamp with time zone;
ALTER TABLE merakiaccesspoints_tags ADD created_by character varying(150);
ALTER TABLE merakiaccesspoints_tags ADD update_batch_id bigint;
ALTER TABLE merakiaccesspoints_tags ADD updated_at timestamp with time zone;
ALTER TABLE merakiaccesspoints_tags ADD updated_by character varying(150);


-- ropidgtfs_agency

ALTER TABLE ropidgtfs_agency DROP created_time;
ALTER TABLE ropidgtfs_agency DROP last_modify;
ALTER TABLE ropidgtfs_agency ADD create_batch_id bigint;
ALTER TABLE ropidgtfs_agency ADD created_at timestamp with time zone;
ALTER TABLE ropidgtfs_agency ADD created_by character varying(150);
ALTER TABLE ropidgtfs_agency ADD update_batch_id bigint;
ALTER TABLE ropidgtfs_agency ADD updated_at timestamp with time zone;
ALTER TABLE ropidgtfs_agency ADD updated_by character varying(150);

-- ropidgtfs_calendar

ALTER TABLE ropidgtfs_calendar DROP created_time;
ALTER TABLE ropidgtfs_calendar DROP last_modify;
ALTER TABLE ropidgtfs_calendar ADD create_batch_id bigint;
ALTER TABLE ropidgtfs_calendar ADD created_at timestamp with time zone;
ALTER TABLE ropidgtfs_calendar ADD created_by character varying(150);
ALTER TABLE ropidgtfs_calendar ADD update_batch_id bigint;
ALTER TABLE ropidgtfs_calendar ADD updated_at timestamp with time zone;
ALTER TABLE ropidgtfs_calendar ADD updated_by character varying(150);

-- ropidgtfs_calendar_dates

ALTER TABLE ropidgtfs_calendar_dates DROP created_time;
ALTER TABLE ropidgtfs_calendar_dates DROP last_modify;
ALTER TABLE ropidgtfs_calendar_dates ADD create_batch_id bigint;
ALTER TABLE ropidgtfs_calendar_dates ADD created_at timestamp with time zone;
ALTER TABLE ropidgtfs_calendar_dates ADD created_by character varying(150);
ALTER TABLE ropidgtfs_calendar_dates ADD update_batch_id bigint;
ALTER TABLE ropidgtfs_calendar_dates ADD updated_at timestamp with time zone;
ALTER TABLE ropidgtfs_calendar_dates ADD updated_by character varying(150);

-- ropidgtfs_cis_stop_groups

ALTER TABLE ropidgtfs_cis_stop_groups ADD create_batch_id bigint;
ALTER TABLE ropidgtfs_cis_stop_groups RENAME COLUMN time_created TO created_at;
ALTER TABLE ropidgtfs_cis_stop_groups ADD created_by character varying(150);
ALTER TABLE ropidgtfs_cis_stop_groups ADD update_batch_id bigint;
ALTER TABLE ropidgtfs_cis_stop_groups ADD updated_at timestamp with time zone;
ALTER TABLE ropidgtfs_cis_stop_groups ADD updated_by character varying(150);

-- ropidgtfs_cis_stops

ALTER TABLE ropidgtfs_cis_stops ADD create_batch_id bigint;
ALTER TABLE ropidgtfs_cis_stops RENAME COLUMN time_created TO created_at;
ALTER TABLE ropidgtfs_cis_stops ADD created_by character varying(150);
ALTER TABLE ropidgtfs_cis_stops ADD update_batch_id bigint;
ALTER TABLE ropidgtfs_cis_stops ADD updated_at timestamp with time zone;
ALTER TABLE ropidgtfs_cis_stops ADD updated_by character varying(150);

-- ropidgtfs_metadata

ALTER TABLE ropidgtfs_metadata ADD create_batch_id bigint;
ALTER TABLE ropidgtfs_metadata ADD created_at timestamp with time zone;
ALTER TABLE ropidgtfs_metadata ADD created_by character varying(150);
ALTER TABLE ropidgtfs_metadata ADD update_batch_id bigint;
ALTER TABLE ropidgtfs_metadata ADD updated_at timestamp with time zone;
ALTER TABLE ropidgtfs_metadata ADD updated_by character varying(150);

-- ropidgtfs_routes

ALTER TABLE ropidgtfs_routes DROP created_time;
ALTER TABLE ropidgtfs_routes DROP last_modify;
ALTER TABLE ropidgtfs_routes ADD create_batch_id bigint;
ALTER TABLE ropidgtfs_routes ADD created_at timestamp with time zone;
ALTER TABLE ropidgtfs_routes ADD created_by character varying(150);
ALTER TABLE ropidgtfs_routes ADD update_batch_id bigint;
ALTER TABLE ropidgtfs_routes ADD updated_at timestamp with time zone;
ALTER TABLE ropidgtfs_routes ADD updated_by character varying(150);

-- ropidgtfs_shapes

ALTER TABLE ropidgtfs_shapes DROP created_time;
ALTER TABLE ropidgtfs_shapes DROP last_modify;
ALTER TABLE ropidgtfs_shapes ADD create_batch_id bigint;
ALTER TABLE ropidgtfs_shapes ADD created_at timestamp with time zone;
ALTER TABLE ropidgtfs_shapes ADD created_by character varying(150);
ALTER TABLE ropidgtfs_shapes ADD update_batch_id bigint;
ALTER TABLE ropidgtfs_shapes ADD updated_at timestamp with time zone;
ALTER TABLE ropidgtfs_shapes ADD updated_by character varying(150);

-- ropidgtfs_stop_times

ALTER TABLE ropidgtfs_stop_times DROP created_time;
ALTER TABLE ropidgtfs_stop_times DROP last_modify;
ALTER TABLE ropidgtfs_stop_times ADD create_batch_id bigint;
ALTER TABLE ropidgtfs_stop_times ADD created_at timestamp with time zone;
ALTER TABLE ropidgtfs_stop_times ADD created_by character varying(150);
ALTER TABLE ropidgtfs_stop_times ADD update_batch_id bigint;
ALTER TABLE ropidgtfs_stop_times ADD updated_at timestamp with time zone;
ALTER TABLE ropidgtfs_stop_times ADD updated_by character varying(150);

-- ropidgtfs_stops

ALTER TABLE ropidgtfs_stops DROP created_time;
ALTER TABLE ropidgtfs_stops DROP last_modify;
ALTER TABLE ropidgtfs_stops ADD create_batch_id bigint;
ALTER TABLE ropidgtfs_stops ADD created_at timestamp with time zone;
ALTER TABLE ropidgtfs_stops ADD created_by character varying(150);
ALTER TABLE ropidgtfs_stops ADD update_batch_id bigint;
ALTER TABLE ropidgtfs_stops ADD updated_at timestamp with time zone;
ALTER TABLE ropidgtfs_stops ADD updated_by character varying(150);

-- ropidgtfs_trips

ALTER TABLE ropidgtfs_trips DROP created_time;
ALTER TABLE ropidgtfs_trips DROP last_modify;
ALTER TABLE ropidgtfs_trips ADD create_batch_id bigint;
ALTER TABLE ropidgtfs_trips ADD created_at timestamp with time zone;
ALTER TABLE ropidgtfs_trips ADD created_by character varying(150);
ALTER TABLE ropidgtfs_trips ADD update_batch_id bigint;
ALTER TABLE ropidgtfs_trips ADD updated_at timestamp with time zone;
ALTER TABLE ropidgtfs_trips ADD updated_by character varying(150);

