alter table python.openproject_mhmp_projects
add column action_status varchar(20);

drop VIEW analytic.v_openproject_mhmp_workpackages;
drop view analytic.v_openproject_mhmp_projects;

-- franz.v_openproject_mhmp_projects source

CREATE OR REPLACE VIEW analytic.v_openproject_mhmp_projects
AS WITH domain AS (
         SELECT openproject_mhmp_projects.id
           FROM python.openproject_mhmp_projects
          WHERE openproject_mhmp_projects.parent_id = 3
        ), topic AS (
         SELECT openproject_mhmp_projects.id
           FROM python.openproject_mhmp_projects
          WHERE (openproject_mhmp_projects.parent_id IN ( SELECT domain.id
                   FROM domain))
        ), projects AS (
         SELECT openproject_mhmp_projects.id,
            openproject_mhmp_projects.project_name,
            openproject_mhmp_projects.active,
            openproject_mhmp_projects.public,
            openproject_mhmp_projects.project_description,
            openproject_mhmp_projects.status,
            openproject_mhmp_projects.action_status,
            openproject_mhmp_projects.status_explanation,
            openproject_mhmp_projects.project_start,
            openproject_mhmp_projects.project_end,
            openproject_mhmp_projects.supplier_name,
            openproject_mhmp_projects.hashtag,
            openproject_mhmp_projects.budget_expected,
            openproject_mhmp_projects.budget_reality,
            openproject_mhmp_projects.council_rule_number,
            openproject_mhmp_projects.council_rule_url,
            openproject_mhmp_projects.contract_number,
            openproject_mhmp_projects.contract_registry_url,
            openproject_mhmp_projects.tender_number,
            openproject_mhmp_projects.tender_url,
            openproject_mhmp_projects.solver_name,
            openproject_mhmp_projects.councilor_name,
            openproject_mhmp_projects.mhmp_responsible_name,
            openproject_mhmp_projects.parent_id,
            openproject_mhmp_projects.parent_project,
            openproject_mhmp_projects.updated_at
           FROM python.openproject_mhmp_projects
          WHERE (openproject_mhmp_projects.parent_id IN ( SELECT topic.id
                   FROM topic))
        ), subprojects AS (
         SELECT sp.id,
            sp.project_name,
            sp.active,
            sp.public,
            sp.project_description,
            sp.status,
            sp.action_status,
            sp.status_explanation,
            sp.project_start,
            sp.project_end,
            sp.supplier_name,
            sp.hashtag,
            sp.budget_expected,
            sp.budget_reality,
            sp.council_rule_number,
            sp.council_rule_url,
            sp.contract_number,
            sp.contract_registry_url,
            sp.tender_number,
            sp.tender_url,
            sp.solver_name,
            sp.councilor_name,
            sp.mhmp_responsible_name,
            p.parent_id,
            sp.parent_project,
            sp.updated_at
           FROM python.openproject_mhmp_projects sp
             LEFT JOIN projects p ON sp.parent_id = p.id
          WHERE (sp.parent_id IN ( SELECT projects.id
                   FROM projects))
        ), next_lvl AS (
         SELECT projects.id,
            projects.project_name,
            projects.active,
            projects.public,
            projects.project_description,
            projects.status,
            projects.action_status,
            projects.status_explanation,
            projects.project_start,
            projects.project_end,
            projects.supplier_name,
            projects.hashtag,
            projects.budget_expected,
            projects.budget_reality,
            projects.council_rule_number,
            projects.council_rule_url,
            projects.contract_number,
            projects.contract_registry_url,
            projects.tender_number,
            projects.tender_url,
            projects.solver_name,
            projects.councilor_name,
            projects.mhmp_responsible_name,
            projects.parent_id,
            projects.parent_project,
            projects.updated_at
           FROM projects
        UNION ALL
         SELECT subprojects.id,
            subprojects.project_name,
            subprojects.active,
            subprojects.public,
            subprojects.project_description,
            subprojects.status,
            subprojects.action_status,
            subprojects.status_explanation,
            subprojects.project_start,
            subprojects.project_end,
            subprojects.supplier_name,
            subprojects.hashtag,
            subprojects.budget_expected,
            subprojects.budget_reality,
            subprojects.council_rule_number,
            subprojects.council_rule_url,
            subprojects.contract_number,
            subprojects.contract_registry_url,
            subprojects.tender_number,
            subprojects.tender_url,
            subprojects.solver_name,
            subprojects.councilor_name,
            subprojects.mhmp_responsible_name,
            subprojects.parent_id,
            subprojects.parent_project,
            subprojects.updated_at
           FROM subprojects
        )
 SELECT nl.id,
    nl.project_name,
    'https://op.praha.eu/projects/'::text || nl.id::character varying::text AS project_url,
    nl.active,
    nl.public,
    nl.project_description,
    "left"(nl.project_description, 150) || '...'::text AS project_description_short,
    nl.status,
    nl.action_status,
    (nl.status || ' - '::text) || nl.action_status::text AS status_full,
    nl.status_explanation,
    nl.project_start::timestamp with time zone AS project_start,
    nl.project_end::timestamp with time zone AS project_end,
    nl.supplier_name,
    nl.hashtag,
    nl.budget_expected,
    nl.budget_reality,
    nl.council_rule_number,
    nl.council_rule_url,
    nl.contract_number,
    nl.contract_registry_url,
    nl.tender_number,
    nl.tender_url,
    nl.solver_name,
    nl.councilor_name,
    nl.mhmp_responsible_name,
    nl.parent_id,
    nl.parent_project,
    COALESCE(nl.updated_at::timestamp with time zone, w.wps_up_at::timestamp with time zone) AS updated_at,
        CASE
            WHEN nl.project_start::timestamp with time zone IS NULL AND nl.project_end::timestamp with time zone IS NULL THEN 0
            WHEN (date_part('month'::text, CURRENT_DATE) = ANY (ARRAY[4::double precision, 5::double precision, 6::double precision, 7::double precision, 8::double precision, 9::double precision])) AND (nl.project_start::timestamp with time zone >= format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE), 4, 1)::date AND nl.project_start::timestamp with time zone <= format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE), 9, 30)::date OR nl.project_end::timestamp with time zone >= format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE), 4, 1)::date AND nl.project_end::timestamp with time zone <= format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE), 9, 30)::date OR nl.project_start::timestamp with time zone < format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE), 10, 1)::date AND nl.project_end::timestamp with time zone > format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE), 3, 31)::date) THEN 1
            WHEN (date_part('month'::text, CURRENT_DATE) = ANY (ARRAY[1::double precision, 2::double precision, 3::double precision, 10::double precision, 11::double precision, 12::double precision])) AND (nl.project_start::timestamp with time zone >= format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE - 200), 9, 30)::date AND nl.project_start::timestamp with time zone <= format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE + 200), 4, 1)::date OR nl.project_end::timestamp with time zone >= format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE - 200), 9, 30)::date AND nl.project_end::timestamp with time zone <= format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE + 200), 4, 1)::date OR nl.project_start::timestamp with time zone < format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE + 200), 3, 31)::date AND nl.project_end::timestamp with time zone > format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE - 200), 10, 1)::date) THEN 1
            ELSE 0
        END AS this_period,
        CASE
            WHEN nl.project_start::timestamp with time zone IS NULL AND nl.project_end::timestamp with time zone IS NULL THEN 0
            WHEN (date_part('month'::text, CURRENT_DATE) = ANY (ARRAY[4::double precision, 5::double precision, 6::double precision, 7::double precision, 8::double precision, 9::double precision])) AND (nl.project_start::timestamp with time zone >= format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE), 9, 30)::date AND nl.project_start::timestamp with time zone <= format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE) + 1::double precision, 4, 1)::date OR nl.project_end::timestamp with time zone >= format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE), 9, 30)::date AND nl.project_end::timestamp with time zone <= format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE) + 1::double precision, 4, 1)::date OR nl.project_start::timestamp with time zone < format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE) + 1::double precision, 3, 31)::date AND nl.project_end::timestamp with time zone > format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE), 10, 1)::date) THEN 1
            WHEN (date_part('month'::text, CURRENT_DATE) = ANY (ARRAY[1::double precision, 2::double precision, 3::double precision, 10::double precision, 11::double precision, 12::double precision])) AND (nl.project_start::timestamp with time zone >= format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE + 200), 4, 1)::date AND nl.project_start::timestamp with time zone <= format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE + 200), 9, 30)::date OR nl.project_end::timestamp with time zone >= format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE + 200), 4, 1)::date AND nl.project_end::timestamp with time zone <= format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE + 200), 9, 30)::date OR nl.project_start::timestamp with time zone < format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE + 200), 10, 1)::date AND nl.project_end::timestamp with time zone > format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE + 200), 3, 31)::date) THEN 1
            ELSE 0
        END AS next_period,
        CASE
            WHEN nl.project_start::timestamp with time zone IS NULL AND nl.project_end::timestamp with time zone IS NULL THEN 0
            WHEN (date_part('month'::text, CURRENT_DATE) = ANY (ARRAY[4::double precision, 5::double precision, 6::double precision, 7::double precision, 8::double precision, 9::double precision])) AND (nl.project_start::timestamp with time zone >= format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE) + 1::double precision, 4, 1)::date AND nl.project_start::timestamp with time zone <= format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE) + 1::double precision, 9, 30)::date OR nl.project_end::timestamp with time zone >= format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE) + 1::double precision, 4, 1)::date AND nl.project_end::timestamp with time zone <= format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE) + 1::double precision, 9, 30)::date OR nl.project_start::timestamp with time zone < format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE) + 1::double precision, 10, 1)::date AND nl.project_end::timestamp with time zone > format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE) + 1::double precision, 3, 31)::date) THEN 1
            WHEN (date_part('month'::text, CURRENT_DATE) = ANY (ARRAY[1::double precision, 2::double precision, 3::double precision, 10::double precision, 11::double precision, 12::double precision])) AND (nl.project_start::timestamp with time zone >= format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE - 200) + 1::double precision, 10, 1)::date AND nl.project_start::timestamp with time zone <= format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE + 200) + 1::double precision, 3, 31)::date OR nl.project_end::timestamp with time zone >= format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE - 200) + 1::double precision, 10, 1)::date AND nl.project_end::timestamp with time zone <= format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE + 200) + 1::double precision, 3, 31)::date OR nl.project_start::timestamp with time zone < format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE + 200) + 1::double precision, 4, 1)::date AND nl.project_end::timestamp with time zone > format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE - 200) + 1::double precision, 9, 30)::date) THEN 1
            ELSE 0
        END AS next_next_period
   FROM next_lvl nl
     LEFT JOIN ( SELECT openproject_mhmp_wps.project_id,
            max(openproject_mhmp_wps.updated_at) AS wps_up_at
           FROM python.openproject_mhmp_wps
          GROUP BY openproject_mhmp_wps.project_id) w ON w.project_id = nl.id;

-- analytic.v_openproject_mhmp_workpackages source

CREATE OR REPLACE VIEW analytic.v_openproject_mhmp_workpackages
AS SELECT openproject_mhmp_wps.derived_start_date,
    openproject_mhmp_wps.derived_due_date,
    openproject_mhmp_wps.spent_time,
    openproject_mhmp_wps.labor_costs,
    openproject_mhmp_wps.material_costs,
    openproject_mhmp_wps.overall_costs,
    openproject_mhmp_wps.type_of_object,
    openproject_mhmp_wps.wp_id,
    openproject_mhmp_wps.wp_name,
    openproject_mhmp_wps.start_date,
    openproject_mhmp_wps.due_date,
    openproject_mhmp_wps.estimated_time,
    openproject_mhmp_wps.derived_estimated_time,
    openproject_mhmp_wps.percentage_done,
    openproject_mhmp_wps.created_at,
    openproject_mhmp_wps.updated_at,
    openproject_mhmp_wps.story_points,
    openproject_mhmp_wps.remaining_time,
    openproject_mhmp_wps.supplier_name,
    openproject_mhmp_wps.weight,
    openproject_mhmp_wps.contract_number,
    openproject_mhmp_wps.tender_number,
    openproject_mhmp_wps.council_rule_number,
    openproject_mhmp_wps.media_url,
    openproject_mhmp_wps.media_out_date,
    openproject_mhmp_wps.wp_description,
    openproject_mhmp_wps.tender_url,
    openproject_mhmp_wps.contract_registry_url,
    openproject_mhmp_wps.council_rule_url,
    openproject_mhmp_wps.wp_type,
    openproject_mhmp_wps.priority,
    openproject_mhmp_wps.project_id,
    openproject_mhmp_wps.project_name,
    openproject_mhmp_wps.wp_status,
    openproject_mhmp_wps.author_id,
    openproject_mhmp_wps.author_name,
    openproject_mhmp_wps.responsible_id,
    openproject_mhmp_wps.responsible_name,
    openproject_mhmp_wps.assignee_id,
    openproject_mhmp_wps.assignee_name,
    openproject_mhmp_wps.department_resp_name,
    openproject_mhmp_wps.department_resp_id,
    openproject_mhmp_wps.supplier_resp_id,
    openproject_mhmp_wps.supplier_resp_name,
    openproject_mhmp_wps.parent_id,
    openproject_mhmp_wps.parent_name,
    openproject_mhmp_wps.completion_date
   FROM python.openproject_mhmp_wps
  WHERE (openproject_mhmp_wps.project_id IN ( SELECT v_openproject_mhmp_projects.id
           FROM analytic.v_openproject_mhmp_projects));