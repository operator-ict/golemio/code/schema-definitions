CREATE TABLE public.bicyclecounters_api_logs_hits (
	id int8 NOT NULL,
	latency int8 NOT NULL,
	ping_time int8 NOT NULL,
	measured_at timestamp NOT NULL,
	updated_at timestamp NOT NULL,
	created_at timestamp NOT NULL,
	CONSTRAINT bicyclecounters_api_logs_hits_pk PRIMARY KEY (id)
);

CREATE TABLE public.bicyclecounters_api_logs_failures (
	id int8 NOT NULL,
	issue text NOT NULL,
	error_code int8 NOT NULL,
	ping float8 NOT NULL,
	measured_at timestamp NOT NULL,
	updated_at timestamp NOT NULL,
	created_at timestamp NOT NULL,
	CONSTRAINT bicyclecounters_api_logs_failures_pk PRIMARY KEY (id)
);

-- analytic.v_camea_bikecounters_api_availability_day source

CREATE OR REPLACE VIEW analytic.v_camea_bikecounters_api_availability_day
AS WITH united_table AS (
         SELECT bicyclecounters_api_logs_failures.id,
            bicyclecounters_api_logs_failures.issue,
                CASE
                    WHEN bicyclecounters_api_logs_failures.error_code = 200 THEN 222::bigint
                    ELSE bicyclecounters_api_logs_failures.error_code
                END AS error_code,
            bicyclecounters_api_logs_failures.measured_at
           FROM bicyclecounters_api_logs_failures
        UNION ALL
         SELECT bicyclecounters_api_logs_hits.id,
            'none'::text AS issue,
            200 AS error_code,
            bicyclecounters_api_logs_hits.measured_at
           FROM bicyclecounters_api_logs_hits
        ), fail_steps AS (
         SELECT united_table.measured_at,
            united_table.error_code,
            united_table.issue,
                CASE
                    WHEN united_table.error_code <> 200 AND lag(united_table.error_code, 1) OVER (ORDER BY united_table.measured_at) = 200 THEN 'begin'::text
                    WHEN united_table.error_code <> 200 AND (united_table.measured_at::date - lag(united_table.measured_at::date, 1) OVER (ORDER BY united_table.measured_at)) <> 0 THEN 'begin_midnight'::text
                    WHEN united_table.error_code <> 200 AND lead(united_table.error_code, 1) OVER (ORDER BY united_table.measured_at) = 200 THEN 'end'::text
                    WHEN united_table.error_code <> 200 AND (united_table.measured_at::date - lead(united_table.measured_at::date, 1) OVER (ORDER BY united_table.measured_at)) <> 0 THEN 'end_midnight'::text
                    WHEN united_table.error_code <> 200 THEN 'body'::text
                    ELSE 'ok'::text
                END AS status
           FROM united_table
        ), fail_start_end AS (
         SELECT
                CASE
                    WHEN fail_steps.status = 'begin_midnight'::text THEN date_trunc('day'::text, fail_steps.measured_at)
                    ELSE fail_steps.measured_at
                END AS start_time,
                CASE
                    WHEN fail_steps.status ~~ 'begin%'::text AND lead(fail_steps.status, 1) OVER (ORDER BY fail_steps.measured_at) = 'end_midnight'::text THEN date_trunc('day'::text, fail_steps.measured_at) + '23:59:59'::interval
                    WHEN fail_steps.status ~~ 'begin%'::text AND lead(fail_steps.status, 1) OVER (ORDER BY fail_steps.measured_at) = 'end'::text THEN lead(fail_steps.measured_at, 1) OVER (ORDER BY fail_steps.measured_at)
                    WHEN fail_steps.status ~~ 'begin%'::text AND lead(fail_steps.status, 1) OVER (ORDER BY fail_steps.measured_at) = 'begin'::text THEN fail_steps.measured_at + '00:01:00'::interval
                    ELSE NULL::timestamp without time zone
                END AS end_time,
            fail_steps.error_code,
            fail_steps.issue,
            fail_steps.status
           FROM fail_steps
          WHERE fail_steps.status = ANY (ARRAY['begin'::text, 'begin_midnight'::text, 'end%'::text, 'end_midnight'::text])
        ), fail_duration AS (
         SELECT fail_start_end.start_time,
            fail_start_end.end_time,
            fail_start_end.end_time - fail_start_end.start_time + '00:00:01'::interval AS duration,
            fail_start_end.error_code,
            fail_start_end.issue
           FROM fail_start_end
          WHERE fail_start_end.status ~~ 'begin%'::text
        ), fail_duration_daily AS (
         SELECT date_trunc('day'::text, fail_duration.start_time) AS date,
            min(fail_duration.error_code) AS error_code,
            max(fail_duration.issue) AS issue,
            '24:00:00'::interval - sum(fail_duration.duration) AS run,
            sum(fail_duration.duration) AS error
           FROM fail_duration
          GROUP BY (date_trunc('day'::text, fail_duration.start_time))
        ), calendar AS (
         SELECT generate_series(min(united_table.measured_at::date)::timestamp with time zone, max(united_table.measured_at::date)::timestamp with time zone, '1 day'::interval) AS measured_at
           FROM united_table
        ), fail_duration_daily_complete AS (
         SELECT c.measured_at,
            COALESCE(fdd.run, '24:00:00'::interval) AS run,
            COALESCE(fdd.error, '00:00:00'::interval) AS error
           FROM calendar c
             LEFT JOIN fail_duration_daily fdd ON c.measured_at = fdd.date
        )
 SELECT fail_duration_daily_complete.measured_at,
    fail_duration_daily_complete.run,
    fail_duration_daily_complete.error,
    date_part('epoch'::text, fail_duration_daily_complete.run) / 86400::double precision AS run_ratio,
    date_part('epoch'::text, fail_duration_daily_complete.error) / 86400::double precision AS error_ratio
   FROM fail_duration_daily_complete;
   
-- analytic.v_camea_bikecounters_api_availability source

CREATE OR REPLACE VIEW analytic.v_camea_bikecounters_api_availability
AS WITH united_table AS (
         SELECT bicyclecounters_api_logs_failures.id,
            bicyclecounters_api_logs_failures.issue,
                CASE
                    WHEN bicyclecounters_api_logs_failures.error_code = 200 THEN 222::bigint
                    ELSE bicyclecounters_api_logs_failures.error_code
                END AS error_code,
            bicyclecounters_api_logs_failures.measured_at
           FROM bicyclecounters_api_logs_failures
        UNION ALL
         SELECT bicyclecounters_api_logs_hits.id,
            'none'::text AS issue,
            200 AS error_code,
            bicyclecounters_api_logs_hits.measured_at
           FROM bicyclecounters_api_logs_hits
        ), fail_steps AS (
         SELECT united_table.id,
            united_table.measured_at,
            united_table.error_code,
            united_table.issue,
                CASE
                    WHEN united_table.error_code <> 200 AND lag(united_table.error_code, 1) OVER (ORDER BY united_table.measured_at) = 200 THEN 'begin'::text
                    WHEN united_table.error_code <> 200 AND lead(united_table.error_code, 1) OVER (ORDER BY united_table.measured_at) = 200 THEN 'end'::text
                    WHEN united_table.error_code <> 200 THEN 'body'::text
                    ELSE 'ok'::text
                END AS status
           FROM united_table
        ), fail_duration AS (
         SELECT fail_steps.id,
            fail_steps.measured_at,
            fail_steps.error_code,
            fail_steps.issue,
            fail_steps.status,
                CASE
                    WHEN fail_steps.status = 'begin'::text AND lead(fail_steps.status, 1) OVER (ORDER BY fail_steps.measured_at) = 'end'::text THEN lead(fail_steps.measured_at, 1) OVER (ORDER BY fail_steps.measured_at)
                    WHEN fail_steps.status = 'begin'::text AND lead(fail_steps.status, 1) OVER (ORDER BY fail_steps.measured_at) = 'begin'::text THEN fail_steps.measured_at + '00:01:00'::interval
                    ELSE NULL::timestamp without time zone
                END AS end_time
           FROM fail_steps
          WHERE fail_steps.status = ANY (ARRAY['begin'::text, 'end'::text])
        )
 SELECT fail_duration.measured_at AS start_time,
    fail_duration.end_time,
        CASE
            WHEN ((fail_duration.end_time - fail_duration.measured_at)::text) ~~ '%days%'::text THEN replace((fail_duration.end_time - fail_duration.measured_at)::text, 'days'::text, 'dny'::text)
            ELSE replace((fail_duration.end_time - fail_duration.measured_at)::text, 'day'::text, 'den'::text)
        END AS duration,
    fail_duration.error_code,
    fail_duration.issue
   FROM fail_duration
  WHERE fail_duration.status = 'begin'::text
  ORDER BY fail_duration.measured_at;   