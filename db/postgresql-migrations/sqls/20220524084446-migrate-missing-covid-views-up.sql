CREATE OR REPLACE VIEW analytic.v_uzis_covid19_regions
AS SELECT ek.datum AS "Datum",
    ek.kraj AS kraj_kod,
    codebook_regions."kraj-nazev" AS kraj_nazev,
    population_regions.population AS pocet_obyvatel,
    ek.kumulativni_pocet_pozitivnich_osob,
    ek.kumulativni_pocet_zemrelych,
    ek.kumulativni_pocet_vylecenych,
        CASE
            WHEN ek.kumulativni_pocet_zemrelych IS NULL THEN 0::numeric
            ELSE ek.kumulativni_pocet_zemrelych::numeric
        END -
        CASE
            WHEN lag(ek.kumulativni_pocet_zemrelych) OVER (PARTITION BY ek.kraj ORDER BY ek.datum) IS NULL THEN 0::numeric
            ELSE lag(ek.kumulativni_pocet_zemrelych) OVER (PARTITION BY ek.kraj ORDER BY ek.datum)::numeric
        END AS pocet_zemrelych_den,
    ek.incidence AS pocet_pozitivnich_osob_den,
        CASE
            WHEN ek.prevalence IS NULL THEN 0::numeric
            ELSE ek.prevalence::numeric
        END -
        CASE
            WHEN lag(ek.prevalence) OVER (PARTITION BY ek.kraj ORDER BY ek.datum) IS NULL THEN 0::numeric
            ELSE lag(ek.prevalence) OVER (PARTITION BY ek.kraj ORDER BY ek.datum)::numeric
        END AS pocet_aktivnich_pripadu_den,
    ek.prevalence AS aktualni_pocet_aktivnich_pripadu,
    ek.aktualni_pocet_hospitalizovanych_osob,
        CASE
            WHEN ek.aktualni_pocet_hospitalizovanych_osob IS NULL THEN 0::numeric::double precision
            ELSE ek.aktualni_pocet_hospitalizovanych_osob
        END -
        CASE
            WHEN lag(ek.aktualni_pocet_hospitalizovanych_osob) OVER (PARTITION BY ek.kraj ORDER BY ek.datum) IS NULL THEN 0::numeric::double precision
            ELSE lag(ek.aktualni_pocet_hospitalizovanych_osob) OVER (PARTITION BY ek.kraj ORDER BY ek.datum)
        END AS pocet_hospitalizovanych_osob,
    ek.casova_znamka AS posledni_aktualizace,
        CASE
            WHEN ek.datum = (date_trunc('day'::text, now()) - '1 day'::interval) THEN true
            ELSE false
        END AS vcerejsi_den,
    ek.kumulativni_pocet_hospitalizovanych_osob,
    ek.kumulativni_pocet_zemrelych + ek.kumulativni_pocet_vylecenych AS aktualni_pocet_neaktivnich_pripadu,
    round(ek.podil_65__na_kumulativnim_poctu_pozitivnich * ek.kumulativni_pocet_pozitivnich_osob::double precision) AS pocet_pozitivnich_65,
    round(ek.podil_65__na_kumulativnim_poctu_hospitalizovanych * ek.kumulativni_pocet_hospitalizovanych_osob) AS pocet_hospitalizovanych_65,
    round(ek.podil_65__na_kumulativnim_poctu_zemrelych * ek.kumulativni_pocet_zemrelych::double precision) AS pocet_zemrelych_65,
        CASE
            WHEN ek.datum >= (CURRENT_DATE - '7 days'::interval) THEN true
            ELSE false
        END AS last_7days,
        CASE
            WHEN ek.datum >= (CURRENT_DATE - '14 days'::interval) THEN true
            ELSE false
        END AS last_14days,
        CASE
            WHEN ek.datum >= (CURRENT_DATE - '7 days'::interval) THEN 'Posledních 7 dní'::text
            ELSE NULL::text
        END AS last_7days_desc,
        CASE
            WHEN ek.datum >= (CURRENT_DATE - '14 days'::interval) THEN 'Posledních 14 dní'::text
            ELSE NULL::text
        END AS last_14days_desc
   FROM uzis.covid19_regions ek
     LEFT JOIN uzis.codebook_regions ON ek.kraj = codebook_regions."kraj-kod"
     LEFT JOIN uzis.population_regions ON ek.kraj = population_regions.region_code
  WHERE ek.datum < CURRENT_DATE;

CREATE OR REPLACE VIEW analytic.v_uzis_covid19_vaccinated_split_booster
AS WITH ockovani_druha AS (
         SELECT v_uzis_covid19_vaccination_regions_details.datum_vakcinace,
            v_uzis_covid19_vaccination_regions_details.kraj_bydliste,
            sum(sum(v_uzis_covid19_vaccination_regions_details.pocet_davek)) OVER (PARTITION BY v_uzis_covid19_vaccination_regions_details.kraj_bydliste ORDER BY v_uzis_covid19_vaccination_regions_details.datum_vakcinace) AS pocet_uzavrenych_ockovani,
            sum(sum(v_uzis_covid19_vaccination_regions_details.pocet_davek) FILTER (WHERE v_uzis_covid19_vaccination_regions_details.vekova_skupina = ANY (ARRAY['65-69'::text, '70-74'::text, '75-79'::text, '80+'::text]))) OVER (PARTITION BY v_uzis_covid19_vaccination_regions_details.kraj_bydliste ORDER BY v_uzis_covid19_vaccination_regions_details.datum_vakcinace) AS pocet_uzavrenych_ockovani65
           FROM analytic.v_uzis_covid19_vaccination_regions_details
          WHERE v_uzis_covid19_vaccination_regions_details.poradi_davky = 2
          GROUP BY v_uzis_covid19_vaccination_regions_details.datum_vakcinace, v_uzis_covid19_vaccination_regions_details.kraj_bydliste
        ), ockovani_treti AS (
         SELECT v_uzis_covid19_vaccination_regions_details.datum_vakcinace,
            v_uzis_covid19_vaccination_regions_details.kraj_bydliste,
            sum(sum(v_uzis_covid19_vaccination_regions_details.pocet_davek)) OVER (PARTITION BY v_uzis_covid19_vaccination_regions_details.kraj_bydliste ORDER BY v_uzis_covid19_vaccination_regions_details.datum_vakcinace) AS pocet_uzavrenych_ockovani,
            sum(sum(v_uzis_covid19_vaccination_regions_details.pocet_davek) FILTER (WHERE v_uzis_covid19_vaccination_regions_details.vekova_skupina = ANY (ARRAY['65-69'::text, '70-74'::text, '75-79'::text, '80+'::text]))) OVER (PARTITION BY v_uzis_covid19_vaccination_regions_details.kraj_bydliste ORDER BY v_uzis_covid19_vaccination_regions_details.datum_vakcinace) AS pocet_uzavrenych_ockovani65
           FROM analytic.v_uzis_covid19_vaccination_regions_details
          WHERE v_uzis_covid19_vaccination_regions_details.poradi_davky = 3
          GROUP BY v_uzis_covid19_vaccination_regions_details.datum_vakcinace, v_uzis_covid19_vaccination_regions_details.kraj_bydliste
        )
 SELECT i.datum,
    date_trunc('week'::text, i.datum) AS tyden,
    date_trunc('month'::text, i.datum) AS mesic,
        CASE
            WHEN i.datum = (( SELECT max(covid19_vaccinated_split_incidence.datum) AS max
               FROM uzis.covid19_vaccinated_split_incidence)) THEN 'poslední den'::text
            ELSE NULL::text
        END AS last_day,
        CASE
            WHEN i.datum > ((( SELECT max(covid19_vaccinated_split_incidence.datum) AS max
               FROM uzis.covid19_vaccinated_split_incidence)) - '7 days'::interval) THEN 'posledních 7 dnů'::text
            ELSE NULL::text
        END AS last_7_days,
        CASE
            WHEN i.datum > ((( SELECT max(covid19_vaccinated_split_incidence.datum) AS max
               FROM uzis.covid19_vaccinated_split_incidence)) - '14 days'::interval) THEN 'posledních 14 dnů'::text
            ELSE NULL::text
        END AS last_14_days,
        CASE
            WHEN i.datum < date_trunc('week'::text, CURRENT_DATE::timestamp with time zone) THEN false
            ELSE true
        END AS unfinished_calendar_week,
    i.krajkod,
    cr."kraj-nazev" AS kraj_nazev,
    pr.population,
    par.population65,
    ockovani_druha.pocet_uzavrenych_ockovani AS pocet_uzavrenych_ockovani_druha_davka,
    ockovani_treti.pocet_uzavrenych_ockovani AS pocet_uzavrenych_ockovani_treti_davka,
    i.poz_bez_ockovani + i.poz_po_prvni AS incidence_neockovani,
    (100000 * (i.poz_bez_ockovani + i.poz_po_prvni))::numeric / (pr.population::numeric - ockovani_druha.pocet_uzavrenych_ockovani) AS incidence_neockovani_100tis,
    i.poz_po_ukonceni AS incidence_druha_davka,
    i.poz_po_ukonceni_s_posilujici AS incidence_treti_davka,
    (100000 * i.poz_po_ukonceni)::numeric / (ockovani_druha.pocet_uzavrenych_ockovani - ockovani_treti.pocet_uzavrenych_ockovani) AS incidence_ockovani_druha_davka_100tis,
    (100000 * i.poz_po_ukonceni_s_posilujici)::numeric / ockovani_treti.pocet_uzavrenych_ockovani AS incidence_ockovani_treti_davka_100tis,
    h.hos_bez_ockovani + h.hos_po_prvni AS hospitalizace_neockovani,
    (100000 * (h.hos_bez_ockovani + h.hos_po_prvni))::numeric / (pr.population::numeric - ockovani_druha.pocet_uzavrenych_ockovani) AS hospitalizace_neockovani_100tis,
    h.hos_po_ukonceni AS hospitalizace_ockovani_druha_davka,
    h.hos_po_ukonceni_s_posilujici AS hospitalizace_ockovani_treti_davka,
    (100000 * h.hos_po_ukonceni)::numeric / (ockovani_druha.pocet_uzavrenych_ockovani - ockovani_treti.pocet_uzavrenych_ockovani) AS hospitalizace_ockovani_druha_davka_100tis,
    (100000 * h.hos_po_ukonceni_s_posilujici)::numeric / ockovani_treti.pocet_uzavrenych_ockovani AS hospitalizace_ockovani_treti_davka_100tis,
    icu.jip_bez_ockovani + icu.jip_po_prvni AS jip_neockovani,
    (100000 * (icu.jip_bez_ockovani + icu.jip_po_prvni))::numeric / (pr.population::numeric - ockovani_druha.pocet_uzavrenych_ockovani) AS jip_neockovani_100tis,
    icu.jip_po_ukonceni AS jip_ockovani_druha_davka,
    icu.jip_po_ukonceni_s_posilujici AS jip_ockovani_treti_davka,
    (100000 * icu.jip_po_ukonceni)::numeric / (ockovani_druha.pocet_uzavrenych_ockovani - ockovani_treti.pocet_uzavrenych_ockovani) AS jip_ockovani_druha_davka_100tis,
    (100000 * icu.jip_po_ukonceni_s_posilujici)::numeric / ockovani_treti.pocet_uzavrenych_ockovani AS jip_ockovani_treti_davka_100tis,
    d.zem_bez_ockovani + d.zem_po_prvni AS zemreli_neockovani,
    (100000 * (d.zem_bez_ockovani + d.zem_po_prvni))::numeric / (pr.population::numeric - ockovani_druha.pocet_uzavrenych_ockovani) AS zemreli_neockovani_100tis,
    d.zem_po_ukonceni AS zemreli_ockovani_druha_davka,
    d.zem_po_ukonceni_s_posilujici AS zemreli_ockovani_treti_davka,
    (100000 * d.zem_po_ukonceni)::numeric / (ockovani_druha.pocet_uzavrenych_ockovani - ockovani_treti.pocet_uzavrenych_ockovani) AS zemreli_ockovani_druha_davka_100tis,
    (100000 * d.zem_po_ukonceni_s_posilujici)::numeric / ockovani_treti.pocet_uzavrenych_ockovani AS zemreli_ockovani_treti_davka_100tis
   FROM uzis.covid19_vaccinated_split_incidence i
     JOIN uzis.covid19_vaccinated_split_incidence65 i65 ON i.datum = i65.datum AND i.krajkod = i65.krajkod
     JOIN uzis.covid19_vaccinated_split_hospitalized h ON i.datum = h.datum AND i.krajkod = h.krajkod
     JOIN uzis.covid19_vaccinated_split_icu icu ON i.datum = icu.datum AND i.krajkod = icu.krajkod
     JOIN uzis.covid19_vaccinated_split_deaths d ON i.datum = d.datum AND i.krajkod = d.krajkod
     LEFT JOIN uzis.population_regions pr ON i.krajkod = pr.region_code
     LEFT JOIN ( SELECT sum(population_age_regions.population) AS population65,
            population_age_regions.region_code
           FROM uzis.population_age_regions
          WHERE population_age_regions.age >= 65
          GROUP BY population_age_regions.region_code) par ON i.krajkod = par.region_code
     LEFT JOIN ockovani_druha ON ockovani_druha.datum_vakcinace = i.datum AND ockovani_druha.kraj_bydliste = pr.region_name
     LEFT JOIN ockovani_treti ON ockovani_treti.datum_vakcinace = i.datum AND ockovani_treti.kraj_bydliste = pr.region_name
     LEFT JOIN uzis.codebook_regions cr ON i.krajkod = cr."kraj-kod"
  WHERE i.datum >= '2021-02-01 00:00:00'::timestamp without time zone;


CREATE OR REPLACE VIEW analytic.v_uzis_covid19_regions_positive_7_14_days
AS SELECT v_uzis_covid19_regions."Datum",
    v_uzis_covid19_regions.kraj_kod,
    v_uzis_covid19_regions.kraj_nazev,
    v_uzis_covid19_regions.pocet_obyvatel,
    v_uzis_covid19_regions.pocet_pozitivnich_osob_den,
    avg(v_uzis_covid19_regions.pocet_pozitivnich_osob_den) OVER (PARTITION BY v_uzis_covid19_regions.kraj_nazev ORDER BY v_uzis_covid19_regions."Datum" ROWS BETWEEN 6 PRECEDING AND CURRENT ROW) AS avg_pozitivni_7_dnu,
    sum(v_uzis_covid19_regions.pocet_pozitivnich_osob_den) OVER (PARTITION BY v_uzis_covid19_regions.kraj_nazev ORDER BY v_uzis_covid19_regions."Datum" ROWS BETWEEN 13 PRECEDING AND CURRENT ROW) AS sum_pozitivni_14_dnu,
    avg(v_uzis_covid19_regions.pocet_pozitivnich_osob_den) OVER (PARTITION BY v_uzis_covid19_regions.kraj_nazev ORDER BY v_uzis_covid19_regions."Datum" ROWS BETWEEN 6 PRECEDING AND CURRENT ROW) / (v_uzis_covid19_regions.pocet_obyvatel / 100000)::numeric AS avg_pozitivni_7_dnu_na_100tis,
    sum(v_uzis_covid19_regions.pocet_pozitivnich_osob_den) OVER (PARTITION BY v_uzis_covid19_regions.kraj_nazev ORDER BY v_uzis_covid19_regions."Datum" ROWS BETWEEN 13 PRECEDING AND CURRENT ROW) / (v_uzis_covid19_regions.pocet_obyvatel / 100000)::numeric AS sum_pozitivni_14_dnu_na_100tis
   FROM analytic.v_uzis_covid19_regions
UNION ALL
 SELECT sub."Datum",
    'CZ00'::text AS kraj_kod,
    'Celá ČR'::text AS kraj_nazev,
    sub.pocet_obyvatel,
    sub.pocet_pozitivnich_osob_den,
    avg(sub.pocet_pozitivnich_osob_den) OVER (ORDER BY sub."Datum" ROWS BETWEEN 6 PRECEDING AND CURRENT ROW) AS avg_pozitivni_7_dnu,
    sum(sub.pocet_pozitivnich_osob_den) OVER (ORDER BY sub."Datum" ROWS BETWEEN 13 PRECEDING AND CURRENT ROW) AS sum_pozitivni_14_dnu,
    avg(sub.pocet_pozitivnich_osob_den) OVER (ORDER BY sub."Datum" ROWS BETWEEN 6 PRECEDING AND CURRENT ROW) / (sub.pocet_obyvatel / 100000::numeric) AS avg_pozitivni_7_dnu_na_100tis,
    sum(sub.pocet_pozitivnich_osob_den) OVER (ORDER BY sub."Datum" ROWS BETWEEN 13 PRECEDING AND CURRENT ROW) / (sub.pocet_obyvatel / 100000::numeric) AS sum_pozitivni_14_dnu_na_100tis
   FROM ( SELECT v_uzis_covid19_regions."Datum",
            'CZ00'::text AS kraj_kod,
            'Celá ČR'::text AS kraj_nazev,
            sum(v_uzis_covid19_regions.pocet_obyvatel) AS pocet_obyvatel,
            sum(v_uzis_covid19_regions.pocet_pozitivnich_osob_den) AS pocet_pozitivnich_osob_den
           FROM analytic.v_uzis_covid19_regions
          GROUP BY v_uzis_covid19_regions."Datum") sub;
