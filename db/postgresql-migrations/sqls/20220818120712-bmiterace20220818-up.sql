-- analytic.barrande_route_hour_core definition

CREATE TABLE analytic.barrande_route_hour_core (
	"name" text NOT NULL,
	datum date NOT NULL,
	hodina float8 NOT NULL,
	cas_interval text NOT NULL,
	avg_time numeric NULL,
	avg_speed numeric NULL,
	CONSTRAINT barrande_route_hour_core_pkey PRIMARY KEY (name, datum, hodina, cas_interval)
);
CREATE INDEX barrande_route_hour_core_date ON analytic.barrande_route_hour_core USING btree (datum);


-- analytic.barrande_data_all definition

CREATE TABLE analytic.barrande_data_all (
	"name" text NOT NULL,
	name_trasa text NOT NULL,
	name_usek text NOT NULL,
	smer text NOT NULL,
	datum date NOT NULL,
	cas_interval text NOT NULL,
	avg_time numeric NULL,
	avg_speed numeric NULL,
	normal_avg_time numeric NULL,
	normal_avg_speed numeric NULL,
	CONSTRAINT barrande_data_all_pkey PRIMARY KEY (name, name_trasa, name_usek, smer, datum, cas_interval)
);


CREATE OR REPLACE PROCEDURE analytic.update_barande()
 LANGUAGE plpgsql
AS $procedure$
declare
	lastupdatetimestamp timestamptz;
	lastupdateunix bigint;
begin
	select 
		case
			when start_day is not null
			then start_day 
			else '2021-01-01'
		end as start_day_from into lastupdatetimestamp
	from (select max(datum)-interval '1 days' start_day from analytic.barrande_route_hour_core) barandemax;

	lastupdateunix := extract ('epoch' from lastupdatetimestamp) * 1000;


	insert into analytic.barrande_route_hour_core
	SELECT 
		wr.name,
		to_timestamp((rl.update_time / 1000)::double precision)::date AS datum,
		date_part('hour'::text, to_timestamp((rl.update_time / 1000)::double precision)) AS hodina,
		(to_char(date_part('hour'::text, to_timestamp((rl.update_time / 1000)::double precision)), '09'::text) || ' -'::text) || to_char(date_part('hour'::text, to_timestamp((rl.update_time / 1000)::double precision)) + 1::double precision, '09'::text) AS cas_interval,
		round(avg(rl."time"), 2) AS avg_time,
		round(avg(rl.length * 1000 / (rl."time" * 360)), 2) AS avg_speed
	FROM wazett_route_lives rl
		JOIN wazett_routes wr ON wr.id = rl.route_id
	WHERE 
		rl.update_time > lastupdateunix and
	rl.length > 0 AND "left"(wr.name, 2) = 'BM'::text AND NOT (EXISTS ( SELECT 1
          FROM analytic.barrande_black_list_route bl
          WHERE bl.route_id = rl.route_id))
	GROUP BY wr.name, (to_timestamp((rl.update_time / 1000)::double precision)::date), (date_part('hour'::text, to_timestamp((rl.update_time / 1000)::double precision)))
	ON CONFLICT (name,datum,hodina,cas_interval)
		DO update
		SET avg_time = EXCLUDED.avg_time,
		avg_speed = EXCLUDED.avg_speed;


	insert into	analytic.barrande_data_all
	WITH
		v_barrande_route_hour as (
	SELECT p.name,
    p.datum,
    p.hodina,
    p.cas_interval,
    p.avg_time,
    p.avg_speed,
    n.avg_time AS normal_avg_time,
    n.avg_speed AS normal_avg_speed
   FROM analytic.barrande_route_hour_core p
     LEFT JOIN analytic.mv_barrande_normal n ON n.name = p.name AND p.hodina = n.hodina
	where datum > lastupdatetimestamp
		),
-- konec v_barrande_route_hour	
		v_barrande_union_time_interval as (
		WITH hodiny AS (
         SELECT brh.name,
            brh.datum,
            brh.hodina,
            brh.cas_interval,
            brh.avg_time,
            brh.avg_speed,
            brh.normal_avg_time,
            brh.normal_avg_speed
           FROM v_barrande_route_hour brh
          WHERE brh.datum < CURRENT_DATE OR brh.hodina < date_part('hour'::text, now())
        ), den AS (
         SELECT hodiny.name,
            hodiny.datum,
            '0 - 24h'::text AS cas_interval,
            round(avg(hodiny.avg_time), 2) AS avg_time,
            round(avg(hodiny.avg_speed), 2) AS avg_speed,
            round(avg(hodiny.normal_avg_time), 2) AS normal_avg_time,
            round(avg(hodiny.normal_avg_speed), 2) AS normal_avg_speed
           FROM hodiny
          WHERE hodiny.datum < CURRENT_DATE
          GROUP BY hodiny.name, hodiny.datum
        ), h15_18 AS (
         SELECT hodiny.name,
            hodiny.datum,
            '15 -18'::text AS cas_interval,
            round(avg(hodiny.avg_time), 2) AS avg_time,
            round(avg(hodiny.avg_speed), 2) AS avg_speed,
            round(avg(hodiny.normal_avg_time), 2) AS normal_avg_time,
            round(avg(hodiny.normal_avg_speed), 2) AS normal_avg_speed
           FROM hodiny
          WHERE hodiny.hodina >= 15::double precision AND hodiny.hodina <= 17::double precision AND (hodiny.datum < CURRENT_DATE OR hodiny.hodina > 17::double precision)
          GROUP BY hodiny.name, hodiny.datum
        ), h22_6 AS (
         SELECT hodiny.name,
            hodiny.datum,
            '22 - 06'::text AS cas_interval,
            round(avg(hodiny.avg_time), 2) AS avg_time,
            round(avg(hodiny.avg_speed), 2) AS avg_speed,
            round(avg(hodiny.normal_avg_time), 2) AS normal_avg_time,
            round(avg(hodiny.normal_avg_speed), 2) AS normal_avg_speed
           FROM hodiny
          WHERE NOT (hodiny.hodina >= 6::double precision AND hodiny.hodina <= 21::double precision) AND hodiny.datum < CURRENT_DATE
          GROUP BY hodiny.name, hodiny.datum
        ), h6_22 AS (
         SELECT hodiny.name,
            hodiny.datum,
            '06 - 22'::text AS cas_interval,
            round(avg(hodiny.avg_time), 2) AS avg_time,
            round(avg(hodiny.avg_speed), 2) AS avg_speed,
            round(avg(hodiny.normal_avg_time), 2) AS normal_avg_time,
            round(avg(hodiny.normal_avg_speed), 2) AS normal_avg_speed
           FROM hodiny
          WHERE hodiny.hodina >= 6::double precision AND hodiny.hodina <= 21::double precision AND (hodiny.datum < CURRENT_DATE OR hodiny.hodina > 21::double precision)
          GROUP BY hodiny.name, hodiny.datum
        ), h7_10 AS (
         SELECT hodiny.name,
            hodiny.datum,
            '07 - 10'::text AS cas_interval,
            round(avg(hodiny.avg_time), 2) AS avg_time,
            round(avg(hodiny.avg_speed), 2) AS avg_speed,
            round(avg(hodiny.normal_avg_time), 2) AS normal_avg_time,
            round(avg(hodiny.normal_avg_speed), 2) AS normal_avg_speed
           FROM hodiny
          WHERE hodiny.hodina >= 7::double precision AND hodiny.hodina <= 9::double precision AND (hodiny.datum < CURRENT_DATE OR hodiny.hodina > 10::double precision)
          GROUP BY hodiny.name, hodiny.datum
        ), h7_11_15_19 AS (
         SELECT hodiny.name,
            hodiny.datum,
            '07 - 11, 15 - 19'::text AS cas_interval,
            round(avg(hodiny.avg_time), 2) AS avg_time,
            round(avg(hodiny.avg_speed), 2) AS avg_speed,
            round(avg(hodiny.normal_avg_time), 2) AS normal_avg_time,
            round(avg(hodiny.normal_avg_speed), 2) AS normal_avg_speed
           FROM hodiny
          WHERE (hodiny.hodina >= 7::double precision AND hodiny.hodina <= 10::double precision OR hodiny.hodina >= 15::double precision AND hodiny.hodina <= 18::double precision) AND (hodiny.datum < CURRENT_DATE OR hodiny.hodina > 18::double precision)
          GROUP BY hodiny.name, hodiny.datum
        )
 SELECT hodiny.name,
    hodiny.datum,
    hodiny.cas_interval,
    hodiny.avg_time,
    hodiny.avg_speed,
    hodiny.normal_avg_time,
    hodiny.normal_avg_speed
   FROM hodiny
UNION ALL
 SELECT den.name,
    den.datum,
    den.cas_interval,
    den.avg_time,
    den.avg_speed,
    den.normal_avg_time,
    den.normal_avg_speed
   FROM den
UNION ALL
 SELECT h15_18.name,
    h15_18.datum,
    h15_18.cas_interval,
    h15_18.avg_time,
    h15_18.avg_speed,
    h15_18.normal_avg_time,
    h15_18.normal_avg_speed
   FROM h15_18
UNION ALL
 SELECT h22_6.name,
    h22_6.datum,
    h22_6.cas_interval,
    h22_6.avg_time,
    h22_6.avg_speed,
    h22_6.normal_avg_time,
    h22_6.normal_avg_speed
   FROM h22_6
UNION ALL
 SELECT h6_22.name,
    h6_22.datum,
    h6_22.cas_interval,
    h6_22.avg_time,
    h6_22.avg_speed,
    h6_22.normal_avg_time,
    h6_22.normal_avg_speed
   FROM h6_22
UNION ALL
 SELECT h7_10.name,
    h7_10.datum,
    h7_10.cas_interval,
    h7_10.avg_time,
    h7_10.avg_speed,
    h7_10.normal_avg_time,
    h7_10.normal_avg_speed
   FROM h7_10
UNION ALL
 SELECT h7_11_15_19.name,
    h7_11_15_19.datum,
    h7_11_15_19.cas_interval,
    h7_11_15_19.avg_time,
    h7_11_15_19.avg_speed,
    h7_11_15_19.normal_avg_time,
    h7_11_15_19.normal_avg_speed
   FROM h7_11_15_19
		),
-----------------------		
		data1 AS (
        SELECT 
			trasy.name,
            trasy.name_trasa,
            trasy.name_usek,
			CASE
				WHEN trasy.is_opacny_smer THEN 'Zpět'::text
				ELSE 'Tam'::text
			END AS smer,
            vbu.datum,
            vbu.cas_interval,
            vbu.avg_time,
            vbu.avg_speed,
            vbu.normal_avg_time,
            vbu.normal_avg_speed
        FROM v_barrande_union_time_interval vbu
            JOIN analytic.v_barrande_route trasy ON trasy.name = vbu.name
        ), 
		data2 AS (
        SELECT 
			data1.name_trasa,
            data1.name_usek,
            'Oba'::text AS smer,
            data1.datum,
            data1.cas_interval,
            sum(data1.avg_time) AS avg_time,
            avg(data1.avg_speed) AS avg_speed,
            sum(data1.normal_avg_time) AS normal_avg_time,
            avg(data1.normal_avg_speed) AS normal_avg_speed
        FROM data1
        GROUP BY data1.name_trasa, data1.name_usek, 'Oba'::text, data1.datum, data1.cas_interval
        )
 SELECT data1.name,
    data1.name_trasa,
    data1.name_usek,
    data1.smer,
    data1.datum,
    data1.cas_interval,
    data1.avg_time,
    data1.avg_speed,
    data1.normal_avg_time,
    data1.normal_avg_speed
   FROM data1
UNION
 SELECT vbt.name,
    data2.name_trasa,
    data2.name_usek,
    data2.smer,
    data2.datum,
    data2.cas_interval,
    data2.avg_time,
    data2.avg_speed,
    data2.normal_avg_time,
    data2.normal_avg_speed
   FROM data2
     JOIN analytic.v_barrande_route vbt ON vbt.name_trasa = data2.name_trasa AND vbt.name_usek = data2.name_usek AND NOT vbt.is_opacny_smer
	on conflict (name,name_trasa,name_usek,smer,datum,cas_interval)
	do update 
		SET avg_time = EXCLUDED.avg_time,
		avg_speed = EXCLUDED.avg_speed,
		normal_avg_time = EXCLUDED.normal_avg_time,
		normal_avg_speed = EXCLUDED.normal_avg_speed;
end;
$procedure$
;
