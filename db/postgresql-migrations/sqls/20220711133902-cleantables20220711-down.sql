CREATE TABLE public.airqualitystations (
	id varchar(255) NOT NULL,
	state_code varchar(50) NOT NULL,
	state_name varchar(255) NOT NULL,
	region_code varchar(50) NOT NULL,
	region_name varchar(255) NOT NULL,
	station_vendor_id varchar(255) NOT NULL,
	station_name varchar(255) NOT NULL,
	"owner" varchar(255) NULL,
	classification text NULL,
	latitude float8 NULL,
	longitude float8 NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	district varchar NULL,
	CONSTRAINT airqualitystations_pkey PRIMARY KEY (id)
);

CREATE TABLE public.airqualitystations_component_types (
	id int4 NOT NULL,
	component_code varchar(50) NOT NULL,
	unit varchar(50) NOT NULL,
	description_cs text NOT NULL,
	description_en text NOT NULL,
	CONSTRAINT airqualitystations_component_types_pkey PRIMARY KEY (id)
);

CREATE TABLE public.airqualitystations_index_types (
	id int4 NOT NULL,
	index_code varchar(50) NOT NULL,
	limit_gte float8 NULL,
	limit_lt float8 NULL,
	color varchar(50) NOT NULL,
	color_text varchar(50) NOT NULL,
	description_cs text NOT NULL,
	description_en text NOT NULL,
	CONSTRAINT airqualitystations_index_types_pkey PRIMARY KEY (id)
);

CREATE TABLE public.airqualitystations_indexes (
	station_id varchar(255) NOT NULL,
	measured_from int8 NOT NULL,
	measured_to int8 NOT NULL,
	index_code varchar(50) NOT NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT airqualitystations_indexes_pkey PRIMARY KEY (station_id,measured_from,measured_to)
);

CREATE TABLE public.airqualitystations_measurements (
	station_id varchar(255) NOT NULL,
	measured_from int8 NOT NULL,
	measured_to int8 NOT NULL,
	component_code varchar(50) NOT NULL,
	aggregation_interval varchar(50) NOT NULL,
	value float8 NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT airqualitystations_measurements_pkey PRIMARY KEY (station_id,measured_from,measured_to,component_code,aggregation_interval)
);
CREATE INDEX airqualitystations_measurements_aggregation_interval ON public.airqualitystations_measurements (aggregation_interval);
CREATE INDEX airqualitystations_measurements_component_code ON public.airqualitystations_measurements (component_code);

CREATE TABLE public.fcd_traff_params_part (
	publication_time timestamptz NULL,
	source_identification varchar(50) NOT NULL,
	measurement_or_calculation_time timestamptz NOT NULL,
	predefined_location varchar(50) NOT NULL,
	version_of_predefined_location int4 NULL,
	traffic_level varchar(50) NOT NULL,
	queue_exists bool NULL,
	queue_length int4 NULL,
	from_point float4 NULL,
	to_point float4 NULL,
	data_quality float4 NULL,
	input_values int4 NULL,
	average_vehicle_speed int4 NULL,
	travel_time int4 NULL,
	free_flow_travel_time int4 NULL,
	free_flow_speed int4 NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT fcd_traff_params_part_pk PRIMARY KEY (source_identification, measurement_or_calculation_time)
)  partition by range(measurement_or_calculation_time);

CREATE INDEX fcd_traff_params_part_time ON public.fcd_traff_params_part USING btree (measurement_or_calculation_time);

CREATE TABLE fcd_traff_params_part_min PARTITION OF public.fcd_traff_params_part
    FOR VALUES FROM (MINVALUE) TO ('2020-05-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2020m05 PARTITION OF public.fcd_traff_params_part
    FOR VALUES FROM ('2020-05-01'::timestamptz) TO ('2020-06-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2020m06 PARTITION OF public.fcd_traff_params_part
    FOR VALUES FROM ('2020-06-01'::timestamptz) TO ('2020-07-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2020m07 PARTITION OF public.fcd_traff_params_part
    FOR VALUES FROM ('2020-07-01'::timestamptz) TO ('2020-08-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2020m08 PARTITION OF public.fcd_traff_params_part
    FOR VALUES FROM ('2020-08-01'::timestamptz) TO ('2020-09-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2020m09 PARTITION OF public.fcd_traff_params_part
    FOR VALUES FROM ('2020-09-01'::timestamptz) TO ('2020-10-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2020m10 PARTITION OF public.fcd_traff_params_part
    FOR VALUES FROM ('2020-10-01'::timestamptz) TO ('2020-11-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2020m11 PARTITION OF public.fcd_traff_params_part
    FOR VALUES FROM ('2020-11-01'::timestamptz) TO ('2020-12-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2020m12 PARTITION OF public.fcd_traff_params_part
    FOR VALUES FROM ('2020-12-01'::timestamptz) TO ('2021-01-01'::timestamptz);
	
-- 2021	
CREATE TABLE fcd_traff_params_part_y2021m01 PARTITION OF public.fcd_traff_params_part
    FOR VALUES FROM ('2021-01-01'::timestamptz) TO ('2021-02-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2021m02 PARTITION OF public.fcd_traff_params_part
    FOR VALUES FROM ('2021-02-01'::timestamptz) TO ('2021-03-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2021m03 PARTITION OF public.fcd_traff_params_part
    FOR VALUES FROM ('2021-03-01'::timestamptz) TO ('2021-04-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2021m04 PARTITION OF public.fcd_traff_params_part
    FOR VALUES FROM ('2021-04-01'::timestamptz) TO ('2021-05-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2021m05 PARTITION OF public.fcd_traff_params_part
    FOR VALUES FROM ('2021-05-01'::timestamptz) TO ('2021-06-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2021m06 PARTITION OF public.fcd_traff_params_part
    FOR VALUES FROM ('2021-06-01'::timestamptz) TO ('2021-07-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2021m07 PARTITION OF public.fcd_traff_params_part
    FOR VALUES FROM ('2021-07-01'::timestamptz) TO ('2021-08-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2021m08 PARTITION OF public.fcd_traff_params_part
    FOR VALUES FROM ('2021-08-01'::timestamptz) TO ('2021-09-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2021m09 PARTITION OF public.fcd_traff_params_part
    FOR VALUES FROM ('2021-09-01'::timestamptz) TO ('2021-10-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2021m10 PARTITION OF public.fcd_traff_params_part
    FOR VALUES FROM ('2021-10-01'::timestamptz) TO ('2021-11-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2021m11 PARTITION OF public.fcd_traff_params_part
    FOR VALUES FROM ('2021-11-01'::timestamptz) TO ('2021-12-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2021m12 PARTITION OF public.fcd_traff_params_part
    FOR VALUES FROM ('2021-12-01'::timestamptz) TO ('2022-01-01'::timestamptz);

-- 2022
CREATE TABLE fcd_traff_params_part_y2022m01 PARTITION OF public.fcd_traff_params_part
    FOR VALUES FROM ('2022-01-01'::timestamptz) TO ('2022-02-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2022m02 PARTITION OF public.fcd_traff_params_part
    FOR VALUES FROM ('2022-02-01'::timestamptz) TO ('2022-03-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2022m03 PARTITION OF public.fcd_traff_params_part
    FOR VALUES FROM ('2022-03-01'::timestamptz) TO ('2022-04-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2022m04 PARTITION OF public.fcd_traff_params_part
    FOR VALUES FROM ('2022-04-01'::timestamptz) TO ('2022-05-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2022m05 PARTITION OF public.fcd_traff_params_part
    FOR VALUES FROM ('2022-05-01'::timestamptz) TO ('2022-06-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2022m06 PARTITION OF public.fcd_traff_params_part
    FOR VALUES FROM ('2022-06-01'::timestamptz) TO ('2022-07-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2022m07 PARTITION OF public.fcd_traff_params_part
    FOR VALUES FROM ('2022-07-01'::timestamptz) TO ('2022-08-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2022m08 PARTITION OF public.fcd_traff_params_part
    FOR VALUES FROM ('2022-08-01'::timestamptz) TO ('2022-09-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2022m09 PARTITION OF public.fcd_traff_params_part
    FOR VALUES FROM ('2022-09-01'::timestamptz) TO ('2022-10-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2022m10 PARTITION OF public.fcd_traff_params_part
    FOR VALUES FROM ('2022-10-01'::timestamptz) TO ('2022-11-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2022m11 PARTITION OF public.fcd_traff_params_part
    FOR VALUES FROM ('2022-11-01'::timestamptz) TO ('2022-12-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2022m12 PARTITION OF public.fcd_traff_params_part
    FOR VALUES FROM ('2022-12-01'::timestamptz) TO ('2023-01-01'::timestamptz);


-- 2023
CREATE TABLE fcd_traff_params_part_y2023m01 PARTITION OF public.fcd_traff_params_part
    FOR VALUES FROM ('2023-01-01'::timestamptz) TO ('2023-02-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2023m02 PARTITION OF public.fcd_traff_params_part
    FOR VALUES FROM ('2023-02-01'::timestamptz) TO ('2023-03-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2023m03 PARTITION OF public.fcd_traff_params_part
    FOR VALUES FROM ('2023-03-01'::timestamptz) TO ('2023-04-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2023m04 PARTITION OF public.fcd_traff_params_part
    FOR VALUES FROM ('2023-04-01'::timestamptz) TO ('2023-05-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2023m05 PARTITION OF public.fcd_traff_params_part
    FOR VALUES FROM ('2023-05-01'::timestamptz) TO ('2023-06-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2023m06 PARTITION OF public.fcd_traff_params_part
    FOR VALUES FROM ('2023-06-01'::timestamptz) TO ('2023-07-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2023m07 PARTITION OF public.fcd_traff_params_part
    FOR VALUES FROM ('2023-07-01'::timestamptz) TO ('2023-08-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2023m08 PARTITION OF public.fcd_traff_params_part
    FOR VALUES FROM ('2023-08-01'::timestamptz) TO ('2023-09-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2023m09 PARTITION OF public.fcd_traff_params_part
    FOR VALUES FROM ('2023-09-01'::timestamptz) TO ('2023-10-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2023m10 PARTITION OF public.fcd_traff_params_part
    FOR VALUES FROM ('2023-10-01'::timestamptz) TO ('2023-11-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2023m11 PARTITION OF public.fcd_traff_params_part
    FOR VALUES FROM ('2023-11-01'::timestamptz) TO ('2023-12-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2023m12 PARTITION OF public.fcd_traff_params_part
    FOR VALUES FROM ('2023-12-01'::timestamptz) TO ('2024-01-01'::timestamptz);

-- 2024
CREATE TABLE fcd_traff_params_part_y2024m01 PARTITION OF public.fcd_traff_params_part
    FOR VALUES FROM ('2024-01-01'::timestamptz) TO ('2024-02-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2024m02 PARTITION OF public.fcd_traff_params_part
    FOR VALUES FROM ('2024-02-01'::timestamptz) TO ('2024-03-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2024m03 PARTITION OF public.fcd_traff_params_part
    FOR VALUES FROM ('2024-03-01'::timestamptz) TO ('2024-04-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2024m04 PARTITION OF public.fcd_traff_params_part
    FOR VALUES FROM ('2024-04-01'::timestamptz) TO ('2024-05-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2024m05 PARTITION OF public.fcd_traff_params_part
    FOR VALUES FROM ('2024-05-01'::timestamptz) TO ('2024-06-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2024m06 PARTITION OF public.fcd_traff_params_part
    FOR VALUES FROM ('2024-06-01'::timestamptz) TO ('2024-07-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2024m07 PARTITION OF public.fcd_traff_params_part
    FOR VALUES FROM ('2024-07-01'::timestamptz) TO ('2024-08-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2024m08 PARTITION OF public.fcd_traff_params_part
    FOR VALUES FROM ('2024-08-01'::timestamptz) TO ('2024-09-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2024m09 PARTITION OF public.fcd_traff_params_part
    FOR VALUES FROM ('2024-09-01'::timestamptz) TO ('2024-10-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2024m10 PARTITION OF public.fcd_traff_params_part
    FOR VALUES FROM ('2024-10-01'::timestamptz) TO ('2024-11-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2024m11 PARTITION OF public.fcd_traff_params_part
    FOR VALUES FROM ('2024-11-01'::timestamptz) TO ('2024-12-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2024m12 PARTITION OF public.fcd_traff_params_part
    FOR VALUES FROM ('2024-12-01'::timestamptz) TO ('2025-01-01'::timestamptz);

-- 2025
CREATE TABLE fcd_traff_params_part_y2025m01 PARTITION OF public.fcd_traff_params_part
    FOR VALUES FROM ('2025-01-01'::timestamptz) TO ('2025-02-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2025m02 PARTITION OF public.fcd_traff_params_part
    FOR VALUES FROM ('2025-02-01'::timestamptz) TO ('2025-03-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2025m03 PARTITION OF public.fcd_traff_params_part
    FOR VALUES FROM ('2025-03-01'::timestamptz) TO ('2025-04-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2025m04 PARTITION OF public.fcd_traff_params_part
    FOR VALUES FROM ('2025-04-01'::timestamptz) TO ('2025-05-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2025m05 PARTITION OF public.fcd_traff_params_part
    FOR VALUES FROM ('2025-05-01'::timestamptz) TO ('2025-06-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2025m06 PARTITION OF public.fcd_traff_params_part
    FOR VALUES FROM ('2025-06-01'::timestamptz) TO ('2025-07-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2025m07 PARTITION OF public.fcd_traff_params_part
    FOR VALUES FROM ('2025-07-01'::timestamptz) TO ('2025-08-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2025m08 PARTITION OF public.fcd_traff_params_part
    FOR VALUES FROM ('2025-08-01'::timestamptz) TO ('2025-09-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2025m09 PARTITION OF public.fcd_traff_params_part
    FOR VALUES FROM ('2025-09-01'::timestamptz) TO ('2025-10-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2025m10 PARTITION OF public.fcd_traff_params_part
    FOR VALUES FROM ('2025-10-01'::timestamptz) TO ('2025-11-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2025m11 PARTITION OF public.fcd_traff_params_part
    FOR VALUES FROM ('2025-11-01'::timestamptz) TO ('2025-12-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2025m12 PARTITION OF public.fcd_traff_params_part
    FOR VALUES FROM ('2025-12-01'::timestamptz) TO ('2026-01-01'::timestamptz);
-- > 25

CREATE TABLE fcd_traff_params_part_y2026mxx PARTITION OF public.fcd_traff_params_part
    FOR VALUES FROM ('2026-01-01'::timestamptz) TO (MAXVALUE);


CREATE TABLE public.ndic_traffic_info (
	exchange json NULL,
	publication_time timestamptz NULL,
	publication_creator json NULL,
	situation_id varchar(100) NOT NULL,
	situation_version varchar(100) NOT NULL,
	situation_version_time timestamptz NULL,
	situation_confidentiality varchar(100) NULL,
	situation_information_status varchar(100) NULL,
	situation_urgency varchar(100) NULL,
	situation_record_version varchar(100) NOT NULL,
	situation_record_type varchar(100) NOT NULL,
	situation_record_creation_time timestamptz NOT NULL,
	situation_record_version_time timestamptz NOT NULL,
	probability_of_occurrence varchar(100) NULL,
	"source" varchar(100) NULL,
	validity_status varchar(100) NULL,
	validity_overall_start_time timestamptz NULL,
	validity_overall_end_time timestamptz NULL,
	impact_capacity_remaining float4 NULL,
	impact_number_of_lanes_restricted int4 NULL,
	impact_number_of_operational_lanes int4 NULL,
	impact_original_number_of_lanes int4 NULL,
	impact_traffic_constriction_type varchar(100) NULL,
	impact_delays_type varchar(100) NULL,
	impact_delay_time_value float4 NULL,
	cause json NULL,
	general_public_comment text NULL,
	group_of_locations_type varchar(100) NULL,
	supplementary_positional_description json NULL,
	destination json NULL,
	alert_c_linear json NULL,
	linear_within_linear_element json NULL,
	global_network_linear json NULL,
	linear_extension json NULL,
	geom_gn_line public.geometry NULL,
	geom_openlr_line public.geometry NULL,
	situation_record_extension json NULL,
	number_of_obstructions int4 NULL,
	mobility_of_obstruction json NULL,
	compliance_option varchar(100) NULL,
	applicable_for_traffic_direction varchar(100) NULL,
	applicable_for_traffic_type varchar(100) NULL,
	places_at_which_applicable varchar(100) NULL,
	for_vehicles_with_characteristics_of json NULL,
	roadworks_duration varchar(100) NULL,
	roadworks_scale varchar(100) NULL,
	under_traffic bool NULL,
	urgent_roadworks bool NULL,
	mobility json NULL,
	subjects json NULL,
	maintenance_vehicles json NULL,
	abnormal_traffic_type varchar(100) NULL,
	relative_traffic_flow varchar(100) NULL,
	traffic_trend_type varchar(100) NULL,
	accident_type varchar(100) NULL,
	total_number_of_vehicles_involved int4 NULL,
	vehicle_involved json NULL,
	group_of_vehicles_involved json NULL,
	group_of_people_involved json NULL,
	animal_presence_type varchar(100) NULL,
	alive bool NULL,
	authority_operation_type varchar(100) NULL,
	car_park_configuration varchar(100) NULL,
	car_park_identity text NULL,
	car_park_occupancy varchar(100) NULL,
	car_park_status varchar(100) NULL,
	number_of_vacant_parking_spaces int4 NULL,
	occupied_spaces int4 NULL,
	total_capacity int4 NULL,
	driving_condition_type varchar(100) NULL,
	construction_work_type varchar(100) NULL,
	disturbance_activity_type varchar(100) NULL,
	"depth" float4 NULL,
	environmental_obstruction_type varchar(100) NULL,
	equipment_or_system_fault_type varchar(100) NULL,
	faulty_equipment_or_system_type varchar(100) NULL,
	general_instruction_to_road_users_type varchar(100) NULL,
	general_message_to_road_users json NULL,
	general_network_management_type varchar(100) NULL,
	traffic_manually_directed_by varchar(100) NULL,
	obstruction_type varchar(100) NULL,
	infrastructure_damage_type varchar(100) NULL,
	road_maintenance_type varchar(100) NULL,
	non_weather_related_road_condition_type varchar(100) NULL,
	poor_environment_type varchar(100) NULL,
	precipitation_detail json NULL,
	visibility json NULL,
	temperature json NULL,
	wind json NULL,
	public_event_type varchar(100) NULL,
	rerouting_management_type varchar(100) NULL,
	rerouting_itinerary_description json NULL,
	signed_rerouting bool NULL,
	entry text NULL,
	"exit" text NULL,
	road_or_junction_number text NULL,
	road_operator_service_disruption_type varchar(100) NULL,
	road_or_carriageway_or_lane_management_type varchar(100) NULL,
	minimum_car_occupancy int4 NULL,
	roadside_assistance_type varchar(100) NULL,
	roadside_service_disruption_type varchar(100) NULL,
	speed_management_type varchar(100) NULL,
	temporary_speed_limit float4 NULL,
	transit_service_information varchar(100) NULL,
	transit_service_type varchar(100) NULL,
	vehicle_obstruction_type varchar(100) NULL,
	obstructing_vehicle json NULL,
	weather_related_road_condition_type varchar(100) NULL,
	road_surface_condition_measurements json NULL,
	winter_equipment_management_type varchar(100) NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	geom_alert_c_line public.geometry NULL,
	alert_c_direction varchar(100) NULL,
	CONSTRAINT ndic_traffic_info_pk PRIMARY KEY (situation_id, situation_record_type, situation_record_version_time)
);

CREATE TABLE public.ndic_traffic_restrictions (
	exchange json NULL,
	publication_time timestamptz NULL,
	publication_creator json NULL,
	situation_id varchar(100) NOT NULL,
	situation_version varchar(100) NOT NULL,
	situation_version_time timestamptz NULL,
	situation_confidentiality varchar(100) NULL,
	situation_information_status varchar(100) NULL,
	situation_urgency varchar(100) NULL,
	situation_record_version varchar(100) NOT NULL,
	situation_record_type varchar(100) NOT NULL,
	situation_record_id varchar(100) NOT NULL,
	situation_record_creation_time timestamptz NOT NULL,
	situation_record_version_time timestamptz NOT NULL,
	probability_of_occurrence varchar(100) NULL,
	"source" varchar(100) NULL,
	validity_status varchar(100) NULL,
	validity_overall_start_time timestamptz NULL,
	validity_overall_end_time timestamptz NULL,
	impact_capacity_remaining float4 NULL,
	impact_number_of_lanes_restricted int4 NULL,
	impact_number_of_operational_lanes int4 NULL,
	impact_original_number_of_lanes int4 NULL,
	impact_traffic_constriction_type varchar(100) NULL,
	impact_delays_type varchar(100) NULL,
	impact_delay_time_value float4 NULL,
	cause json NULL,
	general_public_comment text NULL,
	group_of_locations_type varchar(100) NULL,
	supplementary_positional_description json NULL,
	destination json NULL,
	alert_c_linear json NULL,
	linear_within_linear_element json NULL,
	global_network_linear json NULL,
	linear_extension json NULL,
	geom_gn_line public.geometry NULL,
	geom_openlr_line public.geometry NULL,
	situation_record_extension json NULL,
	number_of_obstructions int4 NULL,
	mobility_of_obstruction json NULL,
	compliance_option varchar(100) NULL,
	applicable_for_traffic_direction varchar(100) NULL,
	applicable_for_traffic_type varchar(100) NULL,
	places_at_which_applicable varchar(100) NULL,
	for_vehicles_with_characteristics_of json NULL,
	roadworks_duration varchar(100) NULL,
	roadworks_scale varchar(100) NULL,
	under_traffic bool NULL,
	urgent_roadworks bool NULL,
	mobility json NULL,
	subjects json NULL,
	maintenance_vehicles json NULL,
	abnormal_traffic_type varchar(100) NULL,
	accident_type varchar(100) NULL,
	group_of_vehicles_involved json NULL,
	car_park_identity text NULL,
	car_park_occupancy varchar(100) NULL,
	number_of_vacant_parking_spaces int4 NULL,
	driving_condition_type varchar(100) NULL,
	construction_work_type varchar(100) NULL,
	disturbance_activity_type varchar(100) NULL,
	environmental_obstruction_type varchar(100) NULL,
	general_instruction_to_road_users_type varchar(100) NULL,
	general_message_to_road_users json NULL,
	general_network_management_type varchar(100) NULL,
	traffic_manually_directed_by varchar(100) NULL,
	infrastructure_damage_type varchar(100) NULL,
	road_maintenance_type varchar(100) NULL,
	non_weather_related_road_condition_type varchar(100) NULL,
	poor_environment_type varchar(100) NULL,
	public_event_type varchar(100) NULL,
	rerouting_management_type varchar(100) NULL,
	rerouting_itinerary_description json NULL,
	signed_rerouting bool NULL,
	entry text NULL,
	"exit" text NULL,
	road_or_junction_number text NULL,
	road_or_carriageway_or_lane_management_type varchar(100) NULL,
	minimum_car_occupancy int4 NULL,
	roadside_assistance_type varchar(100) NULL,
	speed_management_type varchar(100) NULL,
	temporary_speed_limit float4 NULL,
	vehicle_obstruction_type varchar(100) NULL,
	obstructing_vehicle json NULL,
	winter_equipment_management_type varchar(100) NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	geom_alert_c_line public.geometry NULL,
	alert_c_direction varchar(100) NULL,
	CONSTRAINT ndic_traffic_restrictions_pk PRIMARY KEY (situation_record_id, situation_record_version_time)
);

CREATE TABLE public.ropid_departures_directions (
	id serial4 NOT NULL,
	departure_stop_id text NOT NULL,
	next_stop_id_regexp text NOT NULL,
	direction text NOT NULL,
	rule_order int4 NOT NULL DEFAULT 0,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT ropid_departures_directions_rules_pkey PRIMARY KEY (id)
);

CREATE TABLE public.ropid_departures_presets (
	id serial4 NOT NULL,
	route_name varchar(100) NOT NULL,
	api_version int2 NOT NULL,
	route varchar(100) NOT NULL,
	url_query_params varchar(250) NOT NULL,
	note varchar(1000) NOT NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT ropid_departures_presets_pkey PRIMARY KEY (id)
);

CREATE TABLE public.ropidgtfs_agency (
	agency_fare_url varchar(255) NULL,
	agency_id varchar(255) NOT NULL,
	agency_lang varchar(255) NULL,
	agency_name varchar(255) NULL,
	agency_phone varchar(255) NULL,
	agency_timezone varchar(255) NULL,
	agency_url varchar(255) NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	agency_email varchar(255) NULL,
	CONSTRAINT ropidgtfs_agency_pkey PRIMARY KEY (agency_id)
);

CREATE TABLE public.ropidgtfs_calendar (
	end_date varchar(255) NULL,
	friday int4 NULL,
	monday int4 NULL,
	saturday int4 NULL,
	service_id varchar(255) NOT NULL,
	start_date varchar(255) NULL,
	sunday int4 NULL,
	thursday int4 NULL,
	tuesday int4 NULL,
	wednesday int4 NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT ropidgtfs_calendar_pkey1 PRIMARY KEY (service_id)
);


CREATE TABLE public.ropidgtfs_calendar_dates (
	"date" varchar(255) NOT NULL,
	exception_type int4 NULL,
	service_id varchar(255) NOT NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT ropidgtfs_calendar_dates_pkey1 PRIMARY KEY (date, service_id)
);

CREATE TABLE public.ropidgtfs_cis_stop_groups (
	avg_jtsk_x float8 NULL,
	avg_jtsk_y float8 NULL,
	avg_lat float8 NULL,
	avg_lon float8 NULL,
	cis int4 NOT NULL,
	district_code varchar(255) NULL,
	full_name varchar(255) NULL,
	idos_category varchar(255) NULL,
	idos_name varchar(255) NULL,
	municipality varchar(255) NULL,
	"name" varchar(255) NULL,
	node int4 NULL,
	created_at timestamptz NULL,
	unique_name varchar(255) NULL,
	create_batch_id int8 NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT ropidgtfs_cis_stop_groups_pkey PRIMARY KEY (cis)
);

CREATE TABLE public.ropidgtfs_cis_stops (
	alt_idos_name varchar(255) NULL,
	cis int4 NULL,
	id varchar(255) NOT NULL,
	jtsk_x float8 NULL,
	jtsk_y float8 NULL,
	lat float8 NULL,
	lon float8 NULL,
	platform varchar(255) NULL,
	created_at timestamptz NULL,
	wheelchair_access varchar(255) NULL,
	"zone" varchar(255) NULL,
	create_batch_id int8 NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT ropidgtfs_cis_stops_pkey PRIMARY KEY (id)
);

CREATE TABLE public.ropidgtfs_metadata (
	id serial4 NOT NULL,
	dataset varchar(255) NULL,
	"key" varchar(255) NULL,
	"type" varchar(255) NULL,
	value varchar(255) NULL,
	"version" int4 NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT ropidgtfs_metadata_pkey PRIMARY KEY (id)
);

CREATE TABLE public.ropidgtfs_ois (
	ois int4 NOT NULL,
	node int4 NOT NULL,
	"name" varchar(255) NOT NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT ropidgtfs_ois_pkey PRIMARY KEY (ois)
);

CREATE TABLE public.ropidgtfs_routes (
	agency_id varchar(255) NULL,
	is_night varchar(255) NULL,
	route_color varchar(255) NULL,
	route_desc varchar(255) NULL,
	route_id varchar(255) NOT NULL,
	route_long_name varchar(255) NULL,
	route_short_name varchar(255) NULL,
	route_text_color varchar(255) NULL,
	route_type varchar(255) NULL,
	route_url varchar(255) NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	is_regional varchar(255) NULL,
	is_substitute_transport varchar(255) NULL,
	CONSTRAINT ropidgtfs_routes_pkey PRIMARY KEY (route_id)
);

CREATE TABLE public.ropidgtfs_run_numbers (
	route_id varchar(50) NOT NULL,
	run_number int4 NOT NULL,
	service_id varchar(50) NOT NULL,
	trip_id varchar(50) NOT NULL,
	vehicle_type int4 NOT NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT ropidgtfs_run_numbers_pkey PRIMARY KEY (run_number, service_id, trip_id)
);

CREATE TABLE public.ropidgtfs_shapes (
	shape_dist_traveled float8 NULL,
	shape_id varchar(255) NOT NULL,
	shape_pt_lat float8 NULL,
	shape_pt_lon float8 NULL,
	shape_pt_sequence int4 NOT NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT ropidgtfs_shapes_pkey PRIMARY KEY (shape_id, shape_pt_sequence)
);

CREATE TABLE public.ropidgtfs_stop_times (
	arrival_time varchar(255) NULL,
	arrival_time_seconds int4 NULL,
	departure_time varchar(255) NULL,
	departure_time_seconds int4 NULL,
	drop_off_type varchar(255) NULL,
	pickup_type varchar(255) NULL,
	shape_dist_traveled float8 NULL,
	stop_headsign varchar(255) NULL,
	stop_id varchar(255) NULL,
	stop_sequence int4 NOT NULL,
	trip_id varchar(255) NOT NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	timepoint int4 NULL,
	CONSTRAINT ropidgtfs_stop_times_pkey1 PRIMARY KEY (stop_sequence, trip_id)
);

CREATE TABLE public.ropidgtfs_stops (
	location_type int4 NULL,
	parent_station varchar(255) NULL,
	platform_code varchar(255) NULL,
	stop_id varchar(255) NOT NULL,
	stop_lat float8 NULL,
	stop_lon float8 NULL,
	stop_name varchar(255) NULL,
	stop_url varchar(255) NULL,
	wheelchair_boarding int4 NULL,
	zone_id varchar(255) NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	level_id varchar(255) NULL,
	stop_code varchar(255) NULL,
	stop_desc varchar(255) NULL,
	stop_timezone varchar(255) NULL,
	CONSTRAINT ropidgtfs_stops_pkey PRIMARY KEY (stop_id)
);
CREATE INDEX ropidgtfs_stops_stop_id ON public.ropidgtfs_stops USING btree (stop_id);

CREATE TABLE public.ropidgtfs_trips (
	bikes_allowed int4 NULL,
	block_id varchar(255) NULL,
	direction_id int4 NULL,
	exceptional int4 NULL,
	route_id varchar(255) NULL,
	service_id varchar(255) NULL,
	shape_id varchar(255) NULL,
	trip_headsign varchar(255) NULL,
	trip_id varchar(255) NOT NULL,
	wheelchair_accessible int4 NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	trip_operation_type int4 NULL,
	trip_short_name varchar(255) NULL,
	CONSTRAINT ropidgtfs_trips_pkey PRIMARY KEY (trip_id)
);

CREATE TABLE public.ropidvymi_events (
	id serial4 NOT NULL,
	vymi_id int4 NOT NULL,
	vymi_id_dtb int4 NOT NULL,
	state int4 NOT NULL,
	pa_01 varchar(150) NOT NULL,
	pa_02 varchar(150) NOT NULL,
	pa_03 varchar(150) NOT NULL,
	pa_04 varchar(150) NOT NULL,
	record_type int4 NOT NULL,
	event_type int8 NULL,
	channels int4 NULL,
	title varchar(255) NULL,
	time_from timestamptz NOT NULL,
	time_to_type int4 NOT NULL,
	time_to timestamptz NULL,
	expiration_date timestamptz NULL,
	transportation_type int4 NOT NULL,
	priority int4 NULL,
	cause varchar(1000) NULL,
	link varchar(255) NULL,
	ropid_action varchar(100000) NULL,
	dpp_action varchar(100000) NULL,
	description varchar(1000) NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT ropidvymi_events_pkey PRIMARY KEY (id),
	CONSTRAINT ropidvymi_events_vymi_id_uniq UNIQUE (vymi_id)
);


CREATE TABLE public.ropidvymi_events_stops (
	id serial4 NOT NULL,
	event_id int4 NOT NULL,
	gtfs_stop_id varchar(255) NULL,
	vymi_id int4 NOT NULL,
	vymi_id_dtb int4 NOT NULL,
	node_number int4 NOT NULL,
	stop_number int4 NOT NULL,
	stop_type int4 NOT NULL,
	valid_from timestamptz NULL,
	valid_to timestamptz NULL,
	"text" varchar(10000) NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT ropidvymi_events_stops_pkey PRIMARY KEY (event_id, vymi_id)
);
CREATE INDEX ropidvymi_events_stops_idx ON public.ropidvymi_events_stops USING btree (event_id, gtfs_stop_id);


-- public.ropidvymi_events_stops foreign keys

ALTER TABLE public.ropidvymi_events_stops ADD CONSTRAINT ropidvymi_events_stops_fkey FOREIGN KEY (event_id) REFERENCES public.ropidvymi_events(id) ON DELETE CASCADE;

CREATE TABLE public.ropidvymi_events_routes (
	id serial4 NOT NULL,
	event_id int4 NOT NULL,
	gtfs_route_id varchar(255) NULL,
	vymi_id int4 NOT NULL,
	vymi_id_dtb int4 NOT NULL,
	"number" int4 NOT NULL,
	"name" varchar(255) NOT NULL,
	route_type int4 NOT NULL,
	valid_from timestamptz NULL,
	valid_to timestamptz NULL,
	"text" varchar(10000) NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT ropidvymi_events_routes_pkey PRIMARY KEY (event_id, vymi_id)
);
CREATE INDEX ropidvymi_events_routes_idx ON public.ropidvymi_events_routes USING btree (event_id, gtfs_route_id);


-- public.ropidvymi_events_routes foreign keys

ALTER TABLE public.ropidvymi_events_routes ADD CONSTRAINT ropidvymi_events_routes_fkey FOREIGN KEY (event_id) REFERENCES public.ropidvymi_events(id) ON DELETE CASCADE;

CREATE TABLE public.vehiclepositions_positions (
	created_at timestamptz NULL,
	delay int4 NULL,
	delay_stop_arrival int4 NULL,
	delay_stop_departure int4 NULL,
	next_stop_id varchar(255) NULL,
	shape_dist_traveled numeric NULL,
	is_canceled bool NULL,
	lat numeric NULL,
	lng numeric NULL,
	origin_time time NULL,
	origin_timestamp int8 NULL,
	tracking int4 NULL,
	trips_id varchar(255) NULL,
	create_batch_id int8 NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	id bigserial NOT NULL,
	bearing int4 NULL,
	cis_last_stop_id int4 NULL,
	cis_last_stop_sequence int4 NULL,
	last_stop_id varchar(255) NULL,
	last_stop_sequence int4 NULL,
	next_stop_sequence int4 NULL,
	speed int4 NULL,
	last_stop_arrival_time int8 NULL,
	last_stop_departure_time int8 NULL,
	next_stop_arrival_time int8 NULL,
	next_stop_departure_time int8 NULL,
	asw_last_stop_id varchar(50) NULL,
	state_process varchar(50) NULL,
	state_position varchar(50) NULL,
	this_stop_id varchar(50) NULL,
	this_stop_sequence int4 NULL,
	tcp_event varchar(50) NULL,
	last_stop_headsign varchar(255) NULL,
	CONSTRAINT vehiclepositions_positions_pkey PRIMARY KEY (id)
);
CREATE INDEX vehiclepositions_positions_origin_time ON public.vehiclepositions_positions USING btree (origin_time);
CREATE INDEX vehiclepositions_positions_trips_id ON public.vehiclepositions_positions USING btree (trips_id);
CREATE INDEX vehiclepositions_positions_updated_at ON public.vehiclepositions_positions USING btree (updated_at DESC);

CREATE TABLE public.vehiclepositions_runs (
	id varchar(50) NOT NULL,
	route_id varchar(50) NOT NULL,
	run_number varchar(50) NOT NULL,
	line_short_name varchar(50) NULL,
	registration_number varchar(50) NOT NULL,
	msg_start_timestamp timestamptz NOT NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	msg_last_timestamp timestamptz NULL,
	wheelchair_accessible bool NULL,
	CONSTRAINT vehiclepositions_runs_pkey PRIMARY KEY (id)
);
CREATE INDEX vehiclepositions_runs_idx ON public.vehiclepositions_runs USING btree (route_id, run_number, registration_number, msg_start_timestamp);

CREATE TABLE public.vehiclepositions_runs_messages (
	id bigserial NOT NULL,
	runs_id varchar(50) NOT NULL,
	lat float8 NULL,
	lng float8 NULL,
	actual_stop_asw_id varchar(50) NULL,
	actual_stop_timestamp_real int8 NULL,
	actual_stop_timestamp_scheduled int8 NULL,
	last_stop_asw_id varchar(50) NULL,
	packet_number varchar(50) NULL,
	msg_timestamp timestamptz NULL,
	events varchar(50) NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT vehiclepositions_runs_messages_pkey PRIMARY KEY (id)
);
CREATE INDEX vehiclepositions_runs_messages_fkey ON public.vehiclepositions_runs_messages USING btree (runs_id);
-- public.vehiclepositions_runs_messages foreign keys
ALTER TABLE public.vehiclepositions_runs_messages ADD CONSTRAINT vehiclepositions_runs_messages_fkey FOREIGN KEY (runs_id) REFERENCES public.vehiclepositions_runs(id) ON DELETE CASCADE;

CREATE TABLE public.vehiclepositions_stops (
	arrival_time time NULL,
	arrival_timestamp int8 NULL,
	cis_stop_id int4 NULL,
	cis_stop_platform_code varchar(255) NULL,
	cis_stop_sequence int4 NOT NULL,
	created_at timestamptz NULL,
	delay_arrival int4 NULL,
	delay_departure int4 NULL,
	delay_type int4 NULL,
	departure_time time NULL,
	departure_timestamp int8 NULL,
	updated_at timestamptz NULL,
	trips_id varchar(255) NOT NULL,
	create_batch_id int8 NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_by varchar(150) NULL,
	arrival_delay_type int4 NULL,
	asw_stop_id varchar(50) NULL,
	stop_id varchar(50) NULL,
	stop_sequence int4 NULL,
	arrival_timestamp_real int8 NULL,
	departure_timestamp_real int8 NULL,
	CONSTRAINT vehiclepositions_stops_pkey PRIMARY KEY (cis_stop_sequence, trips_id)
);

CREATE TABLE public.vehiclepositions_trips (
	cis_line_id varchar(50) NULL,
	cis_trip_number int4 NULL,
	sequence_id int4 NULL,
	cis_line_short_name varchar(255) NULL,
	created_at timestamptz NULL,
	gtfs_route_id varchar(255) NULL,
	gtfs_route_short_name varchar(255) NULL,
	gtfs_trip_id varchar(255) NULL,
	id varchar(255) NOT NULL,
	updated_at timestamptz NULL,
	start_cis_stop_id int4 NULL,
	start_cis_stop_platform_code varchar(255) NULL,
	start_time time NULL,
	start_timestamp int8 NULL,
	vehicle_type_id int4 NULL,
	wheelchair_accessible bool NULL,
	create_batch_id int8 NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_by varchar(150) NULL,
	agency_name_scheduled varchar(255) NULL,
	origin_route_name varchar(255) NULL,
	agency_name_real varchar(255) NULL,
	vehicle_registration_number int4 NULL,
	gtfs_trip_headsign varchar(255) NULL,
	start_asw_stop_id varchar(50) NULL,
	gtfs_route_type int4 NULL,
	gtfs_block_id varchar(255) NULL,
	last_position_id int8 NULL,
	is_canceled bool NULL,
	end_timestamp int8 NULL,
	CONSTRAINT vehiclepositions_trips_pkey PRIMARY KEY (id)
);
CREATE INDEX vehiclepositions_trips_start_timestamp ON public.vehiclepositions_trips USING btree (start_timestamp);

CREATE TABLE public.vehiclepositions_vehicle_types (
	id int4 NOT NULL,
	abbreviation text NOT NULL,
	description_cs text NOT NULL,
	description_en text NOT NULL,
	CONSTRAINT vehiclepositions_vehicle_types_pkey PRIMARY KEY (id)
);

-- public.wazeccp_alert_types definition
CREATE TABLE public.wazeccp_alert_types (
	id serial4 NOT NULL,
	"type" text NOT NULL,
	subtype text NULL,
	CONSTRAINT wazeccp_alert_types_pkey PRIMARY KEY (id)
);

CREATE TABLE public.wazeccp_alerts (
	id varchar(40) NOT NULL,
	uuid text NOT NULL,
	pub_millis int8 NOT NULL,
	pub_utc_date timestamp NULL,
	road_type int4 NULL,
	"location" jsonb NULL,
	street text NULL,
	city text NULL,
	country text NULL,
	magvar int4 NULL,
	reliability int4 NULL,
	report_description text NULL,
	report_rating int4 NULL,
	confidence int4 NULL,
	"type" text NULL,
	subtype text NULL,
	report_by_municipality_user bool NULL,
	thumbs_up int4 NULL,
	jam_uuid text NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	downloaded_at int8 NULL,
	CONSTRAINT wazeccp_alerts_pkey PRIMARY KEY (id)
);
CREATE INDEX wazeccp_alerts_idx1 ON public.wazeccp_alerts USING btree (subtype, pub_utc_date);
CREATE INDEX wazeccp_alerts_uuid ON public.wazeccp_alerts USING btree (uuid);

CREATE TABLE public.wazeccp_irregularities (
	id varchar(40) NOT NULL,
	uuid text NOT NULL,
	detection_date_millis int8 NOT NULL,
	detection_date text NULL,
	detection_utc_date timestamp NULL,
	update_date_millis int8 NOT NULL,
	update_date text NULL,
	update_utc_date timestamp NULL,
	street text NULL,
	city text NULL,
	country text NULL,
	is_highway bool NULL,
	speed float4 NULL,
	regular_speed float4 NULL,
	delay_seconds int4 NULL,
	seconds int4 NULL,
	length int4 NULL,
	trend int4 NULL,
	"type" text NULL,
	severity float4 NULL,
	jam_level int4 NULL,
	drivers_count int4 NULL,
	alerts_count int4 NULL,
	n_thumbs_up int4 NULL,
	n_comments int4 NULL,
	n_images int4 NULL,
	line jsonb NULL,
	cause_type text NULL,
	start_node text NULL,
	end_node text NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	downloaded_at int8 NULL,
	CONSTRAINT wazeccp_irregularities_pkey PRIMARY KEY (id)
);

CREATE TABLE public.wazeccp_jams (
	id varchar(40) NOT NULL,
	uuid text NOT NULL,
	pub_millis int8 NOT NULL,
	pub_utc_date timestamp NULL,
	start_node text NULL,
	end_node text NULL,
	road_type int4 NULL,
	street text NULL,
	city text NULL,
	country text NULL,
	delay int4 NULL,
	speed float4 NULL,
	speed_kmh float4 NULL,
	length int4 NULL,
	turn_type text NULL,
	"level" int4 NULL,
	blocking_alert_id text NULL,
	line jsonb NULL,
	"type" text NULL,
	turn_line jsonb NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	downloaded_at int8 NULL,
	CONSTRAINT wazeccp_jams_pkey PRIMARY KEY (id)
);
CREATE INDEX wazeccp_jams_blocking_alert_id ON public.wazeccp_jams USING btree (blocking_alert_id);
CREATE INDEX wazeccp_jams_downloaded_at ON public.wazeccp_jams USING btree (downloaded_at);
CREATE INDEX wazeccp_jams_pub_utc_date ON public.wazeccp_jams USING btree (pub_utc_date);

CREATE TABLE public.wazeccp_roads (
	id serial4 NOT NULL,
	value int4 NOT NULL,
	"name" varchar(100) NOT NULL,
	CONSTRAINT wazeccp_roads_pkey PRIMARY KEY (id),
	CONSTRAINT wazeccp_roads_unique_combo UNIQUE (value, name)
);













-- public.v_ropidgtfs_trips_minmaxsequences source

CREATE MATERIALIZED VIEW public.v_ropidgtfs_trips_minmaxsequences
TABLESPACE pg_default
AS SELECT ropidgtfs_stop_times.trip_id,
    max(ropidgtfs_stop_times.stop_sequence) AS max_stop_sequence,
    min(ropidgtfs_stop_times.stop_sequence) AS min_stop_sequence,
    GREATEST(max(ropidgtfs_stop_times.arrival_time::interval), max(ropidgtfs_stop_times.departure_time::interval))::text AS max_stop_time,
    LEAST(min(ropidgtfs_stop_times.arrival_time::interval), min(ropidgtfs_stop_times.departure_time::interval))::text AS min_stop_time
   FROM ropidgtfs_stop_times
  GROUP BY ropidgtfs_stop_times.trip_id
WITH DATA;

-- View indexes:
CREATE UNIQUE INDEX v_ropidgtfs_trips_minmaxsequences_trip_id_idx ON public.v_ropidgtfs_trips_minmaxsequences USING btree (trip_id);  

-- public.v_ropidgtfs_services_first14days source

CREATE MATERIALIZED VIEW public.v_ropidgtfs_services_first14days
TABLESPACE pg_default
AS SELECT dates.date,
    date_part('day'::text, dates.date::timestamp without time zone - to_char(timezone('Europe/Prague'::text, now()), 'YYYY-MM-DD'::text)::timestamp without time zone)::integer AS day_diff,
    ropidgtfs_calendar.service_id
   FROM ( SELECT date_trunc('day'::text, dd.dd)::date AS date
           FROM generate_series((( SELECT CURRENT_TIMESTAMP - '1 day'::interval))::date::timestamp with time zone, (( SELECT CURRENT_TIMESTAMP + '12 days'::interval))::date::timestamp with time zone, '1 day'::interval) dd(dd)) dates
     JOIN ropidgtfs_calendar ON 1 = 1
  WHERE dates.date >= ropidgtfs_calendar.start_date::date AND dates.date <= ropidgtfs_calendar.end_date::date AND (btrim(to_char(dates.date::timestamp with time zone, 'day'::text)) = 'monday'::text AND ropidgtfs_calendar.monday = 1 OR btrim(to_char(dates.date::timestamp with time zone, 'day'::text)) = 'tuesday'::text AND ropidgtfs_calendar.tuesday = 1 OR btrim(to_char(dates.date::timestamp with time zone, 'day'::text)) = 'wednesday'::text AND ropidgtfs_calendar.wednesday = 1 OR btrim(to_char(dates.date::timestamp with time zone, 'day'::text)) = 'thursday'::text AND ropidgtfs_calendar.thursday = 1 OR btrim(to_char(dates.date::timestamp with time zone, 'day'::text)) = 'friday'::text AND ropidgtfs_calendar.friday = 1 OR btrim(to_char(dates.date::timestamp with time zone, 'day'::text)) = 'saturday'::text AND ropidgtfs_calendar.saturday = 1 OR btrim(to_char(dates.date::timestamp with time zone, 'day'::text)) = 'sunday'::text AND ropidgtfs_calendar.sunday = 1) AND NOT (ropidgtfs_calendar.service_id::text IN ( SELECT ropidgtfs_calendar_dates.service_id
           FROM ropidgtfs_calendar_dates
          WHERE ropidgtfs_calendar_dates.exception_type = 2 AND dates.date = ropidgtfs_calendar_dates.date::date)) OR (ropidgtfs_calendar.service_id::text IN ( SELECT ropidgtfs_calendar_dates.service_id
           FROM ropidgtfs_calendar_dates
          WHERE ropidgtfs_calendar_dates.exception_type = 1 AND dates.date = ropidgtfs_calendar_dates.date::date))
  ORDER BY dates.date
WITH DATA;

-- View indexes:
CREATE UNIQUE INDEX v_ropidgtfs_services_first14days_date_service_id_idx ON public.v_ropidgtfs_services_first14days USING btree (date, service_id);
CREATE INDEX v_ropidgtfs_services_first14days_service_id ON public.v_ropidgtfs_services_first14days USING btree (service_id);

-- public.v_ropidgtfs_departures source

CREATE MATERIALIZED VIEW public.v_ropidgtfs_departures
TABLESPACE pg_default
AS SELECT t.stop_sequence,
    t.stop_headsign,
    t.pickup_type,
    t.drop_off_type,
    t.arrival_time,
        CASE
            WHEN date_part('hour'::text, t.arrival_time::interval) > 23::double precision THEN make_timestamptz(date_part('year'::text, t4.date + '1 day'::interval)::integer, date_part('month'::text, t4.date + '1 day'::interval)::integer, date_part('day'::text, t4.date + '1 day'::interval)::integer, date_part('hour'::text, t.arrival_time::interval)::integer - 24, date_part('minute'::text, t.arrival_time::interval)::integer, date_part('second'::text, t.arrival_time::interval), 'Europe/Prague'::text)
            ELSE make_timestamptz(date_part('year'::text, t4.date)::integer, date_part('month'::text, t4.date)::integer, date_part('day'::text, t4.date)::integer, date_part('hour'::text, t.arrival_time::interval)::integer, date_part('minute'::text, t.arrival_time::interval)::integer, date_part('second'::text, t.arrival_time::interval), 'Europe/Prague'::text)
        END AS arrival_datetime,
    t.departure_time,
        CASE
            WHEN date_part('hour'::text, t.departure_time::interval) > 23::double precision THEN make_timestamptz(date_part('year'::text, t4.date + '1 day'::interval)::integer, date_part('month'::text, t4.date + '1 day'::interval)::integer, date_part('day'::text, t4.date + '1 day'::interval)::integer, date_part('hour'::text, t.departure_time::interval)::integer - 24, date_part('minute'::text, t.departure_time::interval)::integer, date_part('second'::text, t.departure_time::interval), 'Europe/Prague'::text)
            ELSE make_timestamptz(date_part('year'::text, t4.date)::integer, date_part('month'::text, t4.date)::integer, date_part('day'::text, t4.date)::integer, date_part('hour'::text, t.departure_time::interval)::integer, date_part('minute'::text, t.departure_time::interval)::integer, date_part('second'::text, t.departure_time::interval), 'Europe/Prague'::text)
        END AS departure_datetime,
    t0.stop_id,
    t0.stop_name,
    t0.platform_code,
    t0.wheelchair_boarding,
    t1.min_stop_sequence,
    t1.max_stop_sequence,
    t2.trip_id,
    t2.trip_headsign,
    t2.trip_short_name,
    t2.wheelchair_accessible,
    t3.service_id,
    t4.date,
    t5.route_short_name,
    t5.route_type,
    t5.route_id,
    t5.is_night,
    t5.is_regional,
    t5.is_substitute_transport,
    t6.stop_sequence AS next_stop_sequence,
    t6.stop_id AS next_stop_id,
    t7.stop_sequence AS last_stop_sequence,
    t7.stop_id AS last_stop_id
   FROM ropidgtfs_stop_times t
     LEFT JOIN ropidgtfs_stops t0 ON t.stop_id::text = t0.stop_id::text
     LEFT JOIN ropidgtfs_trips t2 ON t.trip_id::text = t2.trip_id::text
     JOIN v_ropidgtfs_services_first14days t4 ON t2.service_id::text = t4.service_id::text
     LEFT JOIN v_ropidgtfs_trips_minmaxsequences t1 ON t.trip_id::text = t1.trip_id::text
     LEFT JOIN ropidgtfs_calendar t3 ON t2.service_id::text = t3.service_id::text
     LEFT JOIN ropidgtfs_routes t5 ON t2.route_id::text = t5.route_id::text
     LEFT JOIN ropidgtfs_stop_times t6 ON t.trip_id::text = t6.trip_id::text AND t6.stop_sequence = (t.stop_sequence + 1)
     LEFT JOIN ropidgtfs_stop_times t7 ON t.trip_id::text = t7.trip_id::text AND t7.stop_sequence = (t.stop_sequence - 1)
WITH DATA;

-- View indexes:
CREATE INDEX v_ropidgtfs_departures_stop_id_idx ON public.v_ropidgtfs_departures USING btree (stop_id);
CREATE UNIQUE INDEX v_ropidgtfs_departures_unique_id ON public.v_ropidgtfs_departures USING btree (service_id, date, trip_id, stop_sequence);

-- public.v_ropidgtfs_trips_shapes_view source

CREATE OR REPLACE VIEW public.v_ropidgtfs_trips_shapes_view
AS SELECT ropidgtfs_trips.bikes_allowed,
    ropidgtfs_trips.block_id,
    ropidgtfs_trips.direction_id,
    ropidgtfs_trips.exceptional,
    ropidgtfs_trips.route_id,
    ropidgtfs_trips.service_id,
    ropidgtfs_trips.shape_id,
    ropidgtfs_trips.trip_headsign,
    ropidgtfs_trips.trip_id,
    ropidgtfs_trips.wheelchair_accessible,
    shapes.shape_dist_traveled AS shapes_shape_dist_traveled,
    shapes.shape_id AS shapes_shape_id,
    shapes.shape_pt_lat AS shapes_shape_pt_lat,
    shapes.shape_pt_lon AS shapes_shape_pt_lon,
    shapes.shape_pt_sequence AS shapes_shape_pt_sequence
   FROM ropidgtfs_trips ropidgtfs_trips
     LEFT JOIN ropidgtfs_shapes shapes ON ropidgtfs_trips.shape_id::text = shapes.shape_id::text
  ORDER BY shapes.shape_pt_sequence;
  
-- public.v_ropidgtfs_trips_stop_times_view source

CREATE OR REPLACE VIEW public.v_ropidgtfs_trips_stop_times_view
AS SELECT ropidgtfs_trips.bikes_allowed,
    ropidgtfs_trips.block_id,
    ropidgtfs_trips.direction_id,
    ropidgtfs_trips.exceptional,
    ropidgtfs_trips.route_id,
    ropidgtfs_trips.service_id,
    ropidgtfs_trips.shape_id,
    ropidgtfs_trips.trip_headsign,
    ropidgtfs_trips.trip_id,
    ropidgtfs_trips.wheelchair_accessible,
    stop_times.arrival_time AS stop_times_arrival_time,
    stop_times.arrival_time_seconds AS stop_times_arrival_time_seconds,
    stop_times.departure_time AS stop_times_departure_time,
    stop_times.departure_time_seconds AS stop_times_departure_time_seconds,
    stop_times.shape_dist_traveled AS stop_times_shape_dist_traveled,
    stop_times.stop_id AS stop_times_stop_id,
    stop_times.stop_sequence AS stop_times_stop_sequence,
    stop_times.stop_headsign AS stop_times_stop_headsign,
    stop_times.trip_id AS stop_times_trip_id,
    "stop_times->stop".stop_id AS stop_times_stop_stop_id,
    "stop_times->stop".stop_lat AS stop_times_stop_stop_lat,
    "stop_times->stop".stop_lon AS stop_times_stop_stop_lon
   FROM ropidgtfs_trips ropidgtfs_trips
     LEFT JOIN ropidgtfs_stop_times stop_times ON ropidgtfs_trips.trip_id::text = stop_times.trip_id::text
     LEFT JOIN ropidgtfs_stops "stop_times->stop" ON stop_times.stop_id::text = "stop_times->stop".stop_id::text
  ORDER BY stop_times.stop_sequence;  
  


-- public.v_vehiclepositions_last_position source

CREATE OR REPLACE VIEW public.v_vehiclepositions_last_position
AS SELECT t2.created_at,
    t2.updated_at,
    t2.delay,
    t2.delay_stop_arrival,
    t2.delay_stop_departure,
    t2.next_stop_id,
    t2.shape_dist_traveled,
    t2.is_canceled,
    t2.lat,
    t2.lng,
    t2.origin_time,
    t2.origin_timestamp,
    t2.tracking,
    t2.trips_id,
    t2.create_batch_id,
    t2.created_by,
    t2.update_batch_id,
    t2.updated_by,
    t2.id,
    t2.bearing,
    t2.cis_last_stop_id,
    t2.cis_last_stop_sequence,
    t2.last_stop_id,
    t2.last_stop_sequence,
    t2.next_stop_sequence,
    t2.speed,
    t2.last_stop_arrival_time,
    t2.last_stop_departure_time,
    t2.next_stop_arrival_time,
    t2.next_stop_departure_time,
    t2.asw_last_stop_id,
    t1.gtfs_route_id,
    t1.gtfs_route_short_name,
    t1.gtfs_route_type,
    t1.gtfs_trip_id,
    t1.gtfs_trip_headsign,
    t1.vehicle_type_id,
    t1.wheelchair_accessible
   FROM vehiclepositions_trips t1
     LEFT JOIN vehiclepositions_positions t2 ON t1.last_position_id = t2.id
  WHERE t2.updated_at > (now() - '00:10:00'::interval) AND (t2.state_position::text = ANY (ARRAY['on_track'::character varying::text, 'off_track'::character varying::text, 'at_stop'::character varying::text, 'before_track'::character varying::text, 'after_track'::character varying::text, 'canceled'::character varying::text]))
  ORDER BY t2.trips_id, t2.updated_at DESC;


-- public.v_vehiclepositions_positions_v1 source

CREATE OR REPLACE VIEW public.v_vehiclepositions_positions_v1
AS SELECT v_vehiclepositions_positions_v1.created_at,
    v_vehiclepositions_positions_v1.delay,
    v_vehiclepositions_positions_v1.delay_stop_arrival,
    v_vehiclepositions_positions_v1.delay_stop_departure,
    v_vehiclepositions_positions_v1.next_stop_id AS gtfs_next_stop_id,
    v_vehiclepositions_positions_v1.shape_dist_traveled AS gtfs_shape_dist_traveled,
    v_vehiclepositions_positions_v1.is_canceled,
    v_vehiclepositions_positions_v1.lat,
    v_vehiclepositions_positions_v1.lng,
    v_vehiclepositions_positions_v1.origin_time,
    v_vehiclepositions_positions_v1.origin_timestamp,
    v_vehiclepositions_positions_v1.tracking,
    v_vehiclepositions_positions_v1.trips_id,
    v_vehiclepositions_positions_v1.create_batch_id,
    v_vehiclepositions_positions_v1.created_by,
    v_vehiclepositions_positions_v1.update_batch_id,
    v_vehiclepositions_positions_v1.updated_at,
    v_vehiclepositions_positions_v1.updated_by,
    v_vehiclepositions_positions_v1.id,
    v_vehiclepositions_positions_v1.bearing,
    v_vehiclepositions_positions_v1.cis_last_stop_id,
    v_vehiclepositions_positions_v1.cis_last_stop_sequence,
    v_vehiclepositions_positions_v1.last_stop_id AS gtfs_last_stop_id,
    v_vehiclepositions_positions_v1.last_stop_sequence AS gtfs_last_stop_sequence,
    v_vehiclepositions_positions_v1.next_stop_sequence AS gtfs_next_stop_sequence,
    v_vehiclepositions_positions_v1.speed
   FROM vehiclepositions_positions v_vehiclepositions_positions_v1;
   
-- public.v_vehiclepositions_last_position_v1 source

CREATE OR REPLACE VIEW public.v_vehiclepositions_last_position_v1
AS SELECT v_vehiclepositions_last_position_v1.created_at,
    v_vehiclepositions_last_position_v1.delay,
    v_vehiclepositions_last_position_v1.delay_stop_arrival,
    v_vehiclepositions_last_position_v1.delay_stop_departure,
    v_vehiclepositions_last_position_v1.next_stop_id AS gtfs_next_stop_id,
    v_vehiclepositions_last_position_v1.shape_dist_traveled AS gtfs_shape_dist_traveled,
    v_vehiclepositions_last_position_v1.is_canceled,
    v_vehiclepositions_last_position_v1.lat,
    v_vehiclepositions_last_position_v1.lng,
    v_vehiclepositions_last_position_v1.origin_time,
    v_vehiclepositions_last_position_v1.origin_timestamp,
    v_vehiclepositions_last_position_v1.tracking,
    v_vehiclepositions_last_position_v1.trips_id,
    v_vehiclepositions_last_position_v1.create_batch_id,
    v_vehiclepositions_last_position_v1.created_by,
    v_vehiclepositions_last_position_v1.update_batch_id,
    v_vehiclepositions_last_position_v1.updated_at,
    v_vehiclepositions_last_position_v1.updated_by,
    v_vehiclepositions_last_position_v1.id,
    v_vehiclepositions_last_position_v1.bearing,
    v_vehiclepositions_last_position_v1.cis_last_stop_id,
    v_vehiclepositions_last_position_v1.cis_last_stop_sequence,
    v_vehiclepositions_last_position_v1.last_stop_id AS gtfs_last_stop_id,
    v_vehiclepositions_last_position_v1.last_stop_sequence AS gtfs_last_stop_sequence,
    v_vehiclepositions_last_position_v1.next_stop_sequence AS gtfs_next_stop_sequence,
    v_vehiclepositions_last_position_v1.speed
   FROM v_vehiclepositions_last_position v_vehiclepositions_last_position_v1;
   
-- public.v_vehiclepositions_trips_v1 source

CREATE OR REPLACE VIEW public.v_vehiclepositions_trips_v1
AS SELECT v_vehiclepositions_trips_v1.cis_line_id AS cis_id,
    v_vehiclepositions_trips_v1.cis_trip_number AS cis_number,
    v_vehiclepositions_trips_v1.sequence_id AS cis_order,
    v_vehiclepositions_trips_v1.cis_line_short_name AS cis_short_name,
    v_vehiclepositions_trips_v1.created_at,
    v_vehiclepositions_trips_v1.gtfs_route_id,
    v_vehiclepositions_trips_v1.gtfs_route_short_name,
    v_vehiclepositions_trips_v1.gtfs_trip_id,
    v_vehiclepositions_trips_v1.id,
    v_vehiclepositions_trips_v1.updated_at,
    v_vehiclepositions_trips_v1.start_cis_stop_id,
    v_vehiclepositions_trips_v1.start_cis_stop_platform_code,
    v_vehiclepositions_trips_v1.start_time,
    v_vehiclepositions_trips_v1.start_timestamp,
    v_vehiclepositions_trips_v1.vehicle_type_id AS vehicle_type,
    v_vehiclepositions_trips_v1.wheelchair_accessible,
    v_vehiclepositions_trips_v1.create_batch_id,
    v_vehiclepositions_trips_v1.created_by,
    v_vehiclepositions_trips_v1.update_batch_id,
    v_vehiclepositions_trips_v1.updated_by,
    v_vehiclepositions_trips_v1.agency_name_scheduled AS cis_agency_name,
    v_vehiclepositions_trips_v1.origin_route_name AS cis_parent_route_name,
    v_vehiclepositions_trips_v1.agency_name_real AS cis_real_agency_name,
    v_vehiclepositions_trips_v1.vehicle_registration_number AS cis_vehicle_registration_number
   FROM vehiclepositions_trips v_vehiclepositions_trips_v1;   
   
-- public.v_waze_potholes_last_days source

CREATE OR REPLACE VIEW public.v_waze_potholes_last_days
AS SELECT DISTINCT ON (wa.uuid) wa.uuid,
    wa.city,
    wa.location #>> '{}'::text[] AS location,
    wa.street,
    wa.road_type,
    wa.magvar AS event_direction,
    timezone('Europe/Prague'::text, wa.pub_utc_date) AS published_at,
    timezone('Europe/Prague'::text, wa.created_at) AS last_reported,
    wa.reliability,
    wa.report_rating,
    wa.confidence
   FROM wazeccp_alerts wa
  WHERE wa.subtype = 'HAZARD_ON_ROAD_POT_HOLE'::text AND wa.pub_utc_date > (now()::date - '7 days'::interval)
  ORDER BY wa.uuid, wa.pub_utc_date, wa.created_at DESC;   