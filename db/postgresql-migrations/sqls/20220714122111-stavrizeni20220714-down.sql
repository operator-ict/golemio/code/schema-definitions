drop VIEW analytic.v_construction_procedure;

drop table python.construction_procedure_type_statemnt;
drop TABLE python.construction_procedure_type;
drop TABLE python.construction_procedure_statement;
drop TABLE python.construction_procedure_cadastral_list;
drop TABLE python.construction_procedure_cadastral;
drop TABLE python.construction_procedure_account_unit;
drop TABLE python.construction_procedure;