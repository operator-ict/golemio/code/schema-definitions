DROP TABLE IF EXISTS public.wazeccp_alert_types;
DROP SEQUENCE IF EXISTS public.wazeccp_alert_types_id_seq;
DROP TABLE IF EXISTS public.wazeccp_roads;
DROP SEQUENCE IF EXISTS public.wazeccp_roads_id_seq;

DROP TABLE public.wazeccp_alerts;

DROP TABLE public.wazeccp_irregularities;

DROP INDEX wazeccp_jams_blocking_alert_id;

DROP INDEX wazeccp_jams_pub_utc_date;

DROP TABLE public.wazeccp_jams;
