drop TABLE keboola.internal_reports_zmenteteo_events;
drop TABLE keboola.internal_reports_smart_prague;
drop TABLE keboola.internal_reports_pragozor;
drop TABLE keboola.internal_reports_operatorict;
drop TABLE keboola.internal_reports_mojepraha_events;
drop table keboola.internal_reports_moje_praha;
drop TABLE keboola.internal_reports_golemio_bi;
drop TABLE keboola.internal_reports_golemio;
drop TABLE keboola.internal_reports_covid;

drop schema if exists keboola;