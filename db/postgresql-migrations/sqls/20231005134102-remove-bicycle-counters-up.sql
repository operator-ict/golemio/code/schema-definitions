DROP VIEW IF EXISTS	analytic.v_camea_data_check;
DROP TABLE IF EXISTS  public.bicyclecounters_detections;
DROP TABLE IF EXISTS  public.bicyclecounters_locations;
DROP TABLE IF EXISTS  public.bicyclecounters_directions;
DROP TABLE IF EXISTS  public.bicyclecounters_temperatures;