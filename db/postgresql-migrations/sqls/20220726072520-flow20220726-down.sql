

CREATE TABLE analytic.pedestrians_wifi (
	measured_from timestamp NULL,
	start_region varchar(50) NULL,
	end_region varchar(50) NULL,
	value int4 NULL,
	direction_id varchar(50) NULL,
	location_id varchar(50) NULL
);

CREATE TABLE analytic.pedestrians_locations_api (
	location_id serial4 NOT NULL,
	location_name varchar(250) NULL,
	lat numeric NULL,
	lng numeric NULL,
	address varchar(250) NULL,
	city_district varchar(250) NULL,
	tech varchar(250) NULL,
	map_url varchar(2500) NULL,
	place_url varchar(2500) NULL,
	measurement_start timestamptz NULL,
	measurement_end timestamptz NULL,
	cube_id varchar(250) NULL
);

-- analytic.v_pedestrians_locations_api source

CREATE OR REPLACE VIEW analytic.v_pedestrians_locations_api
AS SELECT l.location_id::character varying(5) AS location_id,
    l.location_name,
    l.lat,
    l.lng,
    l.address,
    l.city_district,
    l.tech,
    l.map_url,
    l.place_url,
    l.measurement_start,
    l.measurement_end
   FROM analytic.pedestrians_locations_api l
  ORDER BY (l.location_id::character varying(5));
  
-- public.v_pedestrians_locations_api source

CREATE OR REPLACE VIEW public.v_pedestrians_locations_api
AS SELECT l.location_id::character varying(5) AS location_id,
    l.location_name,
    l.lat,
    l.lng,
    l.address,
    l.city_district,
    l.tech,
    l.map_url,
    l.place_url,
    l.measurement_start,
    l.measurement_end
   FROM analytic.pedestrians_locations_api l
  ORDER BY (l.location_id::character varying(5));  


CREATE TABLE analytic.pedestrians_directions_api (
	direction_id varchar(250) NOT NULL,
	cube_id varchar(250) NOT NULL,
	location_id int4 NOT NULL,
	direction_name varchar(50) NOT NULL,
	direction_type varchar(250) NOT NULL
);

-- public.v_pedestrians_directions_api source

CREATE OR REPLACE VIEW public.v_pedestrians_directions_api
AS SELECT pedestrians_directions_api.direction_id,
    pedestrians_directions_api.location_id::character varying(5) AS location_id,
    pedestrians_directions_api.direction_name,
    pedestrians_directions_api.direction_type::character varying(5) AS direction_type
   FROM analytic.pedestrians_directions_api
  ORDER BY (pedestrians_directions_api.location_id::character varying(5));

-- analytic.v_pedestrians_directions_api source

CREATE OR REPLACE VIEW analytic.v_pedestrians_directions_api
AS SELECT pedestrians_directions_api.direction_id,
    pedestrians_directions_api.location_id::character varying(5) AS location_id,
    pedestrians_directions_api.direction_name,
    pedestrians_directions_api.direction_type::character varying(5) AS direction_type
   FROM analytic.pedestrians_directions_api
  ORDER BY (pedestrians_directions_api.location_id::character varying(5));

CREATE TABLE public.flow_sinks (
	id int4 NOT NULL,
	"name" varchar(150) NULL,
	history_start_timestamp int8 NOT NULL,
	output_value_type varchar(150) NOT NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	cube_id int4 NOT NULL,
	CONSTRAINT flow_sinkspkey PRIMARY KEY (id, cube_id, output_value_type)
);

CREATE TABLE public.flow_od_measurements (
	cube_id int4 NOT NULL,
	analytic_id int4 NOT NULL,
	sequence_number int4 NOT NULL,
	sink_id int4 NOT NULL,
	start_timestamp int8 NOT NULL,
	end_timestamp int8 NOT NULL,
	category varchar(150) NOT NULL,
	origin varchar(150) NOT NULL,
	destination varchar(150) NOT NULL,
	value int4 NOT NULL,
	data_validity varchar(50) NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT flow_od_measurementspkey PRIMARY KEY (cube_id, sink_id, start_timestamp, end_timestamp, category, origin, destination, sequence_number)
);
CREATE INDEX flow_od_measurements_created_at ON public.flow_od_measurements USING btree (created_at);

CREATE TABLE public.flow_measurements (
	sink_id int4 NOT NULL,
	cube_id int4 NOT NULL,
	sequence_number int4 NOT NULL,
	analytic_id int4 NOT NULL,
	start_timestamp int8 NOT NULL,
	end_timestamp int8 NOT NULL,
	category varchar(150) NOT NULL,
	value int4 NOT NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	data_validity varchar(50) NULL,
	CONSTRAINT flow_measurementspkey PRIMARY KEY (cube_id, sink_id, start_timestamp, end_timestamp, category, sequence_number)
);
CREATE INDEX flow_measurements_ceated_at ON public.flow_measurements USING btree (created_at);

CREATE TABLE public.flow_cubes (
	id int4 NOT NULL,
	"name" varchar(150) NOT NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT flow_cubespkey PRIMARY KEY (id, name)
);

CREATE TABLE public.flow_analytics (
	id int4 NOT NULL,
	"name" varchar(150) NULL,
	sequence_number int4 NOT NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT flow_analyticspkey PRIMARY KEY (id, sequence_number)
);

CREATE TABLE public.counters_locations (
	id varchar(255) NOT NULL,
	vendor_id varchar(255) NOT NULL,
	vendor varchar(100) NULL,
	lat float8 NOT NULL,
	lng float8 NOT NULL,
	"name" varchar(255) NULL,
	route varchar(255) NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT counters_locations_pkey PRIMARY KEY (id)
);

CREATE TABLE public.counters_directions (
	id varchar(255) NOT NULL,
	vendor_id varchar(255) NOT NULL,
	locations_id varchar(255) NOT NULL,
	"name" varchar(255) NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT counters_directions_pkey PRIMARY KEY (id)
);
CREATE INDEX counters_directions_locations_id ON public.counters_directions USING btree (locations_id);

CREATE TABLE public.counters_detections (
	locations_id varchar(255) NOT NULL,
	directions_id varchar(255) NOT NULL,
	measured_from int8 NOT NULL,
	measured_to int8 NOT NULL,
	category varchar(100) NOT NULL,
	value int4 NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT counters_detections_pkey PRIMARY KEY (locations_id, directions_id, measured_from, category)
);
CREATE INDEX counters_detections_directions_id ON public.counters_detections USING btree (directions_id);
CREATE INDEX counters_detections_locations_id ON public.counters_detections USING btree (locations_id);


-- public.mv_pedestrians_detections_api source

CREATE MATERIALIZED VIEW public.mv_pedestrians_detections_api
TABLESPACE pg_default
AS WITH wifi AS (
         SELECT pedestrians_wifi.location_id,
            pedestrians_wifi.direction_id,
            date_trunc('hour'::text, pedestrians_wifi.measured_from) + (date_part('minute'::text, pedestrians_wifi.measured_from)::integer / 15)::double precision * '00:15:00'::interval AS measured_from,
            date_trunc('hour'::text, pedestrians_wifi.measured_from) + (date_part('minute'::text, pedestrians_wifi.measured_from)::integer / 15)::double precision * '00:15:00'::interval + '00:15:00'::interval AS measured_to,
            sum(pedestrians_wifi.value) AS value,
            count(pedestrians_wifi.value) AS count_n,
            3 AS quantity
           FROM analytic.pedestrians_wifi
          GROUP BY pedestrians_wifi.location_id, pedestrians_wifi.direction_id, (date_trunc('hour'::text, pedestrians_wifi.measured_from) + (date_part('minute'::text, pedestrians_wifi.measured_from)::integer / 15)::double precision * '00:15:00'::interval), (date_trunc('hour'::text, pedestrians_wifi.measured_from) + (date_part('minute'::text, pedestrians_wifi.measured_from)::integer / 15)::double precision * '00:15:00'::interval + '00:15:00'::interval)
        ), pyro AS (
         SELECT cd.locations_id AS location_id,
            cd.directions_id AS direction_id,
            to_timestamp((cd.measured_from / 1000)::double precision) AS measured_from,
            to_timestamp((cd.measured_from / 1000)::double precision) + '00:15:00'::interval AS measured_to,
            sum(cd.value) AS value,
            1 AS count_n,
            1 AS quantity
           FROM counters_detections cd
          WHERE cd.category::text = 'pedestrian'::text AND (cd.directions_id::text IN ( SELECT DISTINCT pedestrians_directions_api.direction_id
                   FROM analytic.pedestrians_directions_api))
          GROUP BY cd.locations_id, cd.directions_id, (to_timestamp((cd.measured_from / 1000)::double precision)), (to_timestamp((cd.measured_from / 1000)::double precision) + '00:15:00'::interval)
        ), flow AS (
         SELECT flow_measurements.cube_id::character varying(50) AS location_id,
            flow_measurements.sink_id::character varying(50) AS direction_id,
            date_trunc('hour'::text, to_timestamp((flow_measurements.start_timestamp / 1000)::double precision)) + (date_part('minute'::text, to_timestamp((flow_measurements.start_timestamp / 1000)::double precision))::integer / 15)::double precision * '00:15:00'::interval AS measured_from,
            date_trunc('hour'::text, to_timestamp((flow_measurements.start_timestamp / 1000)::double precision)) + (date_part('minute'::text, to_timestamp((flow_measurements.start_timestamp / 1000)::double precision))::integer / 15)::double precision * '00:15:00'::interval + '00:15:00'::interval AS measured_to,
            sum(flow_measurements.value) AS value,
            count(flow_measurements.value) AS count_n,
            3 AS quantity
           FROM flow_measurements
          WHERE ((flow_measurements.cube_id::character varying(50)::text, flow_measurements.sink_id::character varying(50)::text) IN ( SELECT DISTINCT pedestrians_directions_api.cube_id AS location_id,
                    pedestrians_directions_api.direction_id
                   FROM analytic.pedestrians_directions_api)) AND flow_measurements.category::text = 'pedestrian'::text
          GROUP BY (flow_measurements.cube_id::character varying(50)), (flow_measurements.sink_id::character varying(50)), (date_trunc('hour'::text, to_timestamp((flow_measurements.start_timestamp / 1000)::double precision)) + (date_part('minute'::text, to_timestamp((flow_measurements.start_timestamp / 1000)::double precision))::integer / 15)::double precision * '00:15:00'::interval), (date_trunc('hour'::text, to_timestamp((flow_measurements.start_timestamp / 1000)::double precision)) + (date_part('minute'::text, to_timestamp((flow_measurements.start_timestamp / 1000)::double precision))::integer / 15)::double precision * '00:15:00'::interval + '00:15:00'::interval)
          ORDER BY (date_trunc('hour'::text, to_timestamp((flow_measurements.start_timestamp / 1000)::double precision)) + (date_part('minute'::text, to_timestamp((flow_measurements.start_timestamp / 1000)::double precision))::integer / 15)::double precision * '00:15:00'::interval), (flow_measurements.cube_id::character varying(50)), (flow_measurements.sink_id::character varying(50))
        ), measurements AS (
         SELECT wifi.location_id,
            wifi.direction_id,
            wifi.measured_from,
            wifi.measured_to,
            wifi.value,
            wifi.count_n,
            wifi.quantity
           FROM wifi
        UNION ALL
         SELECT pyro.location_id,
            pyro.direction_id,
            pyro.measured_from,
            pyro.measured_to,
            pyro.value,
            pyro.count_n,
            pyro.quantity
           FROM pyro
        UNION ALL
         SELECT flow.location_id,
            flow.direction_id,
            flow.measured_from,
            flow.measured_to,
            flow.value,
            flow.count_n,
            flow.quantity
           FROM flow
        )
 SELECT measurements.measured_from,
    measurements.measured_to,
    measurements.location_id,
    measurements.direction_id,
    measurements.value,
    measurements.count_n::numeric / measurements.quantity::numeric AS quality
   FROM measurements
  ORDER BY (measurements.count_n::numeric / measurements.quantity::numeric)
WITH NO DATA;

-- View indexes:
CREATE INDEX pedestrians_detections_direction_id_index ON public.mv_pedestrians_detections_api USING btree (direction_id);
CREATE INDEX pedestrians_detections_location_id_index ON public.mv_pedestrians_detections_api USING btree (location_id);
CREATE INDEX pedestrians_detections_measured_from_index ON public.mv_pedestrians_detections_api USING btree (measured_from);
CREATE INDEX pedestrians_detections_measured_to_index ON public.mv_pedestrians_detections_api USING btree (measured_to);


