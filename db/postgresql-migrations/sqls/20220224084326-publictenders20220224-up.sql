-- pridat dva nove sloupce
alter table python.public_tenders
add column zadavatel_nazev text,
add column zadavatel_ico text;

-- prejmenovat anglicky aby to splnovalo jmennou konvenci
alter table python.public_tenders_odbory
rename to public_tenders_departments;


-- franz.public_tenders_funded_organisations definition

CREATE TABLE python.public_tenders_funded_organisations (
	id int8 not NULL primary key,
	jmeno text NULL,
	ulice text NULL,
	cislo_domu text NULL,
	cislo_orientacni text NULL,
	mestska_cast text NULL,
	obec text NULL,
	psc text NULL,
	reditelka text NULL,
	ico text NULL,
	id_ds text NULL,
	odbor_mhmp text NULL,
	oblast text NULL,
	web_kontakty text NULL,
	profil_zadavatele text NULL
);

CREATE unique INDEX public_tenders_funded_organisations_name 
ON python.public_tenders_funded_organisations USING btree (jmeno);
