/* Replace with your SQL commands */

ALTER TABLE python.public_tenders_funded_organisations
	ADD updated_at TIMESTAMPTZ NULL;
ALTER TABLE python.public_tenders_funded_organisations 
    DROP CONSTRAINT public_tenders_funded_organisations_pkey;
ALTER TABLE python.public_tenders_funded_organisations 
    ADD CONSTRAINT public_tenders_funded_organisations_pkey PRIMARY KEY (ico);



