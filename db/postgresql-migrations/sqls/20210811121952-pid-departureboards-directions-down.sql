DROP TABLE IF EXISTS public.ropid_departures_directions;

DROP SEQUENCE IF EXISTS public.ropid_departures_directions_id_seq;

DROP MATERIALIZED VIEW IF EXISTS "public"."v_ropidgtfs_departures";