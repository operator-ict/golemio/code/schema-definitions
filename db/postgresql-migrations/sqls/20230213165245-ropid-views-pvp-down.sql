DROP VIEW analytic.v_ropidbi_coupon_sales_monthly_by_zones;

CREATE OR REPLACE VIEW analytic.v_ropidbi_coupon_sales_monthly_by_zones AS
WITH mos AS (
         SELECT mbc.coupon_id,
            mbc.created::date AS created,
            mbc.customer_profile_name,
                CASE
                    WHEN mbc.tariff_profile_name::text = ANY (ARRAY['Bezplatná4'::text, 'Bezplatná5'::text, 'Bezplatná6'::text]) THEN 'Bezplatná'::text
                    ELSE mbc.tariff_profile_name::text
                END AS tariff_profile_name,
            mbc.price,
                CASE
                    WHEN mbc.created_by_id::integer = 188 THEN 'Mobilní aplikace'::text
                    WHEN mbc.order_payment_type = ANY (ARRAY[1, 2]) THEN 'Eshop'::text
                    WHEN mbc.order_payment_type = ANY (ARRAY[3, 4]) THEN 'Přepážka'::text
                    ELSE 'Nezařazeno'::text
                END AS channel,
                CASE
                    WHEN mbc.seller_id = 1 THEN 'OICT'::text
                    WHEN mbc.seller_id = 29 THEN 'DPP'::text
                    ELSE 'Nezařazeno'::text
                END AS seller
            FROM mos_be_coupons mbc
        ), zones AS (
         SELECT z.coupon_id,
            string_agg(z.zona::text, ', '::text) AS zones,
            count(*) AS zones_count
           FROM ( SELECT mos_be_zones.coupon_id,
                        CASE
                            WHEN mos_be_zones.zone_name::text = ANY (ARRAY['0'::text, '0+B'::character varying::text]) THEN '0+B'::character varying
                            WHEN mos_be_zones.zone_name::text = 'Praha'::text THEN 'P'::character varying
                            ELSE mos_be_zones.zone_name
                        END AS zona,
                        CASE
                            WHEN mos_be_zones.zone_name::text = ANY (ARRAY['0'::text, '0+B'::character varying::text]) THEN 0
                            WHEN mos_be_zones.zone_name::text = 'Praha'::text THEN 0
                            ELSE mos_be_zones.zone_name::integer
                        END AS zone_order
                   FROM mos_be_zones
                  WHERE mos_be_zones.zone_name::text <> ''::text AND mos_be_zones.zone_name::text IS NOT NULL
                  GROUP BY mos_be_zones.coupon_id, (
                        CASE
                            WHEN mos_be_zones.zone_name::text = ANY (ARRAY['0'::text, '0+B'::character varying::text]) THEN '0+B'::character varying
                            WHEN mos_be_zones.zone_name::text = 'Praha'::text THEN 'P'::character varying
                            ELSE mos_be_zones.zone_name
                        END), (
                        CASE
                            WHEN mos_be_zones.zone_name::text = ANY (ARRAY['0'::text, '0+B'::character varying::text]) THEN 0
                            WHEN mos_be_zones.zone_name::text = 'Praha'::text THEN 0
                            ELSE mos_be_zones.zone_name::integer
                        END)
                  ORDER BY mos_be_zones.coupon_id, (
                        CASE
                            WHEN mos_be_zones.zone_name::text = ANY (ARRAY['0'::text, '0+B'::character varying::text]) THEN 0
                            WHEN mos_be_zones.zone_name::text = 'Praha'::text THEN 0
                            ELSE mos_be_zones.zone_name::integer
                        END)) z
          GROUP BY z.coupon_id
        ), main AS (
         SELECT c.coupon_id,
            to_char(c.created::timestamp with time zone, 'YYYY-MM-01'::text)::date AS purchase_date,
            c.tariff_profile_name,
            c.customer_profile_name,
            c.price,
            c.channel,
            c.seller,
                CASE
                    WHEN zones.zones = 'P'::text THEN 'P, 0+B'::text
                    WHEN zones.zones IS NOT NULL THEN zones.zones
                    ELSE 'Nezařazeno'::text
                END AS zones,
                CASE
                    WHEN zones.zones_count IS NOT NULL AND zones.zones <> 'P, 0+B'::text THEN zones.zones_count::text
                    WHEN zones.zones = 'P, 0+B'::text THEN 'P, 0+B'::text
                    ELSE 0::text
                END AS zones_count
           FROM mos c
             LEFT JOIN zones ON c.coupon_id = zones.coupon_id
          ORDER BY c.coupon_id
        )
 SELECT m.tariff_profile_name,
    m.customer_profile_name,
    m.channel,
    m.seller,
    m.purchase_date,
    m.zones,
    m.zones_count,
    count(*) AS coupons_count,
    sum(m.price) AS sales
   FROM main m
  GROUP BY m.tariff_profile_name, m.customer_profile_name, m.channel, m.seller, m.purchase_date, m.zones_count, m.zones
  ORDER BY m.tariff_profile_name, m.customer_profile_name, m.channel, m.seller, m.zones_count, m.zones, m.purchase_date;

DROP VIEW analytic.v_ropidbi_ticket_zones;

CREATE OR REPLACE VIEW analytic.v_ropidbi_ticket_zones
AS SELECT tab.created AS valid_from,
    tab.customer_profile_name AS customer,
    tab.tariff_profile_name AS tarif,
    tab.zona,
    tab.zone_order,
    count(*) AS count
   FROM ( SELECT c.created::date AS created,
            c.coupon_id,
            c.tariff_profile_name,
            c.customer_profile_name,
            z.zona,
            z.zone_order
           FROM mos_be_coupons c
             JOIN ( SELECT z_1.coupon_id,
                        CASE
                            WHEN z_1.zone_name::text = ANY (ARRAY['Praha'::character varying::text, '0'::text, '0+B'::character varying::text]) THEN 'P+0+B'::character varying
                            ELSE z_1.zone_name
                        END AS zona,
                        CASE
                            WHEN z_1.zone_name::text = ANY (ARRAY['Praha'::character varying::text, '0'::text, '0+B'::character varying::text]) THEN '0'::character varying
                            ELSE z_1.zone_name
                        END AS zone_order
                   FROM mos_be_zones z_1
                  GROUP BY z_1.coupon_id, (
                        CASE
                            WHEN z_1.zone_name::text = ANY (ARRAY['Praha'::character varying::text, '0'::text, '0+B'::character varying::text]) THEN 'P+0+B'::character varying
                            ELSE z_1.zone_name
                        END), (
                        CASE
                            WHEN z_1.zone_name::text = ANY (ARRAY['Praha'::character varying::text, '0'::text, '0+B'::character varying::text]) THEN '0'::character varying
                            ELSE z_1.zone_name
                        END)) z ON c.coupon_id = z.coupon_id) tab
  GROUP BY tab.created, tab.customer_profile_name, tab.tariff_profile_name, tab.zona, tab.zone_order
  ORDER BY tab.created DESC;

DROP VIEW analytic.v_ropidbi_coupon_channels_sale;

CREATE OR REPLACE VIEW analytic.v_ropidbi_coupon_channels_sale
AS WITH calendar AS (
         SELECT generate_series('2019-01-01 00:00:00+01'::timestamp with time zone, now()::date::timestamp with time zone - '1 day'::interval, '1 day'::interval)::date AS on_date
        ), channel AS (
         SELECT unnest(string_to_array('Mobilní aplikace,Přepážka,Eshop'::text, ','::text)) AS channel
        ), payment_method AS (
         SELECT unnest(string_to_array('Platební karta,Hotově,Převodem'::text, ','::text)) AS pmethod
        ), seller AS (
         SELECT unnest(string_to_array('OICT,DPP,Nezařazeno'::text, ','::text)) AS seller
        ), mos AS (
         SELECT c.created::date AS created,
                CASE
                    WHEN c.created_by_id::integer = 188 THEN 'Mobilní aplikace'::text
                    WHEN c.order_payment_type = ANY (ARRAY[1, 2]) THEN 'Eshop'::text
                    WHEN c.order_payment_type = ANY (ARRAY[3, 4]) THEN 'Přepážka'::text
                    ELSE 'Nezařazeno'::text
                END AS channel,
                CASE
                    WHEN c.order_status = 2 THEN 'Platební karta'::text
                    WHEN c.order_status = 5 THEN 'Hotově'::text
                    WHEN c.order_status = 3 THEN 'Převodem'::text
                    ELSE 'Nezařazeno'::text
                END AS payment_method,
                CASE
                    WHEN c.seller_id = 1 THEN 'OICT'::text
                    WHEN c.seller_id = 29 THEN 'DPP'::text
                    ELSE 'Nezařazeno'::text
                END AS seller,
            count(*) AS coupon_count,
            sum(c.price) AS sale
           FROM mos_be_coupons c
          GROUP BY (c.created::date), (
                CASE
                    WHEN c.created_by_id::integer = 188 THEN 'Mobilní aplikace'::text
                    WHEN c.order_payment_type = ANY (ARRAY[1, 2]) THEN 'Eshop'::text
                    WHEN c.order_payment_type = ANY (ARRAY[3, 4]) THEN 'Přepážka'::text
                    ELSE 'Nezařazeno'::text
                END), (
                CASE
                    WHEN c.order_status = 2 THEN 'Platební karta'::text
                    WHEN c.order_status = 5 THEN 'Hotově'::text
                    WHEN c.order_status = 3 THEN 'Převodem'::text
                    ELSE 'Nezařazeno'::text
                END), (
                CASE
                    WHEN c.seller_id = 1 THEN 'OICT'::text
                    WHEN c.seller_id = 29 THEN 'DPP'::text
                    ELSE 'Nezařazeno'::text
                END)
          ORDER BY (c.created::date) DESC
        )
 SELECT calendar.on_date AS created,
    channel.channel,
    payment_method.pmethod AS payment_method,
    seller.seller,
    COALESCE(mos.coupon_count, 0::bigint) AS coupon_count,
    COALESCE(mos.sale, 0::numeric) AS sale
   FROM calendar
     LEFT JOIN channel ON true
     LEFT JOIN payment_method ON true
     LEFT JOIN seller ON true
     LEFT JOIN mos ON calendar.on_date = mos.created AND mos.channel = channel.channel AND mos.payment_method = payment_method.pmethod AND mos.seller = seller.seller;

