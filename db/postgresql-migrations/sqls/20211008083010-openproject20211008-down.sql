/* Replace with your SQL commands */

drop VIEW analytic.v_openproject_mhmp_workpackages;
drop VIEW analytic.v_openproject_mhmp_topics;
drop VIEW analytic.v_openproject_mhmp_projects;
drop VIEW analytic.v_openproject_mhmp_domain;

drop TABLE python.openproject_mhmp_wps;
drop TABLE python.openproject_mhmp_projects;