DROP VIEW public.v_vehiclepositions_last_position_v1;
DROP VIEW public.v_vehiclepositions_last_position;
CREATE OR REPLACE VIEW public.v_vehiclepositions_last_position
as
SELECT DISTINCT ON (vehiclepositions_positions.trips_id) vehiclepositions_positions.created_at,
    vehiclepositions_positions.updated_at,
    vehiclepositions_positions.delay,
    vehiclepositions_positions.delay_stop_arrival,
    vehiclepositions_positions.delay_stop_departure,
    vehiclepositions_positions.next_stop_id,
    vehiclepositions_positions.shape_dist_traveled,
    vehiclepositions_positions.is_canceled,
    vehiclepositions_positions.lat,
    vehiclepositions_positions.lng,
    vehiclepositions_positions.origin_time,
    vehiclepositions_positions.origin_timestamp,
    vehiclepositions_positions.tracking,
    vehiclepositions_positions.trips_id,
    vehiclepositions_positions.create_batch_id,
    vehiclepositions_positions.created_by,
    vehiclepositions_positions.update_batch_id,
    vehiclepositions_positions.updated_by,
    vehiclepositions_positions.id,
    vehiclepositions_positions.bearing,
    vehiclepositions_positions.cis_last_stop_id,
    vehiclepositions_positions.cis_last_stop_sequence,
    vehiclepositions_positions.last_stop_id,
    vehiclepositions_positions.last_stop_sequence,
    vehiclepositions_positions.next_stop_sequence,
    vehiclepositions_positions.speed,
    vehiclepositions_positions.last_stop_arrival_time,
    vehiclepositions_positions.last_stop_departure_time,
    vehiclepositions_positions.next_stop_arrival_time,
    vehiclepositions_positions.next_stop_departure_time,
    vehiclepositions_positions.asw_last_stop_id,
    vehiclepositions_trips.gtfs_route_id,
    vehiclepositions_trips.gtfs_route_short_name,
    vehiclepositions_trips.gtfs_trip_id,
    vehiclepositions_trips.gtfs_trip_headsign,
    vehiclepositions_trips.vehicle_type_id,
    vehiclepositions_trips.wheelchair_accessible
   FROM vehiclepositions_positions vehiclepositions_positions
     JOIN vehiclepositions_trips vehiclepositions_trips ON vehiclepositions_trips.id::text = vehiclepositions_positions.trips_id::text AND vehiclepositions_trips.gtfs_trip_id IS NOT NULL
  WHERE vehiclepositions_positions.updated_at > (now() - '00:10:00'::interval) AND (
      (vehiclepositions_positions.tracking != 2 AND vehiclepositions_positions.shape_dist_traveled IS NOT NULL) OR
      (vehiclepositions_positions.tracking = 2 AND vehiclepositions_positions.delay IS NOT NULL)
    )
ORDER BY vehiclepositions_positions.trips_id, vehiclepositions_positions.updated_at DESC;

-- public.v_vehiclepositions_last_position_v1 source

CREATE OR REPLACE VIEW public.v_vehiclepositions_last_position_v1
AS SELECT v_vehiclepositions_last_position_v1.created_at,
    v_vehiclepositions_last_position_v1.delay,
    v_vehiclepositions_last_position_v1.delay_stop_arrival,
    v_vehiclepositions_last_position_v1.delay_stop_departure,
    v_vehiclepositions_last_position_v1.next_stop_id AS gtfs_next_stop_id,
    v_vehiclepositions_last_position_v1.shape_dist_traveled AS gtfs_shape_dist_traveled,
    v_vehiclepositions_last_position_v1.is_canceled,
    v_vehiclepositions_last_position_v1.lat,
    v_vehiclepositions_last_position_v1.lng,
    v_vehiclepositions_last_position_v1.origin_time,
    v_vehiclepositions_last_position_v1.origin_timestamp,
    v_vehiclepositions_last_position_v1.tracking,
    v_vehiclepositions_last_position_v1.trips_id,
    v_vehiclepositions_last_position_v1.create_batch_id,
    v_vehiclepositions_last_position_v1.created_by,
    v_vehiclepositions_last_position_v1.update_batch_id,
    v_vehiclepositions_last_position_v1.updated_at,
    v_vehiclepositions_last_position_v1.updated_by,
    v_vehiclepositions_last_position_v1.id,
    v_vehiclepositions_last_position_v1.bearing,
    v_vehiclepositions_last_position_v1.cis_last_stop_id,
    v_vehiclepositions_last_position_v1.cis_last_stop_sequence,
    v_vehiclepositions_last_position_v1.last_stop_id AS gtfs_last_stop_id,
    v_vehiclepositions_last_position_v1.last_stop_sequence AS gtfs_last_stop_sequence,
    v_vehiclepositions_last_position_v1.next_stop_sequence AS gtfs_next_stop_sequence,
    v_vehiclepositions_last_position_v1.speed
FROM v_vehiclepositions_last_position v_vehiclepositions_last_position_v1;