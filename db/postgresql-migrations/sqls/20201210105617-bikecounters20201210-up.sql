create or replace view analytic.v_camea_bikecounters_api_availability as
with united_table as (
				select id,
						issue,
						case when error_code = 200 then 222
						else error_code end as error_code,
						measured_at
				from bicyclecounters_api_logs_failures
				union all
				select id,
						'none' as issue,
						200 as error_code,
						measured_at
				from bicyclecounters_api_logs_hits
			),
	fail_steps as (
				select id, measured_at, error_code, issue,
					case when error_code <> 200 and lag(error_code, 1) over (order by measured_at asc) = 200 then 'begin'
						when error_code <> 200 and lead(error_code, 1) over (order by measured_at asc)  = 200 then 'end'
						when error_code <> 200 then 'body'
						else 'ok' end as status
				from united_table
			),
	fail_duration as (
				select *,
					case when status = 'begin' and lead(status, 1) over (order by measured_at asc) = 'end' then lead(measured_at, 1) over (order by measured_at asc)
						when status = 'begin' and lead(status, 1) over (order by measured_at asc) = 'begin' then measured_at + interval '1 min'
						else null end as end_time
				from fail_steps
				where status in ('begin', 'end')
			) 
			select measured_at as start_time,
					end_time,
					case when ((end_time - measured_at)::interval)::text like '%days%' then replace(((end_time - measured_at)::interval)::text, 'days', 'dny')
					else replace(((end_time - measured_at)::interval)::text, 'day', 'den')
					end as duration,
					error_code, 
					issue
			from fail_duration
			where status = 'begin'
			order by measured_at asc;
				
				
create or replace view analytic.v_camea_bikecounters_api_availability_day as
with united_table as (
				select id,
						issue,
						case when error_code = 200 then 222
						else error_code end as error_code,
						measured_at
				from bicyclecounters_api_logs_failures
				union all
				select id,
						'none' as issue,
						200 as error_code,
						measured_at
				from bicyclecounters_api_logs_hits
			),
	fail_steps as (
				select measured_at, error_code, issue,
					case when error_code <> 200 and lag(error_code, 1) over (order by measured_at asc) = 200 then 'begin'
						when error_code <> 200 and measured_at::date - lag(measured_at::date, 1) over (order by measured_at) <> 0 then 'begin_midnight'
						when error_code <> 200 and lead(error_code, 1) over (order by measured_at asc)  = 200 then 'end'
						when error_code <> 200 and measured_at::date - lead(measured_at::date, 1) over (order by measured_at) <> 0 then 'end_midnight'
						when error_code <> 200 then 'body'
						else 'ok' end as status
				from united_table
			),
	fail_start_end as (
				select 
					case when status = 'begin_midnight' then date_trunc('day', measured_at)
							else measured_at end as start_time,
					case when status like 'begin%' and lead(status, 1) over (order by measured_at asc) = 'end_midnight' then date_trunc('day', measured_at) + interval '23:59:59'
						when status like 'begin%' and lead(status, 1) over (order by measured_at asc) = 'end' then lead(measured_at, 1) over (order by measured_at)
						when status like 'begin%' and lead(status, 1) over (order by measured_at asc) = 'begin' then measured_at + interval '1 min'
						else null end as end_time,
					error_code, 
					issue, status
				from fail_steps
				where status in ('begin', 'begin_midnight', 'end%', 'end_midnight')
			),
		fail_duration as (
				select start_time,
						end_time,
						(end_time - start_time)::interval + interval '1sec' as duration,
						error_code, 
						issue
					from fail_start_end
					where status like 'begin%'
				),
	fail_duration_daily as (
			select date_trunc('day', start_time) as date,
					min(error_code) as error_code,
					max(issue) as issue,
					'24:00:00.0000'::interval - sum(duration)::interval as run,
					sum(duration)::interval as error
			from fail_duration
			group by date_trunc('day', start_time)
			),
	calendar as (select generate_series(min (measured_at::date), 
										max (measured_at::date), 
										'1 day') as measured_at
			from united_table),
	fail_duration_daily_complete as (
			select c.measured_at as measured_at,
					coalesce(fdd.run, interval '24:00:00') as run,
					coalesce(fdd.error, interval '00:00:00') as error
			from calendar as c
			left join fail_duration_daily as fdd on c.measured_at = fdd.date
	)select *,
				extract(epoch from run) / 86400 as run_ratio,
				extract(epoch from error) / 86400 as error_ratio
			from fail_duration_daily_complete;

