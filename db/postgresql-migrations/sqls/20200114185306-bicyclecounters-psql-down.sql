DROP TABLE IF EXISTS public.bicyclecounters_locations;

DROP TABLE IF EXISTS  public.bicyclecounters_directions;

--DROP INDEX bicyclecounters_directions_locations_id;

DROP TABLE IF EXISTS public.bicyclecounters_detections;

--DROP INDEX bicyclecounters_detections_locations_id;
--DROP INDEX bicyclecounters_detections_directions_id;

DROP TABLE IF EXISTS public.bicyclecounters_temperatures;

--DROP INDEX bicyclecounters_temperatures_locations_id;
