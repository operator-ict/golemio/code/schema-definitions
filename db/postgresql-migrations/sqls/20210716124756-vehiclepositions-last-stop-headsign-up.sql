ALTER TABLE public.vehiclepositions_positions ADD COLUMN last_stop_headsign VARCHAR(255);

DROP VIEW public.v_ropidgtfs_trips_stop_times_view;

CREATE OR REPLACE VIEW public.v_ropidgtfs_trips_stop_times_view AS
 SELECT ropidgtfs_trips.bikes_allowed,
    ropidgtfs_trips.block_id,
    ropidgtfs_trips.direction_id,
    ropidgtfs_trips.exceptional,
    ropidgtfs_trips.route_id,
    ropidgtfs_trips.service_id,
    ropidgtfs_trips.shape_id,
    ropidgtfs_trips.trip_headsign,
    ropidgtfs_trips.trip_id,
    ropidgtfs_trips.wheelchair_accessible,
    stop_times.arrival_time AS stop_times_arrival_time,
    stop_times.arrival_time_seconds AS stop_times_arrival_time_seconds,
    stop_times.departure_time AS stop_times_departure_time,
    stop_times.departure_time_seconds AS stop_times_departure_time_seconds,
    stop_times.shape_dist_traveled AS stop_times_shape_dist_traveled,
    stop_times.stop_id AS stop_times_stop_id,
    stop_times.stop_sequence AS stop_times_stop_sequence,
    stop_times.stop_headsign AS stop_times_stop_headsign, -- Add stop_headsign
    stop_times.trip_id AS stop_times_trip_id,
    "stop_times->stop".stop_id AS stop_times_stop_stop_id,
    "stop_times->stop".stop_lat AS stop_times_stop_stop_lat,
    "stop_times->stop".stop_lon AS stop_times_stop_stop_lon
   FROM ropidgtfs_trips ropidgtfs_trips
     LEFT JOIN ropidgtfs_stop_times stop_times ON ropidgtfs_trips.trip_id::text = stop_times.trip_id::text
     LEFT JOIN ropidgtfs_stops "stop_times->stop" ON stop_times.stop_id::text = "stop_times->stop".stop_id::text
  ORDER BY stop_times.stop_sequence;
