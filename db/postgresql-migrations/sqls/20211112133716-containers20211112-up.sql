drop VIEW public.v_containers_measurement_export;
CREATE VIEW public.v_containers_measurement_export
AS SELECT cc.container_id,
    cc.address,
    cc.district_cz,
    cc.trash_type,
    cm.measured_at::date as date,
    cm.measured_at::time as time,
    cm.temperature,
    cm.percent_calculated
   FROM containers_measurement cm
     JOIN analytic.v_containers_containers cc ON cc.container_id::text = cm.container_code::text;

-- public.v_containers_picks_export source

drop VIEW public.v_containers_picks_export;
CREATE VIEW public.v_containers_picks_export
AS SELECT DISTINCT cc.code,
    cs.address,
    initcap(replace(cs.district,'-',' ')) as district_cz,
    cc.trash_type,
    cp.pick_at::date as date,
    cp.pick_at::time as time,
    cp.percent_before,
    cp.percent_now,
    cp.event_driven,
    cm.temperature
   FROM containers_picks cp
     JOIN containers_containers cc ON cc.code::text = cp.container_code::text
     LEFT JOIN containers_stations cs ON cs.code::text = cc.station_code::text
     left join containers_measurement cm on cc.code::text = cm.container_code and cp.pick_at = cm.measured_at
  where cp.pick_at >= '2019-04-15 03:06:15.000' ;
