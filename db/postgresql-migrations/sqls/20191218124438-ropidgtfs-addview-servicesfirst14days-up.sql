-- [CS]
-- 
-- v_ropidgtfs_services_first14days je view, které je seznamem všech aktivních `gtfs_calendar.services_id` (`ropidgtfs_calendar.services_id`), k daným datům pro nadcházejících 14 dní
-- 
-- POSTUP
-- - vybere se nejstarší datum  z `ropidgtfs_calendar.start_date` - neboli první den plánu jízdních řádů
-- - od toho datumu se vygeneruje řada datumů až do plus 14 dní
-- - JOIN této řady datumů a pouze těch `ropidgtfs_calendar.services_id`, které jsou daný datum platné
-- - zároveň se navíc přidají nebo odeberou `ropidgtfs_calendar.services_id` pro dané dny dle tabulky výjimek `ropidgtfs_calendar_dates.services_id`
-- - výsledkem je seznam datumů od začátku platnosti JŘ až po plus 14 dní s přidruženými `ropidgtfs_calendar.services_id`, které jsou daný den v platnosti


-- [EN]
-- 
-- servicesfirst14days is a view, that is a list of all active `gtfs_calendar.services_id` (`ropidgtfs_calendar.services_id`), on the given dates for the next 14 days
-- 
-- METHOD
-- - selects the oldest date from `ropidgtfs_calendar.start_date` - in fact it is the first day of the timetable
-- - from that date will be generated series of dates up to plus 14 days 
-- - JOIN this date series and only those `ropidgtfs_calendar.services_id` that are valid on the given date
-- - add or remove `ropidgtfs_calendar.services_id` for given days according to the `ropidgtfs_calendar_dates.services_id` exceptions table
-- - the results is a list of dates from the beginning of the timetamble to plus 14 days with associated `ropidgtfs_calendar.services_id` that are planned on that day

CREATE MATERIALIZED VIEW "v_ropidgtfs_services_first14days" AS
SELECT dates.date,
    (date_part('day'::text, ((dates.date)::timestamp without time zone - (to_char(timezone('Europe/Prague'::text, now()), 'YYYY-MM-DD'::text))::timestamp without time zone)))::integer AS day_diff,
    ropidgtfs_calendar.service_id
   FROM (( SELECT (date_trunc('day'::text, dd.dd))::date AS date
           FROM generate_series( ( SELECT ( CURRENT_TIMESTAMP - interval '1 day' ))::date,
                ( SELECT ( CURRENT_TIMESTAMP + interval '12 day' ))::date, '1 day'::interval) dd(dd)) dates
     JOIN ropidgtfs_calendar ON ((1 = 1)))
  WHERE (((dates.date >= (ropidgtfs_calendar.start_date)::date) AND (dates.date <= (ropidgtfs_calendar.end_date)::date) AND (((btrim(to_char((dates.date)::timestamp with time zone, 'day'::text)) = 'monday'::text) AND (ropidgtfs_calendar.monday = 1)) OR ((btrim(to_char((dates.date)::timestamp with time zone, 'day'::text)) = 'tuesday'::text) AND (ropidgtfs_calendar.tuesday = 1)) OR ((btrim(to_char((dates.date)::timestamp with time zone, 'day'::text)) = 'wednesday'::text) AND (ropidgtfs_calendar.wednesday = 1)) OR ((btrim(to_char((dates.date)::timestamp with time zone, 'day'::text)) = 'thursday'::text) AND (ropidgtfs_calendar.thursday = 1)) OR ((btrim(to_char((dates.date)::timestamp with time zone, 'day'::text)) = 'friday'::text) AND (ropidgtfs_calendar.friday = 1)) OR ((btrim(to_char((dates.date)::timestamp with time zone, 'day'::text)) = 'saturday'::text) AND (ropidgtfs_calendar.saturday = 1)) OR ((btrim(to_char((dates.date)::timestamp with time zone, 'day'::text)) = 'sunday'::text) AND (ropidgtfs_calendar.sunday = 1))) AND (NOT ((ropidgtfs_calendar.service_id)::text IN ( SELECT ropidgtfs_calendar_dates.service_id
           FROM ropidgtfs_calendar_dates
          WHERE ((ropidgtfs_calendar_dates.exception_type = 2) AND (dates.date = (ropidgtfs_calendar_dates.date)::date)))))) OR ((ropidgtfs_calendar.service_id)::text IN ( SELECT ropidgtfs_calendar_dates.service_id
           FROM ropidgtfs_calendar_dates
          WHERE ((ropidgtfs_calendar_dates.exception_type = 1) AND (dates.date = (ropidgtfs_calendar_dates.date)::date)))))
  ORDER BY dates.date;

REFRESH MATERIALIZED VIEW "v_ropidgtfs_services_first14days"