-- public.v_lkpr_export source

CREATE OR REPLACE VIEW public.v_lkpr_export
AS SELECT COALESCE(vrd.id, wrl.route_id) AS route_id,
    vrd.name,
    vrd.route_name,
    vrd.from_name,
    vrd.to_name,
    vrd.route_num,
    vrd.direction,
    vrd.order_idx,
    to_timestamp((wrl.update_time / 1000)::double precision) AS update_time,
    wrl."time" AS actual_time,
    round(wrl.length::numeric * 3.6 / wrl."time"::numeric) AS actual_speed,
    wrl.historic_time,
    round(wrl.length::numeric * 3.6 / wrl.historic_time::numeric) AS historic_speed,
    wrl.length
   FROM analytic.v_lkpr_route_details vrd
     FULL JOIN wazett_route_lives wrl ON vrd.id = wrl.route_id
  WHERE (vrd.name ~~ 'TN-%'::text OR vrd.route_code ~~ 'TN-%'::text) AND to_timestamp((wrl.update_time / 1000)::double precision) >= (CURRENT_DATE - '1 day'::interval);