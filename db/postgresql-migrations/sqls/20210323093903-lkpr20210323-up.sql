/* Replace with your SQL commands */
-- analytic.v_lkpr_route_details source

CREATE OR REPLACE VIEW analytic.v_lkpr_route_details
AS SELECT re.id,
    re.name,
    re.route_code,
    re.route_name,
    re.from_name,
    re.to_name,
    re.route_type,
    re.route_num,
    re.route_variant,
        CASE
            WHEN re.route_variant = 'A1'::text THEN 'Na letiště'::text
            WHEN re.route_variant = 'B1'::text THEN 'Z letiště'::text
            ELSE 'Ostatní'::text
        END AS direction,
    drn.route_group_name,
    drn.order_idx
   FROM ( SELECT rc.id,
            rc.name,
            rc.route_code,
            rc.route_name,
            rc.from_name,
            rc.to_name,
            split_part(rc.route_code, '-'::text, 1) AS route_type,
            split_part(rc.route_code, '-'::text, 2) AS route_num,
            split_part(rc.route_code, '-'::text, 3) AS route_variant
           FROM ( SELECT r.id,
                    r.name,
                    split_part(r.name, ' '::text, 1) AS route_code,
                    "substring"(r.name, "position"(r.name, ' '::text) + 1) AS route_name,
                    r.from_name,
                    r.to_name
                   FROM wazett_routes r
				   where r.feed_id = 1 -- přidáno 23.3.2021
				   ) rc) re
     LEFT JOIN analytic.lkpr_dashboard_route_names drn ON re.route_type = drn.route_type AND re.route_num = drn.route_num
  ORDER BY drn.order_idx;