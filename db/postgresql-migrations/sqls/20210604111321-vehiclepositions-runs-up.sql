CREATE TABLE public.vehiclepositions_runs
(
    id varchar(50) NOT NULL,
    route_id VARCHAR(50) NOT NULL,
    run_number VARCHAR(50) NOT NULL,
    line_short_name VARCHAR(50),
    registration_number VARCHAR(50) NOT NULL,
    msg_start_timestamp TIMESTAMP WITH TIME ZONE NOT NULL,
    -- audit fields
    create_batch_id BIGINT NULL,
    created_at TIMESTAMP WITH TIME ZONE NULL,
    created_by VARCHAR(150) NULL,
    update_batch_id BIGINT NULL,
    updated_at TIMESTAMP WITH TIME ZONE NULL,
    updated_by VARCHAR(150) NULL,
    CONSTRAINT "vehiclepositions_runs_pkey" PRIMARY KEY (id)
);

CREATE INDEX vehiclepositions_runs_idx
    ON public.vehiclepositions_runs
    USING btree (route_id, run_number, registration_number, msg_start_timestamp);

CREATE TABLE public.vehiclepositions_runs_messages
(
    id BIGSERIAL NOT NULL,
    runs_id varchar(50) NOT NULL,
    lat DOUBLE PRECISION,
    lng DOUBLE PRECISION,
    actual_stop_aws_id VARCHAR(50),
    actual_stop_timestamp_real BIGINT,
    actual_stop_timestamp_scheduled BIGINT,
    last_stop_aws_id VARCHAR(50),
    pkt VARCHAR(50),
    tm TIMESTAMP WITH TIME ZONE,
    events VARCHAR(50),
    -- audit fields
    create_batch_id BIGINT NULL,
    created_at TIMESTAMP WITH TIME ZONE NULL,
    created_by VARCHAR(150) NULL,
    update_batch_id BIGINT NULL,
    updated_at TIMESTAMP WITH TIME ZONE NULL,
    updated_by VARCHAR(150) NULL,
    CONSTRAINT "vehiclepositions_runs_messages_pkey" PRIMARY KEY (id),
    CONSTRAINT "vehiclepositions_runs_messages_fkey" FOREIGN KEY ("runs_id")
        REFERENCES public.vehiclepositions_runs (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE CASCADE
);
