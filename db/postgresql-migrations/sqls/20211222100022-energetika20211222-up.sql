CREATE TABLE python.consumption_electricity_oict_api2 (
	timeutc text NULL,
	timelocal text NULL,
	value_txt text NULL,
	addr text NULL,
	meter text NULL,
	meternumber text NULL,
	var text NULL,
	type_txt text NULL,
	commodity text NULL,
	unit text NULL,
	ismanualentry bool NULL
);

-- siveko.v_consumption_electricity_oict_api source

CREATE OR REPLACE VIEW analytic.v_consumption_electricity_oict_api
AS SELECT date_trunc('day'::text, c.timeutc::date::timestamp with time zone) AS den,
    c.timeutc::timestamp with time zone AS time_utc,
    c.addr,
    c.unit,
    c.value_txt::integer::numeric * 1.04 AS value_num,
    row_number() OVER (PARTITION BY c.addr, (date_trunc('month'::text, c.timeutc::date::timestamp with time zone)) ORDER BY c.value_txt DESC) AS ran
   FROM python.consumption_electricity_oict_api2 c;