
DROP TABLE public.airqualitystations_component_types;

DROP TABLE public.airqualitystations_index_types;

DROP TABLE public.airqualitystations;

DROP TABLE public.airqualitystations_indexes;

DROP INDEX public.airqualitystations_measurements_component_code;
DROP INDEX public.airqualitystations_measurements_aggregation_interval;
DROP TABLE public.airqualitystations_measurements;