CREATE OR REPLACE VIEW analytic.v_public_tenders_oict_opendata
AS  WITH inner_table AS (
         SELECT public_tenders_oict.id_zakazky,
            public_tenders_oict.systemove_cislo_zakazky,
            public_tenders_oict.nazev_zakazky,
            public_tenders_oict.rezim,
            public_tenders_oict.faze_zadavaciho_rizeni,
            public_tenders_oict.datum_zahajeni,
            public_tenders_oict.predpokladana_hodnota,
            public_tenders_oict.druh_zakazky,
            public_tenders_oict.druh_zadavaciho_rizeni,
            public_tenders_oict.strucny_popis_predmetu,
            public_tenders_oict.lhuta_pro_nabidky,
            public_tenders_oict.dodavatel_nazev,
            public_tenders_oict.dodavatel_ico,
            public_tenders_oict.dodavatel_adresa,
            public_tenders_oict.pocet_obdrzenych_nabidek,
            public_tenders_oict.datum_uzavreni_smlouvy,
            public_tenders_oict.konecna_cena_bez_dph,
            public_tenders_oict.konecna_cena_s_dph,
            public_tenders_oict.ramcova_dohoda,
            public_tenders_oict.pocet_casti,
            public_tenders_oict.datum_zruseni,
            public_tenders_oict.updated_at,
            COALESCE(public_tenders_oict.pocet_casti, 0::bigint) AS pocet_casti_,
                CASE
                    WHEN public_tenders_oict.druh_zadavaciho_rizeni = 'VZMR - pouze zveřejnění v EZAK'::text OR public_tenders_oict.druh_zadavaciho_rizeni = 'VZMR s uveřejněním výzvy'::text OR public_tenders_oict.rezim = 'VZ malého rozsahu'::text THEN 'VZMR'::text
                    ELSE public_tenders_oict.druh_zadavaciho_rizeni
                END AS druh_zadavaciho_rizeni_
           FROM python.public_tenders_oict
          WHERE public_tenders_oict.rezim <> 'mimo režim ZZVZ'::text
        )
 SELECT inner_table.id_zakazky,
    inner_table.systemove_cislo_zakazky,
    inner_table.nazev_zakazky,
    inner_table.rezim,
    inner_table.faze_zadavaciho_rizeni,
    inner_table.datum_zahajeni,
    inner_table.predpokladana_hodnota,
    inner_table.druh_zakazky,
    inner_table.strucny_popis_predmetu,
    inner_table.lhuta_pro_nabidky,
    inner_table.dodavatel_nazev,
    inner_table.dodavatel_ico,
    inner_table.dodavatel_adresa,
    inner_table.pocet_obdrzenych_nabidek,
    inner_table.datum_uzavreni_smlouvy,
    inner_table.konecna_cena_bez_dph,
    inner_table.konecna_cena_s_dph,
    inner_table.ramcova_dohoda,
    inner_table.datum_zruseni,
    inner_table.updated_at,
    inner_table.pocet_casti_ AS pocet_casti,
    inner_table.druh_zadavaciho_rizeni_ AS druh_zadavaciho_rizeni
   FROM inner_table
  WHERE inner_table.pocet_casti_ < 1 AND inner_table.faze_zadavaciho_rizeni = 'Zadáno'::text;
