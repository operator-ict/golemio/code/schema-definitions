/* Replace with your SQL commands */
-- Odstranění metadat
delete from  meta."extract"
where id_dataset = (select id_dataset from meta.dataset where code_dataset = 'consumption_energy');

delete from meta.dataset
where code_dataset = 'consumption_energy';

delete from  meta."extract"
where id_dataset = (select id_dataset from meta.dataset where code_dataset = 'vpalac');

delete from meta.dataset
where code_dataset = 'vpalac';

-- pohledy
drop VIEW analytic.v_consumption_energy_outside_temperature;
drop VIEW analytic.v_consumption_energy_vrtbovsky_palac_last_update;
drop VIEW analytic.v_consumption_energy_missing_devices_last_update;
drop VIEW analytic.v_consumption_energy_last_update;
drop VIEW analytic.v_consumption_energy_alerts;
drop VIEW analytic.v_consumption_gas_reserve;
drop VIEW analytic.v_consumption_energy_reserve_day_gas;
drop view analytic.v_consumption_gas_day;

drop VIEW analytic.v_consumption_energy_change;
drop VIEW analytic.v_consumption_energy_consumption_month_v_f;
drop VIEW analytic.v_consumption_energy_consumption_month_t_f;
drop VIEW analytic.v_consumption_energy_consumption_month_p_f;
drop VIEW analytic.v_consumption_energy_consumption_month_ptv;
drop VIEW analytic.v_consumption_energy_consumption_month_el_f;
drop VIEW analytic.v_consumption_energy_deduct_month_ptv;
drop VIEW analytic.v_consumption_energy_devices_active;
drop VIEW analytic.v_consumption_energy_delta;
drop VIEW analytic.v_consumption_energy_avg_consumption_min;
drop VIEW analytic.v_consumption_energy_buildings_w_data;
drop VIEW analytic.v_consumption_energy_consumption;

drop VIEW analytic.v_consumption_energy_buildings_real_mc_nogeom;
drop VIEW analytic.v_consumption_energy_buildings_mimo_prahu;
drop VIEW analytic.v_consumption_energy_buildings_real_mc;
drop VIEW analytic.v_consumption_energy_buildings_real;

drop VIEW analytic.v_consumption_vrtbovsky_palac;

-- /pohledy



delete from  meta."extract"
where id_dataset = (select id_dataset from meta.dataset where code_dataset = 'common_tables')
and name_extract = 'citydistricts';

delete from meta.dataset
where code_dataset = 'common_tables'
and  NOT EXISTS(
	SELECT * FROM meta."extract" e 
	WHERE e.id_dataset = (select id_dataset from meta.dataset where code_dataset = 'common_tables')
); 
 
-- DROP TABLE citydistricts;

--drop extension PostGIS;

DROP TABLE public.consumption_energy_consumption;

DROP TABLE public.consumption_energy_devices;

DROP TABLE public.consumption_energy_buildings;

DROP TABLE analytic.consumption_gas_daily_reserves;

-- vpalac

DROP TABLE public.vpalac_measurement;
DROP TABLE public.vpalac_measuring_equipment;
DROP TABLE public.vpalac_meter_type;
DROP TABLE public.vpalac_type_measuring_equipment;
DROP TABLE public.vpalac_units;