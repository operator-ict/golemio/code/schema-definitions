ALTER TABLE public.parkings RENAME COLUMN category TO usage_scenario;
ALTER TABLE public.parkings ALTER COLUMN usage_scenario TYPE json USING to_json(usage_scenario);

ALTER TABLE public.parkings DROP COLUMN tariff_id;
ALTER TABLE public.parkings DROP COLUMN valid_from;
ALTER TABLE public.parkings DROP COLUMN valid_to;

DROP TABLE IF EXISTS public.parkings_tariffs;
DROP SEQUENCE public.parkings_tariffs_id_seq;
