ALTER TABLE public.parkings ADD parking_type varchar(100) NULL;
ALTER TABLE public.parkings ADD zone_type varchar(100) NULL;
ALTER TABLE public.parkings ADD centroid geometry NULL;
ALTER TABLE public.parkings ADD android_app_payment_url text NULL;
ALTER TABLE public.parkings ADD ios_app_payment_url text NULL;
ALTER TABLE public.parkings RENAME COLUMN payment_url TO web_app_payment_url;


ALTER TABLE public.parkings_tariffs ADD accepts_litacka bool NULL;


CREATE TABLE public.parkings_measurements_actual (
	"source" varchar(255) NOT NULL,
	source_id varchar(255) NOT NULL,
	available_spot_number int4 NULL,
	closed_spot_number int4 NULL,
	occupied_spot_number int4 NULL,
	total_spot_number int4 NULL,
	date_modified int8 NOT NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT parkings_measurements_actual_pk PRIMARY KEY (source, source_id)
);


