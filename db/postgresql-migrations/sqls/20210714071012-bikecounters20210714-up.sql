drop view if exists analytic.v_camea_bikecounters_api_availability;
drop view if exists analytic.v_camea_bikecounters_api_availability_day;

drop table if exists public.bicyclecounters_api_logs_failures;
drop table if exists public.bicyclecounters_api_logs_hits;