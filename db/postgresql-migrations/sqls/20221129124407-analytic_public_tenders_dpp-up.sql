CREATE OR REPLACE VIEW analytic.v_public_tenders_dpp
AS SELECT pt.id_zakazky,
    pt.nazev_zakazky,
    pt.organizacni_jednotka_spravy,
    pt.zadavatel_nazev,
    pt.varianta_druhu_rizeni,
    pt.druh_verejne_zakazky,
    pt.nazev_smluvniho_partnera,
    pt.ico_smluvniho_partnera,
    pt.zadavatel_ico,
    pt.datum_vytvoreni_zakazky,
        CASE
            WHEN pt.pocet_nabidek = 0 THEN 1::bigint
            ELSE pt.pocet_nabidek
        END AS pocet_nabidek,
    pt.datum_uzavreni_smlouvy,
    pt.smluvni_cena_s_dph_kc,
    pt.smluvni_cena_bez_dph_kc,
    pt.datum_zahajeni_zadavaciho_rizeni,
        CASE
            WHEN pt.pocet_nabidek = 1 OR pt.pocet_nabidek = 0 THEN '1 nabídka'::text
            WHEN pt.pocet_nabidek = ANY (ARRAY[2::bigint, 3::bigint, 4::bigint]) THEN concat(pt.pocet_nabidek, ' nabídky')
            WHEN pt.pocet_nabidek > 4 THEN concat(pt.pocet_nabidek, ' nabídek')
            ELSE 'Neuvedeno'::text
        END AS pocet_nabidek2,
    pt.updated_at,
    pt.created_at
   FROM python.public_tenders_dpp pt
  WHERE (pt.faze_zakazky = ANY (ARRAY['Zadána'::text])) AND pt.datum_vytvoreni_zakazky >= '2021-10-01 00:00:00'::timestamp without time zone AND (pt.datum_uzavreni_smlouvy < CURRENT_DATE OR pt.datum_uzavreni_smlouvy IS NULL) AND (pt.vysledkem_je_ramcova_dohoda = 'Ne'::text OR (pt.varianta_druhu_rizeni = ANY (ARRAY['Výzva k plnění rámcové smlouvy'::text, 'Minitendr'::text])));

CREATE OR REPLACE VIEW analytic.v_public_tenders_dpp_internal
AS SELECT pt.id_zakazky,
    pt.nazev_zakazky,
    pt.zadavatel_ico,
    pt.organizacni_jednotka_spravy,
    pt.varianta_druhu_rizeni,
    pt.druh_verejne_zakazky,
    pt.nazev_smluvniho_partnera,
    pt.ico_smluvniho_partnera,
        CASE
            WHEN pt.pocet_nabidek = 0 THEN 1::bigint
            ELSE pt.pocet_nabidek
        END AS pocet_nabidek,
    pt.smluvni_cena_s_dph_kc,
    pt.smluvni_cena_bez_dph_kc,
        CASE
            WHEN pt.pocet_nabidek = 1 OR pt.pocet_nabidek = 0 THEN '1 nabídka'::text
            WHEN pt.pocet_nabidek = ANY (ARRAY[2::bigint, 3::bigint, 4::bigint]) THEN concat(pt.pocet_nabidek, ' nabídky')
            WHEN pt.pocet_nabidek > 4 THEN concat(pt.pocet_nabidek, ' nabídek')
            ELSE 'Neuvedeno'::text
        END AS pocet_nabidek2,
    pt.celkova_predpokladana_hodnota_zakazky_bez_dph_kc,
    pt.smluvni_cena_bez_dph_kc - NULLIF(pt.celkova_predpokladana_hodnota_zakazky_bez_dph_kc, 0::double precision) AS rozdil_od_predp_hodnoty_kc,
    (pt.smluvni_cena_bez_dph_kc - pt.celkova_predpokladana_hodnota_zakazky_bez_dph_kc) / NULLIF(pt.celkova_predpokladana_hodnota_zakazky_bez_dph_kc, 0::double precision) AS rozdil_od_predp_hodnoty_procent,
    pt.datum_uzavreni_smlouvy,
    pt.datum_vytvoreni_zakazky,
    pt.lhuta_pro_podani_nabidek_na_profilu,
    pt.datum_zahajeni_zadavaciho_rizeni,
    pt.datum_uzavreni_smlouvy - pt.datum_zahajeni_zadavaciho_rizeni AS delka_zadavaciho_rizeni,
    pt.lhuta_pro_podani_nabidek_na_profilu - pt.datum_zahajeni_zadavaciho_rizeni AS delka_lhuty_pro_podani_nabidek,
        CASE
            WHEN pt.hodnotici_kriteria ~~ '%100%'::text THEN 'Pouze cena'::text
            ELSE 'Více kritérií'::text
        END AS hodnotici_kriteria_typ,
    pt.faze_zakazky,
    pt.updated_at,
    pt.created_at,
        CASE
            WHEN pt.datum_uzavreni_smlouvy < pt.datum_vytvoreni_zakazky THEN 'Uzavření smlouvy před datem vytvoření zakázky'::text
            WHEN pt.datum_uzavreni_smlouvy > pt.updated_at THEN 'Uzavření smlouvy v budoucnosti'::text
            ELSE 'Říjen 2021 - nyní'::text
        END AS uzavreni_smlouvy_filtr
   FROM python.public_tenders_dpp pt
  WHERE (pt.faze_zakazky = ANY (ARRAY['Zadána'::text, 'Zrušena'::text])) AND pt.datum_vytvoreni_zakazky >= '2021-10-01 01:00:00+02'::timestamp with time zone AND (pt.vysledkem_je_ramcova_dohoda = 'Ne'::text OR (pt.varianta_druhu_rizeni = ANY (ARRAY['Výzva k plnění rámcové smlouvy'::text, 'Minitendr'::text])));

CREATE OR REPLACE VIEW analytic.v_public_tenders_dpp_internal_frequent_suppliers
AS SELECT count(DISTINCT pt.id_zakazky) AS pocet_zakazek,
    pt.organizacni_jednotka_spravy,
    pt.varianta_druhu_rizeni,
    pt.datum_uzavreni_smlouvy,
    pt.datum_vytvoreni_zakazky,
    pt.faze_zakazky,
    pt.nazev_smluvniho_partnera,
    sum(
        CASE
            WHEN pt.spolecnosti_oslovene_k_podani_nabidky_nazev ~~ (('%'::text || s2.nazev_smluvniho_partnera) || '%'::text) THEN 1
            ELSE 0
        END) AS pocet_podanych_nabidek,
    sum(
        CASE
            WHEN s.spolecnosti_oslovene_k_podani_nabidky_nazev = s2.nazev_smluvniho_partnera OR s.spolecnosti_oslovene_k_podani_nabidky_nazev IS NULL THEN 1
            ELSE 0
        END) AS pocet_vyhranych_zakazek,
        CASE
            WHEN pt.datum_uzavreni_smlouvy < pt.datum_vytvoreni_zakazky THEN 'Uzavření smlouvy před datem vytvoření zakázky'::text
            WHEN pt.datum_uzavreni_smlouvy > pt.updated_at THEN 'Uzavření smlouvy v budoucnosti'::text
            ELSE 'Říjen 2021 - nyní'::text
        END AS uzavreni_smlouvy_filtr
   FROM python.public_tenders_dpp pt
     LEFT JOIN LATERAL unnest(string_to_array(pt.spolecnosti_oslovene_k_podani_nabidky_nazev, '; '::text)) s(spolecnosti_oslovene_k_podani_nabidky_nazev) ON true
     LEFT JOIN LATERAL unnest(string_to_array(pt.nazev_smluvniho_partnera, '; '::text)) s2(nazev_smluvniho_partnera) ON true
  WHERE (pt.faze_zakazky = ANY (ARRAY['Zadána'::text])) AND (pt.varianta_druhu_rizeni = ANY (ARRAY['Zakázka zadaná na základě výjimky'::text])) AND (pt.vysledkem_je_ramcova_dohoda = 'Ne'::text OR (pt.varianta_druhu_rizeni = ANY (ARRAY['Výzva k plnění rámcové smlouvy'::text, 'Minitendr'::text]))) AND pt.datum_vytvoreni_zakazky >= '2021-10-01 01:00:00+02'::timestamp with time zone
  GROUP BY pt.organizacni_jednotka_spravy, pt.nazev_smluvniho_partnera, pt.varianta_druhu_rizeni, pt.datum_uzavreni_smlouvy, pt.datum_vytvoreni_zakazky, pt.faze_zakazky, (
        CASE
            WHEN pt.datum_uzavreni_smlouvy < pt.datum_vytvoreni_zakazky THEN 'Uzavření smlouvy před datem vytvoření zakázky'::text
            WHEN pt.datum_uzavreni_smlouvy > pt.updated_at THEN 'Uzavření smlouvy v budoucnosti'::text
            ELSE 'Říjen 2021 - nyní'::text
        END);

CREATE OR REPLACE VIEW analytic.v_public_tenders_dpp_opendata
AS SELECT pt.id_zakazky,
    pt.id_casti_zakazky,
    pt.systemove_cislo_zakazky,
    pt.evidencni_cislo_zakazky,
    pt.cislo_vysledku_zadavaciho_rizeni,
    pt.interni_cisla_smluv_objednavek,
    pt.nazev_zakazky,
    pt.nazev_casti_zakazky,
    pt.druh_verejne_zakazky,
    pt.druh_zadavaciho_rizeni,
    pt.typ_zakazky,
    pt.jedna_se_o_cast_vz,
    pt.zakazka_zavadejici_dns,
    pt.faze_zakazky,
    pt.celkova_predpokladana_hodnota_zakazky_bez_dph_kc,
    pt.zakazka_souvisejici_s_vykonem_relevantni_cinnosti,
    pt.organizacni_jednotka_spravy,
    pt.datum_vytvoreni_zakazky,
    pt.datum_zahajeni_zadavaciho_rizeni,
    pt.lhuta_pro_podani_nabidek_na_profilu,
    pt.datum_otevirani_nabidek_na_profilu,
    pt.lhuta_pro_doruceni_zadosti_o_ucast_na_profilu,
    pt.hodnotici_kriteria,
    pt.vysledkem_je_ramcova_dohoda,
    pt.hodnoceni_formou_elektronicke_aukce_prostrednictvim_ta,
    pt.hodnoceni_formou_elektronicke_aukce_mimo_aplikaci_ta,
    pt.spolecnosti_oslovene_k_podani_nabidky_pocet,
    pt.spolecnosti_oslovene_k_podani_nabidky_nazev,
    pt.pocet_zadosti_o_ucast,
    pt.seznam_zadosti_o_ucast,
    pt.seznam_nabidek_a_nabidkova_cena_bez_dph,
    pt.ucastnik_zadavaciho_rizeni_nazev,
    pt.nazev_smluvniho_partnera,
    pt.ico_smluvniho_partnera,
    pt.smluvni_cena_bez_dph_kc,
    pt.smluvni_cena_s_dph_kc,
    pt.datum_uzavreni_smlouvy,
    pt.skutecne_uhrazena_cena_bez_dph,
    pt.skutecne_uhrazena_cena_s_dph,
        CASE
            WHEN pt.pocet_nabidek = 0 THEN 1::bigint
            ELSE pt.pocet_nabidek
        END AS pocet_nabidek,
        CASE
            WHEN pt.pocet_nabidek = 1 OR pt.pocet_nabidek = 0 THEN '1 nabídka'::text
            WHEN pt.pocet_nabidek = ANY (ARRAY[2::bigint, 3::bigint, 4::bigint]) THEN concat(pt.pocet_nabidek, ' nabídky')
            WHEN pt.pocet_nabidek > 4 THEN concat(pt.pocet_nabidek, ' nabídek')
            ELSE 'Neuvedeno'::text
        END AS pocet_nabidek2
   FROM python.public_tenders_dpp pt
  WHERE (pt.faze_zakazky = ANY (ARRAY['Zadána'::text])) AND pt.datum_vytvoreni_zakazky >= '2021-10-01 00:00:00'::timestamp without time zone AND (pt.datum_uzavreni_smlouvy < CURRENT_DATE OR pt.datum_uzavreni_smlouvy IS NULL) AND (pt.vysledkem_je_ramcova_dohoda = 'Ne'::text OR (pt.varianta_druhu_rizeni = ANY (ARRAY['Výzva k plnění rámcové smlouvy'::text, 'Minitendr'::text])));
