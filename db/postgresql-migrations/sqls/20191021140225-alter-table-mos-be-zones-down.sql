/* Replace with your SQL commands */
DROP TABLE public.mos_be_zones;

CREATE TABLE public.mos_be_zones (
	coupon_id int4 NOT NULL,
	zone_name varchar(255) NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT mos_be_zones_pkey PRIMARY KEY (coupon_id)
);
