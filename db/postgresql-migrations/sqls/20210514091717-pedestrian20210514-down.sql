drop VIEW analytic.v_pedestrians_locations_api;

drop TABLE analytic.pedestrians_locations_api;



CREATE TABLE analytic.pedestrians_locations_api (
    location_id serial NOT NULL,
    location_name varchar(250) NULL,
    lat numeric NULL,
    lng numeric NULL,
    address varchar(250) NULL,
    city_district varchar(250) NULL,
    tech varchar(250) NULL,
    map_picture varchar(2500) NULL,
    place_picture varchar(2500) NULL,
    measurement_start timestamptz NULL,
    measurement_end timestamptz NULL,
    cube_id varchar(250) null
	);
	
    
CREATE OR REPLACE VIEW analytic.v_pedestrians_locations_api
AS SELECT l.location_id::character varying(5) AS location_id,
    l.location_name,
    l.lat,
    l.lng,
    l.address,
    l.city_district,
    l.tech,
    l.map_picture,
    l.place_picture,
    l.measurement_start,
    l.measurement_end
   FROM analytic.pedestrians_locations_api l
  ORDER BY (l.location_id::character varying(5));
  

