drop view analytic.v_mos_coupons_all_channels;
drop view analytic.v_ropidbi_coupon_channels_sale;
drop view analytic.v_ropidbi_coupon_tokens;
drop view analytic.v_ropidbi_customer_age;
drop view analytic.v_ropidbi_device_counts;
drop view analytic.v_ropidbi_soubeh_jizdenek;
drop view analytic.v_ropidbi_ticket_activation_location;
drop view analytic.v_ropidbi_ticket_activation_times;
drop view analytic.v_ropidbi_ticket_activation_types;
drop view analytic.v_ropidbi_ticket_sales;
drop view analytic.v_ropidbi_ticket_with_rollingmean;
drop view analytic.v_ropidbi_ticket_zones;

DROP MATERIALIZED VIEW analytic.v_ropidbi_ticket;

drop view analytic.v_ropidbi_coupon_sales_monthly_by_zones;
drop view analytic.v_ropidbi_capping_agregation;
drop view analytic.v_ropidbi_capping_daily_counts;
drop view analytic.v_ropidbi_capping_distinct_accounts;
drop view analytic.v_ropidbi_capping_more_rides;
drop view analytic.v_ropidbi_ticket_sales_monthly_by_zones;

drop table public.mos_be_accounts;
drop table public.mos_be_coupons;
drop table public.mos_be_customers;
drop table public.mos_ma_devicemodels;
drop table public.mos_ma_ticketinspections;
drop table public.mos_ma_ticketpurchases;
drop table public.mos_be_tokens;
drop table public.mos_be_zones;
drop table public.mos_ma_ticketactivations;