
CREATE TABLE public.airbnb_apartment_count (
	name_mc varchar(500) NULL, -- nazev_mc
	apartment_fond numeric NULL, -- bytovy_fond
	valid_year date NULL
);

CREATE TABLE public.airbnb_listings (
	room_id varchar(500) NOT NULL,
	"name" varchar(500) NULL,
	propertytype varchar(500) NULL,
	roomtype varchar(500) NULL,
	bathrooms numeric NULL,
	bedrooms numeric NULL,
	capacity numeric NULL,
	cleaningfee numeric NULL,
	latitude varchar(500) NULL,
	longitude varchar(500) NULL,
	minnights numeric NULL,
	picturecount numeric NULL,
	overall_rating numeric NULL,
	user_id varchar(500) NULL,
	user_name varchar(500) NULL,
	user_superhost varchar(500) NULL,
	reviews_total varchar(500) NULL,
	room_created date NULL,
	room_lastupdate date NULL,
	create_batch_id int8 NULL,
	created_at timestamp NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamp NULL,
	updated_by varchar(150) NULL,
	geom geometry(POINT, 4326) NULL,
	CONSTRAINT airbnb_listings_pk PRIMARY KEY (room_id)
);

CREATE TABLE public.airbnb_occupancy (
	room_id varchar(500) NOT NULL,
	yearmon varchar(500) NOT NULL,
	days_occupied numeric NULL,
	occupancy_rate numeric NULL,
	mean_price numeric NULL,
	reviews_count numeric NULL,
	create_batch_id int8 NULL,
	created_at timestamp NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamp NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT airbnb_occupancy_pk PRIMARY KEY (room_id, yearmon)
);

-- views

CREATE OR REPLACE VIEW analytic.v_airbnb_dim_district
AS SELECT DISTINCT d.district_name::character varying(500) AS name_mc,
    ltrim("right"(d.district_name::text, 2))::numeric AS order_by
   FROM common.citydistricts d
  WHERE d.district_name::text <> ''::text AND (ltrim("right"(d.district_name::text, 2)) ~ '^[0-9]+$'::text) = true
UNION ALL
 SELECT d.district_name::character varying(500) AS name_mc,
    100 + row_number() OVER (ORDER BY d.district_name) AS order_by
   FROM ( SELECT DISTINCT citydistricts.district_name
           FROM common.citydistricts
          WHERE (ltrim("right"(citydistricts.district_name::text, 2)) ~ '^[0-9]+$'::text) = false AND (citydistricts.district_name::text <> ALL (ARRAY[''::character varying::text, 'name_mc'::character varying::text]))) d;
		  

CREATE OR REPLACE VIEW analytic.v_airbnb_listings
AS WITH last_activity AS (
         SELECT airbnb_occupancy.room_id,
            max(concat(airbnb_occupancy.yearmon, '-01')::date) AS last_activity
           FROM airbnb_occupancy
          WHERE airbnb_occupancy.reviews_count > 0::numeric
          GROUP BY airbnb_occupancy.room_id
        )
 SELECT l.room_id,
    l.name,
        CASE
            WHEN l.roomtype::text = 'Entire home/apt'::text THEN 'Celý dům/byt'::text
            WHEN l.roomtype::text = 'Private room'::text THEN 'Celý pokoj'::text
            WHEN l.roomtype::text = 'Shared room'::text THEN 'Sdílený pokoj'::text
            WHEN l.roomtype::text = 'Hotel room'::text THEN 'Hotelový pokoj'::text
            ELSE NULL::text
        END AS offer_type,
    l.capacity,
    l.cleaningfee,
    l.overall_rating,
    l.reviews_total,
    l.room_created,
    citydistricts.district_name::character varying(500) AS name_mc,
        CASE
            WHEN la.last_activity >= date_trunc('month'::text, now() - '3 mons'::interval)::date THEN 'Otevřená'::text
            ELSE 'Ukončená'::text
        END AS open_closed,
        CASE
            WHEN la.last_activity >= date_trunc('month'::text, now() - '3 mons'::interval)::date THEN NULL::date::timestamp without time zone
            WHEN la.last_activity IS NULL THEN l.room_created + '1 mon'::interval
            ELSE la.last_activity + '1 mon'::interval
        END::date AS ended_at,
    l.user_id,
        CASE
            WHEN l.longitude::text <> ''::text THEN l.longitude::numeric
            ELSE NULL::numeric
        END AS longitude,
        CASE
            WHEN l.latitude::text <> ''::text THEN l.latitude::numeric
            ELSE NULL::numeric
        END AS latitude,
    l.created_at AS record_created_at
   FROM airbnb_listings l
     JOIN common.citydistricts ON st_within(st_setsrid(st_point(l.longitude::double precision, l.latitude::double precision), 4326), citydistricts.geom)
     LEFT JOIN last_activity la ON l.room_id::text = la.room_id::text
  WHERE l.roomtype IS NOT NULL AND l.roomtype::text <> ''::text AND l.latitude::text <> ''::text AND l.longitude::text <> ''::text;
  

CREATE MATERIALIZED VIEW analytic.v_airbnb_occupancy_monthly_materialized
TABLESPACE pg_default
AS WITH constants AS (
         SELECT 0.55 AS reviewers_ratio,
            3.2 AS avg_accomodation_duration
        ), yearly_occupancy AS (
         SELECT airbnb_occupancy.room_id,
            "left"(airbnb_occupancy.yearmon::text, 4) AS o_year,
            (( SELECT constants.avg_accomodation_duration
                   FROM constants)) * sum(airbnb_occupancy.reviews_count) / (( SELECT constants.reviewers_ratio
                   FROM constants)) AS yearly_estimated_days_occupied
           FROM airbnb_occupancy
          GROUP BY airbnb_occupancy.room_id, ("left"(airbnb_occupancy.yearmon::text, 4))
        ), last_12m_occupancy AS (
         SELECT airbnb_occupancy.room_id,
            (( SELECT constants.avg_accomodation_duration
                   FROM constants)) * sum(airbnb_occupancy.reviews_count) / (( SELECT constants.reviewers_ratio
                   FROM constants)) AS yearly_estimated_days_occupied
           FROM airbnb_occupancy
          WHERE concat(airbnb_occupancy.yearmon, '-01')::date >= (date_trunc('month'::text, airbnb_occupancy.created_at) - '1 year'::interval) AND concat(airbnb_occupancy.yearmon, '-01')::date < date_trunc('month'::text, airbnb_occupancy.created_at)
          GROUP BY airbnb_occupancy.room_id
        )
 SELECT o.room_id,
    "left"(o.yearmon::text, 4) AS o_year,
    concat(o.yearmon, '-01')::date AS o_month,
    l.name_mc,
    l.offer_type,
    l.room_created,
    l.open_closed,
    l.ended_at,
    o.reviews_count / (( SELECT constants.reviewers_ratio
           FROM constants)) AS stays_count,
    (( SELECT constants.avg_accomodation_duration
           FROM constants)) * o.reviews_count / (( SELECT constants.reviewers_ratio
           FROM constants)) AS estimated_days_occupied,
    date_part('day'::text, concat(o.yearmon, '-01')::date + '1 mon'::interval - concat(o.yearmon, '-01')::date::timestamp without time zone) AS days_in_month,
    o.reviews_count,
    o.days_occupied,
    0::numeric AS esimated_price_per_night,
    o.mean_price,
        CASE
            WHEN yo.yearly_estimated_days_occupied > 60::numeric THEN 'Pravidelné'::text
            ELSE 'Příležitostní'::text
        END AS regularity2,
    l.user_id,
    bf.apartment_fond,
    l.ended_at AS ukonceno,
        CASE
            WHEN yo.yearly_estimated_days_occupied > 60::numeric AND l.offer_type = 'Celý dům/byt'::text THEN 'Pravidelné, celý byt/dům'::text
            ELSE 'Sdílená ekonomika'::text
        END AS offer_type_group,
        CASE
            WHEN concat(o.yearmon, '-01')::date >= (date_trunc('month'::text, o.created_at) - '1 year'::interval) AND concat(o.yearmon, '-01')::date < date_trunc('month'::text, o.created_at) THEN 'Y'::text
            ELSE 'N'::text
        END AS last_12m,
    l.latitude,
    l.longitude,
    o.created_at AS record_created_at,
        CASE
            WHEN concat(o.yearmon, '-01')::date >= (date_trunc('month'::text, o.created_at) - '1 year'::interval) AND concat(o.yearmon, '-01')::date < date_trunc('month'::text, o.created_at) THEN true
            ELSE false
        END AS last_12_months,
        CASE
            WHEN lo.yearly_estimated_days_occupied > 60::numeric THEN 'Pravidelné'::text
            ELSE 'Příležitostní'::text
        END AS regularity_last_12m,
        CASE
            WHEN lo.yearly_estimated_days_occupied > 60::numeric AND l.offer_type = 'Celý dům/byt'::text THEN 'Pravidelné, celý byt/dům'::text
            ELSE 'Sdílená ekonomika'::text
        END AS offer_type_group_last_12m
   FROM airbnb_occupancy o
     JOIN analytic.v_airbnb_listings l ON o.room_id::text = l.room_id::text
     LEFT JOIN yearly_occupancy yo ON yo.room_id::text = o.room_id::text AND yo.o_year = "left"(o.yearmon::text, 4)
     LEFT JOIN airbnb_apartment_count bf ON l.name_mc::text = bf.name_mc::text
     LEFT JOIN last_12m_occupancy lo ON lo.room_id::text = o.room_id::text
  WHERE concat(o.yearmon, '-01')::date >= l.room_created AND concat(o.yearmon, '-01')::date < date_trunc('month'::text, o.created_at) AND concat(o.yearmon, '-01')::date >= '2017-01-01'::date AND l.name_mc::text <> ''::text AND l.name_mc IS NOT NULL AND o.reviews_count > 0::numeric
WITH DATA;  

CREATE OR REPLACE VIEW analytic.v_airbnb_dim_offertype
AS SELECT DISTINCT v_airbnb_listings.offer_type
   FROM analytic.v_airbnb_listings
  WHERE v_airbnb_listings.offer_type <> ''::text AND v_airbnb_listings.offer_type IS NOT NULL;  
  

CREATE OR REPLACE VIEW analytic.v_airbnb_occupancy_monthly
AS SELECT v_airbnb_occupancy_monthly.room_id,
    v_airbnb_occupancy_monthly.o_year,
    v_airbnb_occupancy_monthly.o_month,
    v_airbnb_occupancy_monthly.name_mc,
    v_airbnb_occupancy_monthly.offer_type,
    v_airbnb_occupancy_monthly.room_created,
    v_airbnb_occupancy_monthly.open_closed,
    v_airbnb_occupancy_monthly.ended_at,
    v_airbnb_occupancy_monthly.stays_count,
    v_airbnb_occupancy_monthly.estimated_days_occupied,
    v_airbnb_occupancy_monthly.days_in_month,
    v_airbnb_occupancy_monthly.reviews_count,
    v_airbnb_occupancy_monthly.days_occupied,
    v_airbnb_occupancy_monthly.esimated_price_per_night,
    v_airbnb_occupancy_monthly.mean_price,
    v_airbnb_occupancy_monthly.regularity2,
    v_airbnb_occupancy_monthly.user_id,
    v_airbnb_occupancy_monthly.apartment_fond,
    v_airbnb_occupancy_monthly.ukonceno,
    v_airbnb_occupancy_monthly.offer_type_group,
    v_airbnb_occupancy_monthly.last_12m,
    v_airbnb_occupancy_monthly.latitude,
    v_airbnb_occupancy_monthly.longitude,
    v_airbnb_occupancy_monthly.record_created_at,
    v_airbnb_occupancy_monthly.last_12_months,
    v_airbnb_occupancy_monthly.regularity_last_12m,
    v_airbnb_occupancy_monthly.offer_type_group_last_12m
   FROM analytic.v_airbnb_occupancy_monthly_materialized v_airbnb_occupancy_monthly;  
   
CREATE OR REPLACE VIEW analytic.v_airbnb_dim_regularity
AS SELECT DISTINCT v_airbnb_occupancy_monthly.regularity2
   FROM analytic.v_airbnb_occupancy_monthly
  WHERE v_airbnb_occupancy_monthly.regularity2 <> ''::text AND v_airbnb_occupancy_monthly.regularity2 IS NOT NULL;   
 
CREATE OR REPLACE VIEW analytic.v_airbnb_districts_last12m
AS WITH districts_list AS (
         SELECT v_airbnb_occupancy_monthly.name_mc,
            count(DISTINCT v_airbnb_occupancy_monthly.room_id) AS active_count
           FROM analytic.v_airbnb_occupancy_monthly
          WHERE v_airbnb_occupancy_monthly.last_12_months = true AND v_airbnb_occupancy_monthly.reviews_count > 0::numeric
          GROUP BY v_airbnb_occupancy_monthly.name_mc
         HAVING count(DISTINCT v_airbnb_occupancy_monthly.room_id) >= 10
        )
 SELECT el.name_mc,
    'Ukončené nabídky'::text AS metric,
    count(el.room_id) AS metric_value
   FROM analytic.v_airbnb_listings el
     JOIN districts_list dl ON el.name_mc::text = dl.name_mc::text
  WHERE el.ended_at IS NOT NULL AND el.name_mc IS NOT NULL AND date_trunc('month'::text, el.ended_at + '3 mons'::interval) >= (date_trunc('month'::text, el.record_created_at) - '1 year'::interval) AND date_trunc('month'::text, el.ended_at + '3 mons'::interval) < date_trunc('month'::text, el.record_created_at)
  GROUP BY el.name_mc
UNION ALL
 SELECT nl.name_mc,
    'Nové nabídky'::text AS metric,
    count(nl.room_id) AS metric_value
   FROM analytic.v_airbnb_listings nl
     JOIN districts_list dl ON nl.name_mc::text = dl.name_mc::text
  WHERE nl.name_mc IS NOT NULL AND nl.room_created >= (date_trunc('month'::text, nl.record_created_at) - '1 year'::interval) AND nl.room_created < date_trunc('month'::text, nl.record_created_at)
  GROUP BY nl.name_mc
UNION ALL
 SELECT districts_list.name_mc,
    'Počet aktivních nabídek'::text AS metric,
    districts_list.active_count AS metric_value
   FROM districts_list
UNION ALL
 SELECT o1.name_mc,
    'Medián ceny za celý byt/dům'::text AS metric,
    round(percentile_disc(0.5::double precision) WITHIN GROUP (ORDER BY o1.mean_price)) AS metric_value
   FROM analytic.v_airbnb_occupancy_monthly o1
     JOIN districts_list dl ON o1.name_mc::text = dl.name_mc::text
  WHERE o1.last_12_months = true AND o1.name_mc IS NOT NULL AND o1.offer_type = 'Celý dům/byt'::text AND o1.mean_price < 5250::numeric AND o1.mean_price > 588::numeric AND o1.reviews_count > 0::numeric
  GROUP BY o1.name_mc
UNION ALL
 SELECT o2.name_mc,
    'Medián ceny za pokoj'::text AS metric,
    round(percentile_disc(0.5::double precision) WITHIN GROUP (ORDER BY o2.mean_price)) AS metric_value
   FROM analytic.v_airbnb_occupancy_monthly o2
     JOIN districts_list dl ON o2.name_mc::text = dl.name_mc::text
  WHERE o2.last_12_months = true AND o2.name_mc IS NOT NULL AND o2.offer_type <> 'Celý dům/byt'::text AND o2.mean_price < 5250::numeric AND o2.mean_price > 588::numeric AND o2.reviews_count > 0::numeric
  GROUP BY o2.name_mc
UNION ALL
 SELECT o3.name_mc,
    'Počet pobytů'::text AS metric,
    round(sum(o3.stays_count)) AS metric_value
   FROM analytic.v_airbnb_occupancy_monthly o3
     JOIN districts_list dl ON o3.name_mc::text = dl.name_mc::text
  WHERE o3.last_12_months = true AND o3.name_mc IS NOT NULL AND o3.reviews_count > 0::numeric
  GROUP BY o3.name_mc; 
  
CREATE OR REPLACE VIEW analytic.v_airbnb_overview_12m
AS WITH pravidelny AS (
         SELECT o.name_mc,
            count(DISTINCT o.room_id) AS pravidelny_count
           FROM analytic.v_airbnb_occupancy_monthly o
          WHERE o.offer_type_group_last_12m = 'Pravidelné, celý byt/dům'::text AND o.last_12_months = true
          GROUP BY o.name_mc
        ), prilezitostni AS (
         SELECT o.name_mc,
            count(DISTINCT o.room_id) AS prilezitostni_count
           FROM analytic.v_airbnb_occupancy_monthly o
          WHERE o.offer_type_group_last_12m = 'Sdílená ekonomika'::text AND o.last_12_months = true
          GROUP BY o.name_mc
        )
 SELECT bf.name_mc,
    pra.pravidelny_count,
    pri.prilezitostni_count,
    bf.apartment_fond,
    pra.pravidelny_count + pri.prilezitostni_count AS airbnb_rented,
    bf.apartment_fond - (pra.pravidelny_count + pri.prilezitostni_count)::numeric AS airbnb_not_rented,
    bf.apartment_fond - pra.pravidelny_count::numeric AS airbnb_not_rented_or_irregular
   FROM airbnb_apartment_count bf
     JOIN pravidelny pra ON bf.name_mc::text = pra.name_mc::text
     JOIN prilezitostni pri ON bf.name_mc::text = pri.name_mc::text;
	 
CREATE OR REPLACE VIEW analytic.v_airbnb_room_per_user
AS SELECT date_trunc('year'::text, v_airbnb_occupancy_monthly.o_month::timestamp with time zone) AS o_year,
    v_airbnb_occupancy_monthly.user_id,
    count(DISTINCT v_airbnb_occupancy_monthly.room_id) AS room_count
   FROM analytic.v_airbnb_occupancy_monthly
  WHERE v_airbnb_occupancy_monthly.user_id::text <> ''::text
  GROUP BY (date_trunc('year'::text, v_airbnb_occupancy_monthly.o_month::timestamp with time zone)), v_airbnb_occupancy_monthly.user_id;	 
  
CREATE OR REPLACE VIEW analytic.v_airbnb_start_end_monthly
AS WITH ended AS (
         SELECT date_trunc('month'::text, v_airbnb_listings.ended_at + '3 mons'::interval) AS ended_month,
            v_airbnb_listings.name_mc,
            v_airbnb_listings.offer_type,
            count(v_airbnb_listings.room_id) AS ended_count
           FROM analytic.v_airbnb_listings
          WHERE v_airbnb_listings.ended_at IS NOT NULL
          GROUP BY v_airbnb_listings.name_mc, v_airbnb_listings.offer_type, (date_trunc('month'::text, v_airbnb_listings.ended_at + '3 mons'::interval))
        )
 SELECT date_trunc('month'::text, created.room_created::timestamp with time zone) AS months,
    count(DISTINCT created.room_id) AS created_count,
    ended.ended_count,
    created.name_mc,
    created.offer_type,
        CASE
            WHEN date_trunc('month'::text, created.room_created::timestamp with time zone) >= (date_trunc('month'::text, created.record_created_at) - '1 year'::interval) THEN 'Y'::text
            ELSE 'N'::text
        END AS last_12m
   FROM analytic.v_airbnb_listings created
     LEFT JOIN ended ON ended.ended_month = date_trunc('month'::text, created.room_created::timestamp with time zone) AND ended.name_mc::text = created.name_mc::text AND ended.offer_type = created.offer_type
  WHERE created.room_created >= '2017-01-01'::date AND created.room_created < date_trunc('month'::text, created.record_created_at)
  GROUP BY created.name_mc, created.offer_type, (date_trunc('month'::text, created.room_created::timestamp with time zone)), ended.ended_count, (date_trunc('month'::text, created.record_created_at));  