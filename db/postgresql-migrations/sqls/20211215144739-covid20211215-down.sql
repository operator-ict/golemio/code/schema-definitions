-- analytic.v_uzis_covid19_tests_districts source

drop VIEW analytic.v_uzis_covid19_tests_districts;
CREATE OR REPLACE VIEW analytic.v_uzis_covid19_tests_districts
AS SELECT eo.datum,
    eo.kraj_kod,
    eo.kraj_nazev,
    eo.okres_kod,
    eo.okres_nazev,
    eo.pocet_pozitivnich_osob_den,
    t.prirustkovy_pocet_testu_okres,
    t.prirustkovy_pocet_prvnich_testu_okres,
    t.kumulativni_pocet_testu_okres,
    t.kumulativni_pocet_prvnich_testu_okres,
    t.prirustkovy_pocet_testu_okres - t.prirustkovy_pocet_prvnich_testu_okres AS prirustkovy_pocet_naslednych_testu
   FROM ( SELECT eo_1.datum,
            eo_1.kraj_kod,
            eo_1.kraj_nazev,
            eo_1.okres_kod,
            eo_1.okres_nazev,
            sum(eo_1.incidence) AS pocet_pozitivnich_osob_den
           FROM uzis.covid19_municipalities eo_1
          WHERE eo_1.datum < CURRENT_DATE
          GROUP BY eo_1.datum, eo_1.kraj_kod, eo_1.kraj_nazev, eo_1.okres_kod, eo_1.okres_nazev) eo
     JOIN uzis.covid19_cz_tests_regional t ON eo.datum::date = t.datum::date AND eo.kraj_kod = t.kraj_nuts_kod AND eo.okres_kod = t.okres_lau_kod;