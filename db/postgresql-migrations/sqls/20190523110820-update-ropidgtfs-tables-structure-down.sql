
ALTER TABLE ropidgtfs_agency DROP COLUMN agency_email;

ALTER TABLE ropidgtfs_stop_times DROP COLUMN timepoint;

ALTER TABLE ropidgtfs_stops DROP COLUMN level_id;
ALTER TABLE ropidgtfs_stops DROP COLUMN stop_code;
ALTER TABLE ropidgtfs_stops DROP COLUMN stop_desc;
ALTER TABLE ropidgtfs_stops DROP COLUMN stop_timezone;

ALTER TABLE ropidgtfs_trips DROP COLUMN trip_operation_type;
ALTER TABLE ropidgtfs_trips DROP COLUMN trip_short_name;
