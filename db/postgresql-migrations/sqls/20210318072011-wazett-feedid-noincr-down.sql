CREATE SEQUENCE public.wazett_feeds_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 2147483647
  START 1
  CACHE 1;

ALTER TABLE public.wazett_feeds ALTER COLUMN id SET DEFAULT nextval('public.wazett_feeds_id_seq');