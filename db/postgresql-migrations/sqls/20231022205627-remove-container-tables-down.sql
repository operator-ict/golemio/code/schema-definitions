CREATE TABLE IF NOT EXISTS public.containers_containers
(
    id character varying(50) COLLATE pg_catalog."default" NOT NULL,
    code character varying(50) COLLATE pg_catalog."default" NOT NULL,
    knsko_id integer,
    knsko_code character varying(50) COLLATE pg_catalog."default",
    station_code character varying(50) COLLATE pg_catalog."default" NOT NULL,
    total_volume integer,
    prediction timestamp with time zone,
    bin_type character varying(150) COLLATE pg_catalog."default",
    installed_at timestamp with time zone,
    network character varying(50) COLLATE pg_catalog."default",
    cleaning_frequency_interval bigint,
    cleaning_frequency_frequency bigint,
    company text COLLATE pg_catalog."default",
    container_type character varying(150) COLLATE pg_catalog."default",
    trash_type bigint,
    source character varying(10) COLLATE pg_catalog."default",
    create_batch_id bigint,
    created_at timestamp with time zone,
    created_by character varying(150) COLLATE pg_catalog."default",
    update_batch_id bigint,
    updated_at timestamp with time zone,
    updated_by character varying(150) COLLATE pg_catalog."default",
    sensor_id bigint,
    pick_days character varying(100) COLLATE pg_catalog."default",
    sensoneo_code character varying(50) COLLATE pg_catalog."default",
    CONSTRAINT containers_containers_pkey PRIMARY KEY (code)
);

CREATE SEQUENCE IF NOT EXISTS public.containers_ksnko_notifications_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1;

CREATE TABLE IF NOT EXISTS public.containers_ksnko_notifications
(
    id integer NOT NULL DEFAULT nextval('containers_ksnko_notifications_id_seq'::regclass),
    knsko_id bigint,
    sensor_id bigint,
    mode character varying(50) COLLATE pg_catalog."default",
    success boolean,
    request json,
    response json,
    create_batch_id bigint,
    created_at timestamp with time zone,
    created_by character varying(150) COLLATE pg_catalog."default",
    update_batch_id bigint,
    updated_at timestamp with time zone,
    updated_by character varying(150) COLLATE pg_catalog."default",
    CONSTRAINT containers_ksnko_notifications_pkey PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS public.containers_measurement
(
    container_id character varying(50) COLLATE pg_catalog."default" NOT NULL,
    container_code character varying(50) COLLATE pg_catalog."default" NOT NULL,
    station_code character varying(50) COLLATE pg_catalog."default" NOT NULL,
    percent_calculated integer,
    upturned integer,
    temperature integer,
    battery_status numeric,
    measured_at timestamp without time zone,
    measured_at_utc timestamp without time zone NOT NULL,
    prediction timestamp without time zone,
    prediction_utc timestamp without time zone,
    firealarm integer,
    create_batch_id bigint,
    created_at timestamp with time zone,
    created_by character varying(150) COLLATE pg_catalog."default",
    update_batch_id bigint,
    updated_at timestamp with time zone,
    updated_by character varying(150) COLLATE pg_catalog."default",
    CONSTRAINT containers_measuremnt_pkey PRIMARY KEY (container_code, measured_at_utc)
);

CREATE INDEX IF NOT EXISTS containers_measurement_measured_at_utc
    ON public.containers_measurement USING btree
    (measured_at_utc ASC NULLS LAST)
    TABLESPACE pg_default;

CREATE INDEX IF NOT EXISTS containers_measurement_station_code
    ON public.containers_measurement USING btree
    (station_code COLLATE pg_catalog."default" ASC NULLS LAST)
    TABLESPACE pg_default;

CREATE TABLE IF NOT EXISTS public.containers_opening_monitor
(
    id integer NOT NULL,
    container_code character varying(50) COLLATE pg_catalog."default" NOT NULL,
    opening_blocked character varying(20) COLLATE pg_catalog."default",
    percent_calculated integer,
    server_at timestamp without time zone NOT NULL,
    last_modify timestamp without time zone,
    create_batch_id bigint,
    created_at timestamp with time zone,
    created_by character varying(150) COLLATE pg_catalog."default",
    update_batch_id bigint,
    updated_at timestamp with time zone,
    updated_by character varying(150) COLLATE pg_catalog."default",
    CONSTRAINT containers_opening_monitor_pkey PRIMARY KEY (container_code, server_at)
);

CREATE TABLE IF NOT EXISTS public.containers_picks
(
    container_id character varying(50) COLLATE pg_catalog."default" NOT NULL,
    container_code character varying(50) COLLATE pg_catalog."default" NOT NULL,
    station_code character varying(50) COLLATE pg_catalog."default" NOT NULL,
    pick_at timestamp without time zone,
    pick_at_utc timestamp without time zone NOT NULL,
    percent_before integer,
    percent_now integer,
    event_driven boolean,
    decrease integer,
    pick_minfilllevel integer,
    create_batch_id bigint,
    created_at timestamp with time zone,
    created_by character varying(150) COLLATE pg_catalog."default",
    update_batch_id bigint,
    updated_at timestamp with time zone,
    updated_by character varying(150) COLLATE pg_catalog."default",
    CONSTRAINT containers_picks_pkey PRIMARY KEY (container_code, pick_at_utc)
);

CREATE INDEX IF NOT EXISTS containers_picks_container_id
    ON public.containers_picks USING btree
    (container_id COLLATE pg_catalog."default" ASC NULLS LAST)
    TABLESPACE pg_default;
-- Index: containers_picks_pick_at_utc

-- DROP INDEX IF EXISTS public.containers_picks_pick_at_utc;

CREATE INDEX IF NOT EXISTS containers_picks_pick_at_utc
    ON public.containers_picks USING btree
    (pick_at_utc ASC NULLS LAST)
    TABLESPACE pg_default;
-- Index: containers_picks_station_code

-- DROP INDEX IF EXISTS public.containers_picks_station_code;

CREATE INDEX IF NOT EXISTS containers_picks_station_code
    ON public.containers_picks USING btree
    (station_code COLLATE pg_catalog."default" ASC NULLS LAST)
    TABLESPACE pg_default;

    CREATE TABLE IF NOT EXISTS public.containers_picks_dates
(
    container_id character varying(50) COLLATE pg_catalog."default" NOT NULL,
    pick_date timestamp with time zone NOT NULL,
    create_batch_id bigint,
    created_at timestamp with time zone,
    created_by character varying(150) COLLATE pg_catalog."default",
    update_batch_id bigint,
    updated_at timestamp with time zone,
    updated_by character varying(150) COLLATE pg_catalog."default",
    CONSTRAINT containers_picks_dates_pkey PRIMARY KEY (container_id, pick_date)
);

CREATE TABLE IF NOT EXISTS public.containers_scenar_items
(
    container_code character varying(50) COLLATE pg_catalog."default",
    code character varying(100) COLLATE pg_catalog."default",
    "interval" character varying(250) COLLATE pg_catalog."default",
    pocatek integer,
    konec integer,
    s_code character varying(50) COLLATE pg_catalog."default",
    netw_code character varying(50) COLLATE pg_catalog."default",
    device character varying(50) COLLATE pg_catalog."default",
    pocet_mereni numeric(18,0)
);

CREATE TABLE IF NOT EXISTS public.containers_stations
(
    id character varying(50) COLLATE pg_catalog."default" NOT NULL,
    code character varying(50) COLLATE pg_catalog."default" NOT NULL,
    knsko_id integer,
    accessibility numeric,
    latitude numeric,
    longitude numeric,
    address character varying(250) COLLATE pg_catalog."default",
    district character varying(50) COLLATE pg_catalog."default",
    district_code numeric,
    source character varying(10) COLLATE pg_catalog."default",
    last_modify timestamp without time zone,
    create_batch_id bigint,
    created_at timestamp with time zone,
    created_by character varying(150) COLLATE pg_catalog."default",
    update_batch_id bigint,
    updated_at timestamp with time zone,
    updated_by character varying(150) COLLATE pg_catalog."default",
    CONSTRAINT containers_containers_stations PRIMARY KEY (code)
);

CREATE INDEX IF NOT EXISTS containers_stations_id
    ON public.containers_stations USING btree
    (id COLLATE pg_catalog."default" ASC NULLS LAST)
    TABLESPACE pg_default;

CREATE INDEX if not exists containers_containers_trash_type
ON public.containers_containers 
USING hash (trash_type);

CREATE INDEX if not exists containers_containers_station_code
ON public.containers_containers 
USING hash (station_code);
