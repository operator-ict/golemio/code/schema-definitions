alter table analytic.pid_price_list add primary key (tarif, cislo_tarifu_cptp, platnost_min, cena_s_dph, nabizeno_od);

drop view analytic.v_ropidbi_ticket_sales;
drop view analytic.v_ropidbi_ticket_with_rollingmean;
drop view analytic.v_ropidbi_ticket_sales_monthly_by_zones;
drop view analytic.v_ropidbi_ticket_activation_types;
drop view analytic.v_ropidbi_ticket_activation_times;


alter table analytic.pid_price_list
alter column nabizeno_od type date using nabizeno_od::date,
alter column nabizeno_do type date using nabizeno_do::date,
drop column zpusob_urceni_konce_platnosti,
drop column zpusob_nabidky;

create or replace view analytic.v_ropidbi_ticket_with_rollingmean
as select tab.date,
    count(tab.tariff_name) as ticket_count,
    sum(tab.cena_s_dph) as total_sale,
    avg(count(tab.tariff_name)) over (order by tab.date rows between 6 preceding and current row) as roll_ticket_count,
    avg(sum(tab.cena_s_dph)) over (order by tab.date rows between 6 preceding and current row) as roll_sale
   from ( select pur.date::date,
            pur.tariff_name,
            pur.cptp,
            cen.cena_s_dph
           from mos_ma_ticketpurchases pur
             left join analytic.pid_price_list cen on pur.tariff_name::text = cen.tarif::text and pur.cptp = cen.cislo_tarifu_cptp and pur.date::date >= cen.nabizeno_od and pur.date::date <= cen.nabizeno_do) tab
  group by tab.date;

create or replace view analytic.v_ropidbi_ticket_sales
as select
	rt.purchase_date::date as purchase_date,
    rt.tariff_name,
    rt.oblast,
    count(*) as count_ticket,
    pid.cena_s_dph,
    count(*) * pid.cena_s_dph as sale,
    pid.nabizeno_od::date as nabizeno_od,
    pid.nabizeno_do::date as nabizeno_do,
    pid.druh as cestujici
   from analytic.v_ropidbi_ticket rt
     left join analytic.pid_price_list pid on rt.tariff_name::text = pid.tarif::text and rt.cptp = pid.cislo_tarifu_cptp and rt.purchase_date::date >= pid.nabizeno_od and rt.purchase_date::date <= pid.nabizeno_do
  where rt.purchase_date::date is not null and rt.tariff_name is not null
  group by (rt.purchase_date::date), rt.tariff_name, rt.oblast, pid.cena_s_dph, pid.nabizeno_do, pid.nabizeno_od, pid.druh;


create or replace view analytic.v_ropidbi_ticket_sales_monthly_by_zones
as with purchases_w_prices as (
         select p.ticket_id,
            p.tariff_name,
            p.date::date as date,
            p.zone_count,
            coalesce(pid.zony, 'nezařazeno'::character varying(50)) as zony,
            coalesce(pid.cena_s_dph, 0) as price
           from mos_ma_ticketpurchases p
             left join analytic.pid_price_list pid on p.tariff_name::text = pid.tarif::text and p.cptp = pid.cislo_tarifu_cptp and p.date::date >= pid.nabizeno_od and p.date::date <= pid.nabizeno_do
        ), activations as (
         select mos_ma_ticketactivations.ticket_id,
            btrim(unnest(string_to_array(mos_ma_ticketactivations.zones::text, ','::text))) as zones
           from mos_ma_ticketactivations
          where mos_ma_ticketactivations.zones::text <> ''::text and mos_ma_ticketactivations.zones is not null
          group by mos_ma_ticketactivations.ticket_id, mos_ma_ticketactivations.zones
        ), main as (
         select c.ticket_id,
            c.tariff_name,
            c.date,
            c.zone_count,
            c.zony,
            c.price,
            string_agg(c.zones, ', '::text) as zones
           from ( select distinct p.ticket_id,
                    p.tariff_name,
                    to_char(p.date::timestamp with time zone, 'yyyy-mm-01'::text) as date,
                    p.zone_count,
                    p.zony,
                    p.price,
                        case
                            when a.zones = any (array['p'::character varying::text, '0'::text, 'b'::character varying::text]) then 'p+0+b'::character varying::text
                            when a.zones is null then 'nezařazeno'::text
                            else a.zones
                        end as zones,
                        case
                            when a.zones = any (array['p'::character varying::text, '0'::text, 'b'::character varying::text]) then 0
                            when a.zones is null then '-1'::integer
                            else a.zones::integer
                        end as zone_order
                   from purchases_w_prices p
                     left join activations a on p.ticket_id = a.ticket_id
                  order by p.ticket_id, (
                        case
                            when a.zones = any (array['p'::character varying::text, '0'::text, 'b'::character varying::text]) then 0
                            when a.zones is null then '-1'::integer
                            else a.zones::integer
                        end)) c
          group by c.ticket_id, c.tariff_name, c.date, c.zone_count, c.zony, c.price
        )
 select main.tariff_name,
    main.date,
    main.zone_count,
    main.zony,
    main.zones,
    count(*) as tickets_count,
    sum(main.price) as sales
   from main
  group by main.tariff_name, main.date, main.zone_count, main.zony, main.zones
  order by main.tariff_name, main.zony, main.zone_count, main.zones, main.date;

create or replace view analytic.v_ropidbi_ticket_activation_types
as select rop.act_date,
    rop.activation_type,
    rop.oblast,
    rop.tariff_name,
    count(*) as count,
    pid.druh as cestujici
   from analytic.v_ropidbi_ticket rop
     left join analytic.pid_price_list pid on rop.tariff_name::text = pid.tarif::text and rop.cptp = pid.cislo_tarifu_cptp and rop.purchase_date::date >= pid.nabizeno_od and rop.purchase_date::date <= pid.nabizeno_do
  where rop.activation_type <> 'zatím neaktivováno'::text and rop.tariff_name::text <> ''::text
  group by rop.act_date, rop.activation_type, rop.oblast, rop.tariff_name, pid.druh;


create or replace view analytic.v_ropidbi_ticket_activation_times
as select rop.act_date,
    rop.act_time,
    rop.tariff_name,
    rop.oblast,
    count(*) as count,
    pid.druh as cestujici
   from analytic.v_ropidbi_ticket rop
     left join analytic.pid_price_list pid on rop.tariff_name::text = pid.tarif::text and rop.cptp = pid.cislo_tarifu_cptp and rop.purchase_date::date >= pid.nabizeno_od and rop.purchase_date::date <= pid.nabizeno_do
  group by rop.act_date, rop.act_time, rop.tariff_name, rop.oblast, pid.druh;
