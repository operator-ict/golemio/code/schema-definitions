drop MATERIALIZED VIEW public.mv_pedestrians_detections_api;
DROP TABLE public.counters_detections;
DROP TABLE public.counters_directions;
DROP TABLE public.counters_locations;

DROP TABLE public.flow_analytics;
DROP TABLE public.flow_cubes;

DROP TABLE public.flow_measurements;
DROP TABLE public.flow_od_measurements;
DROP TABLE public.flow_sinks;

drop VIEW public.v_pedestrians_directions_api;
DROP VIEW analytic.v_pedestrians_directions_api;
DROP TABLE analytic.pedestrians_directions_api;

DROP VIEW public.v_pedestrians_locations_api;
DROP VIEW analytic.v_pedestrians_locations_api;
DROP TABLE analytic.pedestrians_locations_api;

DROP TABLE analytic.pedestrians_wifi;