CREATE OR REPLACE VIEW analytic.v_ropidbi_capping
AS SELECT m.account_id,
    m.cptp,
    m.date,
    m.duration,
    m.lat as misto_nakupu_lat,
    m.lon as misto_nakupu_lon,
    m.tariff_id,
    m.tariff_name,
    m.ticket_id,
    m.zone_count,
    a.date AS datum_aktivace,
    a.zones AS zona_aktivace,
    a.type,
    a.lat AS misto_aktivace_lat,
    a.lon AS misto_aktivace_lon
   FROM mos_ma_ticketpurchases m
     LEFT JOIN mos_ma_ticketactivations a ON m.ticket_id = a.ticket_id
  WHERE m.date >= '2022-04-01 00:00:00+02'::timestamp with time zone;