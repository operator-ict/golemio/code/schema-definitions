-- analytic.v_barrande_route source

CREATE OR REPLACE VIEW analytic.v_barrande_route
AS SELECT DISTINCT ON (wazett_routes.name) wazett_routes.name,
        CASE
            WHEN "position"(wazett_routes.name, ' TN'::text) > 0 THEN true
            ELSE false
        END AS is_trasa,
        CASE
            WHEN "position"(wazett_routes.name, ' ÚS'::text) > 0 THEN true
            ELSE false
        END AS is_usek,
        CASE
            WHEN "position"(wazett_routes.name, ' OS'::text) > 0 THEN true
            ELSE false
        END AS is_opacny_smer,
        CASE
            WHEN wazett_routes.id = ANY (ARRAY[32875, 32876]) THEN 'OICT 3 DO'::text
            WHEN wazett_routes.id = ANY (ARRAY[32888, 32889]) THEN 'OICT 3 OD'::text
            ELSE "substring"(wazett_routes.name, 4,
            CASE
                WHEN "position"(wazett_routes.name, ' TN'::text) > 0 THEN "position"(wazett_routes.name, ' TN'::text) - 3
                WHEN "position"(wazett_routes.name, ' ÚS'::text) > 0 THEN "position"(wazett_routes.name, ' ÚS'::text) - 3
                ELSE NULL::integer
            END)
        END AS name_trasa,
    "substring"(wazett_routes.name,
        CASE
            WHEN "position"(wazett_routes.name, ' ÚS'::text) > 0 THEN "position"(wazett_routes.name, ' ÚS'::text) + 4
            ELSE length(wazett_routes.name) + 1
        END,
        CASE
            WHEN "position"(wazett_routes.name, ' OS'::text) > 0 THEN "position"(wazett_routes.name, ' OS'::text) - "position"(wazett_routes.name, ' ÚS'::text) - 4
            ELSE length(wazett_routes.name) - "position"(wazett_routes.name, ' ÚS'::text)
        END) AS name_usek
   FROM wazett_routes
  WHERE "left"(wazett_routes.name, 2) = 'BM'::text AND NOT (EXISTS ( SELECT 1
           FROM analytic.barrande_black_list_route bl
          WHERE bl.route_id = wazett_routes.id))
  ORDER BY wazett_routes.name, wazett_routes.created_at DESC;
  

-- analytic.v_barrande_route_hour_core source

CREATE OR REPLACE VIEW analytic.v_barrande_route_hour_core
AS SELECT wr.name,
    to_timestamp((rl.update_time / 1000)::double precision)::date AS datum,
    date_part('hour'::text, to_timestamp((rl.update_time / 1000)::double precision)) AS hodina,
    (to_char(date_part('hour'::text, to_timestamp((rl.update_time / 1000)::double precision)), '09'::text) || ' -'::text) || to_char(date_part('hour'::text, to_timestamp((rl.update_time / 1000)::double precision)) + 1::double precision, '09'::text) AS cas_interval,
    round(avg(rl."time"), 2) AS avg_time,
    round(avg(rl.length * 1000 / (rl."time" * 360)), 2) AS avg_speed
   FROM wazett_route_lives rl
     JOIN wazett_routes wr ON wr.id = rl.route_id
  WHERE rl.length > 0 AND "left"(wr.name, 2) = 'BM'::text AND NOT (EXISTS ( SELECT 1
           FROM analytic.barrande_black_list_route bl
          WHERE bl.route_id = rl.route_id))
  GROUP BY wr.name, (to_timestamp((rl.update_time / 1000)::double precision)::date), (date_part('hour'::text, to_timestamp((rl.update_time / 1000)::double precision)));  