ALTER TABLE public.ropidvymi_events
    ADD CONSTRAINT ropidvymi_events_vymi_id_uniq
    UNIQUE (vymi_id);

ALTER TABLE public.ropidvymi_events_routes DROP CONSTRAINT ropidvymi_events_routes_pkey;
ALTER TABLE public.ropidvymi_events_routes
    ADD CONSTRAINT ropidvymi_events_routes_pkey
    PRIMARY KEY (event_id, vymi_id);

ALTER TABLE public.ropidvymi_events_stops DROP CONSTRAINT ropidvymi_events_stops_pkey;
ALTER TABLE public.ropidvymi_events_stops
    ADD CONSTRAINT ropidvymi_events_stops_pkey
    PRIMARY KEY (event_id, vymi_id);
