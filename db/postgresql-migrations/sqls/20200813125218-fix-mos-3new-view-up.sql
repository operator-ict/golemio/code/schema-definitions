create or replace view analytic.v_ropidbi_ticket_with_rollingmean as
select "date"::date,
		count(tariff_name) as ticket_count,
		sum (cena_s_dph) as total_sale,
		avg(count(tariff_name)) over(order by "date" rows between 6 preceding and current row) as roll_ticket_count,
		avg(sum (cena_s_dph)) over(order by "date" rows between 6 preceding and current row) as roll_sale
from (
		select "date"::date,
			   tariff_name,
				case when tariff_name like '%pes%' then 16 else cen.cena_s_dph end as cena_s_dph
		from public.mos_ma_ticketpurchases pur
		left join (select distinct * from analytic.pid_cenik) cen
				on pur.tariff_name = cen.tarif
				) tab
group by "date"::date;
