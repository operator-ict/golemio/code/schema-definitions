alter table python.public_tenders 
ALTER COLUMN zadavatel_ico type varchar(15);

CREATE TABLE python.public_tenders_city_companies (
	ico varchar(50) NOT NULL primary key,
	jmeno text NULL
);

/*
CREATE TABLE python.public_tenders_funded_organisations (
	id int8 primary key,
	jmeno text NULL,
	ulice text NULL,
	cislo_domu text NULL,
	cislo_orientacni text NULL,
	mestska_cast text NULL,
	obec text NULL,
	psc text NULL,
	reditelka text NULL,
	ico text NULL,
	id_ds text NULL,
	odbor_mhmp text NULL,
	oblast text NULL,
	web_kontakty text NULL,
	profil_zadavatele text NULL,
	",Sedeke  Martin Ing. ,Stuchlík  Petr Ing.  ,Stárek  Jakub Mgr" varchar(1024) NULL,
	"Příspěvková organizace vlastního hl. m. Prahy" varchar(128) NULL,
	"Název ulice" varchar(32) NULL,
	"Číslo domovní" int4 NULL,
	"Číslo orientační" int4 NULL,
	"Městská část" varchar(8) NULL,
	psč varchar(8) NULL,
	"Jméno ředitele/ředitelky" varchar(32) NULL,
	ičo varchar(16) NULL,
	"ID DS" varchar(8) NULL,
	"Odbor MHMP" varchar(8) NULL,
	"web - kontakty" varchar(128) NULL,
	"odkaz na profil zadavatele" varchar(128) NULL
);
*/

-- franz.v_public_tenders source
drop VIEW analytic.v_public_tenders;
CREATE OR REPLACE VIEW analytic.v_public_tenders
AS WITH inhouse_ico AS (
         SELECT public_tenders_city_companies.ico
           FROM python.public_tenders_city_companies
        UNION ALL
         SELECT public_tenders_funded_organisations.ico
           FROM python.public_tenders_funded_organisations
        )
 SELECT pt.id_zakazky,
    pt.nazev_zakazky,
        CASE
            WHEN "position"(pt.organizacni_jednotka_spravy, '-'::text) = 0 THEN pt.organizacni_jednotka_spravy
            ELSE "left"(pt.organizacni_jednotka_spravy, "position"(pt.organizacni_jednotka_spravy, '-'::text) - 2)
        END AS organizacni_jednotka_spravy,
        CASE
            WHEN pto.ico IS NULL THEN 'MHMP'::text
            ELSE 'Příspěvková organizace'::text
        END AS mhmp_po,
    pt.varianta_druhu_rizeni,
    pt.druh_verejne_zakazky,
    pt.nazev_smluvniho_partnera,
    pt.ico_smluvniho_partnera,
        CASE
            WHEN (pt.ico_smluvniho_partnera IN ( SELECT inhouse_ico.ico
               FROM inhouse_ico)) THEN 'Ano'::text
            ELSE 'Ne'::text
        END AS inhouse,
        CASE
            WHEN pt.pocet_nabidek = 0 THEN 1::bigint
            ELSE pt.pocet_nabidek
        END AS pocet_nabidek,
    pt.datum_uzavreni_smlouvy,
    pt.smluvni_cena_s_dph_kc,
    pt.smluvni_cena_bez_dph_kc,
        CASE
            WHEN top10.id_zakazky IS NOT NULL THEN 'Y'::text
            ELSE 'N'::text
        END AS top10,
        CASE
            WHEN pt.smluvni_cena_s_dph_kc > 300000::double precision THEN 'Více než 300 000'::text
            ELSE 'Méně než 300 000'::text
        END AS price_range,
    pt.datum_zahajeni_zadavaciho_rizeni,
        CASE
            WHEN m2.mmr_group IS NOT NULL THEN m2.mmr_group::character varying
            WHEN character_length(pt.hlavni_nipez_kod_zakazky) = 2 THEN 'Neuvedeno'::character varying
            WHEN g3.cpv_group IS NOT NULL THEN g3.cpv_group::character varying
            WHEN g2.cpv_group IS NOT NULL THEN g2.cpv_group::character varying
            ELSE 'Neuvedeno'::character varying
        END AS cpv_name,
        CASE
            WHEN pt.pocet_nabidek = 1 OR pt.pocet_nabidek = 0 THEN '1 nabídka'::text
            WHEN pt.pocet_nabidek = ANY (ARRAY[2::bigint, 3::bigint, 4::bigint]) THEN concat(pt.pocet_nabidek, ' nabídky')
            WHEN pt.pocet_nabidek > 4 THEN concat(pt.pocet_nabidek, ' nabídek')
            ELSE 'Neuvedeno'::text
        END AS pocet_nabidek2,
    pt.updated_at,
    pt.created_at
   FROM python.public_tenders pt
     LEFT JOIN ( SELECT public_tenders.id_zakazky
           FROM python.public_tenders
          ORDER BY public_tenders.smluvni_cena_bez_dph_kc DESC
         LIMIT 10) top10 ON pt.id_zakazky = top10.id_zakazky
     LEFT JOIN python.public_tenders_cpv_groups g3 ON g3.cpv_code::text = "left"(pt.hlavni_nipez_kod_zakazky, 3)
     LEFT JOIN python.public_tenders_cpv_groups g2 ON g2.cpv_code::text = "left"(pt.hlavni_nipez_kod_zakazky, 2)
     LEFT JOIN python.public_tenders_mmr_commodity_groups m2 ON m2.mmr_commodity_code::text = pt.hlavni_nipez_kod_zakazky
     LEFT JOIN python.public_tenders_funded_organisations pto ON pt.zadavatel_ico::text = pto.ico
  WHERE (pt.faze_zakazky = ANY (ARRAY['Ukončeno plnění smlouvy na základě veřejné zakázky'::text, 'Zadána'::text])) AND pt.datum_uzavreni_smlouvy >= '2020-09-01 00:00:00'::timestamp without time zone AND pt.datum_uzavreni_smlouvy < CURRENT_DATE AND (pt.vysledkem_je_ramcova_dohoda = 'Ne'::text OR (pt.varianta_druhu_rizeni = ANY (ARRAY['Výzva k plnění rámcové smlouvy'::text, 'Minitendr'::text])));
  
drop VIEW analytic.v_public_tenders_internal;
  -- franz.v_public_tenders_internal source

CREATE OR REPLACE VIEW analytic.v_public_tenders_internal
AS WITH inhouse_ico AS (
         SELECT public_tenders_city_companies.ico
           FROM python.public_tenders_city_companies
        UNION ALL
         SELECT public_tenders_funded_organisations.ico
           FROM python.public_tenders_funded_organisations
        )
 SELECT pt.id_zakazky,
    pt.nazev_zakazky,
    pto.oblast,
    pto.odbor_mhmp,
    pt.zadavatel_ico,
    pt.organizacni_jednotka_spravy,
    pt.varianta_druhu_rizeni,
    pt.druh_verejne_zakazky,
    pt.nazev_smluvniho_partnera,
    pt.ico_smluvniho_partnera,
        CASE
            WHEN (pt.ico_smluvniho_partnera IN ( SELECT inhouse_ico.ico
               FROM inhouse_ico)) THEN 'Ano'::text
            ELSE 'Ne'::text
        END AS inhouse,
        CASE
            WHEN pt.pocet_nabidek = 0 THEN 1::bigint
            ELSE pt.pocet_nabidek
        END AS pocet_nabidek,
    pt.smluvni_cena_s_dph_kc,
    pt.smluvni_cena_bez_dph_kc,
        CASE
            WHEN top10.id_zakazky IS NOT NULL THEN 'Y'::text
            ELSE 'N'::text
        END AS top10,
        CASE
            WHEN pt.smluvni_cena_s_dph_kc > 300000::double precision THEN 'Více než 300 000'::text
            ELSE 'Méně než 300 000'::text
        END AS price_range,
        CASE
            WHEN m2.mmr_group IS NOT NULL THEN m2.mmr_group::character varying
            WHEN character_length(pt.hlavni_nipez_kod_zakazky) = 2 THEN 'Neuvedeno'::character varying
            WHEN g3.cpv_group IS NOT NULL THEN g3.cpv_group::character varying
            WHEN g2.cpv_group IS NOT NULL THEN g2.cpv_group::character varying
            ELSE 'Neuvedeno'::character varying
        END AS cpv_name,
        CASE
            WHEN pt.pocet_nabidek = 1 OR pt.pocet_nabidek = 0 THEN '1 nabídka'::text
            WHEN pt.pocet_nabidek = ANY (ARRAY[2::bigint, 3::bigint, 4::bigint]) THEN concat(pt.pocet_nabidek, ' nabídky')
            WHEN pt.pocet_nabidek > 4 THEN concat(pt.pocet_nabidek, ' nabídek')
            ELSE 'Neuvedeno'::text
        END AS pocet_nabidek2,
    pt.celkova_predpokladana_hodnota_zakazky_bez_dph_kc,
    pt.smluvni_cena_bez_dph_kc - NULLIF(pt.celkova_predpokladana_hodnota_zakazky_bez_dph_kc, 0::double precision) AS rozdil_od_predp_hodnoty_kc,
    (pt.smluvni_cena_bez_dph_kc - pt.celkova_predpokladana_hodnota_zakazky_bez_dph_kc) / NULLIF(pt.celkova_predpokladana_hodnota_zakazky_bez_dph_kc, 0::double precision) AS rozdil_od_predp_hodnoty_procent,
    pt.datum_uzavreni_smlouvy,
    pt.datum_vytvoreni_zakazky,
    pt.lhuta_pro_podani_nabidek_na_profilu,
    pt.datum_zahajeni_zadavaciho_rizeni,
    pt.datum_uzavreni_smlouvy - pt.datum_zahajeni_zadavaciho_rizeni AS delka_zadavaciho_rizeni,
    pt.lhuta_pro_podani_nabidek_na_profilu - pt.datum_zahajeni_zadavaciho_rizeni AS delka_lhuty_pro_podani_nabidek,
        CASE
            WHEN pt.hodnotici_kriteria ~~ '%100%'::text THEN 'Pouze cena'::text
            ELSE 'Více kritérií'::text
        END AS hodnotici_kriteria_typ,
    pt.externi_zastoupeni_zadavatele_v_rizeni,
    pt.externi_zastoupeni_zadavatele_v_rizeni_nazev,
    pt.externi_zastoupeni_zadavatele_v_rizeni_ico,
    "left"(pt.hlavni_nipez_kod_zakazky, 3) AS cpv_kod,
    pt.hlavni_nipez_kod_zakazky,
    pt.druh_zadavaciho_rizeni,
    pt.faze_zakazky,
        CASE
            WHEN pto.ico IS NULL THEN 'MHMP'::text
            ELSE 'Příspěvková organizace'::text
        END AS mhmp_po,
    pt.updated_at,
    pt.created_at,
        CASE
            WHEN pt.datum_uzavreni_smlouvy < '2020-09-01'::date THEN 'Uzavření smlouvy před datem vytvoření zakázky'::text
            WHEN pt.datum_uzavreni_smlouvy > pt.updated_at THEN 'Uzavření smlouvy v budoucnosti'::text
            ELSE 'Září 2020 - nyní'::text
        END AS uzavreni_smlouvy_filtr
   FROM python.public_tenders pt
     LEFT JOIN ( SELECT public_tenders.id_zakazky
           FROM python.public_tenders
          ORDER BY public_tenders.smluvni_cena_bez_dph_kc DESC
         LIMIT 10) top10 ON pt.id_zakazky = top10.id_zakazky
     LEFT JOIN python.public_tenders_cpv_groups g3 ON g3.cpv_code::text = "left"(pt.hlavni_nipez_kod_zakazky, 3)
     LEFT JOIN python.public_tenders_cpv_groups g2 ON g2.cpv_code::text = "left"(pt.hlavni_nipez_kod_zakazky, 2)
     LEFT JOIN python.public_tenders_mmr_commodity_groups m2 ON m2.mmr_commodity_code::text = pt.hlavni_nipez_kod_zakazky
     LEFT JOIN python.public_tenders_funded_organisations pto ON pt.zadavatel_ico::text = pto.ico
  WHERE (pt.faze_zakazky = ANY (ARRAY['Ukončeno plnění smlouvy na základě veřejné zakázky'::text, 'Zadána'::text, 'Zrušena'::text])) AND (pt.datum_vytvoreni_zakazky >= '2020-09-01 01:00:00+02'::timestamp with time zone OR pt.datum_vytvoreni_zakazky >= '2021-01-01 00:00:00+01'::timestamp with time zone AND pto.ico IS NOT NULL) AND (pt.vysledkem_je_ramcova_dohoda = 'Ne'::text OR (pt.varianta_druhu_rizeni = ANY (ARRAY['Výzva k plnění rámcové smlouvy'::text, 'Minitendr'::text])));
  
drop VIEW analytic.v_public_tenders_internal_frequent_suppliers;
-- franz.v_public_tenders_internal_frequent_suppliers source

CREATE OR REPLACE VIEW analytic.v_public_tenders_internal_frequent_suppliers
AS WITH inhouse_ico AS (
         SELECT public_tenders_city_companies.ico
           FROM python.public_tenders_city_companies
        UNION ALL
         SELECT public_tenders_funded_organisations.ico
           FROM python.public_tenders_funded_organisations
        )
 SELECT count(DISTINCT pt.id_zakazky) AS pocet_zakazek,
    pt.organizacni_jednotka_spravy,
    pt.varianta_druhu_rizeni,
    pto.oblast,
    pto.odbor_mhmp,
        CASE
            WHEN (pt.ico_smluvniho_partnera IN ( SELECT inhouse_ico.ico
               FROM inhouse_ico)) THEN 'Ano'::text
            ELSE 'Ne'::text
        END AS inhouse,
    pt.datum_uzavreni_smlouvy,
    pt.datum_vytvoreni_zakazky,
    pt.faze_zakazky,
        CASE
            WHEN pto.ico IS NULL THEN 'MHMP'::text
            ELSE 'Příspěvková organizace'::text
        END AS mhmp_po,
        CASE
            WHEN pt.varianta_druhu_rizeni = 'Neveřejná výzva k podání nabídky'::text THEN s.spolecnosti_oslovene_k_podani_nabidky_nazev
            ELSE s2.nazev_smluvniho_partnera
        END AS spolecnost,
    sum(
        CASE
            WHEN pt.spolecnosti_oslovene_k_podani_nabidky_nazev ~~ (('%'::text || s2.nazev_smluvniho_partnera) || '%'::text) THEN 1
            ELSE 0
        END) AS pocet_podanych_nabidek,
    sum(
        CASE
            WHEN s.spolecnosti_oslovene_k_podani_nabidky_nazev = s2.nazev_smluvniho_partnera OR s.spolecnosti_oslovene_k_podani_nabidky_nazev IS NULL THEN 1
            ELSE 0
        END) AS pocet_vyhranych_zakazek,
        CASE
            WHEN pt.datum_uzavreni_smlouvy < '2020-09-01'::date THEN 'Uzavření smlouvy před datem vytvoření zakázky'::text
            WHEN pt.datum_uzavreni_smlouvy > pt.updated_at THEN 'Uzavření smlouvy v budoucnosti'::text
            ELSE 'Září 2020 - nyní'::text
        END AS uzavreni_smlouvy_filtr
   FROM python.public_tenders pt
     LEFT JOIN LATERAL unnest(string_to_array(pt.spolecnosti_oslovene_k_podani_nabidky_nazev, '; '::text)) s(spolecnosti_oslovene_k_podani_nabidky_nazev) ON true
     LEFT JOIN LATERAL unnest(string_to_array(pt.nazev_smluvniho_partnera, '; '::text)) s2(nazev_smluvniho_partnera) ON true
     LEFT JOIN LATERAL unnest(string_to_array(pt.ico_smluvniho_partnera, '; '::text)) s3(ico_smluvniho_partnera) ON true
     LEFT JOIN python.public_tenders_funded_organisations pto ON pt.zadavatel_ico::text = pto.ico
  WHERE (pt.faze_zakazky = ANY (ARRAY['Ukončeno plnění smlouvy na základě veřejné zakázky'::text, 'Zadána'::text])) AND (pt.varianta_druhu_rizeni = ANY (ARRAY['Neveřejná výzva k podání nabídky'::text, 'Drobná objednávka'::text, 'Přímo objednáno u jednoho dodavatele'::text, 'Zakázka zadaná na základě výjimky'::text])) AND (pt.vysledkem_je_ramcova_dohoda = 'Ne'::text OR (pt.varianta_druhu_rizeni = ANY (ARRAY['Výzva k plnění rámcové smlouvy'::text, 'Minitendr'::text]))) AND (pt.datum_vytvoreni_zakazky >= '2020-09-01 01:00:00+02'::timestamp with time zone OR pt.datum_vytvoreni_zakazky >= '2021-01-01 00:00:00+01'::timestamp with time zone AND pto.ico IS NULL)
  GROUP BY pt.organizacni_jednotka_spravy, pt.varianta_druhu_rizeni, pto.oblast, pto.odbor_mhmp, (
        CASE
            WHEN (pt.ico_smluvniho_partnera IN ( SELECT inhouse_ico.ico
               FROM inhouse_ico)) THEN 'Ano'::text
            ELSE 'Ne'::text
        END), pt.datum_uzavreni_smlouvy, pt.datum_vytvoreni_zakazky, pt.faze_zakazky, (
        CASE
            WHEN pto.ico IS NULL THEN 'MHMP'::text
            ELSE 'Příspěvková organizace'::text
        END), (
        CASE
            WHEN pt.varianta_druhu_rizeni = 'Neveřejná výzva k podání nabídky'::text THEN s.spolecnosti_oslovene_k_podani_nabidky_nazev
            ELSE s2.nazev_smluvniho_partnera
        END), (
        CASE
            WHEN pt.datum_uzavreni_smlouvy < '2020-09-01'::date THEN 'Uzavření smlouvy před datem vytvoření zakázky'::text
            WHEN pt.datum_uzavreni_smlouvy > pt.updated_at THEN 'Uzavření smlouvy v budoucnosti'::text
            ELSE 'Září 2020 - nyní'::text
        END);  
		
drop VIEW analytic.v_public_tenders_opendata;
-- franz.v_public_tenders_opendata source

CREATE OR REPLACE VIEW analytic.v_public_tenders_opendata
AS WITH inhouse_ico AS (
         SELECT public_tenders_city_companies.ico
           FROM python.public_tenders_city_companies
        UNION ALL
         SELECT public_tenders_funded_organisations.ico
           FROM python.public_tenders_funded_organisations
        )
 SELECT pt.id_zakazky,
    pt.id_casti_zakazky,
    pt.systemove_cislo_zakazky,
    pt.evidencni_cislo_zakazky,
    pt.cislo_vysledku_zadavaciho_rizeni,
    pt.interni_cisla_smluv_objednavek,
    pt.nazev_zakazky,
    pt.nazev_casti_zakazky,
    pt.externi_zastoupeni_zadavatele_v_rizeni,
    pt.externi_zastoupeni_zadavatele_v_rizeni_nazev,
    pt.externi_zastoupeni_zadavatele_v_rizeni_ico,
    pt.druh_verejne_zakazky,
    pt.druh_zadavaciho_rizeni,
    pt.typ_zakazky,
    pt.varianta_druhu_rizeni,
    pt.jedna_se_o_cast_vz,
    pt.zakazka_zavadejici_dns,
    pt.faze_zakazky,
    pt.celkova_predpokladana_hodnota_zakazky_bez_dph_kc,
    pt.zakazka_souvisejici_s_vykonem_relevantni_cinnosti,
    pt.hlavni_nipez_kod_zakazky,
    pt.organizacni_jednotka_spravy,
    pt.datum_vytvoreni_zakazky,
    pt.datum_zahajeni_zadavaciho_rizeni,
    pt.lhuta_pro_podani_nabidek_na_profilu,
    pt.datum_otevirani_nabidek_na_profilu,
    pt.lhuta_pro_doruceni_zadosti_o_ucast_na_profilu,
    pt.hodnotici_kriteria,
    pt.vysledkem_je_ramcova_dohoda,
    pt.hodnoceni_formou_elektronicke_aukce_prostrednictvim_ta,
    pt.hodnoceni_formou_elektronicke_aukce_mimo_aplikaci_ta,
    pt.spolecnosti_oslovene_k_podani_nabidky_pocet,
    pt.spolecnosti_oslovene_k_podani_nabidky_nazev,
    pt.pocet_zadosti_o_ucast,
    pt.seznam_zadosti_o_ucast,
    pt.seznam_nabidek_a_nabidkova_cena_bez_dph,
    pt.ucastnik_zadavaciho_rizeni_nazev,
    pt.nazev_smluvniho_partnera,
    pt.ico_smluvniho_partnera,
    pt.smluvni_cena_bez_dph_kc,
    pt.smluvni_cena_s_dph_kc,
    pt.datum_uzavreni_smlouvy,
    pt.skutecne_uhrazena_cena_bez_dph,
    pt.skutecne_uhrazena_cena_s_dph,
        CASE
            WHEN (pt.ico_smluvniho_partnera IN ( SELECT inhouse_ico.ico
               FROM inhouse_ico)) THEN 'Ano'::text
            ELSE 'Ne'::text
        END AS inhouse,
        CASE
            WHEN pt.pocet_nabidek = 0 THEN 1::bigint
            ELSE pt.pocet_nabidek
        END AS pocet_nabidek,
        CASE
            WHEN m2.mmr_group IS NOT NULL THEN m2.mmr_group::character varying
            WHEN character_length(pt.hlavni_nipez_kod_zakazky) = 2 THEN 'Neuvedeno'::character varying
            WHEN g3.cpv_group IS NOT NULL THEN g3.cpv_group::character varying
            WHEN g2.cpv_group IS NOT NULL THEN g2.cpv_group::character varying
            ELSE 'Neuvedeno'::character varying
        END AS cpv_name
   FROM python.public_tenders pt
     LEFT JOIN ( SELECT public_tenders.id_zakazky
           FROM python.public_tenders
          ORDER BY public_tenders.smluvni_cena_bez_dph_kc DESC
         LIMIT 10) top10 ON pt.id_zakazky = top10.id_zakazky
     LEFT JOIN python.public_tenders_cpv_groups g3 ON g3.cpv_code::text = "left"(pt.hlavni_nipez_kod_zakazky, 3)
     LEFT JOIN python.public_tenders_cpv_groups g2 ON g2.cpv_code::text = "left"(pt.hlavni_nipez_kod_zakazky, 2)
     LEFT JOIN python.public_tenders_mmr_commodity_groups m2 ON m2.mmr_commodity_code::text = pt.hlavni_nipez_kod_zakazky
     LEFT JOIN python.public_tenders_funded_organisations pto ON pt.zadavatel_ico::text = pto.ico
  WHERE pto.ico IS NULL AND (pt.faze_zakazky = ANY (ARRAY['Ukončeno plnění smlouvy na základě veřejné zakázky'::text, 'Zadána'::text])) AND pt.datum_uzavreni_smlouvy >= '2020-09-01 00:00:00'::timestamp without time zone AND pt.datum_uzavreni_smlouvy < CURRENT_DATE AND (pt.vysledkem_je_ramcova_dohoda = 'Ne'::text OR (pt.varianta_druhu_rizeni = ANY (ARRAY['Výzva k plnění rámcové smlouvy'::text, 'Minitendr'::text])));		
  
drop VIEW analytic.v_public_tenders_opendata_funded_organisations;
-- franz.v_public_tenders_opendata_funded_organisations source

CREATE OR REPLACE VIEW analytic.v_public_tenders_opendata_funded_organisations
AS WITH inhouse_ico AS (
         SELECT public_tenders_city_companies.ico
           FROM python.public_tenders_city_companies
        UNION ALL
         SELECT public_tenders_funded_organisations.ico
           FROM python.public_tenders_funded_organisations
        )
 SELECT pt.id_zakazky,
    pt.id_casti_zakazky,
    pt.systemove_cislo_zakazky,
    pt.evidencni_cislo_zakazky,
    pt.cislo_vysledku_zadavaciho_rizeni,
    pt.interni_cisla_smluv_objednavek,
    pt.nazev_zakazky,
    pt.nazev_casti_zakazky,
    pt.externi_zastoupeni_zadavatele_v_rizeni,
    pt.externi_zastoupeni_zadavatele_v_rizeni_nazev,
    pt.externi_zastoupeni_zadavatele_v_rizeni_ico,
    pt.druh_verejne_zakazky,
    pt.druh_zadavaciho_rizeni,
    pt.typ_zakazky,
    pt.varianta_druhu_rizeni,
    pt.jedna_se_o_cast_vz,
    pt.zakazka_zavadejici_dns,
    pt.faze_zakazky,
    pt.celkova_predpokladana_hodnota_zakazky_bez_dph_kc,
    pt.zakazka_souvisejici_s_vykonem_relevantni_cinnosti,
    pt.hlavni_nipez_kod_zakazky,
    pt.organizacni_jednotka_spravy,
    pt.datum_vytvoreni_zakazky,
    pt.datum_zahajeni_zadavaciho_rizeni,
    pt.lhuta_pro_podani_nabidek_na_profilu,
    pt.datum_otevirani_nabidek_na_profilu,
    pt.lhuta_pro_doruceni_zadosti_o_ucast_na_profilu,
    pt.hodnotici_kriteria,
    pt.vysledkem_je_ramcova_dohoda,
    pt.hodnoceni_formou_elektronicke_aukce_prostrednictvim_ta,
    pt.hodnoceni_formou_elektronicke_aukce_mimo_aplikaci_ta,
    pt.spolecnosti_oslovene_k_podani_nabidky_pocet,
    pt.spolecnosti_oslovene_k_podani_nabidky_nazev,
    pt.pocet_zadosti_o_ucast,
    pt.seznam_zadosti_o_ucast,
    pt.seznam_nabidek_a_nabidkova_cena_bez_dph,
    pt.ucastnik_zadavaciho_rizeni_nazev,
    pt.nazev_smluvniho_partnera,
    pt.ico_smluvniho_partnera,
    pt.smluvni_cena_bez_dph_kc,
    pt.smluvni_cena_s_dph_kc,
    pt.datum_uzavreni_smlouvy,
    pt.skutecne_uhrazena_cena_bez_dph,
    pt.skutecne_uhrazena_cena_s_dph,
        CASE
            WHEN (pt.ico_smluvniho_partnera IN ( SELECT inhouse_ico.ico
               FROM inhouse_ico)) THEN 'Ano'::text
            ELSE 'Ne'::text
        END AS inhouse,
        CASE
            WHEN pt.pocet_nabidek = 0 THEN 1::bigint
            ELSE pt.pocet_nabidek
        END AS pocet_nabidek,
        CASE
            WHEN m2.mmr_group IS NOT NULL THEN m2.mmr_group::character varying
            WHEN character_length(pt.hlavni_nipez_kod_zakazky) = 2 THEN 'Neuvedeno'::character varying
            WHEN g3.cpv_group IS NOT NULL THEN g3.cpv_group::character varying
            WHEN g2.cpv_group IS NOT NULL THEN g2.cpv_group::character varying
            ELSE 'Neuvedeno'::character varying
        END AS cpv_name
   FROM python.public_tenders pt
     LEFT JOIN ( SELECT public_tenders.id_zakazky
           FROM python.public_tenders
          ORDER BY public_tenders.smluvni_cena_bez_dph_kc DESC
         LIMIT 10) top10 ON pt.id_zakazky = top10.id_zakazky
     LEFT JOIN python.public_tenders_cpv_groups g3 ON g3.cpv_code::text = "left"(pt.hlavni_nipez_kod_zakazky, 3)
     LEFT JOIN python.public_tenders_cpv_groups g2 ON g2.cpv_code::text = "left"(pt.hlavni_nipez_kod_zakazky, 2)
     LEFT JOIN python.public_tenders_mmr_commodity_groups m2 ON m2.mmr_commodity_code::text = pt.hlavni_nipez_kod_zakazky
     JOIN python.public_tenders_funded_organisations pto ON pt.zadavatel_ico::text = pto.ico
  WHERE (pt.faze_zakazky = ANY (ARRAY['Ukončeno plnění smlouvy na základě veřejné zakázky'::text, 'Zadána'::text])) AND pt.datum_uzavreni_smlouvy >= '2020-09-01 00:00:00'::timestamp without time zone AND pt.datum_uzavreni_smlouvy < CURRENT_DATE AND (pt.vysledkem_je_ramcova_dohoda = 'Ne'::text OR (pt.varianta_druhu_rizeni = ANY (ARRAY['Výzva k plnění rámcové smlouvy'::text, 'Minitendr'::text])));  