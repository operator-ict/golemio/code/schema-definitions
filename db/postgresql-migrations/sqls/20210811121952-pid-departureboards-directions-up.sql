CREATE SEQUENCE public.ropid_departures_directions_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

CREATE TABLE public.ropid_departures_directions
(
    id int NOT NULL UNIQUE DEFAULT nextval('ropid_departures_directions_id_seq'::regclass),
    departure_stop_id text NOT NULL,
    next_stop_id_regexp text NOT NULL,
    direction text NOT NULL,
    rule_order int NOT NULL DEFAULT '0',

    -- audit fields
    create_batch_id bigint,
    created_at timestamptz,
    created_by varchar(150),
    update_batch_id bigint,
    updated_at timestamptz,
    updated_by varchar(150),

    CONSTRAINT "ropid_departures_directions_rules_pkey" PRIMARY KEY (id)
);

CREATE MATERIALIZED VIEW "public"."v_ropidgtfs_departures" AS 
  SELECT t.stop_sequence,
    t.stop_headsign,
    t.pickup_type,
    t.drop_off_type,
    t.arrival_time,
    CASE
        WHEN EXTRACT(HOUR FROM t.arrival_time::INTERVAL) > 23
            THEN make_timestamptz(
                DATE_PART('year', t4.date + INTERVAL '1 day')::INT,
                DATE_PART('month', t4.date + INTERVAL '1 day')::INT,
                DATE_PART('day', t4.date + INTERVAL '1 day')::INT,
                DATE_PART('hour', t.arrival_time::INTERVAL)::INT - 24,
                DATE_PART('minute', t.arrival_time::INTERVAL)::INT,
                DATE_PART('second', t.arrival_time::INTERVAL),
                'Europe/Prague'
            )
        ELSE make_timestamptz(
                DATE_PART('year', t4.date)::INT,
                DATE_PART('month', t4.date)::INT,
                DATE_PART('day', t4.date)::INT,
                DATE_PART('hour', t.arrival_time::INTERVAL)::INT,
                DATE_PART('minute', t.arrival_time::INTERVAL)::INT,
                DATE_PART('second', t.arrival_time::INTERVAL),
                'Europe/Prague'
            )
    END AS arrival_datetime,
    t.departure_time,
    CASE
        WHEN EXTRACT(HOUR FROM t.departure_time::INTERVAL) > 23
            THEN make_timestamptz(
                DATE_PART('year', t4.date + INTERVAL '1 day')::INT,
                DATE_PART('month', t4.date + INTERVAL '1 day')::INT,
                DATE_PART('day', t4.date + INTERVAL '1 day')::INT,
                DATE_PART('hour', t.departure_time::INTERVAL)::INT - 24,
                DATE_PART('minute', t.departure_time::INTERVAL)::INT,
                DATE_PART('second', t.departure_time::INTERVAL),
                'Europe/Prague'
            )
        ELSE make_timestamptz(
                DATE_PART('year', t4.date)::INT,
                DATE_PART('month', t4.date)::INT,
                DATE_PART('day', t4.date)::INT,
                DATE_PART('hour', t.departure_time::INTERVAL)::INT,
                DATE_PART('minute', t.departure_time::INTERVAL)::INT,
                DATE_PART('second', t.departure_time::INTERVAL),
                'Europe/Prague'
            )
    END AS departure_datetime,
    t0.stop_id,
    t0.stop_name,
    t0.platform_code,
    t0.wheelchair_boarding,
    t1.min_stop_sequence,
    t1.max_stop_sequence,
    t2.trip_id,
    t2.trip_headsign,
    t2.trip_short_name,
    t2.wheelchair_accessible,
    t3.service_id,
    t4.date,
    t5.route_short_name,
    t5.route_type,
    t5.route_id,
    t5.is_night,
    t5.is_regional,
    t5.is_substitute_transport,
    t6.stop_sequence AS next_stop_sequence,
    t6.stop_id AS next_stop_id,
    t7.stop_sequence AS last_stop_sequence,
    t7.stop_id AS last_stop_id
   FROM ropidgtfs_stop_times t
     LEFT JOIN ropidgtfs_stops t0 ON t.stop_id::text = t0.stop_id::text
     LEFT JOIN ropidgtfs_trips t2 ON t.trip_id::text = t2.trip_id::text
     INNER JOIN v_ropidgtfs_services_first14days t4 ON t2.service_id::text = t4.service_id::text
     LEFT JOIN v_ropidgtfs_trips_minmaxsequences t1 ON t.trip_id::text = t1.trip_id::text
     LEFT JOIN ropidgtfs_calendar t3 ON t2.service_id::text = t3.service_id::text
     LEFT JOIN ropidgtfs_routes t5 ON t2.route_id::text = t5.route_id::text
     LEFT JOIN ropidgtfs_stop_times t6 ON t.trip_id::text = t6.trip_id::text AND t6.stop_sequence = t.stop_sequence + 1
     LEFT JOIN ropidgtfs_stop_times t7 ON t.trip_id::text = t7.trip_id::text AND t7.stop_sequence = t.stop_sequence - 1;

CREATE INDEX "v_ropidgtfs_departures_stop_id_idx" ON "public"."v_ropidgtfs_departures"("stop_id");
CREATE UNIQUE INDEX "v_ropidgtfs_departures_unique_id" ON "public"."v_ropidgtfs_departures"("service_id", "date", "trip_id", "stop_sequence");

CREATE UNIQUE INDEX IF NOT EXISTS "v_ropidgtfs_services_first14days_date_service_id_idx" ON "public"."v_ropidgtfs_services_first14days"("date","service_id");
CREATE UNIQUE INDEX IF NOT EXISTS "v_ropidgtfs_trips_minmaxsequences_trip_id_idx" ON "public"."v_ropidgtfs_trips_minmaxsequences"("trip_id");
