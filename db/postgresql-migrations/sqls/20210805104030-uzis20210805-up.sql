-- franz.v_uzis_covid19_population_age_regions source

CREATE OR REPLACE VIEW analytic.v_uzis_covid19_population_age_regions
AS SELECT sub.population,
    sub.region_code,
    sub.age_group,
    concat(sub.region_code, '_', sub.age_group) AS region_age_id
   FROM ( SELECT sum(cpar.population) AS population,
            cpar.region_code,
                CASE
                    WHEN cpar.age >= 12 AND cpar.age < 18 THEN '12-17'::text
                    WHEN cpar.age >= 18 AND cpar.age < 25 THEN '18-24'::text
                    WHEN cpar.age >= 25 AND cpar.age < 30 THEN '25-29'::text
                    WHEN cpar.age >= 30 AND cpar.age < 35 THEN '30-34'::text
                    WHEN cpar.age >= 35 AND cpar.age < 40 THEN '35-39'::text
                    WHEN cpar.age >= 40 AND cpar.age < 45 THEN '40-44'::text
                    WHEN cpar.age >= 45 AND cpar.age < 50 THEN '45-49'::text
                    WHEN cpar.age >= 50 AND cpar.age < 55 THEN '50-54'::text
                    WHEN cpar.age >= 55 AND cpar.age < 60 THEN '55-59'::text
                    WHEN cpar.age >= 60 AND cpar.age < 65 THEN '60-64'::text
                    WHEN cpar.age >= 65 AND cpar.age < 70 THEN '65-69'::text
                    WHEN cpar.age >= 70 AND cpar.age < 75 THEN '70-74'::text
                    WHEN cpar.age >= 75 AND cpar.age < 80 THEN '75-79'::text
                    WHEN cpar.age >= 80 THEN '80+'::text
                    ELSE NULL::text
                END AS age_group
           FROM uzis.population_age_regions cpar
          GROUP BY cpar.region_code, (
                CASE
                    WHEN cpar.age >= 12 AND cpar.age < 18 THEN '12-17'::text
                    WHEN cpar.age >= 18 AND cpar.age < 25 THEN '18-24'::text
                    WHEN cpar.age >= 25 AND cpar.age < 30 THEN '25-29'::text
                    WHEN cpar.age >= 30 AND cpar.age < 35 THEN '30-34'::text
                    WHEN cpar.age >= 35 AND cpar.age < 40 THEN '35-39'::text
                    WHEN cpar.age >= 40 AND cpar.age < 45 THEN '40-44'::text
                    WHEN cpar.age >= 45 AND cpar.age < 50 THEN '45-49'::text
                    WHEN cpar.age >= 50 AND cpar.age < 55 THEN '50-54'::text
                    WHEN cpar.age >= 55 AND cpar.age < 60 THEN '55-59'::text
                    WHEN cpar.age >= 60 AND cpar.age < 65 THEN '60-64'::text
                    WHEN cpar.age >= 65 AND cpar.age < 70 THEN '65-69'::text
                    WHEN cpar.age >= 70 AND cpar.age < 75 THEN '70-74'::text
                    WHEN cpar.age >= 75 AND cpar.age < 80 THEN '75-79'::text
                    WHEN cpar.age >= 80 THEN '80+'::text
                    ELSE NULL::text
                END)) sub;
				
-- franz.v_uzis_covid19_vaccination_regions_details source
drop VIEW if exists analytic.v_uzis_covid19_vaccination_regions_details;
CREATE OR REPLACE VIEW analytic.v_uzis_covid19_vaccination_regions_details
AS SELECT count(*) AS pocet_davek,
    v.datum_vakcinace,
        CASE
            WHEN v.datum_vakcinace >= (CURRENT_DATE - '7 days'::interval) AND v.datum_vakcinace < CURRENT_DATE THEN 'posledních 7 dnů'::text
            WHEN v.datum_vakcinace >= (CURRENT_DATE - '14 days'::interval) AND v.datum_vakcinace < (CURRENT_DATE - '7 days'::interval) THEN 'predchozích 7 dnů'::text
            ELSE NULL::text
        END AS "7_14_dnu",
    date_trunc('week'::text, v.datum_vakcinace)::date AS tyden,
        CASE
            WHEN date_trunc('week'::text, v.datum_vakcinace)::date = date_trunc('week'::text, CURRENT_DATE::timestamp with time zone)::date THEN 'posledni nedokonceny tyden'::text
            ELSE 'cely tyden'::text
        END AS tyden_filter_posledni,
    v.updated_at AS datum_aktualizace,
    v.vakcina,
        CASE
            WHEN v.vekova_skupina = '0-17'::text THEN '12-17'::text
            ELSE v.vekova_skupina
        END AS vekova_skupina,
        CASE
            WHEN v.vekova_skupina = '0-17'::text THEN 1
            WHEN v.vekova_skupina = '18-24'::text THEN 2
            WHEN v.vekova_skupina = '25-29'::text THEN 3
            WHEN v.vekova_skupina = '30-34'::text THEN 4
            WHEN v.vekova_skupina = '35-39'::text THEN 5
            WHEN v.vekova_skupina = '40-44'::text THEN 6
            WHEN v.vekova_skupina = '45-49'::text THEN 7
            WHEN v.vekova_skupina = '50-54'::text THEN 8
            WHEN v.vekova_skupina = '55-59'::text THEN 9
            WHEN v.vekova_skupina = '60-64'::text THEN 10
            WHEN v.vekova_skupina = '65-69'::text THEN 11
            WHEN v.vekova_skupina = '70-74'::text THEN 12
            WHEN v.vekova_skupina = '75-79'::text THEN 13
            WHEN v.vekova_skupina = '80+'::text THEN 14
            WHEN v.vekova_skupina = 'Neznámá'::text THEN 15
            ELSE 15
        END AS vekova_skupina_order,
        CASE
            WHEN v.indikace_zdravotnik = 1 THEN 'Zdravotníci'::text
            WHEN v.indikace_socialni_sluzby = 1 THEN 'Socilání služby'::text
            WHEN v.indikace_pedagog = 1 THEN 'Pedagogové'::text
            WHEN v.indikace_skolstvi_ostatni = 1 THEN 'Školství ostatní'::text
            WHEN v.chronicke_onemocneni = 1 THEN 'Chronicky nemocní'::text
            WHEN v.bezpecnostni_infrastruktura = 1 THEN 'Bezpečnostní infrastruktura'::text
            ELSE NULL::text
        END AS indikacni_skupina,
    v.kraj_nazev AS kraj_ockovani,
        CASE
            WHEN c.kraj_nazev IS NULL THEN 'Neznámý'::text
            ELSE c.kraj_nazev
        END AS kraj_bydliste,
    v.pohlavi,
    v.poradi_davky,
    v.zarizeni_nazev,
    concat(v.kraj_kod, '_',
        CASE
            WHEN v.vekova_skupina = '0-17'::text THEN '12-17'::text
            ELSE v.vekova_skupina
        END) AS kraj_ockovani_vek_id,
    concat(c.kraj_kod, '_',
        CASE
            WHEN v.vekova_skupina = '0-17'::text THEN '12-17'::text
            ELSE v.vekova_skupina
        END) AS kraj_bydliste_vek_id
   FROM uzis.covid19_vaccination_regional_details v
     LEFT JOIN uzis.codebook_regions_orps c ON v.orp_bydliste = c.orp_nazev
  GROUP BY v.datum_vakcinace, v.updated_at, v.vakcina, v.vekova_skupina, v.kraj_nazev, c.kraj_nazev, v.pohlavi, v.poradi_davky, v.zarizeni_nazev, v.zrizovatel, v.kraj_kod, c.kraj_kod, (
        CASE
            WHEN v.indikace_zdravotnik = 1 THEN 'Zdravotníci'::text
            WHEN v.indikace_socialni_sluzby = 1 THEN 'Socilání služby'::text
            WHEN v.indikace_pedagog = 1 THEN 'Pedagogové'::text
            WHEN v.indikace_skolstvi_ostatni = 1 THEN 'Školství ostatní'::text
            WHEN v.chronicke_onemocneni = 1 THEN 'Chronicky nemocní'::text
            WHEN v.bezpecnostni_infrastruktura = 1 THEN 'Bezpečnostní infrastruktura'::text
            ELSE NULL::text
        END);		

-- franz.v_uzis_covid19_vaccination_regions_vaccination_points source
drop VIEW if exists analytic.v_uzis_covid19_vaccination_regions_vaccination_points;
CREATE OR REPLACE VIEW analytic.v_uzis_covid19_vaccination_regions_vaccination_points
AS SELECT v.zarizeni_kod,
        CASE
            WHEN (v.zarizeni_kod IN ( SELECT DISTINCT covid19_vaccination_points.nrpzs_kod
               FROM uzis.covid19_vaccination_points)) THEN 'Registrované očkovací místo'::text
            ELSE 'Ostatní'::text
        END AS typ_mista,
    v.zarizeni_nazev,
    v.kraj_nazev,
    count(*) FILTER (WHERE v.poradi_davky = 1) AS pocet_prvnich_davek,
    count(*) FILTER (WHERE v.poradi_davky = 2) AS pocet_druhych_davek,
    count(*) AS pocet_davek_celkem,
    count(*) FILTER (WHERE v.datum_vakcinace >= (CURRENT_DATE - '7 days'::interval) AND v.datum_vakcinace < CURRENT_DATE) AS pocet_davek_poslednich_7_dnu,
    count(*) FILTER (WHERE v.datum_vakcinace >= (CURRENT_DATE - '14 days'::interval) AND v.datum_vakcinace < (CURRENT_DATE - '7 days'::interval)) AS pocet_davek_predchozich_7_dnu,
    count(*) FILTER (WHERE v.vakcina = 'Comirnaty'::text) AS pocet_davek_pfizer,
    count(*) FILTER (WHERE v.vakcina = 'Spikevax'::text) AS pocet_davek_moderna,
    count(*) FILTER (WHERE v.vakcina = 'VAXZEVRIA'::text) AS pocet_davek_astra,
    count(*) FILTER (WHERE v.vakcina = 'COVID-19 Vaccine Janssen'::text) AS pocet_davek_jansen,
    count(*) FILTER (WHERE v.vakcina = 'Comirnaty'::text AND v.poradi_davky = 1) AS pocet_prvnich_davek_pfizer,
    count(*) FILTER (WHERE v.vakcina = 'Spikevax'::text AND v.poradi_davky = 1) AS pocet_prvnich_davek_moderna,
    count(*) FILTER (WHERE v.vakcina = 'VAXZEVRIA'::text AND v.poradi_davky = 1) AS pocet_prvnich_davek_astra,
    count(*) FILTER (WHERE v.vakcina = 'COVID-19 Vaccine Janssen'::text AND v.poradi_davky = 1) AS pocet_prvnich_davek_jansen,
    count(*) FILTER (WHERE v.vakcina = 'Comirnaty'::text AND v.poradi_davky = 2) AS pocet_druhych_davek_pfizer,
    count(*) FILTER (WHERE v.vakcina = 'Spikevax'::text AND v.poradi_davky = 2) AS pocet_druhych_davek_moderna,
    count(*) FILTER (WHERE v.vakcina = 'VAXZEVRIA'::text AND v.poradi_davky = 2) AS pocet_druhych_davek_astra,
    count(*) FILTER (WHERE v.vakcina = 'COVID-19 Vaccine Janssen'::text AND v.poradi_davky = 2) AS pocet_druhych_davek_jansen,
    count(*) FILTER (WHERE v.pohlavi = 'Z'::text) AS pocet_davek_zeny,
    count(*) FILTER (WHERE v.pohlavi = 'M'::text) AS pocet_davek_muzi,
    count(*) FILTER (WHERE v.vekova_skupina = '80+'::text) AS pocet_davek_80_plus,
    count(*) FILTER (WHERE v.kraj_nazev <> c.kraj_nazev) AS pocet_davek_mimo_kraj_bydliste
   FROM uzis.covid19_vaccination_regional_details v
     LEFT JOIN uzis.codebook_regions_orps c ON v.orp_bydliste = c.orp_nazev
  GROUP BY v.zarizeni_kod, v.zarizeni_nazev, v.kraj_nazev;		
  
  
  -- franz.v_uzis_covid19_vaccination_prague_districts source

CREATE OR REPLACE VIEW analytic.v_uzis_covid19_vaccination_prague_districts
AS SELECT v.mestska_cast,
    p.spravni_obvod,
        CASE
            WHEN v.vekova_skupina = '0-17'::text THEN '12-17'::text
            ELSE v.vekova_skupina
        END AS vekova_skupina,
    p.populace,
    count(*) FILTER (WHERE v.poradi_davky = 1) AS prvni_davky,
    count(*) FILTER (WHERE v.poradi_davky = 2) AS druhe_davky,
    count(*) FILTER (WHERE v.poradi_davky = 1)::double precision / p.populace::double precision AS prvni_davky_podil,
    count(*) FILTER (WHERE v.poradi_davky = 2)::double precision / p.populace::double precision AS druhe_davky_podil
   FROM uzis.covid19_vaccination_prague_details v
     LEFT JOIN ( SELECT population_prague_age_districts.mestska_cast,
            population_prague_age_districts.spravni_obvod,
                CASE
                    WHEN population_prague_age_districts.vekova_skupina = ANY (ARRAY['10-14'::text, '15-19'::text]) THEN '12-17'::text
                    WHEN population_prague_age_districts.vekova_skupina = '20-24'::text THEN '18-24'::text
                    WHEN population_prague_age_districts.vekova_skupina = ANY (ARRAY['80-84'::text, '85'::text]) THEN '80+'::text
                    ELSE population_prague_age_districts.vekova_skupina
                END AS vekova_skupina,
            sum(population_prague_age_districts.populace) AS populace
           FROM uzis.population_prague_age_districts
          GROUP BY population_prague_age_districts.mestska_cast, population_prague_age_districts.spravni_obvod, (
                CASE
                    WHEN population_prague_age_districts.vekova_skupina = ANY (ARRAY['10-14'::text, '15-19'::text]) THEN '12-17'::text
                    WHEN population_prague_age_districts.vekova_skupina = '20-24'::text THEN '18-24'::text
                    WHEN population_prague_age_districts.vekova_skupina = ANY (ARRAY['80-84'::text, '85'::text]) THEN '80+'::text
                    ELSE population_prague_age_districts.vekova_skupina
                END)) p ON v.mestska_cast = p.mestska_cast AND
        CASE
            WHEN v.vekova_skupina = '0-17'::text THEN '12-17'::text
            ELSE v.vekova_skupina
        END = p.vekova_skupina
  GROUP BY v.mestska_cast, p.spravni_obvod, v.vekova_skupina, p.populace;