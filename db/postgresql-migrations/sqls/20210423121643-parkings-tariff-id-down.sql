CREATE SEQUENCE public.parkings_tariffs_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER TABLE public.parkings_tariffs ADD COLUMN id int NOT NULL UNIQUE DEFAULT nextval('parkings_tariffs_id_seq'::regclass);
