-- Function: public.retention(character varying, character varying, integer)

CREATE OR REPLACE FUNCTION public.retention(
    tab character varying,
    col character varying,
    hours integer)
  RETURNS integer AS
$BODY$

declare
	vystup varchar;
	txt_sql varchar;
	c integer;
	cur_time timestamp;
	kontrola integer;
	dat_typ varchar;
begin

-- kontrola existence tabulky
if not exists (SELECT * FROM pg_tables WHERE tablename=tab)
then
	RAISE 'Table % does not exist.', tab USING ERRCODE = '23505';
end if;
-- kontrola na existenci sloupce
if not  exists
	(SELECT * FROM information_schema.columns
		WHERE
  		table_name   = tab
  		and column_name = col
	)
then
	RAISE 'Column % does not exist in %.', col,tab USING ERRCODE = '23505';
end if;

-- kontrola datového typu
SELECT data_type  into dat_typ FROM information_schema.columns
WHERE
	table_name   = tab
	and column_name = col;

if not(dat_typ like 'timestamp%')  then
	RAISE 'Column %(%) does not data type timestamp (%).', col,tab,dat_typ USING ERRCODE = '23505';
end if;

-- kontrola hodnoty hodin
if hours <= 0 then
	RAISE 'Hours (%) does not positive.', hours USING ERRCODE = '23505';
end if;

	cur_time = current_timestamp;
	cur_time = cur_time - hours* interval '1 hours';

	--txt_sql = 'Select count(*) from '||tab||' where '||col||'<'''||cur_time||'''';
	txt_sql = 'DELETE FROM '||tab||' where '||col||'<'''||cur_time||'''; --returning count(*)';
	execute txt_sql; --into c;
	GET DIAGNOSTICS c := ROW_COUNT;
	--select result.rowCount into c;
	return c;
end;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;


-- Function: public.count_rows(text, text)

CREATE OR REPLACE FUNCTION public.count_rows(
    schema text,
    tablename text)
  RETURNS integer AS
$BODY$ DECLARE result integer; query varchar; BEGIN query := 'SELECT count(1) FROM ' || schema || '.' || tablename; execute query into result; return result; END; $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
