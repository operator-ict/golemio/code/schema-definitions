-- Function: public.import_vehiclepositions_stops(bigint, json, character varying)

CREATE OR REPLACE FUNCTION public.import_vehiclepositions_stops(
    IN p_batch_id bigint,
    IN p_data json,
    IN p_worker_name character varying,
    OUT x_inserted json,
    OUT x_updated json)
  RETURNS record AS
$BODY$


declare
	p_cas timestamp;
begin
/*
fce provádí merge p_data do tabulky vehicleposition_trips
výstup do dvou polí (záznamy insertované a updatované)
*/
	p_cas = now();

--create temp table mytemp(c varchar);

--with rows as (
update public.vehiclepositions_stops tar
 set
			arrival_time = sor.arrival_time,
			arrival_timestamp = sor.arrival_timestamp,
			cis_stop_id = sor.cis_stop_id,
			cis_stop_platform_code = sor.cis_stop_platform_code,
			--cis_stop_sequence = sor.cis_stop_sequence,  --PK
			delay_arrival = sor.delay_arrival,
			delay_departure = sor.delay_departure,
			delay_type = sor.delay_type,
			departure_time = sor.departure_time,
			departure_timestamp = sor.departure_timestamp,
			--trips_id = sor.trips_id,  --PK

			--
			update_batch_id = p_batch_id,
			updated_at = now(),	--p_cas,
			updated_by = p_worker_name
from
	(
		select
			cast(json_array_elements(data)->>'arrival_time' as time without time zone) arrival_time
			,cast(json_array_elements(data)->>'arrival_timestamp' as bigint) arrival_timestamp
			,cast(json_array_elements(data)->>'cis_stop_id' as integer) cis_stop_id
			,cast(json_array_elements(data)->>'cis_stop_platform_code' as varchar) cis_stop_platform_code
			,cast(json_array_elements(data)->>'cis_stop_sequence' as integer) cis_stop_sequence
			,cast(json_array_elements(data)->>'delay_arrival' as integer) delay_arrival
			,cast(json_array_elements(data)->>'delay_departure' as integer) delay_departure
			,cast(json_array_elements(data)->>'delay_type' as integer) delay_type
			,cast(json_array_elements(data)->>'departure_time' as time without time zone) departure_time
			,cast(json_array_elements(data)->>'departure_timestamp' as bigint) departure_timestamp
			,cast(json_array_elements(data)->>'trips_id' as varchar) trips_id
		from (select p_data as data) a
	) sor
	where
		tar.cis_stop_sequence = sor.cis_stop_sequence
		and tar.trips_id = sor.trips_id

--	RETURNING  tar.cis_stop_id,tar.cis_stop_platform_code
--	)
--INSERT INTO mytemp (c)
--SELECT id
--FROM rows
;

--select array_to_json(array_agg(row)) into x_updated
--from (select c from mytemp) row;

--truncate table mytemp;

--with rows as (
	insert into public.vehiclepositions_stops
	(
		arrival_time,arrival_timestamp,cis_stop_id,cis_stop_platform_code,
		cis_stop_sequence,delay_arrival,delay_departure,delay_type,departure_time,
		departure_timestamp,trips_id,
		--
		create_batch_id, created_at, created_by
		--, update_batch_id, updated_time, updated_by
	)
	select
		c.arrival_time,c.arrival_timestamp,c.cis_stop_id,c.cis_stop_platform_code,
		c.cis_stop_sequence,c.delay_arrival,c.delay_departure,c.delay_type,c.departure_time,
		c.departure_timestamp,c.trips_id,
		--
		c.create_batch_id, c.created_at, c.created_by
	from (
	select b.*
		,row_number() over(partition by b.cis_stop_sequence,b.trips_id order by b.delay_type desc) rn
	from(
		select

			cast(json_array_elements(data)->>'arrival_time' as time without time zone) arrival_time
			,cast(json_array_elements(data)->>'arrival_timestamp' as bigint) arrival_timestamp
			,cast(json_array_elements(data)->>'cis_stop_id' as integer) cis_stop_id
			,cast(json_array_elements(data)->>'cis_stop_platform_code' as varchar) cis_stop_platform_code
			,cast(json_array_elements(data)->>'cis_stop_sequence' as integer) cis_stop_sequence
			,cast(json_array_elements(data)->>'delay_arrival' as integer) delay_arrival
			,cast(json_array_elements(data)->>'delay_departure' as integer) delay_departure
			,cast(json_array_elements(data)->>'delay_type' as integer) delay_type
			,cast(json_array_elements(data)->>'departure_time' as time without time zone) departure_time
			,cast(json_array_elements(data)->>'departure_timestamp' as bigint) departure_timestamp
			,cast(json_array_elements(data)->>'trips_id' as varchar) trips_id
			,p_batch_id create_batch_id,now() created_at,p_worker_name created_by
		from (select p_data as data) a
	) b
	left join public.vehiclepositions_stops bb
		on
		b.cis_stop_sequence = bb.cis_stop_sequence
		and b.trips_id =bb.trips_id
	where bb.cis_stop_id is null
	)c where rn = 1

	--RETURNING  public.vehiclepositions_trips.id
--	)
--INSERT INTO mytemp (c)
--SELECT id
--FROM rows
;

--select array_to_json(array_agg(row)) into x_inserted
--from (select c from mytemp) row;

--drop table mytemp;

end;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
