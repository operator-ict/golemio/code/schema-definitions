DROP VIEW analytic.v_covid19_airbnb;

-- analytic.v_airbnb_occupancy_monthly_materialized source
CREATE MATERIALIZED VIEW analytic.v_airbnb_occupancy_monthly_materialized
TABLESPACE pg_default
AS WITH constants AS (
         SELECT 0.55 AS reviewers_ratio,
            3.2 AS avg_accomodation_duration
        ), yearly_occupancy AS (
         SELECT airbnb_occupancy.room_id,
            "left"(airbnb_occupancy.yearmon::text, 4) AS o_year,
            (( SELECT constants.avg_accomodation_duration
                   FROM constants))::double precision * sum(airbnb_occupancy.reviews_count) / (( SELECT constants.reviewers_ratio
                   FROM constants))::double precision AS yearly_estimated_days_occupied
           FROM python.airbnb_occupancy
          GROUP BY airbnb_occupancy.room_id, ("left"(airbnb_occupancy.yearmon::text, 4))
        ), last_12m_occupancy AS (
         SELECT airbnb_occupancy.room_id,
            (( SELECT constants.avg_accomodation_duration
                   FROM constants))::double precision * sum(airbnb_occupancy.reviews_count) / (( SELECT constants.reviewers_ratio
                   FROM constants))::double precision AS yearly_estimated_days_occupied
           FROM python.airbnb_occupancy
          WHERE concat(airbnb_occupancy.yearmon, '-01')::date >= (date_trunc('month'::text, airbnb_occupancy.created_at) - '1 year'::interval) AND concat(airbnb_occupancy.yearmon, '-01')::date < date_trunc('month'::text, airbnb_occupancy.created_at)
          GROUP BY airbnb_occupancy.room_id
        )
 SELECT o.room_id,
    "left"(o.yearmon::text, 4) AS o_year,
    concat(o.yearmon, '-01')::date AS o_month,
    l.nazev_mc,
    l.offer_type,
    l.room_created,
    l.open_closed,
    l.ended_at,
    o.reviews_count / (( SELECT constants.reviewers_ratio
           FROM constants))::double precision AS stays_count,
    (( SELECT constants.avg_accomodation_duration
           FROM constants))::double precision * o.reviews_count / (( SELECT constants.reviewers_ratio
           FROM constants))::double precision AS estimated_days_occupied,
    date_part('day'::text, concat(o.yearmon, '-01')::date + '1 mon'::interval - concat(o.yearmon, '-01')::date::timestamp without time zone) AS days_in_month,
    o.reviews_count,
    o.days_occupied,
    0::numeric AS esimated_price_per_night,
    o.mean_price,
        CASE
            WHEN yo.yearly_estimated_days_occupied > 60::numeric::double precision THEN 'Pravidelné'::text
            ELSE 'Příležitostní'::text
        END AS regularity2,
    l.user_id,
    bf.bytovy_fond,
    l.ended_at AS ukonceno,
        CASE
            WHEN yo.yearly_estimated_days_occupied > 60::numeric::double precision AND l.offer_type = 'Celý dům/byt'::text THEN 'Pravidelné, celý byt/dům'::text
            ELSE 'Sdílená ekonomika'::text
        END AS offer_type_group,
        CASE
            WHEN concat(o.yearmon, '-01')::date >= (date_trunc('month'::text, o.created_at) - '1 year'::interval) AND concat(o.yearmon, '-01')::date < date_trunc('month'::text, o.created_at) THEN 'Y'::text
            ELSE 'N'::text
        END AS last_12m,
    l.latitude,
    l.longitude,
    o.created_at AS record_created_at,
        CASE
            WHEN concat(o.yearmon, '-01')::date >= (date_trunc('month'::text, o.created_at) - '1 year'::interval) AND concat(o.yearmon, '-01')::date < date_trunc('month'::text, o.created_at) THEN true
            ELSE false
        END AS last_12_months,
        CASE
            WHEN lo.yearly_estimated_days_occupied > 60::numeric::double precision THEN 'Pravidelné'::text
            ELSE 'Příležitostní'::text
        END AS regularity_last_12m,
        CASE
            WHEN lo.yearly_estimated_days_occupied > 60::numeric::double precision AND l.offer_type = 'Celý dům/byt'::text THEN 'Pravidelné, celý byt/dům'::text
            ELSE 'Sdílená ekonomika'::text
        END AS offer_type_group_last_12m
   FROM python.airbnb_occupancy o
     JOIN analytic.v_airbnb_listings l ON o.room_id::text = l.room_id::text
     LEFT JOIN yearly_occupancy yo ON yo.room_id::text = o.room_id::text AND yo.o_year = "left"(o.yearmon::text, 4)
     LEFT JOIN python.airbnb_apartment_count bf ON l.nazev_mc::text = bf.nazev_mc::text
     LEFT JOIN last_12m_occupancy lo ON lo.room_id::text = o.room_id::text
  WHERE concat(o.yearmon, '-01')::date >= l.room_created AND concat(o.yearmon, '-01')::date < date_trunc('month'::text, o.created_at) AND concat(o.yearmon, '-01')::date >= '2017-01-01'::date AND l.nazev_mc::text <> ''::text AND l.nazev_mc IS NOT NULL AND o.reviews_count > 0::numeric::double precision
WITH DATA;

-- analytic.v_airbnb_occupancy_monthly source

CREATE OR REPLACE VIEW analytic.v_airbnb_occupancy_monthly
AS SELECT v_airbnb_occupancy_monthly.room_id,
    v_airbnb_occupancy_monthly.o_year,
    v_airbnb_occupancy_monthly.o_month,
    v_airbnb_occupancy_monthly.nazev_mc,
    v_airbnb_occupancy_monthly.offer_type,
    v_airbnb_occupancy_monthly.room_created,
    v_airbnb_occupancy_monthly.open_closed,
    v_airbnb_occupancy_monthly.ended_at,
    v_airbnb_occupancy_monthly.stays_count,
    v_airbnb_occupancy_monthly.estimated_days_occupied,
    v_airbnb_occupancy_monthly.days_in_month,
    v_airbnb_occupancy_monthly.reviews_count,
    v_airbnb_occupancy_monthly.days_occupied,
    v_airbnb_occupancy_monthly.esimated_price_per_night,
    v_airbnb_occupancy_monthly.mean_price,
    v_airbnb_occupancy_monthly.regularity2,
    v_airbnb_occupancy_monthly.user_id,
    v_airbnb_occupancy_monthly.bytovy_fond,
    v_airbnb_occupancy_monthly.ukonceno,
    v_airbnb_occupancy_monthly.offer_type_group,
    v_airbnb_occupancy_monthly.last_12m,
    v_airbnb_occupancy_monthly.latitude,
    v_airbnb_occupancy_monthly.longitude,
    v_airbnb_occupancy_monthly.record_created_at,
    v_airbnb_occupancy_monthly.last_12_months,
    v_airbnb_occupancy_monthly.regularity_last_12m,
    v_airbnb_occupancy_monthly.offer_type_group_last_12m
   FROM analytic.v_airbnb_occupancy_monthly_materialized v_airbnb_occupancy_monthly;