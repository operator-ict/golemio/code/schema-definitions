ALTER TABLE public.parkings_measurements_part DROP CONSTRAINT parkings_measurements_part_pk;
ALTER TABLE public.parkings_measurements_part ADD CONSTRAINT parkings_measurements_part_pk PRIMARY KEY (source_id, date_modified);
