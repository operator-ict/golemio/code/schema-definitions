-- Table: public.ropidgtfs_agency

CREATE TABLE public.ropidgtfs_agency
(
  agency_fare_url character varying(255),
  agency_id character varying(255) NOT NULL,
  agency_lang character varying(255),
  agency_name character varying(255),
  agency_phone character varying(255),
  agency_timezone character varying(255),
  agency_url character varying(255),
  created_time time without time zone,
  last_modify time without time zone,
  CONSTRAINT ropidgtfs_agency_pkey PRIMARY KEY (agency_id)
)
WITH (
  OIDS=FALSE
);

-- Table: public.ropidgtfs_calendar

CREATE TABLE public.ropidgtfs_calendar
(
  created_time time without time zone,
  end_date character varying(255),
  friday integer,
  last_modify time without time zone,
  monday integer,
  saturday integer,
  service_id character varying(255) NOT NULL,
  start_date character varying(255),
  sunday integer,
  thursday integer,
  tuesday integer,
  wednesday integer,
  CONSTRAINT ropidgtfs_calendar_pkey1 PRIMARY KEY (service_id)
)
WITH (
  OIDS=FALSE
);

-- Table: public.ropidgtfs_calendar_dates

CREATE TABLE public.ropidgtfs_calendar_dates
(
  created_time time without time zone,
  date character varying(255) NOT NULL,
  exception_type integer,
  last_modify time without time zone,
  service_id character varying(255) NOT NULL,
  CONSTRAINT ropidgtfs_calendar_dates_pkey1 PRIMARY KEY (date, service_id)
)
WITH (
  OIDS=FALSE
);

-- Table: public.ropidgtfs_cis_stop_groups

CREATE TABLE public.ropidgtfs_cis_stop_groups
(
  "avgJtskX" double precision,
  "avgJtskY" double precision,
  "avgLat" double precision,
  "avgLon" double precision,
  cis integer NOT NULL,
  "districtCode" character varying(255),
  "fullName" character varying(255),
  "idosCategory" character varying(255),
  "idosName" character varying(255),
  municipality character varying(255),
  name character varying(255),
  node integer,
  time_created timestamp with time zone,
  "uniqueName" character varying(255),
  CONSTRAINT ropidgtfs_cis_stop_groups_pkey PRIMARY KEY (cis)
)
WITH (
  OIDS=FALSE
);

-- Table: public.ropidgtfs_cis_stops

CREATE TABLE public.ropidgtfs_cis_stops
(
  "altIdosName" character varying(255),
  cis integer,
  id character varying(255) NOT NULL,
  "jtskX" double precision,
  "jtskY" double precision,
  lat double precision,
  lon double precision,
  platform character varying(255),
  time_created timestamp with time zone,
  "wheelchairAccess" character varying(255),
  zone character varying(255),
  CONSTRAINT ropidgtfs_cis_stops_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

-- Sequence: public.ropidgtfs_metadata_id_seq

CREATE SEQUENCE public.ropidgtfs_metadata_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

-- Table: public.ropidgtfs_metadata

CREATE TABLE public.ropidgtfs_metadata
(
  id integer NOT NULL DEFAULT nextval('ropidgtfs_metadata_id_seq'::regclass),
  dataset character varying(255),
  key character varying(255),
  type character varying(255),
  value character varying(255),
  version integer,
  CONSTRAINT ropidgtfs_metadata_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

-- Table: public.ropidgtfs_routes

CREATE TABLE public.ropidgtfs_routes
(
  agency_id character varying(255),
  created_time time without time zone,
  is_night character varying(255),
  last_modify time without time zone,
  route_color character varying(255),
  route_desc character varying(255),
  route_id character varying(255) NOT NULL,
  route_long_name character varying(255),
  route_short_name character varying(255),
  route_text_color character varying(255),
  route_type character varying(255),
  route_url character varying(255),
  CONSTRAINT ropidgtfs_routes_pkey PRIMARY KEY (route_id)
)
WITH (
  OIDS=FALSE
);

-- Table: public.ropidgtfs_shapes

CREATE TABLE public.ropidgtfs_shapes
(
  created_time time without time zone,
  last_modify time without time zone,
  shape_dist_traveled double precision,
  shape_id character varying(255) NOT NULL,
  shape_pt_lat double precision,
  shape_pt_lon double precision,
  shape_pt_sequence integer NOT NULL,
  CONSTRAINT ropidgtfs_shapes_pkey PRIMARY KEY (shape_id, shape_pt_sequence)
)
WITH (
  OIDS=FALSE
);

-- Table: public.ropidgtfs_stop_times

CREATE TABLE public.ropidgtfs_stop_times
(
  arrival_time character varying(255),
  arrival_time_seconds integer,
  created_time time without time zone,
  departure_time character varying(255),
  departure_time_seconds integer,
  drop_off_type character varying(255),
  last_modify time without time zone,
  pickup_type character varying(255),
  shape_dist_traveled double precision,
  stop_headsign character varying(255),
  stop_id character varying(255),
  stop_sequence integer NOT NULL,
  trip_id character varying(255) NOT NULL,
  CONSTRAINT ropidgtfs_stop_times_pkey1 PRIMARY KEY (stop_sequence, trip_id)
)
WITH (
  OIDS=FALSE
);

-- Table: public.ropidgtfs_stops

CREATE TABLE public.ropidgtfs_stops
(
  created_time time without time zone,
  last_modify time without time zone,
  location_type integer,
  parent_station character varying(255),
  platform_code character varying(255),
  stop_id character varying(255) NOT NULL,
  stop_lat double precision,
  stop_lon double precision,
  stop_name character varying(255),
  stop_url character varying(255),
  wheelchair_boarding integer,
  zone_id character varying(255),
  CONSTRAINT ropidgtfs_stops_pkey PRIMARY KEY (stop_id)
)
WITH (
  OIDS=FALSE
);

-- Table: public.ropidgtfs_trips

CREATE TABLE public.ropidgtfs_trips
(
  bikes_allowed integer,
  block_id character varying(255),
  created_time time without time zone,
  direction_id integer,
  exceptional integer,
  last_modify time without time zone,
  route_id character varying(255),
  service_id character varying(255),
  shape_id character varying(255),
  trip_headsign character varying(255),
  trip_id character varying(255) NOT NULL,
  wheelchair_accessible integer,
  CONSTRAINT ropidgtfs_trips_pkey PRIMARY KEY (trip_id)
)
WITH (
  OIDS=FALSE
);
