-- martinamichlova.v_route_live source

CREATE OR REPLACE VIEW analytic.v_route_live
AS SELECT wrl.route_id,
    wrl.update_time,
    wrl."time",
    wrl.length,
    wrl.historic_time,
    wrl.jam_level,
    wrl.create_batch_id,
    wrl.created_at,
    wrl.created_by,
    wrl.update_batch_id,
    wrl.updated_at,
    wrl.updated_by
   FROM wazett_route_lives wrl
   WHERE (wrl.route_id = ANY (ARRAY[24949, 24953, 24962, 24924, 24925, 24950, 24951, 24952, 24954, 24955, 24956, 24957, 24958, 24960, 24961, 24963, 24964, 24965, 24966, 24967, 24968, 24969]))
   and  wrl.update_time >= (
  SELECT max(wazett_route_lives.update_time) - extract (epoch from interval '1 year')*1000 AS max FROM wazett_route_lives) ;
 