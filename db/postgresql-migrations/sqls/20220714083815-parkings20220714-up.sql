drop VIEW analytic.v_parkomats_sales;
drop VIEW analytic.v_park_and_ride_capacities;
drop VIEW analytic.v_park_and_ride;

DROP TABLE public.parkings_location;
DROP TABLE public.parkings;
DROP TABLE public.parkings_measurements_actual;
drop table public.parkings_measurements_part;
DROP TABLE public.parkings_occupancies;
DROP TABLE public.parkings_tariffs;
DROP TABLE public.parkomats;