CREATE OR REPLACE VIEW public.v_ropidgtfs_trips_shapes_view AS
 SELECT ropidgtfs_trips.bikes_allowed,
    ropidgtfs_trips.block_id,
    ropidgtfs_trips.direction_id,
    ropidgtfs_trips.exceptional,
    ropidgtfs_trips.route_id,
    ropidgtfs_trips.service_id,
    ropidgtfs_trips.shape_id,
    ropidgtfs_trips.trip_headsign,
    ropidgtfs_trips.trip_id,
    ropidgtfs_trips.wheelchair_accessible,
    shapes.shape_dist_traveled AS shapes_shape_dist_traveled,
    shapes.shape_id AS shapes_shape_id,
    shapes.shape_pt_lat AS shapes_shape_pt_lat,
    shapes.shape_pt_lon AS shapes_shape_pt_lon,
    shapes.shape_pt_sequence AS shapes_shape_pt_sequence
   FROM ropidgtfs_trips ropidgtfs_trips
     LEFT JOIN ropidgtfs_shapes shapes ON ropidgtfs_trips.shape_id::text = shapes.shape_id::text
  ORDER BY shapes.shape_pt_sequence;
