DROP TABLE IF EXISTS public.parkings;
DROP SEQUENCE public.parkings_id_seq;

DROP TABLE IF EXISTS public.parkings_measurements;
DROP SEQUENCE public.parkings_measurements_id_seq;
