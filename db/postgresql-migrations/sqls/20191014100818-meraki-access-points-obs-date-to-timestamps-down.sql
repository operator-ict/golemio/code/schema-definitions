--merakiaccesspoints_observations column departure_timestamp
ALTER TABLE public.merakiaccesspoints_observations
ALTER COLUMN timestamp TYPE timestamp with time zone
USING to_timestamp(timestamp/1000);
