CREATE TABLE public.parkings_measurements_part (
--	id bigserial NOT NULL,
	"source" varchar(255) NOT NULL,
	source_id varchar(255) NOT NULL,
	available_spot_number int4 NULL,
	closed_spot_number int4 NULL,
	occupied_spot_number int4 NULL,
	total_spot_number int4 NULL,
	date_modified int8 NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT parkings_measurements_part_pk PRIMARY KEY (source_id,date_modified)
) partition by range(date_modified);

CREATE TABLE parkings_measurements_part_min PARTITION OF parkings_measurements_part
    FOR VALUES FROM (minvalue) TO (1609455600000); -- '2021-01-01'

CREATE TABLE parkings_measurements_part_2021 PARTITION OF parkings_measurements_part
    FOR VALUES FROM (1609455600000) TO (1640991600000); -- '2021-01-01'
    
CREATE TABLE parkings_measurements_part_2022 PARTITION OF parkings_measurements_part
    FOR VALUES FROM (1640991600000) TO (1672527600000); -- '2021-01-01'
    
CREATE TABLE parkings_measurements_part_2023 PARTITION OF parkings_measurements_part
    FOR VALUES FROM (1672527600000) TO (1704063600000); -- '2021-01-01'
 
CREATE TABLE parkings_measurements_part_2024 PARTITION OF parkings_measurements_part
    FOR VALUES FROM (1704063600000) TO (1735686000000); -- '2021-01-01'

CREATE TABLE parkings_measurements_part_2025 PARTITION OF parkings_measurements_part
    FOR VALUES FROM (1735686000000) TO (1767222000000); -- '2021-01-01'
    
CREATE TABLE parkings_measurements_part_max PARTITION OF parkings_measurements_part
    FOR VALUES FROM (1767222000000) TO (maxvalue); -- '2021-01-01'
