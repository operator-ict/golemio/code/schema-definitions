CREATE TABLE python.ukraine_statistics (
	kraj text NULL,
	okres text NULL,
	obec text NULL,
	kod_obce int8 NULL,
	obec_mc text NULL,
	celkem int8 NULL,
	m_do_3 int8 NULL,
	z_do_3 int8 NULL,
	x_do_3 int8 NULL,
	m_do_6 int8 NULL,
	z_do_6 int8 NULL,
	x_do_6 int8 NULL,
	m_do_15 int8 NULL,
	z_do_15 int8 NULL,
	x_do_15 int8 NULL,
	m_do_18 int8 NULL,
	z_do_18 int8 NULL,
	x_do_18 int8 NULL,
	m_do_65 int8 NULL,
	z_do_65 int8 NULL,
	x_do_65 int8 NULL,
	m_sen int8 NULL,
	z_sen int8 NULL,
	x_sen int8 NULL,
	"source" text NULL,
	updated_at timestamptz NULL
);

ALTER TABLE python.ukraine_statistics 
    ADD CONSTRAINT ukraine_statistics_pk PRIMARY KEY (obec_mc, okres, "source");