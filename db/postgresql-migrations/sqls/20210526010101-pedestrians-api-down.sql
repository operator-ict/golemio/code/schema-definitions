DROP VIEW public.v_pedestrians_locations_api;
DROP VIEW public.v_pedestrians_directions_api;


DROP INDEX public.pedestrians_detections_measured_from_index;
DROP INDEX public.pedestrians_detections_measured_to_index;
DROP INDEX public.pedestrians_detections_location_id_index;
DROP INDEX public.pedestrians_detections_direction_id_index;

DROP MATERIALIZED VIEW IF EXISTS public.mv_pedestrians_detections_api;



