
ALTER TABLE ropidgtfs_agency ADD COLUMN agency_email character varying(255);

ALTER TABLE ropidgtfs_stop_times ADD COLUMN timepoint integer;

ALTER TABLE ropidgtfs_stops ADD COLUMN level_id character varying(255);
ALTER TABLE ropidgtfs_stops ADD COLUMN stop_code character varying(255);
ALTER TABLE ropidgtfs_stops ADD COLUMN stop_desc character varying(255);
ALTER TABLE ropidgtfs_stops ADD COLUMN stop_timezone character varying(255);

ALTER TABLE ropidgtfs_trips ADD COLUMN trip_operation_type integer;
ALTER TABLE ropidgtfs_trips ADD COLUMN trip_short_name character varying(255);
