
CREATE OR REPLACE FUNCTION public.ropid_refresh_mv()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
  BEGIN
  REFRESH MATERIALIZED VIEW analytic.v_ropidbi_ticket;
  RETURN NULL;
  END $function$
;


-- DROP TRIGGER ropid_refresh_mv_trg ON public.mos_ma_ticketactivations;

create trigger ropid_refresh_mv_trg after
insert
    or
delete
    or
update
    or
truncate
    on
    public.mos_ma_ticketactivations for each statement execute procedure ropid_refresh_mv();

