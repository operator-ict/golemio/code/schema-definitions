/* Replace with your SQL commands */

-- Odstranění metadat
delete from  meta."extract"
where id_dataset = (select id_dataset from meta.dataset where code_dataset = 'firebase_pidlitacka');

delete from meta.dataset
where code_dataset = 'firebase_pidlitacka';

truncate table analytic.pid_cenik;

drop view analytic.v_ropidbi_litacka_functions; 
drop view analytic.v_ropidbi_firebase_eventy;

DROP TABLE public.firebase_pidlitacka_events;

DROP TABLE public.firebase_pidlitacka_applaunch_par;


DROP TABLE public.firebase_pidlitacka_route;

-- DROP TABLE public.firebase_trasy;

CREATE TABLE public.firebase_trasy (
	datum varchar(255) NOT NULL,
	s_from varchar(255) NOT NULL,
	s_to varchar(255) NOT NULL,
	pocet int4 NOT NULL,
	CONSTRAINT firebase_trasy_pkey PRIMARY KEY (datum, s_from, s_to)
);

DROP TABLE public.firebase_pidlitacka_web_events;
drop table analytic.firebase_pidlitacka_events_table;


drop view if exists analytic.v_ropidbi_ticket_activation_types;
drop view if exists analytic.v_ropidbi_ticket_activation_times;
drop view if exists analytic.v_ropidbi_ticket_activation_location;
drop view if exists analytic.v_ropidbi_ticket_sales;

drop materialized view if exists analytic.v_ropidbi_ticket;

CREATE materialized VIEW analytic.v_ropidbi_ticket
AS SELECT
	a.date AS activation_date,
	a.date::date AS act_date,
	date_trunc('hour'::text, a.date)::time AS act_time,
    a.lat AS act_lat,
    a.lon AS act_lon,
    a.ticket_id,
    CASE
            WHEN a.type::text = 'manualNow'::text THEN 'Ruční aktivace ihned'::text
            WHEN a.type::text = 'manualTime'::text THEN 'Ruční aktivace na daný čas'::text
            WHEN a.type::text = 'purchaseNow'::text THEN 'Nákup s aktivací ihned'::text
            WHEN a.type::text = 'purchaseTime'::text THEN 'Nákup s aktivací na daný čas'::text
            ELSE 'Zatím neaktivováno'::text
        END AS activation_type,
    a.zones,
    p.cptp,
    p.date AS purchase_date,
    p.duration AS duration_min,
    p.tariff_name,
    p.zone_count,
    case
    	when p.tariff_name like '%Praha' then 'Praha'
    	else 'Region'
    	end as oblast
   FROM public.mos_ma_ticketactivations a
     RIGHT JOIN mos_ma_ticketpurchases p ON a.ticket_id = p.ticket_id;
	 
CREATE OR REPLACE VIEW analytic.v_ropidbi_ticket_activation_types
AS SELECT
		act_date,
        activation_type,
        oblast,
    	v_ropidbi_ticket.tariff_name,
    	count(*) AS count
  FROM analytic.v_ropidbi_ticket
  WHERE v_ropidbi_ticket.activation_type::text <> ''::text AND v_ropidbi_ticket.tariff_name::text <> ''::text
  GROUP BY act_date,
        activation_type,
        oblast,
    	v_ropidbi_ticket.tariff_name;

-- activation_times

CREATE OR REPLACE VIEW analytic.v_ropidbi_ticket_activation_times
AS SELECT
		  act_date,
    	  act_time,
    	  v_ropidbi_ticket.tariff_name,
    	  oblast,
          count(*) AS count
   FROM analytic.v_ropidbi_ticket
  GROUP BY act_date,
    	  act_time,
    	  v_ropidbi_ticket.tariff_name,
    	  oblast;

-- location

CREATE OR REPLACE VIEW analytic.v_ropidbi_ticket_activation_location
AS SELECT
		  act_date,
	      round (v_ropidbi_ticket.act_lat, 4) as act_lat,
	      round (v_ropidbi_ticket.act_lon, 4) as act_lon,
	      v_ropidbi_ticket.activation_type,
	      oblast,
          count(*) AS count
   FROM analytic.v_ropidbi_ticket
  WHERE act_date IS NOT NULL
  		AND v_ropidbi_ticket.activation_type IS NOT NULL
  		AND v_ropidbi_ticket.act_lat IS NOT NULL
  		AND v_ropidbi_ticket.act_lon IS NOT NULL
  GROUP BY act_date,
	       round (v_ropidbi_ticket.act_lat, 4),
	       round (v_ropidbi_ticket.act_lon, 4),
	       v_ropidbi_ticket.activation_type,
	       oblast;

-- sales

CREATE OR REPLACE VIEW analytic.v_ropidbi_ticket_sales
AS SELECT v_ropidbi_ticket.purchase_date::date AS purchase_date,
		  tariff_name,
		  oblast,
		  count (*)
          FROM analytic.v_ropidbi_ticket
  WHERE purchase_date::date IS NOT NULL
  		AND tariff_name IS NOT NULL
  		AND v_ropidbi_ticket.act_lat IS NOT NULL
  		AND v_ropidbi_ticket.act_lon IS NOT NULL
  GROUP BY v_ropidbi_ticket.purchase_date::date,
		  tariff_name,
		  oblast;
 

