/* Replace with your SQL commands */
drop view if exists analytic.v_ropidbi_ticket_activation_types;
drop view if exists analytic.v_ropidbi_ticket_activation_times;
drop view if exists analytic.v_ropidbi_ticket_activation_location;
drop view if exists analytic.v_ropidbi_ticket_sales;

drop materialized view if exists analytic.v_ropidbi_ticket;

CREATE materialized VIEW analytic.v_ropidbi_ticket
AS SELECT
	a.date AS activation_date,
	a.date::date AS act_date,
	date_trunc('hour'::text, a.date)::time AS act_time,
    a.lat AS act_lat,
    a.lon AS act_lon,
    a.ticket_id,
    CASE
            WHEN a.type::text = 'manualNow'::text THEN 'Ruční aktivace ihned'::text
            WHEN a.type::text = 'manualTime'::text THEN 'Ruční aktivace na daný čas'::text
            WHEN a.type::text = 'purchaseNow'::text THEN 'Nákup s aktivací ihned'::text
            WHEN a.type::text = 'purchaseTime'::text THEN 'Nákup s aktivací na daný čas'::text
            ELSE 'Zatím neaktivováno'::text
        END AS activation_type,
    a.zones,
    p.cptp,
    p.date AS purchase_date,
    p.duration AS duration_min,
    p.tariff_name,
    p.zone_count,
    CASE
            WHEN a.zones::text = ANY (ARRAY['P'::character varying, 'P,0'::character varying, 'P,0,B'::character varying, '0'::character varying, '0,B'::character varying, 'B'::character varying]::text[]) THEN 'Praha'::text
            ELSE 'Region'::text end as oblast
   FROM public.mos_ma_ticketactivations a
     RIGHT JOIN mos_ma_ticketpurchases p ON a.ticket_id = p.ticket_id;

-- activation_types

CREATE OR REPLACE VIEW analytic.v_ropidbi_ticket_activation_types
AS SELECT
		act_date,
        activation_type,
        oblast,
    	v_ropidbi_ticket.tariff_name,
    	count(*) AS count
  FROM analytic.v_ropidbi_ticket
  WHERE v_ropidbi_ticket.activation_type::text <> ''::text AND v_ropidbi_ticket.tariff_name::text <> ''::text
  GROUP BY act_date,
        activation_type,
        oblast,
    	v_ropidbi_ticket.tariff_name;

-- activation_times

CREATE OR REPLACE VIEW analytic.v_ropidbi_ticket_activation_times
AS SELECT
		  act_date,
    	  act_time,
    	  v_ropidbi_ticket.tariff_name,
    	  oblast,
          count(*) AS count
   FROM analytic.v_ropidbi_ticket
  GROUP BY act_date,
    	  act_time,
    	  v_ropidbi_ticket.tariff_name,
    	  oblast;

-- location

CREATE OR REPLACE VIEW analytic.v_ropidbi_ticket_activation_location
AS SELECT
		  act_date,
	      round (v_ropidbi_ticket.act_lat, 4) as act_lat,
	      round (v_ropidbi_ticket.act_lon, 4) as act_lon,
	      v_ropidbi_ticket.activation_type,
	      oblast,
          count(*) AS count
   FROM analytic.v_ropidbi_ticket
  WHERE act_date IS NOT NULL
  		AND v_ropidbi_ticket.activation_type IS NOT NULL
  		AND v_ropidbi_ticket.act_lat IS NOT NULL
  		AND v_ropidbi_ticket.act_lon IS NOT NULL
  GROUP BY act_date,
	       round (v_ropidbi_ticket.act_lat, 4),
	       round (v_ropidbi_ticket.act_lon, 4),
	       v_ropidbi_ticket.activation_type,
	       oblast;

-- sales

CREATE OR REPLACE VIEW analytic.v_ropidbi_ticket_sales
AS SELECT v_ropidbi_ticket.purchase_date::date AS purchase_date,
		  tariff_name,
		  oblast,
		  count (*)
          FROM analytic.v_ropidbi_ticket
  WHERE purchase_date::date IS NOT NULL
  		AND tariff_name IS NOT NULL
  		AND v_ropidbi_ticket.act_lat IS NOT NULL
  		AND v_ropidbi_ticket.act_lon IS NOT NULL
  GROUP BY v_ropidbi_ticket.purchase_date::date,
		  tariff_name,
		  oblast;

-- trigger


drop trigger ropid_refresh_mv_trg on public.mos_ma_ticketactivations;

drop FUNCTION public.ropid_refresh_mv();