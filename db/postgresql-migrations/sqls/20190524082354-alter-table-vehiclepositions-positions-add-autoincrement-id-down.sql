DROP VIEW IF EXISTS v_vehiclepositions_last_position;

ALTER TABLE vehiclepositions_positions DROP COLUMN id;

DROP SEQUENCE public.vehiclepositions_positions_id_seq;

CREATE OR REPLACE VIEW v_vehiclepositions_last_position
AS
SELECT
    DISTINCT ON ("vehiclepositions_positions"."trips_id") "vehiclepositions_positions".*
    FROM "vehiclepositions_positions" AS "vehiclepositions_positions"
    JOIN "vehiclepositions_trips" AS "vehiclepositions_trips"
    ON ("vehiclepositions_trips"."id" = "vehiclepositions_positions"."trips_id" AND
    "vehiclepositions_trips"."gtfs_trip_id" IS NOT NULL) --změna na gtfs_trip_id
    WHERE "vehiclepositions_positions"."created_at" > (NOW() - INTERVAL '5 min') AND "vehiclepositions_positions"."delay" IS NOT NULL --jen vypočtené delay (což zároveň vyfiltruje tracking=0)
    ORDER BY "vehiclepositions_positions"."trips_id", "vehiclepositions_positions"."created_at" DESC;
