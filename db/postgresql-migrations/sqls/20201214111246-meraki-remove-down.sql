CREATE TABLE public.merakiaccesspoints_tags (
	ap_mac varchar(255) NOT NULL,
	tag varchar(255) NOT NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT merakiaccesspoints_tags_pkey PRIMARY KEY (ap_mac, tag)
);

CREATE TABLE public.merakiaccesspoints_observations (
	id serial NOT NULL,
	ap_mac varchar(255) NULL,
	client_mac varchar(255) NULL,
	ipv4 varchar(255) NULL,
	ipv6 varchar(255) NULL,
	lat float8 NULL,
	lng float8 NULL,
	manufacturer varchar(255) NULL,
	os varchar(255) NULL,
	rssi int4 NULL,
	ssid varchar(255) NULL,
	"timestamp" int8 NULL,
	"type" varchar(255) NULL,
	unc float8 NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT merakiaccesspoints_observations_pkey PRIMARY KEY (id)
);