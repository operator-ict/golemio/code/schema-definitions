-- Table: public.parkomats

CREATE TABLE public.parkomats
(
  transaction_id character varying(255) NOT NULL,
  ticket_bought timestamp with time zone,
  validity_from timestamp with time zone,
  validity_to timestamp with time zone,
  parking_zone character varying(255),
  price numeric NOT NULL,
  channel character varying(255),
  create_batch_id bigint,
  created_at timestamp with time zone,
  created_by character varying(150) COLLATE pg_catalog."default",
  update_batch_id bigint,
  updated_at timestamp with time zone,
  updated_by character varying(150) COLLATE pg_catalog."default",
  CONSTRAINT parkomats_pkey PRIMARY KEY (transaction_id)
)
WITH (
  OIDS=FALSE
);
