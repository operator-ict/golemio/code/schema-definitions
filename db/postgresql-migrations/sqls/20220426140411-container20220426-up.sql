-- analytic.v_containers_picks source
drop VIEW analytic.v_containers_picks;

CREATE OR REPLACE VIEW analytic.v_containers_picks
AS SELECT cp.container_code,
    cc.total_volume,
        CASE
            WHEN cc.trash_type = 1 THEN 411
            WHEN cc.trash_type = 3 THEN 120
            WHEN cc.trash_type = 4 THEN 55
            WHEN cc.trash_type = 5 THEN 76
            WHEN cc.trash_type = 6 THEN 39
            WHEN cc.trash_type = 7 THEN 411
            ELSE NULL::integer
        END AS koeficient,
    cp.pick_at_utc,
    cp.percent_before,
    cp.percent_now,
    cp.event_driven,
    wpd.category
   FROM containers_picks cp
     JOIN containers_containers cc ON cp.container_code::text = cc.code::text
     left join python.waste_assigned_pick_dates wpd on cp.container_code = wpd.container_code and cp.pick_at_utc = wpd.pick_at 
  WHERE (cp.container_code::text IN ( SELECT DISTINCT containers_containers.code
           FROM containers_containers));