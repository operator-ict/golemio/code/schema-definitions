--DETECTORS
DROP TABLE archive.tsk_std;
--DETECTOR MEASUREMENTS
DROP INDEX archive.tsk_std_measurements_detector_id;
DROP INDEX archive.tsk_std_measurements_measurement_type;
DROP TABLE archive.tsk_std_measurements;
--DETECTOR ERRORS
DROP INDEX archive.tsk_std_errors_detector_id;
DROP TABLE archive.tsk_std_errors;
--DETECTOR TYPES
DROP TABLE archive.tsk_std_detector_types;
--MEASUREMENT CLASSES
DROP TABLE archive.tsk_std_class_types;
--STATUS TYPES
DROP TABLE archive.tsk_std_status_types;
--FAILURES TYPES
DROP TABLE archive.tsk_std_failure_types;