CREATE TABLE public.ropid_departures_presets (
    "id" serial NOT NULL,
    "route_name" varchar(100) NOT NULL,
    "api_version" smallint NOT NULL,
    "route" varchar(100) NOT NULL,
    "url_query_params" varchar(250) NOT NULL,
    "note" varchar(1000) NOT NULL,

    create_batch_id bigint,
    created_at timestamptz,
    created_by varchar(150),
    update_batch_id bigint,
    updated_at timestamptz,
    updated_by varchar(150),

    CONSTRAINT "ropid_departures_presets_pkey" PRIMARY KEY (id)
)
