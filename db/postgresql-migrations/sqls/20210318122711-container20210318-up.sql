/* Replace with your SQL commands */

--drop view if exists public.v_containers_picks_export;

CREATE OR REPLACE VIEW public.v_containers_picks_export
AS SELECT DISTINCT cc.code,
    cs.address,
    cs.district,
    cc.trash_type,
    cp.pick_at,
    cp.percent_before,
	cp.percent_now,
	cp.event_driven
   FROM containers_picks cp
     JOIN containers_containers cc ON cc.code::text = cp.container_code::text
     LEFT JOIN containers_stations cs ON cs.code::text = cc.station_code::text;