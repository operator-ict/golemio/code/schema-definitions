CREATE TABLE python.gitlab (
	id int4 NOT NULL,
	iid int4 NULL,
	project_id int4 NOT NULL,
	title text NULL,
	created_at timestamptz NULL,
	closed_at timestamptz NULL,
	weight float8 NULL,
	teamdata_at timestamptz NULL,
	current_sprint_at timestamptz NULL,
	doing_at timestamptz NULL,
	to_verify_at timestamptz NULL,
	to_deploy_at timestamptz NULL,
	to_verify_prod_at timestamptz NULL,
	updated_at timestamptz NOT NULL,
    CONSTRAINT gitlab_issue_pkey PRIMARY KEY (id)
);
