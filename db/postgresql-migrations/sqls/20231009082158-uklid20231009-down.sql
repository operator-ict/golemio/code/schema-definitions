-- analytic.v_airbnb_dim_district source

CREATE OR REPLACE VIEW analytic.v_airbnb_dim_district
AS SELECT DISTINCT d.district_name::character varying(500) AS nazev_mc,
    ltrim("right"(d.district_name::text, 2))::numeric AS order_by
   FROM common.citydistricts d
  WHERE d.district_name::text <> ''::text AND (ltrim("right"(d.district_name::text, 2)) ~ '^[0-9]+$'::text) = true
UNION ALL
 SELECT d.district_name::character varying(500) AS nazev_mc,
    100 + row_number() OVER (ORDER BY d.district_name) AS order_by
   FROM ( SELECT DISTINCT citydistricts.district_name
           FROM common.citydistricts
          WHERE (ltrim("right"(citydistricts.district_name::text, 2)) ~ '^[0-9]+$'::text) = false AND (citydistricts.district_name::text <> ALL (ARRAY[''::character varying::text, 'nazev_mc'::character varying::text]))) d;

-- analytic.v_airbnb_listings source

CREATE OR REPLACE VIEW analytic.v_airbnb_listings
AS WITH last_activity AS (
         SELECT airbnb_occupancy.room_id,
            max(concat(airbnb_occupancy.yearmon, '-01')::date) AS last_activity
           FROM python.airbnb_occupancy
          WHERE airbnb_occupancy.reviews_count > 0::numeric::double precision
          GROUP BY airbnb_occupancy.room_id
        )
 SELECT l.room_id,
    l.name,
        CASE
            WHEN l.roomtype = 'Entire home/apt'::text THEN 'Celý dům/byt'::text
            WHEN l.roomtype = 'Private room'::text THEN 'Celý pokoj'::text
            WHEN l.roomtype = 'Shared room'::text THEN 'Sdílený pokoj'::text
            WHEN l.roomtype = 'Hotel room'::text THEN 'Hotelový pokoj'::text
            ELSE NULL::text
        END AS offer_type,
    l.capacity,
    l.cleaningfee,
    l.overall_rating,
    l.reviews_total,
    l.room_created,
    citydistricts.district_name::character varying(500) AS nazev_mc,
        CASE
            WHEN la.last_activity >= date_trunc('month'::text, now() - '3 mons'::interval)::date THEN 'Otevřená'::text
            ELSE 'Ukončená'::text
        END AS open_closed,
        CASE
            WHEN la.last_activity >= date_trunc('month'::text, now() - '3 mons'::interval)::date THEN NULL::date::timestamp without time zone
            WHEN la.last_activity IS NULL THEN l.room_created + '1 mon'::interval
            ELSE la.last_activity + '1 mon'::interval
        END::date AS ended_at,
    l.user_id,
        CASE
            WHEN l.longitude::text <> ''::text THEN l.longitude::numeric
            ELSE NULL::numeric
        END AS longitude,
        CASE
            WHEN l.latitude::text <> ''::text THEN l.latitude::numeric
            ELSE NULL::numeric
        END AS latitude,
    l.created_at AS record_created_at
   FROM python.airbnb_listings l
     JOIN common.citydistricts ON st_within(st_setsrid(st_point(l.longitude, l.latitude), 4326), citydistricts.geom)
     LEFT JOIN last_activity la ON l.room_id::text = la.room_id::text
  WHERE l.roomtype IS NOT NULL AND l.roomtype <> ''::text AND l.latitude::text <> ''::text AND l.longitude::text <> ''::text;

		  
-- analytic.v_airbnb_dim_offertype source

CREATE OR REPLACE VIEW analytic.v_airbnb_dim_offertype
AS SELECT DISTINCT v_airbnb_listings.offer_type
   FROM analytic.v_airbnb_listings
  WHERE v_airbnb_listings.offer_type <> ''::text AND v_airbnb_listings.offer_type IS NOT NULL;


-- analytic.v_airbnb_occupancy_monthly source

CREATE OR REPLACE VIEW analytic.v_airbnb_occupancy_monthly
AS WITH constants AS (
         SELECT 0.55 AS reviewers_ratio,
            3.2 AS avg_accomodation_duration
        ), yearly_occupancy AS (
         SELECT airbnb_occupancy.room_id,
            "left"(airbnb_occupancy.yearmon::text, 4) AS o_year,
            (( SELECT constants.avg_accomodation_duration
                   FROM constants))::double precision * sum(airbnb_occupancy.reviews_count) / (( SELECT constants.reviewers_ratio
                   FROM constants))::double precision AS yearly_estimated_days_occupied
           FROM python.airbnb_occupancy
          GROUP BY airbnb_occupancy.room_id, ("left"(airbnb_occupancy.yearmon::text, 4))
        ), last_12m_occupancy AS (
         SELECT airbnb_occupancy.room_id,
            (( SELECT constants.avg_accomodation_duration
                   FROM constants))::double precision * sum(airbnb_occupancy.reviews_count) / (( SELECT constants.reviewers_ratio
                   FROM constants))::double precision AS yearly_estimated_days_occupied
           FROM python.airbnb_occupancy
          WHERE concat(airbnb_occupancy.yearmon, '-01')::date >= (date_trunc('month'::text, airbnb_occupancy.created_at) - '1 year'::interval) AND concat(airbnb_occupancy.yearmon, '-01')::date < date_trunc('month'::text, airbnb_occupancy.created_at)
          GROUP BY airbnb_occupancy.room_id
        )
 SELECT o.room_id,
    "left"(o.yearmon::text, 4) AS o_year,
    concat(o.yearmon, '-01')::date AS o_month,
    l.nazev_mc,
    l.offer_type,
    l.room_created,
    l.open_closed,
    l.ended_at,
    o.reviews_count / (( SELECT constants.reviewers_ratio
           FROM constants))::double precision AS stays_count,
    (( SELECT constants.avg_accomodation_duration
           FROM constants))::double precision * o.reviews_count / (( SELECT constants.reviewers_ratio
           FROM constants))::double precision AS estimated_days_occupied,
    date_part('day'::text, concat(o.yearmon, '-01')::date + '1 mon'::interval - concat(o.yearmon, '-01')::date::timestamp without time zone) AS days_in_month,
    o.reviews_count,
    o.days_occupied,
    0::numeric AS esimated_price_per_night,
    o.mean_price,
        CASE
            WHEN yo.yearly_estimated_days_occupied > 60::numeric::double precision THEN 'Pravidelné'::text
            ELSE 'Příležitostní'::text
        END AS regularity2,
    l.user_id,
    bf.bytovy_fond,
    l.ended_at AS ukonceno,
        CASE
            WHEN yo.yearly_estimated_days_occupied > 60::numeric::double precision AND l.offer_type = 'Celý dům/byt'::text THEN 'Pravidelné, celý byt/dům'::text
            ELSE 'Sdílená ekonomika'::text
        END AS offer_type_group,
        CASE
            WHEN concat(o.yearmon, '-01')::date >= (date_trunc('month'::text, o.created_at) - '1 year'::interval) AND concat(o.yearmon, '-01')::date < date_trunc('month'::text, o.created_at) THEN 'Y'::text
            ELSE 'N'::text
        END AS last_12m,
    l.latitude,
    l.longitude,
    o.created_at AS record_created_at,
        CASE
            WHEN concat(o.yearmon, '-01')::date >= (date_trunc('month'::text, o.created_at) - '1 year'::interval) AND concat(o.yearmon, '-01')::date < date_trunc('month'::text, o.created_at) THEN true
            ELSE false
        END AS last_12_months,
        CASE
            WHEN lo.yearly_estimated_days_occupied > 60::numeric::double precision THEN 'Pravidelné'::text
            ELSE 'Příležitostní'::text
        END AS regularity_last_12m,
        CASE
            WHEN lo.yearly_estimated_days_occupied > 60::numeric::double precision AND l.offer_type = 'Celý dům/byt'::text THEN 'Pravidelné, celý byt/dům'::text
            ELSE 'Sdílená ekonomika'::text
        END AS offer_type_group_last_12m
   FROM python.airbnb_occupancy o
     JOIN analytic.v_airbnb_listings l ON o.room_id::text = l.room_id::text
     LEFT JOIN yearly_occupancy yo ON yo.room_id::text = o.room_id::text AND yo.o_year = "left"(o.yearmon::text, 4)
     LEFT JOIN python.airbnb_apartment_count bf ON l.nazev_mc::text = bf.nazev_mc::text
     LEFT JOIN last_12m_occupancy lo ON lo.room_id::text = o.room_id::text
  WHERE concat(o.yearmon, '-01')::date >= l.room_created AND concat(o.yearmon, '-01')::date < date_trunc('month'::text, o.created_at) AND concat(o.yearmon, '-01')::date >= '2017-01-01'::date AND l.nazev_mc::text <> ''::text AND l.nazev_mc IS NOT NULL AND o.reviews_count > 0::numeric::double precision;


-- analytic.v_airbnb_districts_last12m source

CREATE OR REPLACE VIEW analytic.v_airbnb_districts_last12m
AS WITH districts_list AS (
         SELECT v_airbnb_occupancy_monthly.nazev_mc,
            count(DISTINCT v_airbnb_occupancy_monthly.room_id) AS active_count
           FROM analytic.v_airbnb_occupancy_monthly
          WHERE v_airbnb_occupancy_monthly.last_12_months = true AND v_airbnb_occupancy_monthly.reviews_count > 0::numeric::double precision
          GROUP BY v_airbnb_occupancy_monthly.nazev_mc
         HAVING count(DISTINCT v_airbnb_occupancy_monthly.room_id) >= 10
        )
 SELECT el.nazev_mc,
    'Ukončené nabídky'::text AS metric,
    count(el.room_id) AS metric_value
   FROM analytic.v_airbnb_listings el
     JOIN districts_list dl ON el.nazev_mc::text = dl.nazev_mc::text
  WHERE el.ended_at IS NOT NULL AND el.nazev_mc IS NOT NULL AND date_trunc('month'::text, el.ended_at + '3 mons'::interval) >= (date_trunc('month'::text, el.record_created_at) - '1 year'::interval) AND date_trunc('month'::text, el.ended_at + '3 mons'::interval) < date_trunc('month'::text, el.record_created_at)
  GROUP BY el.nazev_mc
UNION ALL
 SELECT nl.nazev_mc,
    'Nové nabídky'::text AS metric,
    count(nl.room_id) AS metric_value
   FROM analytic.v_airbnb_listings nl
     JOIN districts_list dl ON nl.nazev_mc::text = dl.nazev_mc::text
  WHERE nl.nazev_mc IS NOT NULL AND nl.room_created >= (date_trunc('month'::text, nl.record_created_at) - '1 year'::interval) AND nl.room_created < date_trunc('month'::text, nl.record_created_at)
  GROUP BY nl.nazev_mc
UNION ALL
 SELECT districts_list.nazev_mc,
    'Počet aktivních nabídek'::text AS metric,
    districts_list.active_count AS metric_value
   FROM districts_list
UNION ALL
 SELECT o1.nazev_mc,
    'Medián ceny za celý byt/dům'::text AS metric,
    round(percentile_disc(0.5::double precision) WITHIN GROUP (ORDER BY o1.mean_price)) AS metric_value
   FROM analytic.v_airbnb_occupancy_monthly o1
     JOIN districts_list dl ON o1.nazev_mc::text = dl.nazev_mc::text
  WHERE o1.last_12_months = true AND o1.nazev_mc IS NOT NULL AND o1.offer_type = 'Celý dům/byt'::text AND o1.mean_price < 5250::numeric::double precision AND o1.mean_price > 588::numeric::double precision AND o1.reviews_count > 0::numeric::double precision
  GROUP BY o1.nazev_mc
UNION ALL
 SELECT o2.nazev_mc,
    'Medián ceny za pokoj'::text AS metric,
    round(percentile_disc(0.5::double precision) WITHIN GROUP (ORDER BY o2.mean_price)) AS metric_value
   FROM analytic.v_airbnb_occupancy_monthly o2
     JOIN districts_list dl ON o2.nazev_mc::text = dl.nazev_mc::text
  WHERE o2.last_12_months = true AND o2.nazev_mc IS NOT NULL AND o2.offer_type <> 'Celý dům/byt'::text AND o2.mean_price < 5250::numeric::double precision AND o2.mean_price > 588::numeric::double precision AND o2.reviews_count > 0::numeric::double precision
  GROUP BY o2.nazev_mc
UNION ALL
 SELECT o3.nazev_mc,
    'Počet pobytů'::text AS metric,
    round(sum(o3.stays_count)) AS metric_value
   FROM analytic.v_airbnb_occupancy_monthly o3
     JOIN districts_list dl ON o3.nazev_mc::text = dl.nazev_mc::text
  WHERE o3.last_12_months = true AND o3.nazev_mc IS NOT NULL AND o3.reviews_count > 0::numeric::double precision
  GROUP BY o3.nazev_mc;


-- analytic.v_airbnb_dim_regularity source

CREATE OR REPLACE VIEW analytic.v_airbnb_dim_regularity
AS SELECT DISTINCT v_airbnb_occupancy_monthly.regularity2
   FROM analytic.v_airbnb_occupancy_monthly
  WHERE v_airbnb_occupancy_monthly.regularity2 <> ''::text AND v_airbnb_occupancy_monthly.regularity2 IS NOT NULL;


-- analytic.v_airbnb_overview_12m source

CREATE OR REPLACE VIEW analytic.v_airbnb_overview_12m
AS WITH pravidelny AS (
         SELECT o.nazev_mc,
            count(DISTINCT o.room_id) AS pravidelny_count
           FROM analytic.v_airbnb_occupancy_monthly o
          WHERE o.offer_type_group_last_12m = 'Pravidelné, celý byt/dům'::text AND o.last_12_months = true
          GROUP BY o.nazev_mc
        ), prilezitostni AS (
         SELECT o.nazev_mc,
            count(DISTINCT o.room_id) AS prilezitostni_count
           FROM analytic.v_airbnb_occupancy_monthly o
          WHERE o.offer_type_group_last_12m = 'Sdílená ekonomika'::text AND o.last_12_months = true
          GROUP BY o.nazev_mc
        )
 SELECT bf.nazev_mc,
    pra.pravidelny_count,
    pri.prilezitostni_count,
    bf.bytovy_fond,
    pra.pravidelny_count + pri.prilezitostni_count AS airbnb_rented,
    bf.bytovy_fond - (pra.pravidelny_count + pri.prilezitostni_count)::numeric AS airbnb_not_rented,
    bf.bytovy_fond - pra.pravidelny_count::numeric AS airbnb_not_rented_or_irregular
   FROM python.airbnb_apartment_count bf
     JOIN pravidelny pra ON bf.nazev_mc::text = pra.nazev_mc::text
     JOIN prilezitostni pri ON bf.nazev_mc::text = pri.nazev_mc::text;

-- analytic.v_airbnb_room_per_user source

CREATE OR REPLACE VIEW analytic.v_airbnb_room_per_user
AS SELECT date_trunc('year'::text, v_airbnb_occupancy_monthly.o_month::timestamp with time zone) AS o_year,
    v_airbnb_occupancy_monthly.user_id,
    count(DISTINCT v_airbnb_occupancy_monthly.room_id) AS room_count
   FROM analytic.v_airbnb_occupancy_monthly
  WHERE v_airbnb_occupancy_monthly.user_id::text <> ''::text
  GROUP BY (date_trunc('year'::text, v_airbnb_occupancy_monthly.o_month::timestamp with time zone)), v_airbnb_occupancy_monthly.user_id;

-- analytic.v_airbnb_rooms_stays source

CREATE OR REPLACE VIEW analytic.v_airbnb_rooms_stays
AS SELECT o.o_year,
    o.o_month,
    count(DISTINCT o.room_id) AS active_roooms,
    round(sum(o.stays_count)) AS stays_count,
    o.nazev_mc,
    rank() OVER (ORDER BY o.o_month) AS month_order
   FROM analytic.v_airbnb_occupancy_monthly o
  GROUP BY o.o_year, o.o_month, o.nazev_mc;

-- analytic.v_airbnb_sreality_offers source

CREATE OR REPLACE VIEW analytic.v_airbnb_sreality_offers
AS WITH airbnb_offers AS (
         SELECT date_trunc('week'::text, l.room_created::timestamp with time zone) AS week_start_dt,
            date_part('week'::text, l.room_created) AS week_number,
                CASE
                    WHEN char_length(d.kod_mo::text) = 2 THEN concat('Praha ', "left"(d.kod_mo::text, 1))
                    ELSE concat('Praha ', "left"(d.kod_mo::text, 2))
                END AS district,
            count(l.room_id) AS offers_count
           FROM python.airbnb_listings l
             JOIN common.citydistricts d ON st_within(st_setsrid(st_point(l.longitude, l.latitude), 4326), d.geom)
          WHERE l.room_created >= '2020-01-06'::date
          GROUP BY d.kod_mo, (date_trunc('week'::text, l.room_created::timestamp with time zone)), (concat('Praha ', "left"(d.kod_mo::text, 1))), (date_part('week'::text, l.room_created))
        )
 SELECT s.week_start_dt,
    s.week_label,
    s.week_number,
    s.district,
    sum(
        CASE
            WHEN s.offer_type = 'Pronájem'::text THEN s.offers_count::numeric
            ELSE 0::numeric
        END) AS sreality_offers,
        CASE
            WHEN a.offers_count IS NULL THEN 0::bigint
            ELSE a.offers_count
        END AS airbnb_offers,
        CASE
            WHEN s.district IS NULL OR s.district = ''::text OR s.district = 'All'::text THEN NULL::numeric
            WHEN "right"(s.district, 1)::numeric = 0::numeric THEN 10::numeric
            ELSE "right"(s.district, 1)::numeric
        END AS district_order,
    sum(
        CASE
            WHEN s.offer_type = 'Prodej'::text THEN s.offers_count::numeric
            ELSE 0::numeric
        END) AS sreality_sales
   FROM python.sreality s
     LEFT JOIN airbnb_offers a ON a.week_start_dt = s.week_start_dt AND a.district = s.district
  WHERE s.district <> 'All'::text AND s.category = 'All'::text
  GROUP BY s.week_start_dt, s.week_label, s.week_number, a.offers_count, s.district
  ORDER BY s.week_start_dt DESC, s.district;


-- analytic.v_airbnb_start_end_monthly source

CREATE OR REPLACE VIEW analytic.v_airbnb_start_end_monthly
AS WITH ended AS (
         SELECT date_trunc('month'::text, v_airbnb_listings.ended_at + '3 mons'::interval) AS ended_month,
            v_airbnb_listings.nazev_mc,
            v_airbnb_listings.offer_type,
            count(v_airbnb_listings.room_id) AS ended_count
           FROM analytic.v_airbnb_listings
          WHERE v_airbnb_listings.ended_at IS NOT NULL
          GROUP BY v_airbnb_listings.nazev_mc, v_airbnb_listings.offer_type, (date_trunc('month'::text, v_airbnb_listings.ended_at + '3 mons'::interval))
        )
 SELECT date_trunc('month'::text, created.room_created::timestamp with time zone) AS months,
    count(DISTINCT created.room_id) AS created_count,
    ended.ended_count,
    created.nazev_mc,
    created.offer_type,
        CASE
            WHEN date_trunc('month'::text, created.room_created::timestamp with time zone) >= (date_trunc('month'::text, created.record_created_at) - '1 year'::interval) THEN 'Y'::text
            ELSE 'N'::text
        END AS last_12m
   FROM analytic.v_airbnb_listings created
     LEFT JOIN ended ON ended.ended_month = date_trunc('month'::text, created.room_created::timestamp with time zone) AND ended.nazev_mc::text = created.nazev_mc::text AND ended.offer_type = created.offer_type
  WHERE created.room_created >= '2017-01-01'::date AND created.room_created < date_trunc('month'::text, created.record_created_at)
  GROUP BY created.nazev_mc, created.offer_type, (date_trunc('month'::text, created.room_created::timestamp with time zone)), ended.ended_count, (date_trunc('month'::text, created.record_created_at));		  
  
  -- analytic.v_construction_procedure source

CREATE OR REPLACE VIEW analytic.v_construction_procedure
AS SELECT srtr.popis_rizeni,
    sr.uuid,
    sr.dat_zah,
    sr.typ,
    sr.prerus,
    sr.dat_lhuty_nebezi,
    sr.dat_prerus,
    sr.dat_zast,
    sr.dat_stan_do,
    sr.dat_termin,
    sr.dat_ks,
    sr.dat_zp,
    sr.dat_zp_doba,
    sr.dat_roz_vyh,
    sr.dat_roz_vyp,
    sr.dat_roz_dor,
    sr.odv,
    sr.dat_odv,
    sr.dat_moc,
    sr.novy,
    sr.pocet_bytu,
    sr.plocha,
    sr.dat_komplet,
    sr.mestska_cast,
    sr.created_at,
        CASE
            WHEN
            CASE
                WHEN sr.dat_moc IS NOT NULL THEN sr.dat_moc - sr.dat_zah
                WHEN sr.prerus::text = ANY (ARRAY['O'::character varying::text, 'Z'::character varying::text, 'S'::character varying::text]) THEN sr.dat_roz_vyp - sr.dat_zah
                ELSE NULL::integer
            END < 0 THEN NULL::integer
            ELSE
            CASE
                WHEN sr.dat_moc IS NOT NULL THEN sr.dat_moc - sr.dat_zah
                WHEN sr.prerus::text = ANY (ARRAY['O'::character varying::text, 'Z'::character varying::text, 'S'::character varying::text]) THEN sr.dat_roz_vyp - sr.dat_zah
                ELSE NULL::integer
            END
        END AS zpracovano_dnu,
        CASE
            WHEN sr.dat_moc IS NULL AND (sr.prerus::text <> ALL (ARRAY['O'::character varying::text, 'Z'::character varying::text, 'S'::character varying::text])) AND (CURRENT_DATE - sr.dat_zah) > 182 THEN 1
            ELSE NULL::integer
        END AS prekroceno_pul_roku,
        CASE
            WHEN sr.dat_moc IS NULL AND (sr.prerus::text <> ALL (ARRAY['O'::character varying::text, 'Z'::character varying::text, 'S'::character varying::text])) AND (CURRENT_DATE - sr.dat_zah) > 365 THEN 1
            ELSE NULL::integer
        END AS prekrocen_rok,
        CASE
            WHEN sr.dat_moc IS NOT NULL THEN 0
            WHEN sr.prerus::text = ANY (ARRAY['O'::character varying::text, 'Z'::character varying::text, 'S'::character varying::text]) THEN 0
            ELSE 1
        END AS neukoncene
   FROM python.construction_procedure sr
     LEFT JOIN python.construction_procedure_type srtr ON sr.typ::text = srtr.typ_rizeni::text
  ORDER BY sr.dat_zah;

-- analytic.v_dpp_metro_covid_comparison source

CREATE OR REPLACE VIEW analytic.v_dpp_metro_covid_comparison
AS WITH pre_covid AS (
         SELECT pre.metro_name,
            avg(pre.obrat) AS obrat
           FROM python.dpp_travellers_metro_stations_daily pre
          WHERE pre.obdobi < '2020-01-01 00:00:00'::timestamp without time zone
          GROUP BY pre.metro_name
        ), post_covid AS (
         SELECT date_trunc('Month'::text, post.obdobi) AS month,
            post.metro_name,
            avg(post.obrat) AS obrat
           FROM python.dpp_travellers_metro_stations_daily post
          WHERE post.obdobi >= '2020-01-01 00:00:00'::timestamp without time zone
          GROUP BY (date_trunc('Month'::text, post.obdobi)), post.metro_name
        )
 SELECT post_covid.month,
    post_covid.metro_name,
    post_covid.obrat,
    post_covid.obrat / pre_covid.obrat AS share_of_pre_covid
   FROM post_covid
     LEFT JOIN pre_covid ON post_covid.metro_name = pre_covid.metro_name
  ORDER BY post_covid.month;


-- analytic.v_healthy_classroom_source_filtered source

CREATE OR REPLACE VIEW analytic.v_healthy_classroom_source_filtered
AS SELECT to_timestamp(healthy_classroom_source_data.date::text, 'YYYY-MM-DD HH24:MI:SS'::text) AS "timestamp",
    healthy_classroom_source_data.device_id,
    healthy_classroom_source_data.co2_level,
    healthy_classroom_source_data.dryness_level,
    healthy_classroom_source_data.air_quality_level,
    healthy_classroom_source_data.temperature,
    date_part('hour'::text, to_timestamp(healthy_classroom_source_data.date::text, 'YYYY-MM-DD HH24:MI:SS'::text)) AS measure_hour,
    date_trunc('hour'::text, to_timestamp(healthy_classroom_source_data.date::text, 'YYYY-MM-DD HH24:MI:SS'::text)) + (((date_part('minute'::text, to_timestamp(healthy_classroom_source_data.date::text, 'YYYY-MM-DD HH24:MI:SS'::text))::integer / 5 * 5) || ' minutes'::text)::interval) AS five_min_timestamp,
    healthy_classroom_source_data.humidity,
    healthy_classroom_source_data.co2
   FROM python.healthy_classroom_source_data
  WHERE healthy_classroom_source_data.co2 IS NOT NULL OR healthy_classroom_source_data.humidity IS NOT NULL OR healthy_classroom_source_data.air_quality_level IS NOT NULL OR healthy_classroom_source_data.temperature IS NOT NULL;

-- analytic.v_healthy_classroom_source_filtered_technology_comparison source

CREATE OR REPLACE VIEW analytic.v_healthy_classroom_source_filtered_technology_comparison
AS WITH dta AS (
         SELECT DISTINCT i.generate_series,
            d.five_min_timestamp,
            i.device_id::text AS device_id,
            d.co2,
            d.humidity,
            d.temperature,
            date_part('hour'::text, i.generate_series) AS measure_hour,
            d.air_quality_level
           FROM ( SELECT generate_series.generate_series,
                    dev.device_id
                   FROM generate_series('2021-10-01 00:00:00+02'::timestamp with time zone, now(), '00:05:00'::interval) generate_series(generate_series)
                     CROSS JOIN ( SELECT DISTINCT v_healthy_classroom_source_filtered.device_id
                           FROM analytic.v_healthy_classroom_source_filtered) dev) i
             LEFT JOIN analytic.v_healthy_classroom_source_filtered d ON i.generate_series = d.five_min_timestamp AND i.device_id::text = d.device_id::text
          ORDER BY i.generate_series
        )
 SELECT DISTINCT c.generate_series,
    c.measure_hour,
    c.device_id,
    c.co2,
    max(c.co2::text) OVER (PARTITION BY c.co2_grp, c.device_id ORDER BY c.generate_series) AS co2_all,
    c.humidity,
    max(c.humidity::text) OVER (PARTITION BY c.humidity_grp, c.device_id ORDER BY c.generate_series) AS humidity_all,
    c.temperature,
    max(c.temperature::text) OVER (PARTITION BY c.temperature_grp, c.device_id ORDER BY c.generate_series) AS temperature_all,
    c.air_quality_level,
    max(c.air_quality_level::text) OVER (PARTITION BY c.air_quality_grp, c.device_id ORDER BY c.generate_series) AS air_quality_level_all,
    cr.classroom_id,
    cr.cardinal_direction,
    cr.floor_nr,
    cr.new_technlogy
   FROM ( SELECT dta.generate_series,
            dta.device_id,
            dta.co2,
            dta.measure_hour,
            dta.humidity,
            dta.temperature,
            dta.air_quality_level,
            sum(
                CASE
                    WHEN dta.co2 IS NOT NULL THEN 1
                    ELSE NULL::integer
                END) OVER (PARTITION BY dta.device_id ORDER BY dta.generate_series) AS co2_grp,
            sum(
                CASE
                    WHEN dta.humidity IS NOT NULL THEN 1
                    ELSE NULL::integer
                END) OVER (PARTITION BY dta.device_id ORDER BY dta.generate_series) AS humidity_grp,
            sum(
                CASE
                    WHEN dta.temperature IS NOT NULL THEN 1
                    ELSE NULL::integer
                END) OVER (PARTITION BY dta.device_id ORDER BY dta.generate_series) AS temperature_grp,
            sum(
                CASE
                    WHEN dta.air_quality_level IS NOT NULL THEN 1
                    ELSE NULL::integer
                END) OVER (PARTITION BY dta.device_id ORDER BY dta.generate_series) AS air_quality_grp
           FROM dta) c
     LEFT JOIN python.healthy_classroom_classrooms cr ON c.device_id = cr.device_id::text
  WHERE cr.classroom_id::text = ANY (ARRAY['U303'::character varying::text, 'U304'::character varying::text, 'U305'::character varying::text, 'U306'::character varying::text, 'U307'::character varying::text])
  ORDER BY c.generate_series, c.measure_hour;

-- analytic.mv_healthy_classroom_technology_comparison source

CREATE MATERIALIZED VIEW analytic.mv_healthy_classroom_technology_comparison
TABLESPACE pg_default
AS SELECT n.generate_series,
    n.classroom_id,
    o.device_id,
    n.device_id AS device_id_new,
    o.co2,
    n.co2 AS co2_new,
    o.co2_all,
    n.co2_all AS co2_all_new,
    o.humidity,
    n.humidity AS humidity_new,
    o.humidity_all,
    n.humidity_all AS humidity_all_new,
    o.temperature_all,
    n.temperature_all AS temperature_all_new,
    o.temperature,
    n.temperature AS temperature_new
   FROM analytic.v_healthy_classroom_source_filtered_technology_comparison n
     JOIN ( SELECT v_healthy_classroom_source_filtered_technology_comparison.generate_series,
            v_healthy_classroom_source_filtered_technology_comparison.measure_hour,
            v_healthy_classroom_source_filtered_technology_comparison.device_id,
            v_healthy_classroom_source_filtered_technology_comparison.co2,
            v_healthy_classroom_source_filtered_technology_comparison.co2_all,
            v_healthy_classroom_source_filtered_technology_comparison.humidity,
            v_healthy_classroom_source_filtered_technology_comparison.humidity_all,
            v_healthy_classroom_source_filtered_technology_comparison.temperature,
            v_healthy_classroom_source_filtered_technology_comparison.temperature_all,
            v_healthy_classroom_source_filtered_technology_comparison.air_quality_level,
            v_healthy_classroom_source_filtered_technology_comparison.air_quality_level_all,
            v_healthy_classroom_source_filtered_technology_comparison.classroom_id,
            v_healthy_classroom_source_filtered_technology_comparison.cardinal_direction,
            v_healthy_classroom_source_filtered_technology_comparison.floor_nr,
            v_healthy_classroom_source_filtered_technology_comparison.new_technlogy
           FROM analytic.v_healthy_classroom_source_filtered_technology_comparison
          WHERE v_healthy_classroom_source_filtered_technology_comparison.new_technlogy = false) o ON n.classroom_id::text = o.classroom_id::text AND n.generate_series = o.generate_series
  WHERE n.new_technlogy = true AND n.co2_all IS NOT NULL
WITH DATA;


-- analytic.v_healthy_classroom_5min_intervals source

CREATE OR REPLACE VIEW analytic.v_healthy_classroom_5min_intervals
AS WITH dta AS (
         SELECT DISTINCT i.generate_series,
            d.five_min_timestamp,
            i.device_id::text AS device_id,
            d.co2,
            d.humidity,
            d.temperature,
            date_part('hour'::text, i.generate_series) AS measure_hour,
            d.air_quality_level
           FROM ( SELECT generate_series.generate_series,
                    dev.device_id
                   FROM generate_series('2021-10-01 00:00:00+02'::timestamp with time zone, now(), '00:05:00'::interval) generate_series(generate_series)
                     CROSS JOIN ( SELECT DISTINCT v_healthy_classroom_source_filtered.device_id
                           FROM analytic.v_healthy_classroom_source_filtered) dev) i
             LEFT JOIN analytic.v_healthy_classroom_source_filtered d ON i.generate_series = d.five_min_timestamp AND i.device_id::text = d.device_id::text
          ORDER BY i.generate_series
        )
 SELECT DISTINCT c.generate_series,
    c.measure_hour,
    c.device_id,
    c.co2,
    max(c.co2::text) OVER (PARTITION BY c.co2_grp, c.device_id ORDER BY c.generate_series) AS co2_all,
    c.humidity,
    max(c.humidity::text) OVER (PARTITION BY c.humidity_grp, c.device_id ORDER BY c.generate_series) AS humidity_all,
    c.temperature,
    max(c.temperature::text) OVER (PARTITION BY c.temperature_grp, c.device_id ORDER BY c.generate_series) AS temperature_all,
    c.air_quality_level,
    max(c.air_quality_level::text) OVER (PARTITION BY c.air_quality_grp, c.device_id ORDER BY c.generate_series) AS air_quality_level_all,
    cr.classroom_id,
    cr.cardinal_direction,
    cr.floor_nr
   FROM ( SELECT dta.generate_series,
            dta.device_id,
            dta.co2,
            dta.measure_hour,
            dta.humidity,
            dta.temperature,
            dta.air_quality_level,
            sum(
                CASE
                    WHEN dta.co2 IS NOT NULL THEN 1
                    ELSE NULL::integer
                END) OVER (PARTITION BY dta.device_id ORDER BY dta.generate_series) AS co2_grp,
            sum(
                CASE
                    WHEN dta.humidity IS NOT NULL THEN 1
                    ELSE NULL::integer
                END) OVER (PARTITION BY dta.device_id ORDER BY dta.generate_series) AS humidity_grp,
            sum(
                CASE
                    WHEN dta.temperature IS NOT NULL THEN 1
                    ELSE NULL::integer
                END) OVER (PARTITION BY dta.device_id ORDER BY dta.generate_series) AS temperature_grp,
            sum(
                CASE
                    WHEN dta.air_quality_level IS NOT NULL THEN 1
                    ELSE NULL::integer
                END) OVER (PARTITION BY dta.device_id ORDER BY dta.generate_series) AS air_quality_grp
           FROM dta) c
     LEFT JOIN python.healthy_classroom_classrooms cr ON c.device_id = cr.device_id::text
  WHERE cr.new_technlogy = false
  ORDER BY c.generate_series, c.measure_hour;

-- analytic.v_healthy_classroom_evaluations source

CREATE OR REPLACE VIEW analytic.v_healthy_classroom_evaluations
AS SELECT DISTINCT i.generate_series AS measure_time,
    i.measure_hour,
    i.device_id,
    i.air_quality_level,
    to_number(i.co2::text, 'L9G999g999.99'::text) AS co2,
    to_number(i.co2_all, 'L9G999g999.99'::text) AS co2_all,
    to_number(i.humidity::text, 'L9G999g999.99'::text) AS humidity,
    to_number(i.humidity_all, 'L9G999g999.99'::text) AS humidity_all,
    to_number(i.temperature::text, 'L9G999g999.99'::text) AS temperature,
    to_number(i.temperature_all, 'L9G999g999.99'::text) AS temperature_all,
    e_co2.evaluation AS co2_eval,
    e_hum.evaluation AS humidity_eval,
    e_tem.evaluation AS temperature_eval,
    date_trunc('day'::text, i.generate_series) AS measure_time_day,
        CASE
            WHEN lag(i.co2_all) OVER (PARTITION BY i.device_id ORDER BY i.generate_series) > i.co2_all THEN 1
            ELSE 0
        END AS co2_decrease,
    e_co2.evaluation_order AS co2_eval_order,
    e_hum.evaluation_order AS humidity_eval_order,
    e_tem.evaluation_order AS temperature_eval_order,
        CASE
            WHEN i.air_quality_level_all = 'bad'::text THEN 'Špatná'::text
            WHEN i.air_quality_level_all = 'poor'::text THEN 'Slabá'::text
            WHEN i.air_quality_level_all = 'fair'::text THEN 'Dostatečná'::text
            WHEN i.air_quality_level_all = 'good'::text THEN 'Dobrá'::text
            WHEN i.air_quality_level_all = 'excellent'::text THEN 'Vynikající'::text
            ELSE NULL::text
        END AS air_quality_level_all,
        CASE
            WHEN i.air_quality_level_all = 'Bad'::text THEN 1
            WHEN i.air_quality_level_all = 'Poor'::text THEN 2
            WHEN i.air_quality_level_all = 'Fair'::text THEN 3
            WHEN i.air_quality_level_all = 'Good'::text THEN 4
            WHEN i.air_quality_level_all = 'Excellent'::text THEN 5
            ELSE NULL::integer
        END AS air_quality_level_order,
    i.floor_nr,
    i.classroom_id,
    i.cardinal_direction
   FROM analytic.v_healthy_classroom_5min_intervals i
     LEFT JOIN python.healthy_classroom_values_evaluation e_co2 ON i.co2_all::numeric >= e_co2.low_limit::numeric AND i.co2_all::numeric < e_co2.high_limit::numeric AND e_co2.metric = 'co2'::text
     LEFT JOIN python.healthy_classroom_values_evaluation e_hum ON i.humidity_all::numeric >= e_hum.low_limit::numeric AND i.humidity_all::numeric < e_hum.high_limit::numeric AND e_hum.metric = 'humidity'::text
     LEFT JOIN python.healthy_classroom_values_evaluation e_tem ON i.temperature_all::numeric >= e_tem.low_limit::numeric AND i.temperature_all::numeric < e_tem.high_limit::numeric AND e_tem.metric = 'temperature'::text;

  
-- analytic.mv_healthy_classroom_with_timetables source

CREATE MATERIALIZED VIEW analytic.mv_healthy_classroom_with_timetables
TABLESPACE pg_default
AS SELECT DISTINCT e.measure_time,
    e.measure_hour,
    e.device_id,
    e.air_quality_level,
    e.co2,
    e.co2_all,
    e.humidity,
    e.humidity_all,
    e.temperature,
    e.temperature_all,
    e.co2_eval,
    e.humidity_eval,
    e.temperature_eval,
    e.measure_time_day,
    e.co2_decrease,
    e.co2_eval_order,
    e.humidity_eval_order,
    e.temperature_eval_order,
    e.air_quality_level_all,
    e.air_quality_level_order,
    e.floor_nr,
    e.classroom_id,
    e.cardinal_direction,
    t.begin_datetime,
    t.end_datetime,
    t.suject_abbrev,
    t.subject_name,
    t.teacher_name
   FROM analytic.v_healthy_classroom_evaluations e
     LEFT JOIN ( SELECT DISTINCT t_1.room_abbrev,
            t_1.timetable_date + p.begin_time AS begin_datetime,
            t_1.timetable_date + p.end_time AS end_datetime,
            t_1.suject_abbrev,
            t_1.subject_name,
            t_1.teacher_name
           FROM python.healthy_classroom_timetables t_1
             LEFT JOIN python.healthy_classroom_timetable_parameters p ON t_1.hour_index = p.hour_index) t ON t.room_abbrev::text = "right"(e.classroom_id::text, 3) AND e.measure_time >= t.begin_datetime AND e.measure_time <= t.end_datetime
WITH DATA;


-- analytic.v_internal_reports_golemio_bi source

CREATE OR REPLACE VIEW analytic.v_internal_reports_golemio_bi
AS SELECT internal_reports_golemio_bi.date,
    internal_reports_golemio_bi.sessions,
    internal_reports_golemio_bi.users,
    internal_reports_golemio_bi.sessionduration AS session_time_total_sec,
    internal_reports_golemio_bi.newusers AS new_users,
    COALESCE(internal_reports_golemio_bi.sessionduration / NULLIF(internal_reports_golemio_bi.sessions, 0)::numeric / 60::numeric, 0::numeric) AS avg_session_time_min,
    COALESCE(internal_reports_golemio_bi.sessionduration / NULLIF(internal_reports_golemio_bi.users, 0)::numeric / 60::numeric, 0::numeric) AS avg_user_time_min
   FROM keboola.internal_reports_golemio_bi;
 

-- analytic.v_healthy_classroom_mv_data_teachers source

CREATE OR REPLACE VIEW analytic.v_healthy_classroom_mv_data_teachers
AS SELECT mv_healthy_classroom_with_timetables.measure_time_day,
    sum(mv_healthy_classroom_with_timetables.co2_decrease) AS co2_decrease,
    mv_healthy_classroom_with_timetables.floor_nr,
    mv_healthy_classroom_with_timetables.classroom_id,
    mv_healthy_classroom_with_timetables.begin_datetime,
    count(DISTINCT mv_healthy_classroom_with_timetables.begin_datetime) AS classes_cnt,
    mv_healthy_classroom_with_timetables.subject_name,
    min(mv_healthy_classroom_with_timetables.teacher_name::text) AS teacher_name
   FROM analytic.mv_healthy_classroom_with_timetables
  WHERE mv_healthy_classroom_with_timetables.begin_datetime IS NOT NULL
  GROUP BY mv_healthy_classroom_with_timetables.measure_time_day, mv_healthy_classroom_with_timetables.floor_nr, mv_healthy_classroom_with_timetables.classroom_id, mv_healthy_classroom_with_timetables.subject_name, mv_healthy_classroom_with_timetables.begin_datetime;



-- analytic.v_healthy_classroom_mv_data_technology_comparison source

CREATE OR REPLACE VIEW analytic.v_healthy_classroom_mv_data_technology_comparison
AS SELECT mv_healthy_classroom_technology_comparison.generate_series,
    mv_healthy_classroom_technology_comparison.classroom_id,
    mv_healthy_classroom_technology_comparison.device_id,
    mv_healthy_classroom_technology_comparison.device_id_new,
    mv_healthy_classroom_technology_comparison.co2,
    mv_healthy_classroom_technology_comparison.co2_new,
    to_number(mv_healthy_classroom_technology_comparison.co2_all, '9,999.99'::text) AS co2_all,
    to_number(mv_healthy_classroom_technology_comparison.co2_all_new, '9,999.99'::text) AS co2_all_new,
    mv_healthy_classroom_technology_comparison.humidity,
    mv_healthy_classroom_technology_comparison.humidity_new,
    to_number(mv_healthy_classroom_technology_comparison.humidity_all, '9,999.99'::text) AS humidity_all,
    to_number(mv_healthy_classroom_technology_comparison.humidity_all_new, '9,999.99'::text) AS humidity_all_new,
    to_number(mv_healthy_classroom_technology_comparison.temperature_all, '9,999.99'::text) AS temperature_all,
    to_number(mv_healthy_classroom_technology_comparison.temperature_all_new, '9,999.99'::text) AS temperature_all_new,
    mv_healthy_classroom_technology_comparison.temperature,
    mv_healthy_classroom_technology_comparison.temperature_new
   FROM analytic.mv_healthy_classroom_technology_comparison;



-- analytic.v_healthy_classroom_mv_data source

CREATE OR REPLACE VIEW analytic.v_healthy_classroom_mv_data
AS SELECT DISTINCT mv_healthy_classroom_with_timetables.measure_time,
    mv_healthy_classroom_with_timetables.measure_hour,
    mv_healthy_classroom_with_timetables.device_id,
    mv_healthy_classroom_with_timetables.air_quality_level,
    mv_healthy_classroom_with_timetables.co2,
    mv_healthy_classroom_with_timetables.co2_all,
    mv_healthy_classroom_with_timetables.humidity,
    mv_healthy_classroom_with_timetables.humidity_all,
    mv_healthy_classroom_with_timetables.temperature,
    mv_healthy_classroom_with_timetables.temperature_all,
    mv_healthy_classroom_with_timetables.co2_eval,
    mv_healthy_classroom_with_timetables.humidity_eval,
    mv_healthy_classroom_with_timetables.temperature_eval,
    mv_healthy_classroom_with_timetables.measure_time_day,
    mv_healthy_classroom_with_timetables.co2_decrease,
    mv_healthy_classroom_with_timetables.co2_eval_order,
    mv_healthy_classroom_with_timetables.humidity_eval_order,
    mv_healthy_classroom_with_timetables.temperature_eval_order,
    mv_healthy_classroom_with_timetables.air_quality_level_all,
    mv_healthy_classroom_with_timetables.air_quality_level_order,
    mv_healthy_classroom_with_timetables.floor_nr,
    mv_healthy_classroom_with_timetables.classroom_id,
    mv_healthy_classroom_with_timetables.cardinal_direction
   FROM analytic.mv_healthy_classroom_with_timetables;


  
-- analytic.v_obis_comments source

CREATE OR REPLACE VIEW analytic.v_obis_comments
AS SELECT t.tc_multicis_id AS prip_id,
    COALESCE((ozn.jmeno || ' '::text) || ozn.prijmeni, oun.utvar_nazev, t.pripmist_popis) AS prip_jmeno,
    t.pocet_pripom AS prip_pocet,
    t.tc_pripfaze_id AS prip_faze,
    t.pripmist_stav AS prip_stav,
    t.start_faze_od AS prip_start_faze,
    COALESCE(t.datum_podpisu, a.end_date::timestamp without time zone) AS prip_datum_podpisu,
        CASE
            WHEN t.dnu_pripom = 0 THEN 1::bigint
            ELSE abs(t.dnu_pripom)
        END AS dnu_pripom,
    t.cislotisku AS tisk_id
   FROM python.obis_proposals t
     LEFT JOIN ( SELECT obis_proposals_employees.id,
            obis_proposals_employees.platnost,
            obis_proposals_employees.titul_pred,
            obis_proposals_employees.jmeno,
            obis_proposals_employees.jmeno2,
            obis_proposals_employees.prijmeni,
            obis_proposals_employees.prijmeni2,
            obis_proposals_employees.titul_za,
            obis_proposals_employees.misto_id,
            obis_proposals_employees.misto_jmeno,
            obis_proposals_employees.odbor_id,
            obis_proposals_employees.odbor_jmeno,
            obis_proposals_employees.oddeleni_id,
            obis_proposals_employees.oddeleni_jmeno,
            obis_proposals_employees.updated_at
           FROM python.obis_proposals_employees
          WHERE obis_proposals_employees.platnost = 65) ozn ON "right"(t.tc_multicis_id::character varying(50)::text, 5)::integer = ozn.id
     LEFT JOIN ( SELECT obis_proposals_departments.id,
            obis_proposals_departments.platnost,
            obis_proposals_departments.utvar_nazev,
            obis_proposals_departments.utvar_cislo,
            obis_proposals_departments.utvar_nadrizeny_cislo,
            obis_proposals_departments.utvar_kod,
            obis_proposals_departments.updated_at
           FROM python.obis_proposals_departments
          WHERE obis_proposals_departments.platnost = 65) oun ON "right"(t.tc_multicis_id::character varying(50)::text, 5)::integer = oun.id
     JOIN ( SELECT obis_proposals.cislotisku AS tisk_id,
            COALESCE(obis_proposals.datum_uko_pfaze::date, obis_proposals.datumjednani::date) AS end_date
           FROM python.obis_proposals
          GROUP BY obis_proposals.cislotisku, (COALESCE(obis_proposals.datum_uko_pfaze::date, obis_proposals.datumjednani::date))) a ON t.cislotisku = a.tisk_id
  WHERE char_length(t.tc_multicis_id::character varying(50)::text) = 6
UNION ALL
 SELECT t.tc_multicis_id AS prip_id,
    COALESCE((ozn.jmeno || ' '::text) || ozn.prijmeni, t.pripmist_popis) AS prip_jmeno,
    t.pocet_pripom AS prip_pocet,
    t.tc_pripfaze_id AS prip_faze,
    t.pripmist_stav AS prip_stav,
    t.start_faze_od AS prip_start_faze,
    COALESCE(t.datum_podpisu, a.end_date::timestamp without time zone) AS prip_datum_podpisu,
        CASE
            WHEN t.dnu_pripom = 0 THEN 1::bigint
            ELSE abs(t.dnu_pripom)
        END AS dnu_pripom,
    t.cislotisku AS tisk_id
   FROM python.obis_proposals t
     LEFT JOIN ( SELECT obis_proposals_employees.id,
            obis_proposals_employees.platnost,
            obis_proposals_employees.titul_pred,
            obis_proposals_employees.jmeno,
            obis_proposals_employees.jmeno2,
            obis_proposals_employees.prijmeni,
            obis_proposals_employees.prijmeni2,
            obis_proposals_employees.titul_za,
            obis_proposals_employees.misto_id,
            obis_proposals_employees.misto_jmeno,
            obis_proposals_employees.odbor_id,
            obis_proposals_employees.odbor_jmeno,
            obis_proposals_employees.oddeleni_id,
            obis_proposals_employees.oddeleni_jmeno,
            obis_proposals_employees.updated_at
           FROM python.obis_proposals_employees
          WHERE obis_proposals_employees.platnost = 65) ozn ON t.tc_multicis_id = ozn.misto_id
     JOIN ( SELECT obis_proposals.cislotisku AS tisk_id,
            COALESCE(obis_proposals.datum_uko_pfaze::date, obis_proposals.datumjednani::date) AS end_date
           FROM python.obis_proposals
          GROUP BY obis_proposals.cislotisku, (COALESCE(obis_proposals.datum_uko_pfaze::date, obis_proposals.datumjednani::date))) a ON t.cislotisku = a.tisk_id
  WHERE char_length(t.tc_multicis_id::character varying(50)::text) < 6;

-- analytic.v_obis_processor source

CREATE OR REPLACE VIEW analytic.v_obis_processor
AS WITH base AS (
         SELECT obis_proposals.zpr_id_utv,
            obis_proposals.cislotisku AS tisk_id
           FROM python.obis_proposals
          GROUP BY obis_proposals.zpr_id_utv, obis_proposals.cislotisku
        ), unnest AS (
         SELECT unnest(string_to_array(base.zpr_id_utv, ';'::text)) AS zpracovatel_id,
            base.tisk_id
           FROM base
        )
 SELECT unnest.zpracovatel_id,
    unnest.tisk_id,
    u.utvar_kod,
    u.utvar_nazev
   FROM unnest
     LEFT JOIN ( SELECT obis_proposals_departments.id::text AS id,
            obis_proposals_departments.platnost,
            obis_proposals_departments.utvar_nazev,
            obis_proposals_departments.utvar_cislo,
            obis_proposals_departments.utvar_nadrizeny_cislo,
            obis_proposals_departments.utvar_kod,
            obis_proposals_departments.updated_at
           FROM python.obis_proposals_departments) u ON unnest.zpracovatel_id = u.id;


-- analytic.v_obis_proposals source

CREATE OR REPLACE VIEW analytic.v_obis_proposals
AS SELECT obis_proposals.cislotisku AS cislo_tisku,
    obis_proposals.nazevtisku AS nazev_tisku,
    obis_proposals.tt_usneseni_id AS usneseni_id,
    obis_proposals.cislousneseni AS cislo_usneseni,
    obis_proposals.usnkoho AS usneseni_koho,
    obis_proposals.cislojednani AS cislo_jednani,
    obis_proposals.datumjednani::date AS datum_jednani,
    obis_proposals.tc_stadok_id AS tisk_status_id,
    obis_proposals.stavmaterialu AS tisk_status,
    obis_proposals.zalozil,
    obis_proposals.kdy_ins::date AS zalozil_date,
    obis_proposals.kdy_upd::date AS updated_date,
        CASE
            WHEN date_part('year'::text, obis_proposals.datum_zah_1faze::date) < 2000::double precision OR obis_proposals.datum_zah_1faze::date IS NULL THEN COALESCE(obis_proposals.datum_uko_pfaze::date, obis_proposals.datumjednani::date) - '2 days'::interval
            ELSE obis_proposals.datum_zah_1faze::date::timestamp without time zone
        END AS prvni_faze_start,
    COALESCE(obis_proposals.datum_uko_pfaze::date, obis_proposals.datumjednani::date) AS ukonceni_datum,
    obis_proposals.pripdnucelk AS prip_pocet_dnu
   FROM python.obis_proposals
  GROUP BY obis_proposals.cislotisku, obis_proposals.nazevtisku, obis_proposals.tt_usneseni_id, obis_proposals.cislousneseni, obis_proposals.usnkoho, obis_proposals.cislojednani, (obis_proposals.datumjednani::date), obis_proposals.tc_stadok_id, obis_proposals.stavmaterialu, obis_proposals.zalozil, (obis_proposals.kdy_ins::date), (obis_proposals.kdy_upd::date), (
        CASE
            WHEN date_part('year'::text, obis_proposals.datum_zah_1faze::date) < 2000::double precision OR obis_proposals.datum_zah_1faze::date IS NULL THEN COALESCE(obis_proposals.datum_uko_pfaze::date, obis_proposals.datumjednani::date) - '2 days'::interval
            ELSE obis_proposals.datum_zah_1faze::date::timestamp without time zone
        END), (COALESCE(obis_proposals.datum_uko_pfaze::date, obis_proposals.datumjednani::date)), obis_proposals.pripdnucelk;

-- analytic.v_obis_submitter source

CREATE OR REPLACE VIEW analytic.v_obis_submitter
AS WITH base AS (
         SELECT obis_proposals.id_predkl_osoba,
            obis_proposals.cislotisku AS tisk_id
           FROM python.obis_proposals
          GROUP BY obis_proposals.id_predkl_osoba, obis_proposals.cislotisku
        ), unnest AS (
         SELECT unnest(string_to_array(base.id_predkl_osoba, ';'::text)) AS predkladatel_id,
            base.tisk_id
           FROM base
        )
 SELECT unnest.predkladatel_id,
    unnest.tisk_id,
    z.id,
    z.platnost,
    z.title_prefix AS titul_pred,
    z.name AS jmeno,
    z.mid_name AS jmeno2,
    z.surname AS prijmeni,
    z.surname2 AS prijmeni2,
    z.title_suffix AS titul_za,
    z.position_code AS misto_id,
    z.position_name AS misto_jmeno,
    z.utvar_id AS odbor_id,
    z.utvar_name AS odbor_jmeno,
    z.oddeleni_id,
    z.oddeleni_name AS oddeleni_jmeno,
    z.updated_at
   FROM unnest
     LEFT JOIN ( SELECT obis_proposals_employees.id::text AS id,
            obis_proposals_employees.platnost,
            obis_proposals_employees.titul_pred AS title_prefix,
            obis_proposals_employees.jmeno AS name,
            obis_proposals_employees.jmeno2 AS mid_name,
            obis_proposals_employees.prijmeni AS surname,
            obis_proposals_employees.prijmeni2 AS surname2,
            obis_proposals_employees.titul_za AS title_suffix,
            obis_proposals_employees.misto_id AS position_code,
            obis_proposals_employees.misto_jmeno AS position_name,
            obis_proposals_employees.odbor_id AS utvar_id,
            obis_proposals_employees.odbor_jmeno AS utvar_name,
            obis_proposals_employees.oddeleni_id,
            obis_proposals_employees.oddeleni_jmeno AS oddeleni_name,
            obis_proposals_employees.updated_at
           FROM python.obis_proposals_employees) z ON unnest.predkladatel_id = z.id;

-- analytic.v_obis_submitter_comments source

CREATE OR REPLACE VIEW analytic.v_obis_submitter_comments
AS SELECT e.predkladatel_id,
    concat(e.jmeno, ' ', e.prijmeni) AS jmeno_predkladatel,
    i.tisk_id,
    i.prip_id,
    i.prip_jmeno,
        CASE
            WHEN i.dnu_pripom = 0 THEN 1::bigint
            ELSE abs(i.dnu_pripom)
        END AS prip_pocet_dnu
   FROM analytic.v_obis_submitter e
     JOIN analytic.v_obis_comments i ON e.tisk_id = i.tisk_id;

-- analytic.v_obis_proposals_all source

CREATE OR REPLACE VIEW analytic.v_obis_proposals_all
AS SELECT e.id,
    concat(e.jmeno, ' ', e.prijmeni) AS predkladatel_jmeno,
    z.zpracovatel_id,
    z.utvar_kod AS zpracovatel_kod,
    z.utvar_nazev AS zpracovatel_jmeno,
    t.cislo_tisku AS tisk_cislo,
    t.nazev_tisku AS tisk_nazev,
    t.usneseni_id,
    t.cislo_usneseni AS usneseni_cislo,
    t.usneseni_koho,
    t.cislo_jednani AS jednani_cislo,
    t.datum_jednani AS jednani_datum,
    t.tisk_status_id,
    t.tisk_status,
    t.zalozil,
    t.zalozil_date,
    t.updated_date,
    t.prvni_faze_start,
    t.ukonceni_datum,
    t.prip_pocet_dnu,
    e.updated_at,
    i.prip_id,
    i.prip_jmeno,
    i.prip_stav,
    i.prip_faze,
    i.prip_pocet,
    i.prip_start_faze AS prip_start,
    i.prip_datum_podpisu,
    i.dnu_pripom
   FROM analytic.v_obis_submitter e
     LEFT JOIN analytic.v_obis_processor z ON e.tisk_id = z.tisk_id
     LEFT JOIN analytic.v_obis_proposals t ON e.tisk_id = t.cislo_tisku
     LEFT JOIN analytic.v_obis_comments i ON e.tisk_id = i.tisk_id;

-- openproject

-- analytic.v_openproject_mhmp_domain source

CREATE OR REPLACE VIEW analytic.v_openproject_mhmp_domain
AS SELECT openproject_mhmp_projects.id,
    openproject_mhmp_projects.project_name AS domain_name,
    openproject_mhmp_projects.councilor_name
   FROM python.openproject_mhmp_projects
  WHERE openproject_mhmp_projects.parent_id = 3;


-- analytic.v_openproject_mhmp_projects source

CREATE OR REPLACE VIEW analytic.v_openproject_mhmp_projects
AS WITH domain AS (
         SELECT openproject_mhmp_projects.id
           FROM python.openproject_mhmp_projects
          WHERE openproject_mhmp_projects.parent_id = 3
        ), topic AS (
         SELECT openproject_mhmp_projects.id
           FROM python.openproject_mhmp_projects
          WHERE (openproject_mhmp_projects.parent_id IN ( SELECT domain.id
                   FROM domain))
        ), projects AS (
         SELECT openproject_mhmp_projects.id,
            openproject_mhmp_projects.project_name,
            openproject_mhmp_projects.active,
            openproject_mhmp_projects.public,
            openproject_mhmp_projects.project_description,
            openproject_mhmp_projects.status,
            openproject_mhmp_projects.action_status,
            openproject_mhmp_projects.status_explanation,
            openproject_mhmp_projects.project_start,
            openproject_mhmp_projects.project_end,
            openproject_mhmp_projects.supplier_name,
            openproject_mhmp_projects.hashtag,
            openproject_mhmp_projects.budget_expected,
            openproject_mhmp_projects.budget_reality,
            openproject_mhmp_projects.council_rule_number,
            openproject_mhmp_projects.council_rule_url,
            openproject_mhmp_projects.contract_number,
            openproject_mhmp_projects.contract_registry_url,
            openproject_mhmp_projects.tender_number,
            openproject_mhmp_projects.tender_url,
            openproject_mhmp_projects.solver_name,
            openproject_mhmp_projects.councilor_name,
            openproject_mhmp_projects.mhmp_responsible_name,
            openproject_mhmp_projects.parent_id,
            openproject_mhmp_projects.parent_project,
            openproject_mhmp_projects.updated_at
           FROM python.openproject_mhmp_projects
          WHERE (openproject_mhmp_projects.parent_id IN ( SELECT topic.id
                   FROM topic))
        ), subprojects AS (
         SELECT sp.id,
            sp.project_name,
            sp.active,
            sp.public,
            sp.project_description,
            sp.status,
            sp.action_status,
            sp.status_explanation,
            sp.project_start,
            sp.project_end,
            sp.supplier_name,
            sp.hashtag,
            sp.budget_expected,
            sp.budget_reality,
            sp.council_rule_number,
            sp.council_rule_url,
            sp.contract_number,
            sp.contract_registry_url,
            sp.tender_number,
            sp.tender_url,
            sp.solver_name,
            sp.councilor_name,
            sp.mhmp_responsible_name,
            p.parent_id,
            sp.parent_project,
            sp.updated_at
           FROM python.openproject_mhmp_projects sp
             LEFT JOIN projects p ON sp.parent_id = p.id
          WHERE (sp.parent_id IN ( SELECT projects.id
                   FROM projects))
        ), next_lvl AS (
         SELECT projects.id,
            projects.project_name,
            projects.active,
            projects.public,
            projects.project_description,
            projects.status,
            projects.action_status,
            projects.status_explanation,
            projects.project_start,
            projects.project_end,
            projects.supplier_name,
            projects.hashtag,
            projects.budget_expected,
            projects.budget_reality,
            projects.council_rule_number,
            projects.council_rule_url,
            projects.contract_number,
            projects.contract_registry_url,
            projects.tender_number,
            projects.tender_url,
            projects.solver_name,
            projects.councilor_name,
            projects.mhmp_responsible_name,
            projects.parent_id,
            projects.parent_project,
            projects.updated_at
           FROM projects
        UNION ALL
         SELECT subprojects.id,
            subprojects.project_name,
            subprojects.active,
            subprojects.public,
            subprojects.project_description,
            subprojects.status,
            subprojects.action_status,
            subprojects.status_explanation,
            subprojects.project_start,
            subprojects.project_end,
            subprojects.supplier_name,
            subprojects.hashtag,
            subprojects.budget_expected,
            subprojects.budget_reality,
            subprojects.council_rule_number,
            subprojects.council_rule_url,
            subprojects.contract_number,
            subprojects.contract_registry_url,
            subprojects.tender_number,
            subprojects.tender_url,
            subprojects.solver_name,
            subprojects.councilor_name,
            subprojects.mhmp_responsible_name,
            subprojects.parent_id,
            subprojects.parent_project,
            subprojects.updated_at
           FROM subprojects
        )
 SELECT nl.id,
    nl.project_name,
    'https://op.praha.eu/projects/'::text || nl.id::character varying::text AS project_url,
    nl.active,
    nl.public,
    nl.project_description,
    "left"(nl.project_description, 150) || '...'::text AS project_description_short,
    nl.status,
    nl.action_status,
    (nl.status || ' - '::text) || nl.action_status::text AS status_full,
    nl.status_explanation,
    nl.project_start::timestamp with time zone AS project_start,
    nl.project_end::timestamp with time zone AS project_end,
    nl.supplier_name,
    nl.hashtag,
    nl.budget_expected,
    nl.budget_reality,
    nl.council_rule_number,
    nl.council_rule_url,
    nl.contract_number,
    nl.contract_registry_url,
    nl.tender_number,
    nl.tender_url,
    nl.solver_name,
    nl.councilor_name,
    nl.mhmp_responsible_name,
    nl.parent_id,
    nl.parent_project,
    COALESCE(nl.updated_at::timestamp with time zone, w.wps_up_at::timestamp with time zone) AS updated_at,
        CASE
            WHEN nl.project_start::timestamp with time zone IS NULL AND nl.project_end::timestamp with time zone IS NULL THEN 0
            WHEN (date_part('month'::text, CURRENT_DATE) = ANY (ARRAY[4::double precision, 5::double precision, 6::double precision, 7::double precision, 8::double precision, 9::double precision])) AND (nl.project_start::timestamp with time zone >= format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE), 4, 1)::date AND nl.project_start::timestamp with time zone <= format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE), 9, 30)::date OR nl.project_end::timestamp with time zone >= format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE), 4, 1)::date AND nl.project_end::timestamp with time zone <= format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE), 9, 30)::date OR nl.project_start::timestamp with time zone < format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE), 10, 1)::date AND nl.project_end::timestamp with time zone > format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE), 3, 31)::date) THEN 1
            WHEN (date_part('month'::text, CURRENT_DATE) = ANY (ARRAY[1::double precision, 2::double precision, 3::double precision, 10::double precision, 11::double precision, 12::double precision])) AND (nl.project_start::timestamp with time zone >= format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE - 200), 9, 30)::date AND nl.project_start::timestamp with time zone <= format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE + 200), 4, 1)::date OR nl.project_end::timestamp with time zone >= format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE - 200), 9, 30)::date AND nl.project_end::timestamp with time zone <= format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE + 200), 4, 1)::date OR nl.project_start::timestamp with time zone < format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE + 200), 3, 31)::date AND nl.project_end::timestamp with time zone > format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE - 200), 10, 1)::date) THEN 1
            ELSE 0
        END AS this_period,
        CASE
            WHEN nl.project_start::timestamp with time zone IS NULL AND nl.project_end::timestamp with time zone IS NULL THEN 0
            WHEN (date_part('month'::text, CURRENT_DATE) = ANY (ARRAY[4::double precision, 5::double precision, 6::double precision, 7::double precision, 8::double precision, 9::double precision])) AND (nl.project_start::timestamp with time zone >= format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE), 9, 30)::date AND nl.project_start::timestamp with time zone <= format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE) + 1::double precision, 4, 1)::date OR nl.project_end::timestamp with time zone >= format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE), 9, 30)::date AND nl.project_end::timestamp with time zone <= format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE) + 1::double precision, 4, 1)::date OR nl.project_start::timestamp with time zone < format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE) + 1::double precision, 3, 31)::date AND nl.project_end::timestamp with time zone > format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE), 10, 1)::date) THEN 1
            WHEN (date_part('month'::text, CURRENT_DATE) = ANY (ARRAY[1::double precision, 2::double precision, 3::double precision, 10::double precision, 11::double precision, 12::double precision])) AND (nl.project_start::timestamp with time zone >= format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE + 200), 4, 1)::date AND nl.project_start::timestamp with time zone <= format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE + 200), 9, 30)::date OR nl.project_end::timestamp with time zone >= format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE + 200), 4, 1)::date AND nl.project_end::timestamp with time zone <= format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE + 200), 9, 30)::date OR nl.project_start::timestamp with time zone < format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE + 200), 10, 1)::date AND nl.project_end::timestamp with time zone > format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE + 200), 3, 31)::date) THEN 1
            ELSE 0
        END AS next_period,
        CASE
            WHEN nl.project_start::timestamp with time zone IS NULL AND nl.project_end::timestamp with time zone IS NULL THEN 0
            WHEN (date_part('month'::text, CURRENT_DATE) = ANY (ARRAY[4::double precision, 5::double precision, 6::double precision, 7::double precision, 8::double precision, 9::double precision])) AND (nl.project_start::timestamp with time zone >= format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE) + 1::double precision, 4, 1)::date AND nl.project_start::timestamp with time zone <= format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE) + 1::double precision, 9, 30)::date OR nl.project_end::timestamp with time zone >= format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE) + 1::double precision, 4, 1)::date AND nl.project_end::timestamp with time zone <= format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE) + 1::double precision, 9, 30)::date OR nl.project_start::timestamp with time zone < format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE) + 1::double precision, 10, 1)::date AND nl.project_end::timestamp with time zone > format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE) + 1::double precision, 3, 31)::date) THEN 1
            WHEN (date_part('month'::text, CURRENT_DATE) = ANY (ARRAY[1::double precision, 2::double precision, 3::double precision, 10::double precision, 11::double precision, 12::double precision])) AND (nl.project_start::timestamp with time zone >= format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE - 200) + 1::double precision, 10, 1)::date AND nl.project_start::timestamp with time zone <= format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE + 200) + 1::double precision, 3, 31)::date OR nl.project_end::timestamp with time zone >= format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE - 200) + 1::double precision, 10, 1)::date AND nl.project_end::timestamp with time zone <= format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE + 200) + 1::double precision, 3, 31)::date OR nl.project_start::timestamp with time zone < format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE + 200) + 1::double precision, 4, 1)::date AND nl.project_end::timestamp with time zone > format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE - 200) + 1::double precision, 9, 30)::date) THEN 1
            ELSE 0
        END AS next_next_period
   FROM next_lvl nl
     LEFT JOIN ( SELECT openproject_mhmp_wps.project_id,
            max(openproject_mhmp_wps.updated_at) AS wps_up_at
           FROM python.openproject_mhmp_wps
          GROUP BY openproject_mhmp_wps.project_id) w ON w.project_id = nl.id;


-- analytic.v_openproject_mhmp_topics source

CREATE OR REPLACE VIEW analytic.v_openproject_mhmp_topics
AS WITH domain AS (
         SELECT openproject_mhmp_projects_1.id,
            openproject_mhmp_projects_1.project_name AS domain_name,
            openproject_mhmp_projects_1.councilor_name
           FROM python.openproject_mhmp_projects openproject_mhmp_projects_1
          WHERE openproject_mhmp_projects_1.parent_id = 3
        )
 SELECT openproject_mhmp_projects.id,
    openproject_mhmp_projects.project_name AS topic_name,
    openproject_mhmp_projects.active,
    openproject_mhmp_projects.public,
    openproject_mhmp_projects.status,
    openproject_mhmp_projects.councilor_name,
    openproject_mhmp_projects.mhmp_responsible_name,
    openproject_mhmp_projects.parent_id,
    openproject_mhmp_projects.parent_project
   FROM python.openproject_mhmp_projects
  WHERE (openproject_mhmp_projects.parent_id IN ( SELECT domain.id
           FROM domain));


-- analytic.v_openproject_mhmp_workpackages source

CREATE OR REPLACE VIEW analytic.v_openproject_mhmp_workpackages
AS SELECT openproject_mhmp_wps.derived_start_date,
    openproject_mhmp_wps.derived_due_date,
    openproject_mhmp_wps.spent_time,
    openproject_mhmp_wps.labor_costs,
    openproject_mhmp_wps.material_costs,
    openproject_mhmp_wps.overall_costs,
    openproject_mhmp_wps.type_of_object,
    openproject_mhmp_wps.wp_id,
    openproject_mhmp_wps.wp_name,
    openproject_mhmp_wps.start_date,
    openproject_mhmp_wps.due_date,
    openproject_mhmp_wps.estimated_time,
    openproject_mhmp_wps.derived_estimated_time,
    openproject_mhmp_wps.percentage_done,
    openproject_mhmp_wps.created_at,
    openproject_mhmp_wps.updated_at,
    openproject_mhmp_wps.story_points,
    openproject_mhmp_wps.remaining_time,
    openproject_mhmp_wps.supplier_name,
    openproject_mhmp_wps.weight,
    openproject_mhmp_wps.contract_number,
    openproject_mhmp_wps.tender_number,
    openproject_mhmp_wps.council_rule_number,
    openproject_mhmp_wps.media_url,
    openproject_mhmp_wps.media_out_date,
    openproject_mhmp_wps.wp_description,
    openproject_mhmp_wps.tender_url,
    openproject_mhmp_wps.contract_registry_url,
    openproject_mhmp_wps.council_rule_url,
    openproject_mhmp_wps.wp_type,
    openproject_mhmp_wps.priority,
    openproject_mhmp_wps.project_id,
    openproject_mhmp_wps.project_name,
    openproject_mhmp_wps.wp_status,
    openproject_mhmp_wps.author_id,
    openproject_mhmp_wps.author_name,
    openproject_mhmp_wps.responsible_id,
    openproject_mhmp_wps.responsible_name,
    openproject_mhmp_wps.assignee_id,
    openproject_mhmp_wps.assignee_name,
    openproject_mhmp_wps.department_resp_name,
    openproject_mhmp_wps.department_resp_id,
    openproject_mhmp_wps.supplier_resp_id,
    openproject_mhmp_wps.supplier_resp_name,
    openproject_mhmp_wps.parent_id,
    openproject_mhmp_wps.parent_name,
    openproject_mhmp_wps.completion_date
   FROM python.openproject_mhmp_wps
  WHERE (openproject_mhmp_wps.project_id IN ( SELECT v_openproject_mhmp_projects.id
           FROM analytic.v_openproject_mhmp_projects));
		   
		   
-- prague_portal		   
		   
-- analytic.v_prague_portal_census source

CREATE OR REPLACE VIEW analytic.v_prague_portal_census
AS SELECT c.uzemi_kod,
    c.uzemi_txt,
    clcc.uzemi_kod_orp_praha,
    clcc.uzemi_txt_orp_praha,
    c.atribut,
    c.skupina,
    c.podskupina,
    c.pocet_obyvatel
   FROM demographics.code_list_cr_csu clcc
     LEFT JOIN ( SELECT cag.uzemi_kod,
            cag.uzemi_txt,
            cag.pocet_obyvatel,
            cag.pohlavi_txt AS skupina,
            cag.vek_txt AS podskupina,
            cag.atribut
           FROM demographics.census_age_10_gender cag
        UNION
         SELECT cc.uzemi_kod,
            cc.uzemi_txt,
            cc.pocet_obyvatel,
            cc.obcanstvi_txt AS skupina,
            NULL::character varying AS poskupina,
            cc.atribut
           FROM demographics.census_citizenship cc
        UNION
         SELECT ce.uzemi_kod,
            ce.uzemi_txt,
            ce.pocet_obyvatel,
            ce.vzdelani_txt AS skupina,
            NULL::character varying AS poskupina,
            ce.atribut
           FROM demographics.census_education ce
        UNION
         SELECT ce2.uzemi_kod,
            ce2.uzemi_txt,
            ce2.pocet_obyvatel,
            ce2.narodnost_txt AS skupina,
            NULL::character varying AS poskupina,
            ce2.atribut
           FROM demographics.census_ethnicity ce2
        UNION
         SELECT cf.uzemi_kod,
            cf.uzemi_txt,
            cf.pocet_obyvatel,
            cf.pocetdeti_txt AS skupina,
            NULL::character varying AS poskupina,
            cf.atribut
           FROM demographics.census_fertility cf
        UNION
         SELECT cms.uzemi_kod,
            cms.uzemi_txt,
            cms.pocet_obyvatel,
            cms.stav_txt AS skupina,
            NULL::character varying AS poskupina,
            cms.atribut
           FROM demographics.census_marital_status cms
        UNION
         SELECT cmtc.uzemi_kod,
            cmtc.uzemi_txt,
            cmtc.pocet_obyvatel,
            cmtc.jazyk_txt AS skupina,
            NULL::character varying AS poskupina,
            cmtc.atribut
           FROM demographics.census_mother_tongue_exclusive cmtc
        UNION
         SELECT cha.uzemi_kod,
            cha.uzemi_txt,
            cha.pocet_obyvatel,
            cha.bydleni_txt AS skupina,
            NULL::character varying AS poskupina,
            cha.atribut
           FROM demographics.census_housing_arrangement cha) c ON c.uzemi_kod = clcc.uzemi_kod_mc_obec
  WHERE clcc.uzemi_txt_obec::text = 'Praha'::text
  ORDER BY c.uzemi_kod, c.atribut;


-- analytic.v_prague_portal_census_average_age source

CREATE OR REPLACE VIEW analytic.v_prague_portal_census_average_age
AS SELECT cgaa.uzemi_cis,
    cgaa.uzemi_kod,
    cgaa.uzemi_txt,
    cgaa.prumerny_vek,
    cgaa.pohlavi_txt,
    cgaa.atribut,
    cgaa.uroven
   FROM demographics.census_gender_average_age cgaa
     LEFT JOIN demographics.code_list_cr_csu clcc ON clcc.uzemi_kod_mc_obec = cgaa.uzemi_kod
  WHERE clcc.uzemi_txt_obec::text = 'Praha'::text AND cgaa.uzemi_cis = 44
UNION
 SELECT cgaa.uzemi_cis,
    cgaa.uzemi_kod,
    cgaa.uzemi_txt,
    cgaa.prumerny_vek,
    cgaa.pohlavi_txt,
    cgaa.atribut,
    cgaa.uroven
   FROM demographics.census_gender_average_age cgaa
     LEFT JOIN demographics.code_list_cr_csu clcc ON clcc.uzemi_kod_obec = cgaa.uzemi_kod
  WHERE clcc.uzemi_txt_obec::text = 'Praha'::text AND cgaa.uzemi_cis = 43
UNION
 SELECT cgaa.uzemi_cis,
    cgaa.uzemi_kod,
    cgaa.uzemi_txt,
    cgaa.prumerny_vek,
    cgaa.pohlavi_txt,
    cgaa.atribut,
    cgaa.uroven
   FROM demographics.census_gender_average_age cgaa
     LEFT JOIN demographics.code_list_cr_csu clcc ON clcc.uzemi_kod_orp_praha = cgaa.uzemi_kod::double precision
  WHERE clcc.uzemi_txt_obec::text = 'Praha'::text AND cgaa.uzemi_cis = 72
  ORDER BY 2;		   