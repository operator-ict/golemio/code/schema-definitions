/* Replace with your SQL commands */

-- Upravil: Pavel Procházka, dne: 20.1.2020
-- úprava názvů dle standardu

-- zěna datasetu: energie --> consumption_energy

-- zevednuti sekvenceru
select setval('meta.dataset_id_dataset_seq',(SELECT last_value FROM meta.dataset_id_dataset_seq),true);

INSERT INTO meta.dataset(
	--	id_dataset, --nevyplňovat - autoincrement
	code_dataset, 
	name_dataset, 
	description, 
	retention_days, 
	h_retention_days, 
	created_at
	)
	VALUES (
	--id_dataset, 
	'consumption_energy',		--code_dataset, 
	'consumption_energy ',	--name_dataset, 
	'Spotřeby energií městských budov',	--descr, 
	null,			--retention_days, 
	null,			--h_retention_days, 
	now() 			--created_at
	);


CREATE TABLE public.consumption_energy_buildings (
	id int4 NOT NULL,							--Id
	building_name varchar(255) NOT NULL,					-- Name
	description varchar(255) NOT NULL,		-- Description
	building_address_code varchar(255) NOT NULL,	-- BuildingAddressCode
	building_label varchar(255) NOT NULL,		-- "Label"
	current_note varchar(255) NOT NULL,	-- "CurrentNote"
	main_use varchar(255) NOT NULL,	-- "MainUse"
	secondary_use varchar(255) NOT NULL,		-- "SecondaryUse"
	year_of_construction varchar(255) NOT NULL,		-- "YearOfConstruction"
	method_of_protection varchar(255) NOT NULL,		-- "MethodOfProtection"
	built_up_area varchar(255) NOT NULL, 		-- "BuiltUpArea"
	heated_bulding_volume varchar(255) NOT NULL, 	-- "HeatedBuldingVolume"
	students_count varchar(255) NOT NULL,		-- "StudentsCount"
	employees_count varchar(255) NOT NULL,		-- "EmployeesCount"
	classrooms_count varchar(255) NOT NULL,	-- "ClassroomsCount"
	beds_count varchar(255) NOT NULL,		-- "BedsCount"
	eno_id varchar(255) NOT NULL,	 -- "ENOId"
	csu_code varchar(255) NOT NULL,		-- "CSUCode"
	ku_code varchar(255) NOT NULL,		-- "KUCode" 
	allotment_number varchar(255) NOT NULL,		-- "AllotmentNumber"
	registration_unit varchar(255) NOT NULL,	-- "RegistrationUnit"
	gas_consumption_normatives varchar(255) NOT NULL,	-- "GasConsumptionNormatives"
	heat_consumption_normatives varchar(255) NOT NULL,		-- "HeatConsumptionNormatives" 
	water_consumption_normatives varchar(255) NOT NULL,		-- "WaterConsumptionNormatives"
	electricity_consumption_normatives_per_person varchar(255) NOT NULL, -- "ElectricityConsumptionNormativesPerPerson"
	electricity_consumption_normatives varchar(255) NOT NULL,		-- "ElectricityConsumptionNormatives"
	energetic_management varchar(255) NOT NULL,		-- "EnergeticManagement"
	opening_hours varchar(255) NOT NULL,		-- "OpeningHours"
	weekend_opening_hours varchar(255) NOT NULL,	-- "WeekendOpeningHours"
	latitude varchar(255) NOT NULL,	-- "Latitude"
	longitude varchar(255) NOT NULL,		-- "Longitude"
	address_street varchar(255) NOT NULL,		-- "Address_street"
	address_house_number varchar(255) NOT NULL,	-- "Address_houseNumber"
	address_city varchar(255) NOT NULL,		-- "Address_city"
	address_country varchar(255) NOT NULL,  -- "Address_country"
	address_mail varchar(255) NOT NULL,	-- "Address_mail"
	address_phone varchar(255) NOT NULL,		-- "Address_phone"
	address_web_address varchar(255) NOT NULL,		-- "Address_webAddress"
	penb_penbnumber varchar(255) NOT NULL,		-- "PENB_penbnumber"
	penb_issue_date varchar(255) NOT NULL,		-- "PENB_issueDate"
	penb_total_building_envelope_area varchar(255) NOT NULL,		-- "PENB_totalBuildingEnvelopeArea"
	penb_volume_factor_of_avshape varchar(255) NOT NULL,	-- "PENB_volumeFactorOfAvshape"
	penb_total_energy_reference_area varchar(255) NOT NULL,		-- "PENB_totalEnergyReferenceArea"
	penb_total_provided_energy varchar(255) NOT NULL,		-- "PENB_totalProvidedEnergy"
	penb_total_provided_energy_category varchar(255) NOT NULL,  	-- "PENB_totalProvidedEnergyCategory"
	penb_primary_non_renewable_energy varchar(255) NOT NULL,		-- "PENB_primaryNonRenewableEnergy"
	penb_primary_non_renewable_energy_category varchar(255) NOT NULL,		-- "PENB_primaryNonRenewableEnergyCategory"
	penb_building_envelope varchar(255) NOT NULL,		-- "PENB_buildingEnvelope"
	penb_building_envelope_category varchar(255) NOT NULL,		-- "PENB_buildingEnvelopeCategory"
	penb_heating varchar(255) NOT NULL,		-- "PENB_heating"
	penb_heating_category varchar(255) NOT NULL,		-- "PENB_heatingCategory"
	penb_cooling varchar(255) NOT NULL,		-- "PENB_cooling"
	penb_cooling_category varchar(255) NOT NULL,		-- "PENB_coolingCategory"
	penb_ventilation varchar(255) NOT NULL,		-- "PENB_ventilation"
	penb_ventilation_category varchar(255) NOT NULL,		-- "PENB_ventilationCategory"
	penb_humidity_adjustment varchar(255) NOT NULL,		-- "PENB_humidityAdjustment"
	penb_humidity_adjustment_category varchar(255) NOT NULL,		-- "PENB_humidityAdjustmentCategory"
	penb_warm_water varchar(255) NOT NULL,		-- "PENB_warmWater"
	penb_warm_water_category varchar(255) NOT NULL,		-- "PENB_warmWaterCategory"
	penb_lighting varchar(255) NOT NULL,		-- "PENB_lighting"
	penb_lighting_category varchar(255) NOT NULL,		-- "PENB_lightingCategory"
	energy_audits_energy_audit varchar(255) NOT NULL,		-- "EnergyAudits_energyAudit"
	energy_audits_earegistration_number varchar(255) NOT NULL,		-- "EnergyAudits_earegistrationNumber"
	energy_audits_created_at varchar(255) NOT NULL,		-- "EnergyAudits_createdAt"
	building_envelope_side_wall_prevailing_construction varchar(255) NOT NULL,		-- "BuildingEnvelope_sideWall_prevailingConstruction"
	building_envelope_side_wall_area varchar(255) NOT NULL,		-- "BuildingEnvelope_sideWall_area"
	building_envelope_side_wall_heat_insulation varchar(255) NOT NULL,		-- "BuildingEnvelope_sideWall_heatInsulation"
	building_envelope_side_wall_year_of_adjustment varchar(255) NOT NULL,		-- "BuildingEnvelope_sideWall_yearOfAdjustment"
	building_envelope_side_wall_technical_condition varchar(255) NOT NULL,		-- "BuildingEnvelope_sideWall_technicalCondition"
	building_envelope_filling_of_hole_construction varchar(255) NOT NULL,		-- "BuildingEnvelope_fillingOfHole_construction"
	building_envelope_filling_of_hole_area varchar(255) NOT NULL,		-- "BuildingEnvelope_fillingOfHole_area"
	building_envelope_filling_of_hole_year_of_adjustment varchar(255) NOT NULL,		-- "BuildingEnvelope_fillingOfHole_yearOfAdjustment"
	building_envelope_filling_of_hole_technical_condition varchar(255) NOT NULL,		-- "BuildingEnvelope_fillingOfHole_technicalCondition"
	building_envelope_roof_construction varchar(255) NOT NULL,		-- "BuildingEnvelope_roof_construction"
	building_envelope_roof_area varchar(255) NOT NULL,		-- "BuildingEnvelope_roof_area"
	building_envelope_roof_thermal_insulation varchar(255) NOT NULL,		-- "BuildingEnvelope_roof_thermalInsulation"
	building_envelope_roof_year_of_adjustment varchar(255) NOT NULL,		-- "BuildingEnvelope_roof_yearOfAdjustment"
	building_envelope_roof_technical_condition varchar(255) NOT NULL,		-- "BuildingEnvelope_roof_technicalCondition"
	building_envelope_floor_of_the_lowest_heated_floor_construction varchar(255) NOT NULL,		-- "BuildingEnvelope_floorOfTheLowestHeatedFloor_construction"
	building_envelope_floor_of_the_lowest_heated_floor_area varchar(255) NOT NULL,		-- "BuildingEnvelope_floorOfTheLowestHeatedFloor_area"
	floor_of_the_lowest_heated_floor_thermal_insulation varchar(255) NOT NULL,		-- "floorOfTheLowestHeatedFloor_thermalInsulation"
	building_envelope_floor_of_the_lowest_heated_floor_year_of_adjustment varchar(255) NOT NULL,		-- "BuildingEnvelope_floorOfTheLowestHeatedFloor_yearOfAdjustment"
	floor_of_the_lowest_heated_floor_technical_condition varchar(255) NOT NULL,		-- "floorOfTheLowestHeatedFloor_technicalCondition"
	fuel_and_energy_coal varchar(255) NOT NULL,		-- "FuelAndEnergy_coal"
	fuel_and_energy_gas varchar(255) NOT NULL,		-- "FuelAndEnergy_gas"
	fuel_and_energy_electricity varchar(255) NOT NULL,		-- "FuelAndEnergy_electricity"
	fuel_and_energy_czt varchar(255) NOT NULL,		-- "FuelAndEnergy_czt"
	fuel_and_energy_oze varchar(255) NOT NULL,		-- "FuelAndEnergy_oze"
	fuel_and_energy_other varchar(255) NOT NULL,		-- "FuelAndEnergy_other"
	technical_equipment_heating_main_source_of_heat varchar(255) NOT NULL,		-- "TechnicalEquipment_heating_mainSourceOfHeat"
	technical_equipment_heating_heat_percentage varchar(255) NOT NULL,		-- "TechnicalEquipment_heating_heatPercentage"
	technical_equipment_heating_secondary_source_of_heat varchar(255) NOT NULL,		-- "TechnicalEquipment_heating_secondarySourceOfHeat"
	technical_equipment_heating_heating_system varchar(255) NOT NULL,		-- "TechnicalEquipment_heating_heatingSystem"
	technical_equipment_heating_year varchar(255) NOT NULL,		-- "TechnicalEquipment_heating_year"
	technical_equipment_heating_technical_condition varchar(255) NOT NULL,		-- "TechnicalEquipment_heating_technicalCondition"
	technical_equipment_cooling_cooling_system varchar(255) NOT NULL,		-- "TechnicalEquipment_cooling_coolingSystem"
	technical_equipment_cooling_cooling_area_percentage varchar(255) NOT NULL,		-- "TechnicalEquipment_cooling_coolingAreaPercentage"
	technical_equipment_cooling_year varchar(255) NOT NULL,		-- "TechnicalEquipment_cooling_year"
	technical_equipment_cooling_technical_condition varchar(255) NOT NULL,		-- "TechnicalEquipment_cooling_technicalCondition"
	technical_equipment_ventilation_ventilation varchar(255) NOT NULL,		-- "TechnicalEquipment_ventilation_ventilation"
	technical_equipment_ventilation_year varchar(255) NOT NULL,		-- "TechnicalEquipment_ventilation_year"
	technical_equipment_ventilation_technical_condition varchar(255) NOT NULL,		-- "TechnicalEquipment_ventilation_technicalCondition"
	technical_equipment_humidity_adjustment_humidity_adjustment varchar(255) NOT NULL,		-- "TechnicalEquipment_humidityAdjustment_humidityAdjustment"
	technical_equipment_humidity_adjustment_year varchar(255) NOT NULL,		-- "TechnicalEquipment_humidityAdjustment_year"
	technical_equipment_humidity_adjustment_technical_condition varchar(255) NOT NULL,		-- "TechnicalEquipment_humidityAdjustment_technicalCondition"
	technical_equipment_hot_water_predominant_way_of_heating_tv varchar(255) NOT NULL,		-- "TechnicalEquipment_hotWater_predominantWayOfHeatingTv"
	technical_equipment_hot_water_hot_water_source varchar(255) NOT NULL,		-- "TechnicalEquipment_hotWater_hotWaterSource"
	technical_equipment_hot_water_year varchar(255) NOT NULL,		-- "TechnicalEquipment_hotWater_year"
	technical_equipment_hot_water_technical_condition varchar(255) NOT NULL,		-- "TechnicalEquipment_hotWater_technicalCondition"
	technical_equipment_lighting_lighting varchar(255) NOT NULL,		-- "TechnicalEquipment_lighting_lighting"
	technical_equipment_lighting_year varchar(255) NOT NULL,		-- "TechnicalEquipment_lighting_year"
	technical_equipment_lighting_technical_condition varchar(255) NOT NULL,		-- "TechnicalEquipment_lighting_technicalCondition"
	technical_equipment_lighting_other_technological_elements varchar(255) NOT NULL,		-- "TechnicalEquipment_lighting_otherTechnologicalElements"
	technical_equipment_lighting_measurement_method varchar(255) NOT NULL,		-- "TechnicalEquipment_lighting_measurementMethod"
	oze_energy_production_solar_energy_photovoltaic varchar(255) NOT NULL,		-- "OZEEnergyProduction_solarEnergyPhotovoltaic"
	oze_energy_production_solar_energy_photothermal varchar(255) NOT NULL,		-- "OZEEnergyProduction_solarEnergyPhotothermal"
	oze_energy_production_integrated_turbines_wind_energy varchar(255) NOT NULL,		-- "OZEEnergyProduction_integratedTurbinesWindEnergy"
	oze_energy_production_heat_pump varchar(255) NOT NULL,		-- "OZEEnergyProduction_heatPump"
	waste_and_emissions_solid_waste_production varchar(255) NOT NULL,		-- "WasteAndEmissions_solidWasteProduction"
	waste_and_emissions_tied_co2_emissions varchar(255) NOT NULL,		-- "WasteAndEmissions_tiedCo2emissions"
	waste_and_emissions_sox_emissions varchar(255) NOT NULL,		-- "WasteAndEmissions_soxEmissions"
	waste_and_emissions_operating_co_2emissions varchar(255) NOT NULL,		-- "WasteAndEmissions_operatingCo2emissions"
	link varchar(255) NOT NULL,		-- "Link"
	CONSTRAINT consumption_energy_buildings_pkey PRIMARY KEY (id)
);

COMMENT ON TABLE public.consumption_energy_buildings IS 'seznam všech budov';


SELECT meta.add_audit_fields('public','consumption_energy_buildings');

-- zaevidování do meta.extract
INSERT INTO meta."extract"(
	name_extract, 
	id_dataset, 
	id_db, 
	description, 
	schema_extract, 
	retention_days, 
	h_retention_days, 
	created_at)
	
	VALUES (
	'consumption_energy_buildings',		-- name_extract, 
	(select id_dataset from meta.dataset where code_dataset = 'consumption_energy'),		--;id_dataset, 
	1,		--	1 - postgres, 2 mongodb_db	id_db, 
	'Sledované budovy',	--description, 
	'public',	--schema_extract, 
	null,	--retention_days, 
	null,	--h_retention_days, 
	now())
on conflict (name_extract) do nothing;
--------------------------------------------------------------------------------------


CREATE TABLE public.consumption_energy_devices (
	id int4 NOT NULL,
	addr varchar(255) NOT NULL,
	description varchar(255) NOT NULL,
	meter_number varchar(255) NOT NULL,		-- "meterNumber"
	meter_index varchar(255) NOT NULL,		-- "meterIndex"
	location_number varchar(255) NOT NULL,		-- "locationNumber"
	location_description varchar(255) NOT NULL,		-- "locationDescription"
	include_in_evaluation varchar(255) NOT NULL,		-- "includeInEvaluation"
	meter_type varchar(255) NOT NULL,		-- "meterType"
	category varchar(255) NOT NULL,
	unit varchar(255) NOT NULL,
	replaced_meter_id varchar(255) NOT NULL,		-- "replacedMeterId"
	deleted varchar(255) NOT NULL,
	building_id int4 NOT NULL,		-- "buildingId"
	CONSTRAINT consumption_energy_devices_pkey PRIMARY KEY (id)
);

comment on table public.consumption_energy_devices IS 'seznam všech měřidel';

SELECT meta.add_audit_fields('public','consumption_energy_devices');

-- zaevidování do meta.extract
INSERT INTO meta."extract"(
	name_extract, 
	id_dataset, 
	id_db, 
	description, 
	schema_extract, 
	retention_days, 
	h_retention_days, 
	created_at)
	
	VALUES (
	'consumption_energy_devices',		-- name_extract, 
	(select id_dataset from meta.dataset where code_dataset = 'consumption_energy'),		--;id_dataset, 
	1,		--	1 - postgres, 2 mongodb_db	id_db, 
	'Měřící zařízení',	--description, 
	'public',	--schema_extract, 
	null,	--retention_days, 
	null,	--h_retention_days, 
	now())
on conflict (name_extract) do nothing;
--------------------------------------------------------------------------------------

-- změna názvu tabulky: energie_spotreby --> consumption_energy_consumption
CREATE TABLE public.consumption_energy_consumption (
	time_utc timestamp NOT NULL,		-- "timeUtc"
	value numeric(30,15) NOT NULL,
	addr varchar(255) NOT NULL,
	var varchar(255) NOT NULL,
	"type" varchar(255) NULL,
	commodity varchar(255) NULL,
	unit varchar(255) NULL,
	meter varchar(255) NOT NULL,
	CONSTRAINT consumption_energy_consumption_pkey PRIMARY KEY (time_utc, addr, var, meter)
);

comment on table public.consumption_energy_consumption IS 'všechny naměřené hodnoty ze všech měřidel za celou historii';

SELECT meta.add_audit_fields('public','consumption_energy_consumption');

-- zaevidování do meta.extract
INSERT INTO meta."extract"(
	name_extract, 
	id_dataset, 
	id_db, 
	description, 
	schema_extract, 
	retention_days, 
	h_retention_days, 
	created_at)
	
	VALUES (
	'consumption_energy_consumption',		-- name_extract, 
	(select id_dataset from meta.dataset where code_dataset = 'consumption_energy'),		--;id_dataset, 
	1,		--	1 - postgres, 2 mongodb_db	id_db, 
	'Hodnoty měření',	--description, 
	'public',	--schema_extract, 
	null,	--retention_days, 
	null,	--h_retention_days, 
	now())
on conflict (name_extract) do nothing;	
--------------------------------------------------------------------------------------


INSERT INTO meta.dataset(
	--	id_dataset, --nevyplňovat - autoincrement
	code_dataset, 
	name_dataset, 
	description, 
	retention_days, 
	h_retention_days, 
	created_at
	)
	VALUES (
	--id_dataset, 
	'common_tables',		--code_dataset, 
	'common_tables',	--name_dataset, 
	'obecné číselníky, společné tabulky',	--descr, 
	null,			--retention_days, 
	null,			--h_retention_days, 
	now() 			--created_at
	)
ON CONFLICT DO NOTHING;

--CREATE EXTENSION postgis;	

/*
--název tabulky: mestske_casti --> citydistricts
CREATE TABLE public.citydistricts (
	--id serial NOT NULL,
	geom geometry(POLYGON, 4326) NULL,
	objectid int4 NULL,		-- "OBJECTID"
	create_date varchar NULL,		-- "DAT_VZNIK"
	chnage_date varchar NULL,		-- "DAT_ZMENA"
	area float8 NULL,		-- "PLOCHA"
	id int4 NULL,		-- "ID"
	zip int4 NULL,		-- "KOD_MC"
	district_name varchar NULL,		-- "NAZEV_MC"
	kod_mo int4 NULL,		-- "KOD_MO"
	kod_so varchar NULL,		-- "KOD_SO"
	tid_tm_district_p int4 NULL,		-- "TID_TMMESTSKECASTI_P"
	provider varchar NULL,		-- "POSKYT"
	id_provider int4 NULL,		-- "ID_POSKYT"
	chnage_statut varchar NULL,		-- "STAV_ZMENA"
	name_1 varchar NULL,		-- "NAZEV_1"
	shape_length float8 NULL,		-- "Shape_Length"
	shape_area float8 NULL,		-- "Shape_Area"
	CONSTRAINT citydistricts_pkey PRIMARY KEY (id)
);
CREATE INDEX sidx_citydistricts_geom ON public.citydistricts USING gist (geom);

-- zaevidování do meta.extract
INSERT INTO meta."extract"(
	name_extract, 
	id_dataset, 
	id_db, 
	description, 
	schema_extract, 
	retention_days, 
	h_retention_days, 
	created_at)
	
	VALUES (
	'citydistricts',		-- name_extract, 
	(select id_dataset from meta.dataset where code_dataset = 'common_tables'),		--;id_dataset, 
	1,		--	1 - postgres, 2 mongodb_db	id_db, 
	'Seznam městských částí Prahy',	--description, 
	'public',	--schema_extract, 
	null,	--retention_days, 
	null,	--h_retention_days, 
	now())
on conflict (name_extract) do nothing;	
*/



-- analytic.consumption_gas_daily_reserves definition

CREATE TABLE analytic.consumption_gas_daily_reserves (
	object_name varchar(255) NULL,
	device_id varchar(255) NULL,
	relevance_year int4 NULL,
	relevance_month int4 NULL,
	daily_reserved_capacity_m3 numeric NULL
);

-- vpalac
--select setval('meta.dataset_id_dataset_seq',(SELECT last_value FROM meta.dataset_id_dataset_seq),true);

INSERT INTO meta.dataset(
	--	id_dataset, --nevyplňovat - autoincrement
	code_dataset, 
	name_dataset, 
	description, 
	retention_days, 
	h_retention_days, 
	created_at
	)
	VALUES (
	--id_dataset, 
	'vpalac',		--code_dataset, 
	'vpalac',	--name_dataset, 
	'Vrtbovský plaác - Spotřeby energií městských budov',	--descr, 
	null,			--retention_days, 
	null,			--h_retention_days, 
	now() 			--created_at
	);



-- public.vpalac_measurement definition

CREATE TABLE public.vpalac_measurement (
	var_id int4 NOT NULL,
	time_measurement int8 NOT NULL,
	value numeric NULL,
	created_at timestamptz NULL DEFAULT now(),
	updated_at timestamptz NULL,
	CONSTRAINT vpalac_measurement_pk PRIMARY KEY (var_id, time_measurement)
);

-- zaevidování do meta.extract
INSERT INTO meta."extract"(
	name_extract, 
	id_dataset, 
	id_db, 
	description, 
	schema_extract, 
	retention_days, 
	h_retention_days, 
	created_at)
	
	VALUES (
	'vpalac_measurement',		-- name_extract, 
	(select id_dataset from meta.dataset where code_dataset = 'vpalac'),		--;id_dataset, 
	1,		--	1 - postgres, 2 mongodb_db	id_db, 
	'Měření vpalac',	--description, 
	'public',	--schema_extract, 
	null,	--retention_days, 
	null,	--h_retention_days, 
	now())
on conflict (name_extract) do nothing;

-- public.vpalac_measuring_equipment definition
CREATE TABLE public.vpalac_measuring_equipment (
	me_id int4 NOT NULL,
	me_fakt bool NULL,
	umisteni varchar(250) NULL,
	mis_nazev varchar(255) NULL,
	me_serial varchar(50) NULL,
	me_od timestamptz NULL,
	me_do timestamptz NULL,
	pot_id int4 NOT NULL,
	poc_typode int4 NULL,
	var_id int4 NULL,
	mis_id int4 NULL,
	met_id int4 NULL,
	me_extid varchar(50) NULL,
	me_zapoc bool NULL,
	me_plom varchar(250) NULL,
	created_at timestamptz NULL DEFAULT now(),
	updated_at timestamptz NULL,
	CONSTRAINT vpalac_measuring_equipment_pkey PRIMARY KEY (me_id, pot_id)
);

-- zaevidování do meta.extract
INSERT INTO meta."extract"(
	name_extract, 
	id_dataset, 
	id_db, 
	description, 
	schema_extract, 
	retention_days, 
	h_retention_days, 
	created_at)
	
	VALUES (
	'vpalac_measuring_equipment',		-- name_extract, 
	(select id_dataset from meta.dataset where code_dataset = 'vpalac'),		--;id_dataset, 
	1,		--	1 - postgres, 2 mongodb_db	id_db, 
	'Měřící zařízení vpalac',	--description, 
	'public',	--schema_extract, 
	null,	--retention_days, 
	null,	--h_retention_days, 
	now())
on conflict (name_extract) do nothing;

-- public.vpalac_meter_type definition
CREATE TABLE public.vpalac_meter_type (
	met_id int4 NOT NULL,
	met_druh int4 NULL,
	met_nazev varchar(150) NULL,
	met_kod varchar(50) NULL,
	met_ziv numeric NULL,
	vyr_zkr varchar(50) NULL,
	fir_id int4 NULL,
	medium int4 NULL,
	created_at timestamptz NULL DEFAULT now(),
	updated_at timestamptz NULL,
	CONSTRAINT vpalac_meter_type_pkey PRIMARY KEY (met_id)
);

-- zaevidování do meta.extract
INSERT INTO meta."extract"(
	name_extract, 
	id_dataset, 
	id_db, 
	description, 
	schema_extract, 
	retention_days, 
	h_retention_days, 
	created_at)
	
	VALUES (
	'vpalac_meter_type',		-- name_extract, 
	(select id_dataset from meta.dataset where code_dataset = 'vpalac'),		--;id_dataset, 
	1,		--	1 - postgres, 2 mongodb_db	id_db, 
	'Typ měření vpalac',	--description, 
	'public',	--schema_extract, 
	null,	--retention_days, 
	null,	--h_retention_days, 
	now())
on conflict (name_extract) do nothing;


-- public.vpalac_type_measuring_equipment definition
CREATE TABLE public.vpalac_type_measuring_equipment (
	cik_nazev varchar(150) NULL,
	lt_key varchar(50) NOT NULL,
	cik_fk int4 NULL,
	cik_char varchar(50) NULL,
	cik_cislo int4 NULL,
	cik_double numeric NULL,
	cik_pzn varchar(50) NULL,
	cik_cislo2 int4 NULL,
	cik_zprac varchar(50) NULL,
	cik_akt bool NULL,
	created_at timestamptz NULL DEFAULT now(),
	updated_at timestamptz NULL,
	CONSTRAINT vpalac_type_measuring_equipment_pkey PRIMARY KEY (lt_key)
);

-- zaevidování do meta.extract
INSERT INTO meta."extract"(
	name_extract, 
	id_dataset, 
	id_db, 
	description, 
	schema_extract, 
	retention_days, 
	h_retention_days, 
	created_at)
	
	VALUES (
	'vpalac_type_measuring_equipment',		-- name_extract, 
	(select id_dataset from meta.dataset where code_dataset = 'vpalac'),		--;id_dataset, 
	1,		--	1 - postgres, 2 mongodb_db	id_db, 
	'Typ měřícího zařízení vpalac',	--description, 
	'public',	--schema_extract, 
	null,	--retention_days, 
	null,	--h_retention_days, 
	now())
on conflict (name_extract) do nothing;

-- public.vpalac_units definition
CREATE TABLE public.vpalac_units (
	pot_id int4 NULL,
	lt_key varchar(50) NOT NULL,
	jed_nazev varchar(150) NULL,
	jed_zkr varchar(50) NULL,
	pot_defcolor varchar(20) NULL,
	pot_type int4 NULL,
	jed_id int4 NULL,
	ptv_id int4 NULL,
	created_at timestamptz NULL DEFAULT now(),
	updated_at timestamptz NULL,
	CONSTRAINT vpalac_units_pkey PRIMARY KEY (lt_key)
);

-- zaevidování do meta.extract
INSERT INTO meta."extract"(
	name_extract, 
	id_dataset, 
	id_db, 
	description, 
	schema_extract, 
	retention_days, 
	h_retention_days, 
	created_at)
	
	VALUES (
	'vpalac_units',		-- name_extract, 
	(select id_dataset from meta.dataset where code_dataset = 'vpalac'),		--;id_dataset, 
	1,		--	1 - postgres, 2 mongodb_db	id_db, 
	'Jednotky vpalac',	--description, 
	'public',	--schema_extract, 
	null,	--retention_days, 
	null,	--h_retention_days, 
	now())
on conflict (name_extract) do nothing;

--------------------------------------------------------------------------------------
--ini load

-- /iniload


-- pohledy:

CREATE OR REPLACE VIEW analytic.v_consumption_energy_buildings_real
AS SELECT consumption_energy_buildings.id,
    consumption_energy_buildings.building_name,
    consumption_energy_buildings.description,
    consumption_energy_buildings.building_address_code,
    consumption_energy_buildings.building_label,
    consumption_energy_buildings.current_note,
    consumption_energy_buildings.main_use,
    consumption_energy_buildings.secondary_use,
    consumption_energy_buildings.year_of_construction,
    consumption_energy_buildings.method_of_protection,
    consumption_energy_buildings.built_up_area,
    consumption_energy_buildings.heated_bulding_volume,
    consumption_energy_buildings.students_count,
    consumption_energy_buildings.employees_count,
    consumption_energy_buildings.classrooms_count,
    consumption_energy_buildings.beds_count,
    consumption_energy_buildings.eno_id,
    consumption_energy_buildings.csu_code,
    consumption_energy_buildings.ku_code,
    consumption_energy_buildings.allotment_number,
    consumption_energy_buildings.registration_unit,
    consumption_energy_buildings.gas_consumption_normatives,
    consumption_energy_buildings.heat_consumption_normatives,
    consumption_energy_buildings.water_consumption_normatives,
    consumption_energy_buildings.electricity_consumption_normatives_per_person,
    consumption_energy_buildings.electricity_consumption_normatives,
    consumption_energy_buildings.energetic_management,
    consumption_energy_buildings.opening_hours,
    consumption_energy_buildings.weekend_opening_hours,
    consumption_energy_buildings.latitude,
    consumption_energy_buildings.longitude,
    consumption_energy_buildings.address_street,
    consumption_energy_buildings.address_house_number,
    consumption_energy_buildings.address_city,
    consumption_energy_buildings.address_country,
    consumption_energy_buildings.address_mail,
    consumption_energy_buildings.address_phone,
    consumption_energy_buildings.address_web_address,
    consumption_energy_buildings.penb_penbnumber,
    consumption_energy_buildings.penb_issue_date,
    consumption_energy_buildings.penb_total_building_envelope_area,
    consumption_energy_buildings.penb_volume_factor_of_avshape,
    consumption_energy_buildings.penb_total_energy_reference_area,
    consumption_energy_buildings.penb_total_provided_energy,
    consumption_energy_buildings.penb_total_provided_energy_category,
    consumption_energy_buildings.penb_primary_non_renewable_energy,
    consumption_energy_buildings.penb_primary_non_renewable_energy_category,
    consumption_energy_buildings.penb_building_envelope,
    consumption_energy_buildings.penb_building_envelope_category,
    consumption_energy_buildings.penb_heating,
    consumption_energy_buildings.penb_heating_category,
    consumption_energy_buildings.penb_cooling,
    consumption_energy_buildings.penb_cooling_category,
    consumption_energy_buildings.penb_ventilation,
    consumption_energy_buildings.penb_ventilation_category,
    consumption_energy_buildings.penb_humidity_adjustment,
    consumption_energy_buildings.penb_humidity_adjustment_category,
    consumption_energy_buildings.penb_warm_water,
    consumption_energy_buildings.penb_warm_water_category,
    consumption_energy_buildings.penb_lighting,
    consumption_energy_buildings.penb_lighting_category,
    consumption_energy_buildings.energy_audits_energy_audit,
    consumption_energy_buildings.energy_audits_earegistration_number,
    consumption_energy_buildings.energy_audits_created_at,
    consumption_energy_buildings.building_envelope_side_wall_prevailing_construction,
    consumption_energy_buildings.building_envelope_side_wall_area,
    consumption_energy_buildings.building_envelope_side_wall_heat_insulation,
    consumption_energy_buildings.building_envelope_side_wall_year_of_adjustment,
    consumption_energy_buildings.building_envelope_side_wall_technical_condition,
    consumption_energy_buildings.building_envelope_filling_of_hole_construction,
    consumption_energy_buildings.building_envelope_filling_of_hole_area,
    consumption_energy_buildings.building_envelope_filling_of_hole_year_of_adjustment,
    consumption_energy_buildings.building_envelope_filling_of_hole_technical_condition,
    consumption_energy_buildings.building_envelope_roof_construction,
    consumption_energy_buildings.building_envelope_roof_area,
    consumption_energy_buildings.building_envelope_roof_thermal_insulation,
    consumption_energy_buildings.building_envelope_roof_year_of_adjustment,
    consumption_energy_buildings.building_envelope_roof_technical_condition,
    consumption_energy_buildings.building_envelope_floor_of_the_lowest_heated_floor_construction,
    consumption_energy_buildings.building_envelope_floor_of_the_lowest_heated_floor_area,
    consumption_energy_buildings.floor_of_the_lowest_heated_floor_thermal_insulation,
    consumption_energy_buildings.building_envelope_floor_of_the_lowest_heated_floor_year_of_adju,
    consumption_energy_buildings.floor_of_the_lowest_heated_floor_technical_condition,
    consumption_energy_buildings.fuel_and_energy_coal,
    consumption_energy_buildings.fuel_and_energy_gas,
    consumption_energy_buildings.fuel_and_energy_electricity,
    consumption_energy_buildings.fuel_and_energy_czt,
    consumption_energy_buildings.fuel_and_energy_oze,
    consumption_energy_buildings.fuel_and_energy_other,
    consumption_energy_buildings.technical_equipment_heating_main_source_of_heat,
    consumption_energy_buildings.technical_equipment_heating_heat_percentage,
    consumption_energy_buildings.technical_equipment_heating_secondary_source_of_heat,
    consumption_energy_buildings.technical_equipment_heating_heating_system,
    consumption_energy_buildings.technical_equipment_heating_year,
    consumption_energy_buildings.technical_equipment_heating_technical_condition,
    consumption_energy_buildings.technical_equipment_cooling_cooling_system,
    consumption_energy_buildings.technical_equipment_cooling_cooling_area_percentage,
    consumption_energy_buildings.technical_equipment_cooling_year,
    consumption_energy_buildings.technical_equipment_cooling_technical_condition,
    consumption_energy_buildings.technical_equipment_ventilation_ventilation,
    consumption_energy_buildings.technical_equipment_ventilation_year,
    consumption_energy_buildings.technical_equipment_ventilation_technical_condition,
    consumption_energy_buildings.technical_equipment_humidity_adjustment_humidity_adjustment,
    consumption_energy_buildings.technical_equipment_humidity_adjustment_year,
    consumption_energy_buildings.technical_equipment_humidity_adjustment_technical_condition,
    consumption_energy_buildings.technical_equipment_hot_water_predominant_way_of_heating_tv,
    consumption_energy_buildings.technical_equipment_hot_water_hot_water_source,
    consumption_energy_buildings.technical_equipment_hot_water_year,
    consumption_energy_buildings.technical_equipment_hot_water_technical_condition,
    consumption_energy_buildings.technical_equipment_lighting_lighting,
    consumption_energy_buildings.technical_equipment_lighting_year,
    consumption_energy_buildings.technical_equipment_lighting_technical_condition,
    consumption_energy_buildings.technical_equipment_lighting_other_technological_elements,
    consumption_energy_buildings.technical_equipment_lighting_measurement_method,
    consumption_energy_buildings.oze_energy_production_solar_energy_photovoltaic,
    consumption_energy_buildings.oze_energy_production_solar_energy_photothermal,
    consumption_energy_buildings.oze_energy_production_integrated_turbines_wind_energy,
    consumption_energy_buildings.oze_energy_production_heat_pump,
    consumption_energy_buildings.waste_and_emissions_solid_waste_production,
    consumption_energy_buildings.waste_and_emissions_tied_co2_emissions,
    consumption_energy_buildings.waste_and_emissions_sox_emissions,
    consumption_energy_buildings.waste_and_emissions_operating_co_2emissions,
    consumption_energy_buildings.link
   FROM consumption_energy_buildings
  WHERE consumption_energy_buildings.building_label::text <> ALL (ARRAY['ZKB 1'::character varying::text, 'ZKB 2'::character varying::text, 'TB1'::character varying::text, 'TB2'::character varying::text]);

COMMENT ON VIEW analytic.v_consumption_energy_buildings_real IS 'budovy mimo území Prahy';
  
CREATE OR REPLACE VIEW analytic.v_consumption_energy_buildings_real_mc
AS SELECT consumption_energy_buildings.id,
    consumption_energy_buildings.building_name,
    consumption_energy_buildings.description,
    consumption_energy_buildings.building_address_code,
    consumption_energy_buildings.building_label,
    consumption_energy_buildings.current_note,
    consumption_energy_buildings.main_use,
    consumption_energy_buildings.secondary_use,
    consumption_energy_buildings.year_of_construction,
    consumption_energy_buildings.method_of_protection,
    consumption_energy_buildings.built_up_area,
    consumption_energy_buildings.heated_bulding_volume,
    consumption_energy_buildings.students_count,
    consumption_energy_buildings.employees_count,
    consumption_energy_buildings.classrooms_count,
    consumption_energy_buildings.beds_count,
    consumption_energy_buildings.eno_id,
    consumption_energy_buildings.csu_code,
    consumption_energy_buildings.ku_code,
    consumption_energy_buildings.allotment_number,
    consumption_energy_buildings.registration_unit,
    consumption_energy_buildings.gas_consumption_normatives,
    consumption_energy_buildings.heat_consumption_normatives,
    consumption_energy_buildings.water_consumption_normatives,
    consumption_energy_buildings.electricity_consumption_normatives_per_person,
    consumption_energy_buildings.electricity_consumption_normatives,
    consumption_energy_buildings.energetic_management,
    consumption_energy_buildings.opening_hours,
    consumption_energy_buildings.weekend_opening_hours,
    consumption_energy_buildings.latitude,
    consumption_energy_buildings.longitude,
    st_setsrid(st_point(consumption_energy_buildings.longitude::double precision, consumption_energy_buildings.latitude::double precision), 4326) AS geom,
    consumption_energy_buildings.address_street,
    consumption_energy_buildings.address_house_number,
    consumption_energy_buildings.address_city,
    citydistricts.district_name,
    citydistricts.geom AS district_geom,
    consumption_energy_buildings.address_country,
    consumption_energy_buildings.address_mail,
    consumption_energy_buildings.address_phone,
    consumption_energy_buildings.address_web_address,
    consumption_energy_buildings.penb_penbnumber,
    consumption_energy_buildings.penb_issue_date,
    consumption_energy_buildings.penb_total_building_envelope_area,
    consumption_energy_buildings.penb_volume_factor_of_avshape,
    consumption_energy_buildings.penb_total_energy_reference_area,
    consumption_energy_buildings.penb_total_provided_energy,
    consumption_energy_buildings.penb_total_provided_energy_category,
    consumption_energy_buildings.penb_primary_non_renewable_energy,
    consumption_energy_buildings.penb_primary_non_renewable_energy_category,
    consumption_energy_buildings.penb_building_envelope,
    consumption_energy_buildings.penb_building_envelope_category,
    consumption_energy_buildings.penb_heating,
    consumption_energy_buildings.penb_heating_category,
    consumption_energy_buildings.penb_cooling,
    consumption_energy_buildings.penb_cooling_category,
    consumption_energy_buildings.penb_ventilation,
    consumption_energy_buildings.penb_ventilation_category,
    consumption_energy_buildings.penb_humidity_adjustment,
    consumption_energy_buildings.penb_humidity_adjustment_category,
    consumption_energy_buildings.penb_warm_water,
    consumption_energy_buildings.penb_warm_water_category,
    consumption_energy_buildings.penb_lighting,
    consumption_energy_buildings.penb_lighting_category,
    consumption_energy_buildings.energy_audits_energy_audit,
    consumption_energy_buildings.energy_audits_earegistration_number,
    consumption_energy_buildings.energy_audits_created_at,
    consumption_energy_buildings.building_envelope_side_wall_prevailing_construction,
    consumption_energy_buildings.building_envelope_side_wall_area,
    consumption_energy_buildings.building_envelope_side_wall_heat_insulation,
    consumption_energy_buildings.building_envelope_side_wall_year_of_adjustment,
    consumption_energy_buildings.building_envelope_side_wall_technical_condition,
    consumption_energy_buildings.building_envelope_filling_of_hole_construction,
    consumption_energy_buildings.building_envelope_filling_of_hole_area,
    consumption_energy_buildings.building_envelope_filling_of_hole_year_of_adjustment,
    consumption_energy_buildings.building_envelope_filling_of_hole_technical_condition,
    consumption_energy_buildings.building_envelope_roof_construction,
    consumption_energy_buildings.building_envelope_roof_area,
    consumption_energy_buildings.building_envelope_roof_thermal_insulation,
    consumption_energy_buildings.building_envelope_roof_year_of_adjustment,
    consumption_energy_buildings.building_envelope_roof_technical_condition,
    consumption_energy_buildings.building_envelope_floor_of_the_lowest_heated_floor_construction,
    consumption_energy_buildings.building_envelope_floor_of_the_lowest_heated_floor_area,
    consumption_energy_buildings.floor_of_the_lowest_heated_floor_thermal_insulation,
    consumption_energy_buildings.building_envelope_floor_of_the_lowest_heated_floor_year_of_adju,
    consumption_energy_buildings.floor_of_the_lowest_heated_floor_technical_condition,
    consumption_energy_buildings.fuel_and_energy_coal,
    consumption_energy_buildings.fuel_and_energy_gas,
    consumption_energy_buildings.fuel_and_energy_electricity,
    consumption_energy_buildings.fuel_and_energy_czt,
    consumption_energy_buildings.fuel_and_energy_oze,
    consumption_energy_buildings.fuel_and_energy_other,
    consumption_energy_buildings.technical_equipment_heating_main_source_of_heat,
    consumption_energy_buildings.technical_equipment_heating_heat_percentage,
    consumption_energy_buildings.technical_equipment_heating_secondary_source_of_heat,
    consumption_energy_buildings.technical_equipment_heating_heating_system,
    consumption_energy_buildings.technical_equipment_heating_year,
    consumption_energy_buildings.technical_equipment_heating_technical_condition,
    consumption_energy_buildings.technical_equipment_cooling_cooling_system,
    consumption_energy_buildings.technical_equipment_cooling_cooling_area_percentage,
    consumption_energy_buildings.technical_equipment_cooling_year,
    consumption_energy_buildings.technical_equipment_cooling_technical_condition,
    consumption_energy_buildings.technical_equipment_ventilation_ventilation,
    consumption_energy_buildings.technical_equipment_ventilation_year,
    consumption_energy_buildings.technical_equipment_ventilation_technical_condition,
    consumption_energy_buildings.technical_equipment_humidity_adjustment_humidity_adjustment,
    consumption_energy_buildings.technical_equipment_humidity_adjustment_year,
    consumption_energy_buildings.technical_equipment_humidity_adjustment_technical_condition,
    consumption_energy_buildings.technical_equipment_hot_water_predominant_way_of_heating_tv,
    consumption_energy_buildings.technical_equipment_hot_water_hot_water_source,
    consumption_energy_buildings.technical_equipment_hot_water_year,
    consumption_energy_buildings.technical_equipment_hot_water_technical_condition,
    consumption_energy_buildings.technical_equipment_lighting_lighting,
    consumption_energy_buildings.technical_equipment_lighting_year,
    consumption_energy_buildings.technical_equipment_lighting_technical_condition,
    consumption_energy_buildings.technical_equipment_lighting_other_technological_elements,
    consumption_energy_buildings.technical_equipment_lighting_measurement_method,
    consumption_energy_buildings.oze_energy_production_solar_energy_photovoltaic,
    consumption_energy_buildings.oze_energy_production_solar_energy_photothermal,
    consumption_energy_buildings.oze_energy_production_integrated_turbines_wind_energy,
    consumption_energy_buildings.oze_energy_production_heat_pump,
    consumption_energy_buildings.waste_and_emissions_solid_waste_production,
    consumption_energy_buildings.waste_and_emissions_tied_co2_emissions,
    consumption_energy_buildings.waste_and_emissions_sox_emissions,
    consumption_energy_buildings.waste_and_emissions_operating_co_2emissions,
    consumption_energy_buildings.link
   FROM consumption_energy_buildings
     JOIN common.citydistricts ON st_within(st_setsrid(st_point(consumption_energy_buildings.longitude::double precision, consumption_energy_buildings.latitude::double precision), 4326), citydistricts.geom)
  WHERE (consumption_energy_buildings.building_label::text <> ALL (ARRAY['ZKB 1'::character varying::text, 'ZKB 2'::character varying::text, 'TB1'::character varying::text, 'TB2'::character varying::text])) AND consumption_energy_buildings.longitude::text <> ''::text AND consumption_energy_buildings.latitude::text <> ''::text;
  
  COMMENT ON VIEW analytic.v_consumption_energy_buildings_real_mc IS 'budovy s přirazenou městskou částí podle polohy GPS';  
  
  
CREATE OR REPLACE VIEW analytic.v_consumption_energy_buildings_mimo_prahu
AS SELECT v_consumption_energy_buildings_real.id,
    v_consumption_energy_buildings_real.building_name,
    v_consumption_energy_buildings_real.description,
    v_consumption_energy_buildings_real.building_address_code,
    v_consumption_energy_buildings_real.building_label,
    v_consumption_energy_buildings_real.current_note,
    v_consumption_energy_buildings_real.main_use,
    v_consumption_energy_buildings_real.secondary_use,
    v_consumption_energy_buildings_real.year_of_construction,
    v_consumption_energy_buildings_real.method_of_protection,
    v_consumption_energy_buildings_real.built_up_area,
    v_consumption_energy_buildings_real.heated_bulding_volume,
    v_consumption_energy_buildings_real.students_count,
    v_consumption_energy_buildings_real.employees_count,
    v_consumption_energy_buildings_real.classrooms_count,
    v_consumption_energy_buildings_real.beds_count,
    v_consumption_energy_buildings_real.eno_id,
    v_consumption_energy_buildings_real.csu_code,
    v_consumption_energy_buildings_real.ku_code,
    v_consumption_energy_buildings_real.allotment_number,
    v_consumption_energy_buildings_real.registration_unit,
    v_consumption_energy_buildings_real.gas_consumption_normatives,
    v_consumption_energy_buildings_real.heat_consumption_normatives,
    v_consumption_energy_buildings_real.water_consumption_normatives,
    v_consumption_energy_buildings_real.electricity_consumption_normatives_per_person,
    v_consumption_energy_buildings_real.electricity_consumption_normatives,
    v_consumption_energy_buildings_real.energetic_management,
    v_consumption_energy_buildings_real.opening_hours,
    v_consumption_energy_buildings_real.weekend_opening_hours,
    v_consumption_energy_buildings_real.latitude,
    v_consumption_energy_buildings_real.longitude,
    v_consumption_energy_buildings_real.address_street,
    v_consumption_energy_buildings_real.address_house_number,
    v_consumption_energy_buildings_real.address_city,
    v_consumption_energy_buildings_real.address_country,
    v_consumption_energy_buildings_real.address_mail,
    v_consumption_energy_buildings_real.address_phone,
    v_consumption_energy_buildings_real.address_web_address,
    v_consumption_energy_buildings_real.penb_penbnumber,
    v_consumption_energy_buildings_real.penb_issue_date,
    v_consumption_energy_buildings_real.penb_total_building_envelope_area,
    v_consumption_energy_buildings_real.penb_volume_factor_of_avshape,
    v_consumption_energy_buildings_real.penb_total_energy_reference_area,
    v_consumption_energy_buildings_real.penb_total_provided_energy,
    v_consumption_energy_buildings_real.penb_total_provided_energy_category,
    v_consumption_energy_buildings_real.penb_primary_non_renewable_energy,
    v_consumption_energy_buildings_real.penb_primary_non_renewable_energy_category,
    v_consumption_energy_buildings_real.penb_building_envelope,
    v_consumption_energy_buildings_real.penb_building_envelope_category,
    v_consumption_energy_buildings_real.penb_heating,
    v_consumption_energy_buildings_real.penb_heating_category,
    v_consumption_energy_buildings_real.penb_cooling,
    v_consumption_energy_buildings_real.penb_cooling_category,
    v_consumption_energy_buildings_real.penb_ventilation,
    v_consumption_energy_buildings_real.penb_ventilation_category,
    v_consumption_energy_buildings_real.penb_humidity_adjustment,
    v_consumption_energy_buildings_real.penb_humidity_adjustment_category,
    v_consumption_energy_buildings_real.penb_warm_water,
    v_consumption_energy_buildings_real.penb_warm_water_category,
    v_consumption_energy_buildings_real.penb_lighting,
    v_consumption_energy_buildings_real.penb_lighting_category,
    v_consumption_energy_buildings_real.energy_audits_energy_audit,
    v_consumption_energy_buildings_real.energy_audits_earegistration_number,
    v_consumption_energy_buildings_real.energy_audits_created_at,
    v_consumption_energy_buildings_real.building_envelope_side_wall_prevailing_construction,
    v_consumption_energy_buildings_real.building_envelope_side_wall_area,
    v_consumption_energy_buildings_real.building_envelope_side_wall_heat_insulation,
    v_consumption_energy_buildings_real.building_envelope_side_wall_year_of_adjustment,
    v_consumption_energy_buildings_real.building_envelope_side_wall_technical_condition,
    v_consumption_energy_buildings_real.building_envelope_filling_of_hole_construction,
    v_consumption_energy_buildings_real.building_envelope_filling_of_hole_area,
    v_consumption_energy_buildings_real.building_envelope_filling_of_hole_year_of_adjustment,
    v_consumption_energy_buildings_real.building_envelope_filling_of_hole_technical_condition,
    v_consumption_energy_buildings_real.building_envelope_roof_construction,
    v_consumption_energy_buildings_real.building_envelope_roof_area,
    v_consumption_energy_buildings_real.building_envelope_roof_thermal_insulation,
    v_consumption_energy_buildings_real.building_envelope_roof_year_of_adjustment,
    v_consumption_energy_buildings_real.building_envelope_roof_technical_condition,
    v_consumption_energy_buildings_real.building_envelope_floor_of_the_lowest_heated_floor_construction,
    v_consumption_energy_buildings_real.building_envelope_floor_of_the_lowest_heated_floor_area,
    v_consumption_energy_buildings_real.floor_of_the_lowest_heated_floor_thermal_insulation,
    v_consumption_energy_buildings_real.building_envelope_floor_of_the_lowest_heated_floor_year_of_adju,
    v_consumption_energy_buildings_real.floor_of_the_lowest_heated_floor_technical_condition,
    v_consumption_energy_buildings_real.fuel_and_energy_coal,
    v_consumption_energy_buildings_real.fuel_and_energy_gas,
    v_consumption_energy_buildings_real.fuel_and_energy_electricity,
    v_consumption_energy_buildings_real.fuel_and_energy_czt,
    v_consumption_energy_buildings_real.fuel_and_energy_oze,
    v_consumption_energy_buildings_real.fuel_and_energy_other,
    v_consumption_energy_buildings_real.technical_equipment_heating_main_source_of_heat,
    v_consumption_energy_buildings_real.technical_equipment_heating_heat_percentage,
    v_consumption_energy_buildings_real.technical_equipment_heating_secondary_source_of_heat,
    v_consumption_energy_buildings_real.technical_equipment_heating_heating_system,
    v_consumption_energy_buildings_real.technical_equipment_heating_year,
    v_consumption_energy_buildings_real.technical_equipment_heating_technical_condition,
    v_consumption_energy_buildings_real.technical_equipment_cooling_cooling_system,
    v_consumption_energy_buildings_real.technical_equipment_cooling_cooling_area_percentage,
    v_consumption_energy_buildings_real.technical_equipment_cooling_year,
    v_consumption_energy_buildings_real.technical_equipment_cooling_technical_condition,
    v_consumption_energy_buildings_real.technical_equipment_ventilation_ventilation,
    v_consumption_energy_buildings_real.technical_equipment_ventilation_year,
    v_consumption_energy_buildings_real.technical_equipment_ventilation_technical_condition,
    v_consumption_energy_buildings_real.technical_equipment_humidity_adjustment_humidity_adjustment,
    v_consumption_energy_buildings_real.technical_equipment_humidity_adjustment_year,
    v_consumption_energy_buildings_real.technical_equipment_humidity_adjustment_technical_condition,
    v_consumption_energy_buildings_real.technical_equipment_hot_water_predominant_way_of_heating_tv,
    v_consumption_energy_buildings_real.technical_equipment_hot_water_hot_water_source,
    v_consumption_energy_buildings_real.technical_equipment_hot_water_year,
    v_consumption_energy_buildings_real.technical_equipment_hot_water_technical_condition,
    v_consumption_energy_buildings_real.technical_equipment_lighting_lighting,
    v_consumption_energy_buildings_real.technical_equipment_lighting_year,
    v_consumption_energy_buildings_real.technical_equipment_lighting_technical_condition,
    v_consumption_energy_buildings_real.technical_equipment_lighting_other_technological_elements,
    v_consumption_energy_buildings_real.technical_equipment_lighting_measurement_method,
    v_consumption_energy_buildings_real.oze_energy_production_solar_energy_photovoltaic,
    v_consumption_energy_buildings_real.oze_energy_production_solar_energy_photothermal,
    v_consumption_energy_buildings_real.oze_energy_production_integrated_turbines_wind_energy,
    v_consumption_energy_buildings_real.oze_energy_production_heat_pump,
    v_consumption_energy_buildings_real.waste_and_emissions_solid_waste_production,
    v_consumption_energy_buildings_real.waste_and_emissions_tied_co2_emissions,
    v_consumption_energy_buildings_real.waste_and_emissions_sox_emissions,
    v_consumption_energy_buildings_real.waste_and_emissions_operating_co_2emissions,
    v_consumption_energy_buildings_real.link
   FROM analytic.v_consumption_energy_buildings_real
  WHERE NOT (v_consumption_energy_buildings_real.building_name::text IN ( SELECT v_consumption_energy_buildings_real_mc.building_name
           FROM analytic.v_consumption_energy_buildings_real_mc));

COMMENT ON VIEW analytic.v_consumption_energy_buildings_mimo_prahu IS 'budovy mimo území Prahy';  		   
  
CREATE OR REPLACE VIEW analytic.v_consumption_energy_buildings_real_mc_nogeom
AS SELECT v_consumption_energy_buildings_real_mc.id,
    v_consumption_energy_buildings_real_mc.building_name,
    v_consumption_energy_buildings_real_mc.description,
    v_consumption_energy_buildings_real_mc.building_address_code,
    v_consumption_energy_buildings_real_mc.building_label,
    v_consumption_energy_buildings_real_mc.current_note,
    v_consumption_energy_buildings_real_mc.main_use,
    v_consumption_energy_buildings_real_mc.secondary_use,
    v_consumption_energy_buildings_real_mc.year_of_construction,
    v_consumption_energy_buildings_real_mc.method_of_protection,
    v_consumption_energy_buildings_real_mc.built_up_area,
    v_consumption_energy_buildings_real_mc.heated_bulding_volume,
    v_consumption_energy_buildings_real_mc.students_count,
    v_consumption_energy_buildings_real_mc.employees_count,
    v_consumption_energy_buildings_real_mc.classrooms_count,
    v_consumption_energy_buildings_real_mc.beds_count,
    v_consumption_energy_buildings_real_mc.eno_id,
    v_consumption_energy_buildings_real_mc.csu_code,
    v_consumption_energy_buildings_real_mc.ku_code,
    v_consumption_energy_buildings_real_mc.allotment_number,
    v_consumption_energy_buildings_real_mc.registration_unit,
    v_consumption_energy_buildings_real_mc.gas_consumption_normatives,
    v_consumption_energy_buildings_real_mc.heat_consumption_normatives,
    v_consumption_energy_buildings_real_mc.water_consumption_normatives,
    v_consumption_energy_buildings_real_mc.electricity_consumption_normatives_per_person,
    v_consumption_energy_buildings_real_mc.electricity_consumption_normatives,
    v_consumption_energy_buildings_real_mc.energetic_management,
    v_consumption_energy_buildings_real_mc.opening_hours,
    v_consumption_energy_buildings_real_mc.weekend_opening_hours,
    v_consumption_energy_buildings_real_mc.latitude,
    v_consumption_energy_buildings_real_mc.longitude,
    v_consumption_energy_buildings_real_mc.address_street,
    v_consumption_energy_buildings_real_mc.address_house_number,
    v_consumption_energy_buildings_real_mc.address_city,
    v_consumption_energy_buildings_real_mc.district_name,
    v_consumption_energy_buildings_real_mc.address_country,
    v_consumption_energy_buildings_real_mc.address_mail,
    v_consumption_energy_buildings_real_mc.address_phone,
    v_consumption_energy_buildings_real_mc.address_web_address,
    v_consumption_energy_buildings_real_mc.penb_penbnumber,
    v_consumption_energy_buildings_real_mc.penb_issue_date,
    v_consumption_energy_buildings_real_mc.penb_total_building_envelope_area,
    v_consumption_energy_buildings_real_mc.penb_volume_factor_of_avshape,
    v_consumption_energy_buildings_real_mc.penb_total_energy_reference_area,
    v_consumption_energy_buildings_real_mc.penb_total_provided_energy,
    v_consumption_energy_buildings_real_mc.penb_total_provided_energy_category,
    v_consumption_energy_buildings_real_mc.penb_primary_non_renewable_energy,
    v_consumption_energy_buildings_real_mc.penb_primary_non_renewable_energy_category,
    v_consumption_energy_buildings_real_mc.penb_building_envelope,
    v_consumption_energy_buildings_real_mc.penb_building_envelope_category,
    v_consumption_energy_buildings_real_mc.penb_heating,
    v_consumption_energy_buildings_real_mc.penb_heating_category,
    v_consumption_energy_buildings_real_mc.penb_cooling,
    v_consumption_energy_buildings_real_mc.penb_cooling_category,
    v_consumption_energy_buildings_real_mc.penb_ventilation,
    v_consumption_energy_buildings_real_mc.penb_ventilation_category,
    v_consumption_energy_buildings_real_mc.penb_humidity_adjustment,
    v_consumption_energy_buildings_real_mc.penb_humidity_adjustment_category,
    v_consumption_energy_buildings_real_mc.penb_warm_water,
    v_consumption_energy_buildings_real_mc.penb_warm_water_category,
    v_consumption_energy_buildings_real_mc.penb_lighting,
    v_consumption_energy_buildings_real_mc.penb_lighting_category,
    v_consumption_energy_buildings_real_mc.energy_audits_energy_audit,
    v_consumption_energy_buildings_real_mc.energy_audits_earegistration_number,
    v_consumption_energy_buildings_real_mc.energy_audits_created_at,
    v_consumption_energy_buildings_real_mc.building_envelope_side_wall_prevailing_construction,
    v_consumption_energy_buildings_real_mc.building_envelope_side_wall_area,
    v_consumption_energy_buildings_real_mc.building_envelope_side_wall_heat_insulation,
    v_consumption_energy_buildings_real_mc.building_envelope_side_wall_year_of_adjustment,
    v_consumption_energy_buildings_real_mc.building_envelope_side_wall_technical_condition,
    v_consumption_energy_buildings_real_mc.building_envelope_filling_of_hole_construction,
    v_consumption_energy_buildings_real_mc.building_envelope_filling_of_hole_area,
    v_consumption_energy_buildings_real_mc.building_envelope_filling_of_hole_year_of_adjustment,
    v_consumption_energy_buildings_real_mc.building_envelope_filling_of_hole_technical_condition,
    v_consumption_energy_buildings_real_mc.building_envelope_roof_construction,
    v_consumption_energy_buildings_real_mc.building_envelope_roof_area,
    v_consumption_energy_buildings_real_mc.building_envelope_roof_thermal_insulation,
    v_consumption_energy_buildings_real_mc.building_envelope_roof_year_of_adjustment,
    v_consumption_energy_buildings_real_mc.building_envelope_roof_technical_condition,
    v_consumption_energy_buildings_real_mc.building_envelope_floor_of_the_lowest_heated_floor_construction,
    v_consumption_energy_buildings_real_mc.building_envelope_floor_of_the_lowest_heated_floor_area,
    v_consumption_energy_buildings_real_mc.floor_of_the_lowest_heated_floor_thermal_insulation,
    v_consumption_energy_buildings_real_mc.building_envelope_floor_of_the_lowest_heated_floor_year_of_adjustment,
    v_consumption_energy_buildings_real_mc.floor_of_the_lowest_heated_floor_technical_condition,
    v_consumption_energy_buildings_real_mc.fuel_and_energy_coal,
    v_consumption_energy_buildings_real_mc.fuel_and_energy_gas,
    v_consumption_energy_buildings_real_mc.fuel_and_energy_electricity,
    v_consumption_energy_buildings_real_mc.fuel_and_energy_czt,
    v_consumption_energy_buildings_real_mc.fuel_and_energy_oze,
    v_consumption_energy_buildings_real_mc.fuel_and_energy_other,
    v_consumption_energy_buildings_real_mc.technical_equipment_heating_main_source_of_heat,
    v_consumption_energy_buildings_real_mc.technical_equipment_heating_heat_percentage,
    v_consumption_energy_buildings_real_mc.technical_equipment_heating_secondary_source_of_heat,
    v_consumption_energy_buildings_real_mc.technical_equipment_heating_heating_system,
    v_consumption_energy_buildings_real_mc.technical_equipment_heating_year,
    v_consumption_energy_buildings_real_mc.technical_equipment_heating_technical_condition,
    v_consumption_energy_buildings_real_mc.technical_equipment_cooling_cooling_system,
    v_consumption_energy_buildings_real_mc.technical_equipment_cooling_cooling_area_percentage,
    v_consumption_energy_buildings_real_mc.technical_equipment_cooling_year,
    v_consumption_energy_buildings_real_mc.technical_equipment_cooling_technical_condition,
    v_consumption_energy_buildings_real_mc.technical_equipment_ventilation_ventilation,
    v_consumption_energy_buildings_real_mc.technical_equipment_ventilation_year,
    v_consumption_energy_buildings_real_mc.technical_equipment_ventilation_technical_condition,
    v_consumption_energy_buildings_real_mc.technical_equipment_humidity_adjustment_humidity_adjustment,
    v_consumption_energy_buildings_real_mc.technical_equipment_humidity_adjustment_year,
    v_consumption_energy_buildings_real_mc.technical_equipment_humidity_adjustment_technical_condition,
    v_consumption_energy_buildings_real_mc.technical_equipment_hot_water_predominant_way_of_heating_tv,
    v_consumption_energy_buildings_real_mc.technical_equipment_hot_water_hot_water_source,
    v_consumption_energy_buildings_real_mc.technical_equipment_hot_water_year,
    v_consumption_energy_buildings_real_mc.technical_equipment_hot_water_technical_condition,
    v_consumption_energy_buildings_real_mc.technical_equipment_lighting_lighting,
    v_consumption_energy_buildings_real_mc.technical_equipment_lighting_year,
    v_consumption_energy_buildings_real_mc.technical_equipment_lighting_technical_condition,
    v_consumption_energy_buildings_real_mc.technical_equipment_lighting_other_technological_elements,
    v_consumption_energy_buildings_real_mc.technical_equipment_lighting_measurement_method,
    v_consumption_energy_buildings_real_mc.oze_energy_production_solar_energy_photovoltaic,
    v_consumption_energy_buildings_real_mc.oze_energy_production_solar_energy_photothermal,
    v_consumption_energy_buildings_real_mc.oze_energy_production_integrated_turbines_wind_energy,
    v_consumption_energy_buildings_real_mc.oze_energy_production_heat_pump,
    v_consumption_energy_buildings_real_mc.waste_and_emissions_solid_waste_production,
    v_consumption_energy_buildings_real_mc.waste_and_emissions_tied_co2_emissions,
    v_consumption_energy_buildings_real_mc.waste_and_emissions_sox_emissions,
    v_consumption_energy_buildings_real_mc.waste_and_emissions_operating_co_2emissions,
    v_consumption_energy_buildings_real_mc.link
   FROM analytic.v_consumption_energy_buildings_real_mc;

COMMENT ON VIEW analytic.v_consumption_energy_buildings_real_mc_nogeom IS 'budovy s přirazenou městskou částí podle polohy GPS - bez sloupců geom (PowerBI s tím mělo problém)';  		   
   
CREATE OR REPLACE VIEW analytic.v_consumption_energy_consumption
AS SELECT "substring"(consumption_energy_consumption.addr::text, 2, "position"(btrim(consumption_energy_consumption.addr::text, '/'::text), '/'::text) - 1)::character varying(255) AS building_address_code,
    "right"(consumption_energy_consumption.addr::text, char_length(consumption_energy_consumption.addr::text) - "position"(btrim(consumption_energy_consumption.addr::text, '/'::text), '/'::text) - 1)::character varying(255) AS devicetype,
    consumption_energy_consumption.time_utc,
    consumption_energy_consumption.value,
    consumption_energy_consumption.addr,
    consumption_energy_consumption.var,
    consumption_energy_consumption.type,
    consumption_energy_consumption.commodity,
    consumption_energy_consumption.unit,
    consumption_energy_consumption.meter
   FROM consumption_energy_consumption
UNION ALL
 SELECT ((( SELECT btrim(consumption_energy_buildings.building_address_code::text) AS btrim
           FROM consumption_energy_buildings
          WHERE consumption_energy_buildings.building_name::text = 'Vrtbovský palác'::text)))::character varying(255) AS building_address_code,
    mt.met_nazev::character varying(255) AS devicetype,
    to_timestamp((m.time_measurement / 1000)::double precision)::timestamp without time zone AS time_utc,
    m.value::numeric(30,15) AS value,
    NULL::character varying(255) AS addr,
    me.var_id::character varying(255) AS var,
    NULL::character varying(255) AS type,
    NULL::character varying(255) AS commodity,
    NULL::character varying(255) AS unit,
    NULL::character varying(255) AS meter
   FROM vpalac_measurement m
     JOIN vpalac_measuring_equipment me ON me.var_id = m.var_id
     JOIN vpalac_meter_type mt ON mt.met_id = me.met_id;
   
COMMENT ON VIEW analytic.v_consumption_energy_consumption IS 'pomocné view - spotřeby';  		      
  

CREATE OR REPLACE VIEW analytic.v_consumption_energy_buildings_w_data
AS SELECT consumption_energy_buildings.id,
    consumption_energy_buildings.building_name,
    consumption_energy_buildings.description,
    consumption_energy_buildings.building_address_code,
    consumption_energy_buildings.building_label,
    consumption_energy_buildings.current_note,
    consumption_energy_buildings.main_use,
    consumption_energy_buildings.secondary_use,
    consumption_energy_buildings.year_of_construction,
    consumption_energy_buildings.method_of_protection,
    consumption_energy_buildings.built_up_area,
    consumption_energy_buildings.heated_bulding_volume,
    consumption_energy_buildings.students_count,
    consumption_energy_buildings.employees_count,
    consumption_energy_buildings.classrooms_count,
    consumption_energy_buildings.beds_count,
    consumption_energy_buildings.eno_id,
    consumption_energy_buildings.csu_code,
    consumption_energy_buildings.ku_code,
    consumption_energy_buildings.allotment_number,
    consumption_energy_buildings.registration_unit,
    consumption_energy_buildings.gas_consumption_normatives,
    consumption_energy_buildings.heat_consumption_normatives,
    consumption_energy_buildings.water_consumption_normatives,
    consumption_energy_buildings.electricity_consumption_normatives_per_person,
    consumption_energy_buildings.electricity_consumption_normatives,
    consumption_energy_buildings.energetic_management,
    consumption_energy_buildings.opening_hours,
    consumption_energy_buildings.weekend_opening_hours,
    consumption_energy_buildings.latitude,
    consumption_energy_buildings.longitude,
    consumption_energy_buildings.address_street,
    consumption_energy_buildings.address_house_number,
    consumption_energy_buildings.address_city,
    consumption_energy_buildings.address_country,
    consumption_energy_buildings.address_mail,
    consumption_energy_buildings.address_phone,
    consumption_energy_buildings.address_web_address,
    consumption_energy_buildings.penb_penbnumber,
    consumption_energy_buildings.penb_issue_date,
    consumption_energy_buildings.penb_total_building_envelope_area,
    consumption_energy_buildings.penb_volume_factor_of_avshape,
    consumption_energy_buildings.penb_total_energy_reference_area,
    consumption_energy_buildings.penb_total_provided_energy,
    consumption_energy_buildings.penb_total_provided_energy_category,
    consumption_energy_buildings.penb_primary_non_renewable_energy,
    consumption_energy_buildings.penb_primary_non_renewable_energy_category,
    consumption_energy_buildings.penb_building_envelope,
    consumption_energy_buildings.penb_building_envelope_category,
    consumption_energy_buildings.penb_heating,
    consumption_energy_buildings.penb_heating_category,
    consumption_energy_buildings.penb_cooling,
    consumption_energy_buildings.penb_cooling_category,
    consumption_energy_buildings.penb_ventilation,
    consumption_energy_buildings.penb_ventilation_category,
    consumption_energy_buildings.penb_humidity_adjustment,
    consumption_energy_buildings.penb_humidity_adjustment_category,
    consumption_energy_buildings.penb_warm_water,
    consumption_energy_buildings.penb_warm_water_category,
    consumption_energy_buildings.penb_lighting,
    consumption_energy_buildings.penb_lighting_category,
    consumption_energy_buildings.energy_audits_energy_audit,
    consumption_energy_buildings.energy_audits_earegistration_number,
    consumption_energy_buildings.energy_audits_created_at,
    consumption_energy_buildings.building_envelope_side_wall_prevailing_construction,
    consumption_energy_buildings.building_envelope_side_wall_area,
    consumption_energy_buildings.building_envelope_side_wall_heat_insulation,
    consumption_energy_buildings.building_envelope_side_wall_year_of_adjustment,
    consumption_energy_buildings.building_envelope_side_wall_technical_condition,
    consumption_energy_buildings.building_envelope_filling_of_hole_construction,
    consumption_energy_buildings.building_envelope_filling_of_hole_area,
    consumption_energy_buildings.building_envelope_filling_of_hole_year_of_adjustment,
    consumption_energy_buildings.building_envelope_filling_of_hole_technical_condition,
    consumption_energy_buildings.building_envelope_roof_construction,
    consumption_energy_buildings.building_envelope_roof_area,
    consumption_energy_buildings.building_envelope_roof_thermal_insulation,
    consumption_energy_buildings.building_envelope_roof_year_of_adjustment,
    consumption_energy_buildings.building_envelope_roof_technical_condition,
    consumption_energy_buildings.building_envelope_floor_of_the_lowest_heated_floor_construction,
    consumption_energy_buildings.building_envelope_floor_of_the_lowest_heated_floor_area,
    consumption_energy_buildings.floor_of_the_lowest_heated_floor_thermal_insulation,
    consumption_energy_buildings.building_envelope_floor_of_the_lowest_heated_floor_year_of_adju,
    consumption_energy_buildings.floor_of_the_lowest_heated_floor_technical_condition,
    consumption_energy_buildings.fuel_and_energy_coal,
    consumption_energy_buildings.fuel_and_energy_gas,
    consumption_energy_buildings.fuel_and_energy_electricity,
    consumption_energy_buildings.fuel_and_energy_czt,
    consumption_energy_buildings.fuel_and_energy_oze,
    consumption_energy_buildings.fuel_and_energy_other,
    consumption_energy_buildings.technical_equipment_heating_main_source_of_heat,
    consumption_energy_buildings.technical_equipment_heating_heat_percentage,
    consumption_energy_buildings.technical_equipment_heating_secondary_source_of_heat,
    consumption_energy_buildings.technical_equipment_heating_heating_system,
    consumption_energy_buildings.technical_equipment_heating_year,
    consumption_energy_buildings.technical_equipment_heating_technical_condition,
    consumption_energy_buildings.technical_equipment_cooling_cooling_system,
    consumption_energy_buildings.technical_equipment_cooling_cooling_area_percentage,
    consumption_energy_buildings.technical_equipment_cooling_year,
    consumption_energy_buildings.technical_equipment_cooling_technical_condition,
    consumption_energy_buildings.technical_equipment_ventilation_ventilation,
    consumption_energy_buildings.technical_equipment_ventilation_year,
    consumption_energy_buildings.technical_equipment_ventilation_technical_condition,
    consumption_energy_buildings.technical_equipment_humidity_adjustment_humidity_adjustment,
    consumption_energy_buildings.technical_equipment_humidity_adjustment_year,
    consumption_energy_buildings.technical_equipment_humidity_adjustment_technical_condition,
    consumption_energy_buildings.technical_equipment_hot_water_predominant_way_of_heating_tv,
    consumption_energy_buildings.technical_equipment_hot_water_hot_water_source,
    consumption_energy_buildings.technical_equipment_hot_water_year,
    consumption_energy_buildings.technical_equipment_hot_water_technical_condition,
    consumption_energy_buildings.technical_equipment_lighting_lighting,
    consumption_energy_buildings.technical_equipment_lighting_year,
    consumption_energy_buildings.technical_equipment_lighting_technical_condition,
    consumption_energy_buildings.technical_equipment_lighting_other_technological_elements,
    consumption_energy_buildings.technical_equipment_lighting_measurement_method,
    consumption_energy_buildings.oze_energy_production_solar_energy_photovoltaic,
    consumption_energy_buildings.oze_energy_production_solar_energy_photothermal,
    consumption_energy_buildings.oze_energy_production_integrated_turbines_wind_energy,
    consumption_energy_buildings.oze_energy_production_heat_pump,
    consumption_energy_buildings.waste_and_emissions_solid_waste_production,
    consumption_energy_buildings.waste_and_emissions_tied_co2_emissions,
    consumption_energy_buildings.waste_and_emissions_sox_emissions,
    consumption_energy_buildings.waste_and_emissions_operating_co_2emissions,
    consumption_energy_buildings.link
   FROM consumption_energy_buildings
  WHERE (btrim(consumption_energy_buildings.building_address_code::text) IN ( SELECT DISTINCT analytic.v_consumption_energy_consumption.building_address_code
           FROM analytic.v_consumption_energy_consumption));

COMMENT ON VIEW analytic.v_consumption_energy_buildings_w_data IS 'budovy, ve kterých se už méří spotřeba a posíají data';  		         


-- změna názcu: v_energie_prumerna_spotreba_min --> v_consumption_energy_avg_consumption_min
CREATE OR REPLACE VIEW analytic.v_consumption_energy_avg_consumption_min
AS SELECT za_minutu.addr,
    za_minutu.var,
    za_minutu.meter,
    avg(za_minutu.spotreba_za_minutu) AS consumption_min
   FROM ( SELECT consumption_energy_consumption.time_utc,
            consumption_energy_consumption.addr,
            consumption_energy_consumption.var,
            consumption_energy_consumption.commodity,
            consumption_energy_consumption.unit,
            consumption_energy_consumption.value,
            consumption_energy_consumption.meter,
                CASE
                    WHEN ((consumption_energy_consumption.value - lag(consumption_energy_consumption.value) OVER (PARTITION BY consumption_energy_consumption.addr, consumption_energy_consumption.var, consumption_energy_consumption.meter ORDER BY consumption_energy_consumption.time_utc))::double precision / date_part('epoch'::text, consumption_energy_consumption.time_utc - lag(consumption_energy_consumption.time_utc) OVER (PARTITION BY consumption_energy_consumption.addr, consumption_energy_consumption.var, consumption_energy_consumption.meter ORDER BY consumption_energy_consumption.time_utc)) / 60::double precision) < 0::double precision THEN 0::double precision
                    ELSE (consumption_energy_consumption.value - lag(consumption_energy_consumption.value) OVER (PARTITION BY consumption_energy_consumption.addr, consumption_energy_consumption.var, consumption_energy_consumption.meter ORDER BY consumption_energy_consumption.time_utc))::double precision / (date_part('epoch'::text, consumption_energy_consumption.time_utc - lag(consumption_energy_consumption.time_utc) OVER (PARTITION BY consumption_energy_consumption.addr, consumption_energy_consumption.var, consumption_energy_consumption.meter ORDER BY consumption_energy_consumption.time_utc)) / 60::double precision)
                END AS spotreba_za_minutu
           FROM consumption_energy_consumption
          WHERE consumption_energy_consumption.var::text = 'core'::text AND (consumption_energy_consumption.addr::text ~~ '%F%'::text OR consumption_energy_consumption.addr::text = '/2.1/VP2'::text) AND consumption_energy_consumption.value > 0::numeric) za_minutu
  GROUP BY za_minutu.addr, za_minutu.var, za_minutu.meter;

COMMENT ON VIEW analytic.v_consumption_energy_avg_consumption_min IS 'agregační view - spotřeba za minutu';  		         		   

  
CREATE OR REPLACE VIEW analytic.v_consumption_energy_delta
AS WITH delta AS (
         SELECT es.time_utc,
            es.addr,
            es.var,
            es.commodity,
            es.unit,
            es.value,
            es.meter,
            lag(es.value) OVER (PARTITION BY es.addr, es.var, es.meter ORDER BY es.time_utc) AS last_value,
            lag(es.time_utc) OVER (PARTITION BY es.addr, es.var, es.meter ORDER BY es.time_utc) AS last_timeutc,
            es.value - lag(es.value) OVER (PARTITION BY es.addr, es.var, es.meter ORDER BY es.time_utc) AS delta_value,
            min.consumption_min AS prumerna_spotreba,
            date_part('epoch'::text, es.time_utc - lag(es.time_utc) OVER (PARTITION BY es.addr, es.var, es.meter ORDER BY es.time_utc)) / 60::double precision AS minut_mereni
           FROM consumption_energy_consumption es
             JOIN analytic.v_consumption_energy_avg_consumption_min min ON es.addr::text = min.addr::text AND es.var::text = min.var::text AND es.meter::text = min.meter::text
          WHERE es.var::text = 'core'::text AND (es.addr::text ~~ '%F%'::text OR es.addr::text = '/2.1/VP2'::text) AND es.value > 0::numeric
        )
 SELECT delta.time_utc,
    delta.addr,
    delta.var,
    delta.commodity,
    delta.unit,
    delta.value,
    delta.meter,
    delta.last_value,
    delta.last_timeutc,
        CASE
            WHEN abs(delta.delta_value::double precision / 60::double precision) > abs(1000::double precision * delta.prumerna_spotreba) THEN 0::numeric
            ELSE delta.delta_value
        END AS delta_value,
    delta.minut_mereni
   FROM delta;
  
COMMENT ON VIEW analytic.v_consumption_energy_delta IS 'View obsahuje základní údaje z tabulky energie_consumption.
Je rozšířena o hodnoty předchozího měření. Ve sloupci delta_value je stnovena hodnota rozdílu (očisštěna o chyby - výměna měřáku)';
  
  
CREATE OR REPLACE VIEW analytic.v_consumption_energy_devices_active
AS SELECT consumption_energy_devices.id,
    consumption_energy_devices.addr,
    consumption_energy_devices.description,
    consumption_energy_devices.meter_number,
    consumption_energy_devices.meter_index,
    consumption_energy_devices.location_number,
    consumption_energy_devices.location_description,
    consumption_energy_devices.include_in_evaluation,
    consumption_energy_devices.meter_type,
    consumption_energy_devices.category,
    consumption_energy_devices.unit,
    consumption_energy_devices.replaced_meter_id,
    consumption_energy_devices.deleted,
    consumption_energy_devices.building_id
   FROM consumption_energy_devices
  WHERE NOT (consumption_energy_devices.id IN ( SELECT consumption_energy_devices_1.replaced_meter_id::integer AS replaced_meter_id
           FROM consumption_energy_devices consumption_energy_devices_1
          WHERE consumption_energy_devices_1.replaced_meter_id::text <> ''::text)) AND (consumption_energy_devices.addr::text <> ALL (ARRAY['aaa'::character varying::text, 'VF1'::character varying::text, 'TF1'::character varying::text, 'EF1'::character varying::text, 'PF1'::character varying::text, '01/VF1'::character varying::text, 'Návštěvnost'::character varying::text])) AND consumption_energy_devices.deleted::text <> '1'::text
UNION
 SELECT me.me_id AS id,
    me.me_id::character varying(255) AS addr,
    NULL::character varying(255) AS description,
    NULL::character varying(255) AS meter_number,
    NULL::character varying(255) AS meter_index,
    NULL::character varying(255) AS location_number,
    NULL::character varying(255) AS location_description,
    NULL::character varying(255) AS include_in_evaluation,
    NULL::character varying(255) AS meter_type,
    NULL::character varying(255) AS category,
    NULL::character varying(255) AS unit,
    NULL::character varying(255) AS replaced_meter_id,
    NULL::character varying(255) AS deleted,
    111 AS building_id
   FROM vpalac_measuring_equipment me;


COMMENT ON VIEW analytic.v_consumption_energy_devices_active IS 'aktivní měřidla(odstraněné hstorické méřidla, které byly vyměneny)';  		         		     

-- název: v_energie_odecty_mesic_ptv --> v_consumption_energy_deduct_month_ptv
CREATE OR REPLACE VIEW analytic.v_consumption_energy_deduct_month_ptv
AS SELECT date_part('year'::text, consumption_energy_consumption.time_utc) AS rok,
    date_part('month'::text, consumption_energy_consumption.time_utc) AS mesic,
    consumption_energy_consumption.addr,
    consumption_energy_consumption.var,
    consumption_energy_consumption.commodity,
    consumption_energy_consumption.unit,
    sum(consumption_energy_consumption.delta_value) AS value,
    max(consumption_energy_consumption.time_utc::date) AS data_do,
    count(*) AS count
   FROM analytic.v_consumption_energy_delta consumption_energy_consumption
  GROUP BY (date_part('year'::text, consumption_energy_consumption.time_utc)), (date_part('month'::text, consumption_energy_consumption.time_utc)), consumption_energy_consumption.addr, consumption_energy_consumption.var, consumption_energy_consumption.commodity, consumption_energy_consumption.unit;		  
  
COMMENT ON VIEW analytic.v_consumption_energy_deduct_month_ptv IS 'data z měření měřidel (stavy měřidla) pro plyn, teplo a vodu';  		         		     		  

-- public.v_consumption_vrtbovsky_palac source

CREATE OR REPLACE VIEW analytic.v_consumption_vrtbovsky_palac
AS WITH daily_status AS (
         SELECT date_part('year'::text, to_timestamp((m.time_measurement / 1000)::double precision)::timestamp without time zone) AS measure_year,
            date_part('month'::text, to_timestamp((m.time_measurement / 1000)::double precision)::timestamp without time zone) AS measure_month,
            me.me_id::text AS addr,
            m.var_id AS var,
                CASE
                    WHEN mt.met_nazev::text = 'Senzor'::text THEN 'heat'::text
                    WHEN mt.met_nazev::text = 'Elektroměr'::text THEN 'electricity'::text
                    WHEN mt.met_nazev::text = 'Plynoměr'::text THEN 'gas'::text
                    WHEN mt.met_nazev::text = 'Vodoměr'::text THEN 'water'::text
                    ELSE NULL::text
                END AS commodity,
            NULL::text AS unit,
                CASE
                    WHEN mt.met_nazev::text = 'Senzor'::text AND date_part('month'::text, to_timestamp((m.time_measurement / 1000)::double precision)::timestamp without time zone) = 12::double precision THEN 0::numeric
                    ELSE min(m.value)
                END AS start_value,
            me.mis_nazev,
            date_trunc('month'::text, to_timestamp((m.time_measurement / 1000)::double precision)::timestamp without time zone) AS data_do
           FROM vpalac_measuring_equipment me
             JOIN vpalac_measurement m ON me.var_id = m.var_id
             JOIN vpalac_meter_type mt ON me.met_id = mt.met_id
          GROUP BY me.me_id, (date_part('year'::text, to_timestamp((m.time_measurement / 1000)::double precision)::timestamp without time zone)), (date_part('month'::text, to_timestamp((m.time_measurement / 1000)::double precision)::timestamp without time zone)), mt.met_nazev, m.var_id, me.mis_nazev, me.umisteni, (date_trunc('month'::text, to_timestamp((m.time_measurement / 1000)::double precision)::timestamp without time zone))
        )
 SELECT daily_status.measure_year,
    daily_status.measure_month,
    daily_status.addr,
    daily_status.var,
    daily_status.commodity,
    daily_status.unit,
    lead(daily_status.start_value) OVER (PARTITION BY daily_status.var ORDER BY daily_status.data_do) - daily_status.start_value AS consumption,
    daily_status.data_do
   FROM daily_status;	 

-- název:   v_energie_consumption_mesic_el_f --> v_consumption_energy_consumption_month_el_f
CREATE OR REPLACE VIEW analytic.v_consumption_energy_consumption_month_el_f
AS SELECT date_part('year'::text, consumption_energy_consumption.time_utc) AS rok,
    date_part('month'::text, consumption_energy_consumption.time_utc) AS mesic,
    consumption_energy_consumption.addr,
    consumption_energy_consumption.var,
    consumption_energy_consumption.commodity,
    consumption_energy_consumption.unit,
    sum(consumption_energy_consumption.value::numeric(10,2)) / 4::numeric AS value,
    max(consumption_energy_consumption.time_utc::date) AS data_do,
    count(*) AS count
   FROM consumption_energy_consumption
  WHERE (consumption_energy_consumption.var::text = ANY (ARRAY['EFwActi'::character varying::text, 'EFwActiNT'::character varying::text, 'EFwActi'::character varying::text])) AND consumption_energy_consumption.addr::text ~~ '%/EF%'::text
  GROUP BY (date_part('year'::text, consumption_energy_consumption.time_utc)), (date_part('month'::text, consumption_energy_consumption.time_utc)), consumption_energy_consumption.addr, consumption_energy_consumption.var, consumption_energy_consumption.commodity, consumption_energy_consumption.unit
UNION
 SELECT v_consumption_vrtbovsky_palac.measure_year AS rok,
    v_consumption_vrtbovsky_palac.measure_month AS mesic,
    v_consumption_vrtbovsky_palac.addr::character varying(255) AS addr,
    v_consumption_vrtbovsky_palac.var::character varying(255) AS var,
    v_consumption_vrtbovsky_palac.commodity::character varying(255) AS commodity,
    v_consumption_vrtbovsky_palac.unit::character varying(255) AS unit,
    sum(v_consumption_vrtbovsky_palac.consumption) AS value,
    v_consumption_vrtbovsky_palac.data_do::date AS data_do,
    count(*) AS count
   FROM analytic.v_consumption_vrtbovsky_palac
  WHERE v_consumption_vrtbovsky_palac.commodity = 'electricity'::text
  GROUP BY v_consumption_vrtbovsky_palac.measure_year, v_consumption_vrtbovsky_palac.measure_month, v_consumption_vrtbovsky_palac.data_do, v_consumption_vrtbovsky_palac.addr, v_consumption_vrtbovsky_palac.var, v_consumption_vrtbovsky_palac.commodity, v_consumption_vrtbovsky_palac.unit;

COMMENT ON VIEW analytic.v_consumption_energy_consumption_month_el_f IS 'měsíční spotřeby elektřiny pro všechny fakurační měřidla';  		         		     		    

--název: v_energie_consumption_mesic_ptv -- > v_consumption_energy_consumption_month_ptv
CREATE OR REPLACE VIEW analytic.v_consumption_energy_consumption_month_ptv
AS SELECT v_consumption_energy_deduct_month_ptv.rok,
    v_consumption_energy_deduct_month_ptv.mesic,
    v_consumption_energy_deduct_month_ptv.addr,
    v_consumption_energy_deduct_month_ptv.var,
    v_consumption_energy_deduct_month_ptv.commodity,
    v_consumption_energy_deduct_month_ptv.unit,
    v_consumption_energy_deduct_month_ptv.value,
    v_consumption_energy_deduct_month_ptv.data_do,
    v_consumption_energy_deduct_month_ptv.count,
    NULL::numeric AS previous_month_value,
    v_consumption_energy_deduct_month_ptv.value AS mesicni_spotreba
   FROM analytic.v_consumption_energy_deduct_month_ptv
  WHERE v_consumption_energy_deduct_month_ptv.addr::text <> ALL (ARRAY['/2.1/TVF1'::character varying, '/2.1/VF1'::character varying, '/2.1/VP2'::character varying]::text[])
UNION
 SELECT tvf1.rok,
    tvf1.mesic,
    tvf1.addr,
    tvf1.var,
    tvf1.commodity,
    tvf1.unit,
    vf1.value - vp2.value + tvf1.value AS value,
    tvf1.data_do,
    tvf1.count,
    NULL::numeric AS previous_month_value,
    vf1.value - vp2.value + tvf1.value AS mesicni_spotreba
   FROM analytic.v_consumption_energy_deduct_month_ptv tvf1
     LEFT JOIN analytic.v_consumption_energy_deduct_month_ptv vf1 ON vf1.addr::text = '/2.1/VF1'::text AND vf1.rok = tvf1.rok AND vf1.mesic = tvf1.mesic
     LEFT JOIN analytic.v_consumption_energy_deduct_month_ptv vp2 ON vp2.addr::text = '/2.1/VP2'::text AND vp2.rok = tvf1.rok AND vp2.mesic = tvf1.mesic
  WHERE tvf1.addr::text = '/2.1/TVF1'::text;

COMMENT ON VIEW analytic.v_consumption_energy_consumption_month_ptv IS 'měsíční spotřeby pro plyn, teplo a vodu pro všechny měřidla';  		         		     		    

--název: v_energie_consumption_mesic_p_f --> v_consumption_energy_consumption_month_p_f
CREATE OR REPLACE VIEW analytic.v_consumption_energy_consumption_month_p_f
AS SELECT v_consumption_energy_consumption_month_ptv.rok,
    v_consumption_energy_consumption_month_ptv.mesic,
    v_consumption_energy_consumption_month_ptv.addr,
    v_consumption_energy_consumption_month_ptv.var,
    v_consumption_energy_consumption_month_ptv.commodity,
    v_consumption_energy_consumption_month_ptv.unit,
    v_consumption_energy_consumption_month_ptv.value,
    v_consumption_energy_consumption_month_ptv.data_do,
    v_consumption_energy_consumption_month_ptv.count,
    v_consumption_energy_consumption_month_ptv.previous_month_value,
    v_consumption_energy_consumption_month_ptv.mesicni_spotreba
   FROM analytic.v_consumption_energy_consumption_month_ptv
  WHERE v_consumption_energy_consumption_month_ptv.addr::text ~~ '%/PF%'::text AND v_consumption_energy_consumption_month_ptv.rok > 2018::double precision
UNION
 SELECT v_consumption_vrtbovsky_palac.measure_year AS rok,
    v_consumption_vrtbovsky_palac.measure_month AS mesic,
    v_consumption_vrtbovsky_palac.addr::character varying(255) AS addr,
    v_consumption_vrtbovsky_palac.var::character varying(255) AS var,
    v_consumption_vrtbovsky_palac.commodity::character varying(255) AS commodity,
    v_consumption_vrtbovsky_palac.unit::character varying(255) AS unit,
    sum(v_consumption_vrtbovsky_palac.consumption) AS value,
    v_consumption_vrtbovsky_palac.data_do::date AS data_do,
    count(*) AS count,
    NULL::numeric AS previous_month_value,
    sum(v_consumption_vrtbovsky_palac.consumption) AS mesicni_spotreba
   FROM analytic.v_consumption_vrtbovsky_palac
  WHERE v_consumption_vrtbovsky_palac.commodity = 'gas'::text
  GROUP BY v_consumption_vrtbovsky_palac.measure_year, v_consumption_vrtbovsky_palac.measure_month, v_consumption_vrtbovsky_palac.data_do, v_consumption_vrtbovsky_palac.addr, v_consumption_vrtbovsky_palac.var, v_consumption_vrtbovsky_palac.commodity, v_consumption_vrtbovsky_palac.unit;
  
COMMENT ON VIEW analytic.v_consumption_energy_consumption_month_p_f IS 'měsíční spotřeby plynu pro všechny fakurační měřidla';  		         		     		    
 
CREATE OR REPLACE VIEW analytic.v_consumption_energy_consumption_month_t_f
AS SELECT v_consumption_energy_consumption_month_ptv.rok,
    v_consumption_energy_consumption_month_ptv.mesic,
    v_consumption_energy_consumption_month_ptv.addr,
    v_consumption_energy_consumption_month_ptv.var,
    v_consumption_energy_consumption_month_ptv.commodity,
    v_consumption_energy_consumption_month_ptv.unit,
    v_consumption_energy_consumption_month_ptv.value,
    v_consumption_energy_consumption_month_ptv.data_do,
    v_consumption_energy_consumption_month_ptv.count,
    v_consumption_energy_consumption_month_ptv.previous_month_value,
    v_consumption_energy_consumption_month_ptv.mesicni_spotreba
   FROM analytic.v_consumption_energy_consumption_month_ptv
  WHERE v_consumption_energy_consumption_month_ptv.addr::text ~~ '%/TF%'::text AND v_consumption_energy_consumption_month_ptv.rok > 2018::double precision;

COMMENT ON VIEW analytic.v_consumption_energy_consumption_month_t_f IS 'měsíční spotřeby tepla pro všechny fakurační měřidla';  		         		     		      
  
CREATE OR REPLACE VIEW analytic.v_consumption_energy_consumption_month_v_f
AS SELECT v_consumption_energy_consumption_month_ptv.rok,
    v_consumption_energy_consumption_month_ptv.mesic,
    v_consumption_energy_consumption_month_ptv.addr,
    v_consumption_energy_consumption_month_ptv.var,
    v_consumption_energy_consumption_month_ptv.commodity,
    v_consumption_energy_consumption_month_ptv.unit,
    v_consumption_energy_consumption_month_ptv.value,
    v_consumption_energy_consumption_month_ptv.data_do,
    v_consumption_energy_consumption_month_ptv.count,
    v_consumption_energy_consumption_month_ptv.previous_month_value,
    v_consumption_energy_consumption_month_ptv.mesicni_spotreba
   FROM analytic.v_consumption_energy_consumption_month_ptv
  WHERE v_consumption_energy_consumption_month_ptv.addr::text ~~ '%VF%'::text AND v_consumption_energy_consumption_month_ptv.rok > 2018::double precision
UNION
 SELECT v_consumption_vrtbovsky_palac.measure_year AS rok,
    v_consumption_vrtbovsky_palac.measure_month AS mesic,
    v_consumption_vrtbovsky_palac.addr::character varying(255) AS addr,
    v_consumption_vrtbovsky_palac.var::character varying(255) AS var,
    v_consumption_vrtbovsky_palac.commodity::character varying(255) AS commodity,
    v_consumption_vrtbovsky_palac.unit::character varying(255) AS unit,
    sum(v_consumption_vrtbovsky_palac.consumption) AS value,
    v_consumption_vrtbovsky_palac.data_do::date AS data_do,
    count(*) AS count,
    NULL::numeric AS previous_month_value,
    sum(v_consumption_vrtbovsky_palac.consumption) AS mesicni_spotreba
   FROM analytic.v_consumption_vrtbovsky_palac
  WHERE v_consumption_vrtbovsky_palac.commodity = 'water'::text
  GROUP BY v_consumption_vrtbovsky_palac.measure_year, v_consumption_vrtbovsky_palac.measure_month, v_consumption_vrtbovsky_palac.data_do, v_consumption_vrtbovsky_palac.addr, v_consumption_vrtbovsky_palac.var, v_consumption_vrtbovsky_palac.commodity, v_consumption_vrtbovsky_palac.unit;
  
COMMENT ON VIEW analytic.v_consumption_energy_consumption_month_v_f IS 'měsíční spotřeby vody pro všechny fakurační měřidla';  		         		     		      
  
-- v_consumption_energy_alerts - shodne  
CREATE OR REPLACE VIEW analytic.v_consumption_energy_change	-- v_energie_vymeny
AS SELECT a.time_utc,
    a.addr,
    a.var,
    a.commodity,
    a.unit,
    a.value,
    a.meter,
    a.last_value,
    a.last_timeutc,
    a.delta_value,
    a.minut,
    a.consumption_min,
    a.za_minutu_ted,
    a.vymena
   FROM ( SELECT es.time_utc,
            es.addr,
            es.var,
            es.commodity,
            es.unit,
            es.value,
            es.meter,
            lag(es.value) OVER (PARTITION BY es.addr, es.var, es.meter ORDER BY es.time_utc) AS last_value,
            lag(es.time_utc) OVER (PARTITION BY es.addr, es.var, es.meter ORDER BY es.time_utc) AS last_timeutc,
            es.value - lag(es.value) OVER (PARTITION BY es.addr, es.var, es.meter ORDER BY es.time_utc) AS delta_value,
            date_part('epoch'::text, es.time_utc - lag(es.time_utc) OVER (PARTITION BY es.addr, es.var, es.meter ORDER BY es.time_utc)) / 60::double precision AS minut,
            min.consumption_min,
            (es.value - lag(es.value) OVER (PARTITION BY es.addr, es.var, es.meter ORDER BY es.time_utc))::double precision / (date_part('epoch'::text, es.time_utc - lag(es.time_utc) OVER (PARTITION BY es.addr, es.var, es.meter ORDER BY es.time_utc)) / 60::double precision) AS za_minutu_ted,
                CASE
                    WHEN (es.value - lag(es.value) OVER (PARTITION BY es.addr, es.var, es.meter ORDER BY es.time_utc)) < 0::numeric THEN 'pokles'::text
                    WHEN min.consumption_min > 0::double precision AND ((es.value - lag(es.value) OVER (PARTITION BY es.addr, es.var, es.meter ORDER BY es.time_utc))::double precision / (date_part('epoch'::text, es.time_utc - lag(es.time_utc) OVER (PARTITION BY es.addr, es.var, es.meter ORDER BY es.time_utc)) / 60::double precision)) > (1000::double precision * min.consumption_min) THEN 'narust'::text
                    ELSE NULL::text
                END AS vymena
           FROM consumption_energy_consumption es
             JOIN analytic.v_consumption_energy_avg_consumption_min min ON es.addr::text = min.addr::text AND es.var::text = min.var::text AND es.meter::text = min.meter::text
          WHERE es.var::text = 'core'::text AND es.addr::text ~~ '%F%'::text AND es.value > 0::numeric) a
  WHERE a.vymena IS NOT NULL;
  
COMMENT ON VIEW analytic.v_consumption_energy_change IS 'měřidla, kde je podozrivý skok ve spotřebě a tedy předpokládaná výměna měřidla'; 

-- analytic.v_consumption_gas_reserve source
CREATE OR REPLACE VIEW analytic.v_consumption_gas_reserve
AS WITH kalendar AS (
         SELECT den.den
           FROM generate_series('2019-01-01 00:00:00+01'::timestamp with time zone, now() - '1 day'::interval, '1 day'::interval) den(den)
        ), meraky AS (
         SELECT DISTINCT consumption_energy_consumption.addr
           FROM consumption_energy_consumption
          WHERE consumption_energy_consumption.commodity::text = 'gas'::text AND consumption_energy_consumption.var::text = 'core'::text AND (consumption_energy_consumption.addr::text IN ( SELECT DISTINCT consumption_gas_daily_reserves.device_id
                   FROM analytic.consumption_gas_daily_reserves))
        ), daily_delta AS (
         SELECT date_trunc('day'::text, v_consumption_energy_delta.time_utc)::date AS measured_from,
            v_consumption_energy_delta.addr,
            v_consumption_energy_delta.var,
            v_consumption_energy_delta.commodity,
            v_consumption_energy_delta.unit,
            max(v_consumption_energy_delta.value) AS value,
            sum(v_consumption_energy_delta.delta_value) AS delta_value,
            min(date_trunc('day'::text, v_consumption_energy_delta.last_timeutc))::date AS measured_from_before
           FROM analytic.v_consumption_energy_delta
          WHERE v_consumption_energy_delta.commodity::text = 'gas'::text AND date_part('year'::text, v_consumption_energy_delta.time_utc) >= 2019::double precision AND (v_consumption_energy_delta.addr::text IN ( SELECT DISTINCT consumption_gas_daily_reserves.device_id
                   FROM analytic.consumption_gas_daily_reserves))
          GROUP BY (date_trunc('day'::text, v_consumption_energy_delta.time_utc)), v_consumption_energy_delta.addr, v_consumption_energy_delta.var, v_consumption_energy_delta.commodity, v_consumption_energy_delta.unit
          ORDER BY v_consumption_energy_delta.addr, (date_trunc('day'::text, v_consumption_energy_delta.time_utc)) DESC
        ), delta_dates AS (
         SELECT daily_delta.measured_from,
            daily_delta.addr,
            daily_delta.var,
            daily_delta.commodity,
            daily_delta.unit,
            daily_delta.value,
            daily_delta.delta_value,
            daily_delta.measured_from_before,
            lag(daily_delta.measured_from, 1) OVER (PARTITION BY daily_delta.addr ORDER BY daily_delta.measured_from) AS measured_before,
            lead(daily_delta.measured_from, 1) OVER (PARTITION BY daily_delta.addr ORDER BY daily_delta.measured_from) AS measured_after,
            lag(daily_delta.delta_value, 1) OVER (PARTITION BY daily_delta.addr ORDER BY daily_delta.measured_from) AS delta_before,
            lead(daily_delta.delta_value, 1) OVER (PARTITION BY daily_delta.addr ORDER BY daily_delta.measured_from) AS delta_after
           FROM daily_delta
        ), delta_date_diff AS (
         SELECT delta_dates.measured_from,
            delta_dates.addr,
            delta_dates.var,
            delta_dates.commodity,
            delta_dates.unit,
            delta_dates.value,
            delta_dates.delta_value,
            delta_dates.measured_from_before,
            delta_dates.measured_before,
            delta_dates.measured_after,
            delta_dates.delta_before,
            delta_dates.delta_after,
                CASE
                    WHEN (delta_dates.measured_after - delta_dates.measured_from) > 1 THEN 'begin'::text
                    WHEN (delta_dates.measured_from - delta_dates.measured_before) > 1 THEN 'end'::text
                    ELSE 'ok'::text
                END AS value_type,
                CASE
                    WHEN (delta_dates.measured_after - delta_dates.measured_from) > 1 THEN (delta_dates.delta_value + delta_dates.delta_after) / (delta_dates.measured_after - delta_dates.measured_from + 1)::numeric
                    WHEN (delta_dates.measured_from - delta_dates.measured_before) > 1 THEN (delta_dates.delta_value + delta_dates.delta_before) / (delta_dates.measured_from - delta_dates.measured_before + 1)::numeric
                    ELSE NULL::numeric
                END AS fix
           FROM delta_dates
        ), joined_table AS (
         SELECT kalendar.den::date AS measured_from,
            meraky.addr,
            delta_date_diff.delta_value,
            delta_date_diff.fix,
            COALESCE(delta_date_diff.fix, delta_date_diff.delta_value) AS final_fix,
            res.daily_reserved_capacity_m3
           FROM kalendar
             LEFT JOIN meraky ON true
             LEFT JOIN delta_date_diff ON delta_date_diff.measured_from = kalendar.den::date AND delta_date_diff.addr::text = meraky.addr::text
             LEFT JOIN analytic.consumption_gas_daily_reserves res ON meraky.addr::text = res.device_id::text AND date_part('year'::text, kalendar.den) = res.relevance_year::double precision AND date_part('month'::text, kalendar.den) = res.relevance_month::double precision
          ORDER BY meraky.addr, kalendar.den
        ), fixed_table AS (
         SELECT q.measured_from,
            q.addr,
            q.delta_value,
            q.fix,
            q.final_fix,
            q.daily_reserved_capacity_m3,
            q.value_partition,
            first_value(q.final_fix) OVER w AS value
           FROM ( SELECT joined_table.measured_from,
                    joined_table.addr,
                    joined_table.delta_value,
                    joined_table.fix,
                    joined_table.final_fix,
                    joined_table.daily_reserved_capacity_m3,
                    sum(
                        CASE
                            WHEN joined_table.final_fix IS NULL THEN 0
                            ELSE 1
                        END) OVER (PARTITION BY joined_table.addr ORDER BY joined_table.measured_from) AS value_partition
                   FROM joined_table) q
          WINDOW w AS (PARTITION BY q.addr, q.value_partition ORDER BY q.measured_from)
        )
 SELECT fixed_table.measured_from,
    fixed_table.addr,
    fixed_table.value AS delta_value,
    fixed_table.daily_reserved_capacity_m3,
    fixed_table.value / fixed_table.daily_reserved_capacity_m3 AS ratio,
        CASE
            WHEN (fixed_table.value / fixed_table.daily_reserved_capacity_m3) > 1::numeric THEN '100% a více'::text
            WHEN (fixed_table.value / fixed_table.daily_reserved_capacity_m3) >= 0.8 AND (fixed_table.value / fixed_table.daily_reserved_capacity_m3) <= 1::numeric THEN '80 - 100%'::text
            WHEN (fixed_table.value / fixed_table.daily_reserved_capacity_m3) >= 0.6 AND (fixed_table.value / fixed_table.daily_reserved_capacity_m3) <= 0.8 THEN '60 - 80%'::text
            WHEN (fixed_table.value / fixed_table.daily_reserved_capacity_m3) >= 0.4 AND (fixed_table.value / fixed_table.daily_reserved_capacity_m3) <= 0.6 THEN '40 - 60%'::text
            WHEN (fixed_table.value / fixed_table.daily_reserved_capacity_m3) >= 0.2 AND (fixed_table.value / fixed_table.daily_reserved_capacity_m3) <= 0.4 THEN '20 - 40%'::text
            WHEN (fixed_table.value / fixed_table.daily_reserved_capacity_m3) >= 0::numeric AND (fixed_table.value / fixed_table.daily_reserved_capacity_m3) <= 0.2 THEN '0 - 20%'::text
            WHEN fixed_table.value = 0::numeric THEN '0 - 20%'::text
            ELSE 'N/A'::text
        END AS ratio_group
   FROM fixed_table;
   
-- analytic.v_consumption_gas_day source

CREATE OR REPLACE VIEW analytic.v_consumption_gas_day
AS WITH kalendar AS (
         SELECT den.den
           FROM generate_series('2019-01-01 00:00:00+01'::timestamp with time zone, now() - '1 day'::interval, '1 day'::interval) den(den)
        ), meraky AS (
         SELECT DISTINCT consumption_energy_consumption.addr
           FROM consumption_energy_consumption
          WHERE consumption_energy_consumption.commodity::text = 'gas'::text AND consumption_energy_consumption.var::text = 'core'::text
        ), prumery AS (
         SELECT b.den,
            b.den2,
            b.addr,
            b.value,
            b.value2,
            (b.value2 - b.value) / (b.den2 - b.den)::numeric AS prumer,
            b.den2 - b.den AS dnu,
            b.value2 - b.value AS rozdil
           FROM ( SELECT a.time_utc::date AS den,
                    lead(a.time_utc::date) OVER (PARTITION BY a.addr ORDER BY a.time_utc) AS den2,
                    a.addr,
                    a.value,
                    lead(a.value) OVER (PARTITION BY a.addr ORDER BY a.time_utc) AS value2
                   FROM ( SELECT row_number() OVER (PARTITION BY (consumption_energy_consumption.time_utc::date), consumption_energy_consumption.addr ORDER BY consumption_energy_consumption.time_utc) AS rn,
                            consumption_energy_consumption.time_utc,
                            consumption_energy_consumption.value,
                            consumption_energy_consumption.addr,
                            consumption_energy_consumption.var,
                            consumption_energy_consumption.type,
                            consumption_energy_consumption.commodity,
                            consumption_energy_consumption.unit,
                            consumption_energy_consumption.meter,
                            consumption_energy_consumption.create_batch_id,
                            consumption_energy_consumption.created_at,
                            consumption_energy_consumption.created_by,
                            consumption_energy_consumption.update_batch_id,
                            consumption_energy_consumption.updated_at,
                            consumption_energy_consumption.updated_by
                           FROM consumption_energy_consumption
                          WHERE consumption_energy_consumption.commodity::text = 'gas'::text AND consumption_energy_consumption.var::text = 'core'::text) a
                  WHERE a.rn = 1) b
        ), prumery2 AS (
         SELECT prumery_1.den,
            prumery_1.den2,
            prumery_1.addr,
            prumery_1.value,
            prumery_1.value2,
            prumery_1.prumer,
            prumery_1.dnu,
            prumery_1.rozdil
           FROM prumery prumery_1
          WHERE prumery_1.dnu > 1
        )
 SELECT kalendar.den,
    meraky.addr,
    prumery.prumer AS delta_value,
    prumery2.prumer AS oprava
   FROM kalendar
     LEFT JOIN meraky ON true
     LEFT JOIN prumery ON prumery.den = kalendar.den::date AND prumery.addr::text = meraky.addr::text
     LEFT JOIN prumery2 ON prumery2.addr::text = meraky.addr::text AND kalendar.den >= prumery2.den AND kalendar.den <= prumery2.den2
  ORDER BY meraky.addr, kalendar.den;
 		         		     		        
-- analytic.v_consumption_energy_reserve_day_gas source
CREATE OR REPLACE VIEW analytic.v_consumption_energy_reserve_day_gas
AS WITH consumption AS (
         SELECT v_consumption_gas_day.den AS date,
            v_consumption_gas_day.addr,
            COALESCE(v_consumption_gas_day.delta_value, v_consumption_gas_day.oprava) AS delta_value
           FROM analytic.v_consumption_gas_day
        )
 SELECT consumption.date,
    consumption.addr,
    consumption.delta_value,
    res.daily_reserved_capacity_m3,
    consumption.delta_value / res.daily_reserved_capacity_m3 AS ratio,
        CASE
            WHEN (consumption.delta_value / res.daily_reserved_capacity_m3) > 1::numeric THEN 'nad kapacitou'::text
            WHEN (consumption.delta_value / res.daily_reserved_capacity_m3) >= 0.8 AND (consumption.delta_value / res.daily_reserved_capacity_m3) <= 1::numeric THEN '80 - 100%'::text
            WHEN (consumption.delta_value / res.daily_reserved_capacity_m3) >= 0.6 AND (consumption.delta_value / res.daily_reserved_capacity_m3) <= 0.8 THEN '60 - 80%'::text
            WHEN (consumption.delta_value / res.daily_reserved_capacity_m3) >= 0.4 AND (consumption.delta_value / res.daily_reserved_capacity_m3) <= 0.6 THEN '40 - 60%'::text
            WHEN (consumption.delta_value / res.daily_reserved_capacity_m3) >= 0.2 AND (consumption.delta_value / res.daily_reserved_capacity_m3) <= 0.4 THEN '20 - 40%'::text
            WHEN (consumption.delta_value / res.daily_reserved_capacity_m3) >= 0::numeric AND (consumption.delta_value / res.daily_reserved_capacity_m3) <= 0.2 THEN 'do 20%'::text
            ELSE 'none'::text
        END AS ratio_group
   FROM consumption
     LEFT JOIN analytic.consumption_gas_daily_reserves res ON consumption.addr::text = res.device_id::text AND date_part('year'::text, consumption.date) = res.relevance_year::double precision AND date_part('month'::text, consumption.date) = res.relevance_month::double precision;											
	 

-- public.v_consumption_energy_alerts source
CREATE OR REPLACE VIEW analytic.v_consumption_energy_alerts
AS WITH vymena_s AS (
         SELECT es.time_utc,
            es.addr,
            es.var,
            es.commodity,
            es.unit,
            es.value,
            es.meter,
            lag(es.value) OVER (PARTITION BY es.addr, es.var, es.meter ORDER BY es.time_utc) AS last_value,
            lag(es.time_utc) OVER (PARTITION BY es.addr, es.var, es.meter ORDER BY es.time_utc) AS last_timeutc,
            es.value - lag(es.value) OVER (PARTITION BY es.addr, es.var, es.meter ORDER BY es.time_utc) AS delta_value,
            date_part('epoch'::text, es.time_utc - lag(es.time_utc) OVER (PARTITION BY es.addr, es.var, es.meter ORDER BY es.time_utc)) / 60::double precision AS minut,
            min.consumption_min,
            (es.value - lag(es.value) OVER (PARTITION BY es.addr, es.var, es.meter ORDER BY es.time_utc))::double precision / (date_part('epoch'::text, es.time_utc - lag(es.time_utc) OVER (PARTITION BY es.addr, es.var, es.meter ORDER BY es.time_utc)) / 60::double precision) AS za_minutu_ted,
                CASE
                    WHEN (es.value - lag(es.value) OVER (PARTITION BY es.addr, es.var, es.meter ORDER BY es.time_utc)) < 0::numeric THEN 'pokles'::text
                    WHEN min.consumption_min > 0::double precision AND ((es.value - lag(es.value) OVER (PARTITION BY es.addr, es.var, es.meter ORDER BY es.time_utc))::double precision / (date_part('epoch'::text, es.time_utc - lag(es.time_utc) OVER (PARTITION BY es.addr, es.var, es.meter ORDER BY es.time_utc)) / 60::double precision)) > (1000::double precision * min.consumption_min) THEN 'narust'::text
                    ELSE NULL::text
                END AS vymena
           FROM consumption_energy_consumption es
             JOIN analytic.v_consumption_energy_avg_consumption_min min ON es.addr::text = min.addr::text AND es.var::text = min.var::text AND es.meter::text = min.meter::text
          WHERE es.var::text = 'core'::text AND es.addr::text ~~ '%F%'::text AND es.value > 0::numeric
        )
 SELECT vymena_s.time_utc,
    vymena_s.addr,
    vymena_s.var,
    vymena_s.commodity,
    vymena_s.unit,
    vymena_s.value,
    vymena_s.meter,
    vymena_s.last_value,
    vymena_s.last_timeutc,
    vymena_s.delta_value,
    vymena_s.minut,
    vymena_s.consumption_min,
    vymena_s.za_minutu_ted,
    vymena_s.vymena
   FROM vymena_s
  WHERE vymena_s.vymena IS NOT NULL;	 
  
-- public.v_consumption_energy_last_update source

CREATE OR REPLACE VIEW analytic.v_consumption_energy_last_update
AS WITH last_update AS (
         SELECT b.building_name,
            c.addr,
            max(c.time_utc) AS last_update
           FROM consumption_energy_consumption c
             JOIN consumption_energy_devices d ON c.addr::text = d.addr::text
             JOIN consumption_energy_buildings b ON d.building_id = b.id
          GROUP BY b.building_name, c.addr
        ), last_not_zero AS (
         SELECT consumption_energy_consumption.addr,
            max(consumption_energy_consumption.time_utc) AS last_not_zero_value
           FROM consumption_energy_consumption
          WHERE consumption_energy_consumption.value > 0::numeric
          GROUP BY consumption_energy_consumption.addr
        )
 SELECT lu.building_name,
    lu.last_update,
    lu.addr,
        CASE
            WHEN lnz.last_not_zero_value = lu.last_update THEN NULL::timestamp without time zone
            ELSE lnz.last_not_zero_value
        END AS zero_values_after
   FROM last_update lu
     LEFT JOIN last_not_zero lnz ON lu.addr::text = lnz.addr::text;  
	 
-- public.v_consumption_energy_missing_devices_last_update source

CREATE OR REPLACE VIEW analytic.v_consumption_energy_missing_devices_last_update
AS WITH missing_devices_last_update AS (
         SELECT consumption_energy_consumption.addr,
            "left"(consumption_energy_consumption.addr::text, "position"("right"(consumption_energy_consumption.addr::text, length(consumption_energy_consumption.addr::text) - 1), '/'::text)) AS building_code,
            max(consumption_energy_consumption.time_utc) AS last_update
           FROM consumption_energy_consumption
          WHERE NOT (consumption_energy_consumption.addr::text IN ( SELECT consumption_energy_devices.addr
                   FROM consumption_energy_devices))
          GROUP BY consumption_energy_consumption.addr
        ), missing_devices_last_not_zero AS (
         SELECT consumption_energy_consumption.addr,
            max(consumption_energy_consumption.time_utc) AS last_not_zero_value
           FROM consumption_energy_consumption
          WHERE consumption_energy_consumption.value > 0::numeric AND NOT (consumption_energy_consumption.addr::text IN ( SELECT consumption_energy_devices.addr
                   FROM consumption_energy_devices))
          GROUP BY consumption_energy_consumption.addr
        ), buildings_mapping AS (
         SELECT "left"(d.addr::text, "position"("right"(d.addr::text, length(d.addr::text) - 1), '/'::text)) AS building_code,
            b.building_name
           FROM consumption_energy_devices d
             JOIN consumption_energy_buildings b ON d.building_id = b.id
        )
 SELECT bm.building_name,
    lu.last_update,
    lu.addr,
        CASE
            WHEN lnz.last_not_zero_value = lu.last_update THEN NULL::timestamp without time zone
            ELSE lnz.last_not_zero_value
        END AS zero_values_after
   FROM missing_devices_last_update lu
     JOIN buildings_mapping bm ON lu.building_code = bm.building_code
     LEFT JOIN missing_devices_last_not_zero lnz ON lu.addr::text = lnz.addr::text;	 
	 
-- public.v_consumption_energy_vrtbovsky_palac_last_update source

CREATE OR REPLACE VIEW analytic.v_consumption_energy_vrtbovsky_palac_last_update
AS WITH last_update AS (
         SELECT m.var_id,
            me.umisteni,
            me.me_serial,
            max(to_timestamp((m.time_measurement / 1000)::double precision)::timestamp without time zone) AS last_update
           FROM vpalac_measurement m
             JOIN vpalac_measuring_equipment me ON m.var_id = me.var_id
          GROUP BY m.var_id, me.umisteni, me.me_serial
        ), last_not_zero_value AS (
         SELECT vpalac_measurement.var_id,
            max(to_timestamp((vpalac_measurement.time_measurement / 1000)::double precision)::timestamp without time zone) AS last_not_zero_value
           FROM vpalac_measurement
          WHERE vpalac_measurement.value > 0::numeric
          GROUP BY vpalac_measurement.var_id
        )
 SELECT lu.var_id,
    lu.me_serial,
    lu.umisteni,
    lu.last_update,
    lnz.last_not_zero_value,
        CASE
            WHEN lnz.last_not_zero_value = lu.last_update THEN NULL::timestamp without time zone
            ELSE lnz.last_not_zero_value
        END AS zero_values_after
   FROM last_update lu
     LEFT JOIN last_not_zero_value lnz ON lu.var_id = lnz.var_id;	 
	 
-- analytic.v_consumption_energy_outside_temperature source

CREATE OR REPLACE VIEW analytic.v_consumption_energy_outside_temperature
AS SELECT c.time_utc,
    c.value,
    c.addr,
    b.longitude,
    b.latitude,
    b.building_name
   FROM consumption_energy_consumption c
     JOIN ( SELECT max(consumption_energy_consumption.time_utc) AS last_measure_date,
            consumption_energy_consumption.addr
           FROM consumption_energy_consumption
          GROUP BY consumption_energy_consumption.addr) last_measure ON last_measure.addr::text = c.addr::text AND last_measure.last_measure_date = c.time_utc
     JOIN ( SELECT DISTINCT "left"(d.addr::text, "position"("right"(d.addr::text, length(d.addr::text) - 1), '/'::text)) AS building_code,
            b_1.latitude,
            b_1.longitude,
            b_1.building_name
           FROM consumption_energy_devices d
             JOIN consumption_energy_buildings b_1 ON d.building_id = b_1.id
          WHERE b_1.address_city::text ~~ 'Praha%'::text) b ON b.building_code = "left"(c.addr::text, "position"("right"(c.addr::text, length(c.addr::text) - 1), '/'::text))
  WHERE c.addr::text ~~ '%Text%'::text AND c.commodity::text = 'temperature'::text AND c.time_utc >= (now() - '1 year'::interval) AND (c.addr::text <> ALL (ARRAY['/2.8/Text'::text, '/2.5/Text'::text]));