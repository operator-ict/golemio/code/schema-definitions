/* Replace with your SQL commands */
-- analytic.v_uzis_covid19_hospital_capacity_beds source
-- analytic.v_uzis_covid19_hospital_capacity_technologies source

CREATE OR REPLACE VIEW analytic.v_uzis_covid19_hospital_capacity_technologies
AS SELECT covid19_hospital_capacity_details.datum,
        CASE
            WHEN covid19_hospital_capacity_details.datum = max(covid19_hospital_capacity_details.datum) OVER (PARTITION BY covid19_hospital_capacity_details.zz_nazev) THEN true
            ELSE false
        END AS posledni_den,
    covid19_hospital_capacity_details.zz_nazev,
    covid19_hospital_capacity_details.kraj_nazev,
    covid19_hospital_capacity_details.ecmo_kapacita_volna AS kapacita_volna,
    covid19_hospital_capacity_details.ecmo_kapacita_celkem AS kapacita_celkem,
    covid19_hospital_capacity_details.ecmo_kapacita_celkem - covid19_hospital_capacity_details.ecmo_kapacita_volna AS kapacita_zaplnena,
    'ecmo'::text AS typ
   FROM uzis.covid19_hospital_capacity_details
  WHERE covid19_hospital_capacity_details.datum >= '2021-04-01 00:00:00'::timestamp without time zone
UNION ALL
 SELECT covid19_hospital_capacity_details.datum,
        CASE
            WHEN covid19_hospital_capacity_details.datum = max(covid19_hospital_capacity_details.datum) OVER (PARTITION BY covid19_hospital_capacity_details.zz_nazev) THEN true
            ELSE false
        END AS posledni_den,
    covid19_hospital_capacity_details.zz_nazev,
    covid19_hospital_capacity_details.kraj_nazev,
    covid19_hospital_capacity_details.cvvhd_kapacita_volna AS kapacita_volna,
    covid19_hospital_capacity_details.cvvhd_kapacita_celkem AS kapacita_celkem,
    covid19_hospital_capacity_details.cvvhd_kapacita_celkem - covid19_hospital_capacity_details.cvvhd_kapacita_volna AS kapacita_zaplnena,
    'cvvhd'::text AS typ
   FROM uzis.covid19_hospital_capacity_details
  WHERE covid19_hospital_capacity_details.datum >= '2021-04-01 00:00:00'::timestamp without time zone
UNION ALL
 SELECT covid19_hospital_capacity_details.datum,
        CASE
            WHEN covid19_hospital_capacity_details.datum = max(covid19_hospital_capacity_details.datum) OVER (PARTITION BY covid19_hospital_capacity_details.zz_nazev) THEN true
            ELSE false
        END AS posledni_den,
    covid19_hospital_capacity_details.zz_nazev,
    covid19_hospital_capacity_details.kraj_nazev,
    covid19_hospital_capacity_details.ventilatory_prenosne_kapacita_volna AS kapacita_volna,
    covid19_hospital_capacity_details.ventilatory_prenosne_kapacita_celkem AS kapacita_celkem,
    covid19_hospital_capacity_details.ventilatory_prenosne_kapacita_celkem - covid19_hospital_capacity_details.ventilatory_prenosne_kapacita_volna AS kapacita_zaplnena,
    'ventilatory_prenosne'::text AS typ
   FROM uzis.covid19_hospital_capacity_details
  WHERE covid19_hospital_capacity_details.datum >= '2021-04-01 00:00:00'::timestamp without time zone
UNION ALL
 SELECT covid19_hospital_capacity_details.datum,
        CASE
            WHEN covid19_hospital_capacity_details.datum = max(covid19_hospital_capacity_details.datum) OVER (PARTITION BY covid19_hospital_capacity_details.zz_nazev) THEN true
            ELSE false
        END AS posledni_den,
    covid19_hospital_capacity_details.zz_nazev,
    covid19_hospital_capacity_details.kraj_nazev,
    covid19_hospital_capacity_details.ventilatory_operacni_sal_kapacita_volna AS kapacita_volna,
    covid19_hospital_capacity_details.ventilatory_operacni_sal_kapacita_celkem AS kapacita_celkem,
    covid19_hospital_capacity_details.ventilatory_operacni_sal_kapacita_celkem - covid19_hospital_capacity_details.ventilatory_operacni_sal_kapacita_volna AS kapacita_zaplnena,
    'ventilatory_operacni_sal'::text AS typ
   FROM uzis.covid19_hospital_capacity_details
  WHERE covid19_hospital_capacity_details.datum >= '2021-04-01 00:00:00'::timestamp without time zone;

drop VIEW analytic.v_uzis_covid19_hospital_capacity_beds;
CREATE OR REPLACE VIEW analytic.v_uzis_covid19_hospital_capacity_beds
AS SELECT covid19_hospital_capacity_details.datum,
        CASE
            WHEN covid19_hospital_capacity_details.datum = max(covid19_hospital_capacity_details.datum) OVER (PARTITION BY covid19_hospital_capacity_details.zz_nazev) THEN true
            ELSE false
        END AS posledni_den,
    covid19_hospital_capacity_details.zz_nazev,
    covid19_hospital_capacity_details.kraj_nazev,
    covid19_hospital_capacity_details.luzka_standard_kyslik_kapacita_volna_covid_pozitivni AS kapacita_volna_covid_pozitivni_luzka,
    covid19_hospital_capacity_details.luzka_standard_kyslik_kapacita_volna_covid_negativni AS kapacita_volna_covid_negativni_luzka,
    covid19_hospital_capacity_details.luzka_standard_kyslik_kapacita_celkem AS kapacita_celkem_luzka,
    covid19_hospital_capacity_details.luzka_standard_kyslik_kapacita_celkem - covid19_hospital_capacity_details.luzka_standard_kyslik_kapacita_volna_covid_pozitivni - covid19_hospital_capacity_details.luzka_standard_kyslik_kapacita_volna_covid_negativni AS kapacita_zaplnena_luzka,
    'luzka_standard_kyslik'::text AS typ
   FROM uzis.covid19_hospital_capacity_details
  WHERE covid19_hospital_capacity_details.datum >= '2021-04-01 00:00:00'::timestamp without time zone
UNION ALL
 SELECT covid19_hospital_capacity_details.datum,
        CASE
            WHEN covid19_hospital_capacity_details.datum = max(covid19_hospital_capacity_details.datum) OVER (PARTITION BY covid19_hospital_capacity_details.zz_nazev) THEN true
            ELSE false
        END AS posledni_den,
    covid19_hospital_capacity_details.zz_nazev,
    covid19_hospital_capacity_details.kraj_nazev,
    covid19_hospital_capacity_details.luzka_hfno_cpap_kapacita_volna_covid_pozitivni AS kapacita_volna_covid_pozitivni_luzka,
    covid19_hospital_capacity_details.luzka_hfno_cpap_kapacita_volna_covid_negativni AS kapacita_volna_covid_negativni_luzka,
    covid19_hospital_capacity_details.luzka_hfno_cpap_kapacita_celkem AS kapacita_celkem_luzka,
    covid19_hospital_capacity_details.luzka_hfno_cpap_kapacita_celkem - covid19_hospital_capacity_details.luzka_hfno_cpap_kapacita_volna_covid_pozitivni - covid19_hospital_capacity_details.luzka_hfno_cpap_kapacita_volna_covid_negativni AS kapacita_zaplnena_luzka,
    'luzka_hfno_cpap'::text AS typ
   FROM uzis.covid19_hospital_capacity_details
  WHERE covid19_hospital_capacity_details.datum >= '2021-04-01 00:00:00'::timestamp without time zone
UNION ALL
 SELECT covid19_hospital_capacity_details.datum,
        CASE
            WHEN covid19_hospital_capacity_details.datum = max(covid19_hospital_capacity_details.datum) OVER (PARTITION BY covid19_hospital_capacity_details.zz_nazev) THEN true
            ELSE false
        END AS posledni_den,
    covid19_hospital_capacity_details.zz_nazev,
    covid19_hospital_capacity_details.kraj_nazev,
    covid19_hospital_capacity_details.luzka_upv_niv_kapacita_volna_covid_pozitivni AS kapacita_volna_covid_pozitivni_luzka,
    covid19_hospital_capacity_details.luzka_upv_niv_kapacita_volna_covid_negativni AS kapacita_volna_covid_negativni_luzka,
    covid19_hospital_capacity_details.luzka_upv_niv_kapacita_celkem AS kapacita_celkem_luzka,
    covid19_hospital_capacity_details.luzka_upv_niv_kapacita_celkem - covid19_hospital_capacity_details.luzka_upv_niv_kapacita_volna_covid_pozitivni - covid19_hospital_capacity_details.luzka_upv_niv_kapacita_volna_covid_negativni AS kapacita_zaplnena_luzka,
    'luzka_upv_niv'::text AS typ
   FROM uzis.covid19_hospital_capacity_details
  WHERE covid19_hospital_capacity_details.datum >= '2021-04-01 00:00:00'::timestamp without time zone;

-- analytic.v_uzis_covid19_hospital_capacity source
drop VIEW analytic.v_uzis_covid19_hospital_capacity;
CREATE OR REPLACE VIEW analytic.v_uzis_covid19_hospital_capacity
AS SELECT covid19_hospital_capacity_details.datum,
        CASE
            WHEN covid19_hospital_capacity_details.datum = max(covid19_hospital_capacity_details.datum) OVER (PARTITION BY covid19_hospital_capacity_details.zz_nazev) THEN true
            ELSE false
        END AS posledni_den,
    covid19_hospital_capacity_details.zz_nazev,
    covid19_hospital_capacity_details.kraj_nazev,
    covid19_hospital_capacity_details.luzka_standard_kyslik_kapacita_volna_covid_pozitivni,
    covid19_hospital_capacity_details.luzka_standard_kyslik_kapacita_volna_covid_negativni,
    covid19_hospital_capacity_details.luzka_standard_kyslik_kapacita_celkem,
    covid19_hospital_capacity_details.luzka_standard_kyslik_kapacita_celkem - covid19_hospital_capacity_details.luzka_standard_kyslik_kapacita_volna_covid_pozitivni - covid19_hospital_capacity_details.luzka_standard_kyslik_kapacita_volna_covid_negativni AS luzka_standard_kyslik_kapacita_zaplnena,
    covid19_hospital_capacity_details.luzka_hfno_cpap_kapacita_volna_covid_pozitivni,
    covid19_hospital_capacity_details.luzka_hfno_cpap_kapacita_volna_covid_negativni,
    covid19_hospital_capacity_details.luzka_hfno_cpap_kapacita_celkem,
    covid19_hospital_capacity_details.luzka_hfno_cpap_kapacita_celkem - covid19_hospital_capacity_details.luzka_hfno_cpap_kapacita_volna_covid_pozitivni - covid19_hospital_capacity_details.luzka_hfno_cpap_kapacita_volna_covid_negativni AS luzka_hfno_cpap_kapacita_zaplnena,
    covid19_hospital_capacity_details.luzka_upv_niv_kapacita_volna_covid_pozitivni,
    covid19_hospital_capacity_details.luzka_upv_niv_kapacita_volna_covid_negativni,
    covid19_hospital_capacity_details.luzka_upv_niv_kapacita_celkem,
    covid19_hospital_capacity_details.luzka_upv_niv_kapacita_celkem - covid19_hospital_capacity_details.luzka_upv_niv_kapacita_volna_covid_pozitivni - covid19_hospital_capacity_details.luzka_upv_niv_kapacita_volna_covid_negativni AS luzka_upv_niv_kapacita_zaplnena,
    COALESCE(covid19_hospital_capacity_details.luzka_standard_kyslik_kapacita_volna_covid_pozitivni, 0::double precision) + COALESCE(covid19_hospital_capacity_details.luzka_hfno_cpap_kapacita_volna_covid_pozitivni, 0::double precision) + COALESCE(covid19_hospital_capacity_details.luzka_upv_niv_kapacita_volna_covid_pozitivni, 0::double precision) AS luzka_kapacita_covid_pozitivni,
    COALESCE(covid19_hospital_capacity_details.luzka_standard_kyslik_kapacita_volna_covid_negativni, 0::double precision) + COALESCE(covid19_hospital_capacity_details.luzka_hfno_cpap_kapacita_volna_covid_negativni, 0::double precision) + COALESCE(covid19_hospital_capacity_details.luzka_upv_niv_kapacita_volna_covid_negativni, 0::double precision) AS luzka_kapacita_covid_negativni,
    COALESCE(covid19_hospital_capacity_details.luzka_standard_kyslik_kapacita_celkem, 0::double precision) + COALESCE(covid19_hospital_capacity_details.luzka_hfno_cpap_kapacita_celkem, 0::double precision) + COALESCE(covid19_hospital_capacity_details.luzka_upv_niv_kapacita_celkem, 0::double precision) AS luzka_kapacita_celkem,
    covid19_hospital_capacity_details.ecmo_kapacita_volna,
    covid19_hospital_capacity_details.ecmo_kapacita_celkem,
    covid19_hospital_capacity_details.ecmo_kapacita_celkem - covid19_hospital_capacity_details.ecmo_kapacita_volna AS ecmo_kapacita_zaplnena,
    covid19_hospital_capacity_details.cvvhd_kapacita_volna,
    covid19_hospital_capacity_details.cvvhd_kapacita_celkem,
    covid19_hospital_capacity_details.cvvhd_kapacita_celkem - covid19_hospital_capacity_details.cvvhd_kapacita_volna AS cvvhd_kapacita_zaplnena,
    covid19_hospital_capacity_details.ventilatory_prenosne_kapacita_volna,
    covid19_hospital_capacity_details.ventilatory_prenosne_kapacita_celkem,
    covid19_hospital_capacity_details.ventilatory_prenosne_kapacita_celkem - covid19_hospital_capacity_details.ventilatory_prenosne_kapacita_volna AS ventilatory_prenosne_kapacita_zaplnena,
    covid19_hospital_capacity_details.ventilatory_operacni_sal_kapacita_volna,
    covid19_hospital_capacity_details.ventilatory_operacni_sal_kapacita_celkem,
    covid19_hospital_capacity_details.ventilatory_operacni_sal_kapacita_celkem - covid19_hospital_capacity_details.ventilatory_operacni_sal_kapacita_volna AS ventilatory_operacni_sal_kapacita_zaplnena,
    COALESCE(covid19_hospital_capacity_details.ecmo_kapacita_volna, 0::double precision) + COALESCE(covid19_hospital_capacity_details.cvvhd_kapacita_volna, 0::double precision) + COALESCE(covid19_hospital_capacity_details.ventilatory_prenosne_kapacita_volna, 0::double precision) + COALESCE(covid19_hospital_capacity_details.ventilatory_operacni_sal_kapacita_volna, 0::double precision) AS pristroje_kapacita_volna,
    COALESCE(covid19_hospital_capacity_details.ecmo_kapacita_celkem, 0::double precision) + COALESCE(covid19_hospital_capacity_details.cvvhd_kapacita_celkem, 0::double precision) + COALESCE(covid19_hospital_capacity_details.ventilatory_prenosne_kapacita_celkem, 0::double precision) + COALESCE(covid19_hospital_capacity_details.ventilatory_operacni_sal_kapacita_celkem, 0::double precision) AS pristroje_kapacita_celkem
   FROM uzis.covid19_hospital_capacity_details
  WHERE covid19_hospital_capacity_details.datum >= '2021-04-01 00:00:00'::timestamp without time zone;		



-- analytic.v_uzis_covid19_hospital_capacity source
CREATE OR REPLACE VIEW analytic.v_uzis_covid19_hospital_capacity
AS SELECT covid19_hospital_capacity_details.datum,
        CASE
            WHEN covid19_hospital_capacity_details.datum = max(covid19_hospital_capacity_details.datum) OVER (PARTITION BY covid19_hospital_capacity_details.zz_nazev) THEN true
            ELSE false
        END AS posledni_den,
    covid19_hospital_capacity_details.zz_nazev,
    covid19_hospital_capacity_details.kraj_nazev,
    covid19_hospital_capacity_details.luzka_standard_kyslik_kapacita_volna_covid_pozitivni,
    covid19_hospital_capacity_details.luzka_standard_kyslik_kapacita_volna_covid_negativni,
    covid19_hospital_capacity_details.luzka_standard_kyslik_kapacita_celkem,
    covid19_hospital_capacity_details.luzka_standard_kyslik_kapacita_celkem - covid19_hospital_capacity_details.luzka_standard_kyslik_kapacita_volna_covid_pozitivni - covid19_hospital_capacity_details.luzka_standard_kyslik_kapacita_volna_covid_negativni AS luzka_standard_kyslik_kapacita_zaplnena,
    covid19_hospital_capacity_details.luzka_hfno_cpap_kapacita_volna_covid_pozitivni,
    covid19_hospital_capacity_details.luzka_hfno_cpap_kapacita_volna_covid_negativni,
    covid19_hospital_capacity_details.luzka_hfno_cpap_kapacita_celkem,
    covid19_hospital_capacity_details.luzka_hfno_cpap_kapacita_celkem - covid19_hospital_capacity_details.luzka_hfno_cpap_kapacita_volna_covid_pozitivni - covid19_hospital_capacity_details.luzka_hfno_cpap_kapacita_volna_covid_negativni AS luzka_hfno_cpap_kapacita_zaplnena,
    covid19_hospital_capacity_details.luzka_upv_niv_kapacita_volna_covid_pozitivni,
    covid19_hospital_capacity_details.luzka_upv_niv_kapacita_volna_covid_negativni,
    covid19_hospital_capacity_details.luzka_upv_niv_kapacita_celkem,
    covid19_hospital_capacity_details.luzka_upv_niv_kapacita_celkem - covid19_hospital_capacity_details.luzka_upv_niv_kapacita_volna_covid_pozitivni - covid19_hospital_capacity_details.luzka_upv_niv_kapacita_volna_covid_negativni AS luzka_upv_niv_kapacita_zaplnena,
    COALESCE(covid19_hospital_capacity_details.luzka_standard_kyslik_kapacita_volna_covid_pozitivni, 0::double precision) + COALESCE(covid19_hospital_capacity_details.luzka_hfno_cpap_kapacita_volna_covid_pozitivni, 0::double precision) + COALESCE(covid19_hospital_capacity_details.luzka_upv_niv_kapacita_volna_covid_pozitivni, 0::double precision) AS luzka_kapacita_covid_pozitivni,
    COALESCE(covid19_hospital_capacity_details.luzka_standard_kyslik_kapacita_volna_covid_negativni, 0::double precision) + COALESCE(covid19_hospital_capacity_details.luzka_hfno_cpap_kapacita_volna_covid_negativni, 0::double precision) + COALESCE(covid19_hospital_capacity_details.luzka_upv_niv_kapacita_volna_covid_negativni, 0::double precision) AS luzka_kapacita_covid_negativni,
    COALESCE(covid19_hospital_capacity_details.luzka_standard_kyslik_kapacita_celkem, 0::double precision) + COALESCE(covid19_hospital_capacity_details.luzka_hfno_cpap_kapacita_celkem, 0::double precision) + COALESCE(covid19_hospital_capacity_details.luzka_upv_niv_kapacita_celkem, 0::double precision) AS luzka_kapacita_celkem,
    covid19_hospital_capacity_details.ecmo_kapacita_volna,
    covid19_hospital_capacity_details.ecmo_kapacita_celkem,
    covid19_hospital_capacity_details.ecmo_kapacita_celkem - covid19_hospital_capacity_details.ecmo_kapacita_volna AS ecmo_kapacita_zaplnena,
    covid19_hospital_capacity_details.cvvhd_kapacita_volna,
    covid19_hospital_capacity_details.cvvhd_kapacita_celkem,
    covid19_hospital_capacity_details.cvvhd_kapacita_celkem - covid19_hospital_capacity_details.cvvhd_kapacita_volna AS cvvhd_kapacita_zaplnena,
    covid19_hospital_capacity_details.ventilatory_prenosne_kapacita_volna,
    covid19_hospital_capacity_details.ventilatory_prenosne_kapacita_celkem,
    covid19_hospital_capacity_details.ventilatory_prenosne_kapacita_celkem - covid19_hospital_capacity_details.ventilatory_prenosne_kapacita_volna AS ventilatory_prenosne_kapacita_zaplnena,
    covid19_hospital_capacity_details.ventilatory_operacni_sal_kapacita_volna,
    covid19_hospital_capacity_details.ventilatory_operacni_sal_kapacita_celkem,
    covid19_hospital_capacity_details.ventilatory_operacni_sal_kapacita_celkem - covid19_hospital_capacity_details.ventilatory_operacni_sal_kapacita_volna AS ventilatory_operacni_sal_kapacita_zaplnena,
    COALESCE(covid19_hospital_capacity_details.ecmo_kapacita_volna, 0::double precision) + COALESCE(covid19_hospital_capacity_details.cvvhd_kapacita_volna, 0::double precision) + COALESCE(covid19_hospital_capacity_details.ventilatory_prenosne_kapacita_volna, 0::double precision) + COALESCE(covid19_hospital_capacity_details.ventilatory_operacni_sal_kapacita_volna, 0::double precision) AS pristroje_kapacita_volna,
    COALESCE(covid19_hospital_capacity_details.ecmo_kapacita_celkem, 0::double precision) + COALESCE(covid19_hospital_capacity_details.cvvhd_kapacita_celkem, 0::double precision) + COALESCE(covid19_hospital_capacity_details.ventilatory_prenosne_kapacita_celkem, 0::double precision) + COALESCE(covid19_hospital_capacity_details.ventilatory_operacni_sal_kapacita_celkem, 0::double precision) AS pristroje_kapacita_celkem
   FROM uzis.covid19_hospital_capacity_details
  WHERE covid19_hospital_capacity_details.datum >= '2021-04-01 00:00:00'::timestamp without time zone;

alter table uzis.covid19_hospital_capacity_details
drop column inf_luzka_kyslik_kapacita_volna_covid_pozitivni,
drop column inf_luzka_kyslik_kapacita_volna_covid_negativni,
drop column inf_luzka_kyslik_kapacita_celkem,
drop column inf_luzka_hfno_kapacita_volna_covid_pozitivni,
drop column inf_luzka_hfno_kapacita_volna_covid_negativni,
drop column inf_luzka_hfno_kapacita_celkem,
drop column inf_luzka_upv_kapacita_volna_covid_pozitivni,
drop column inf_luzka_upv_kapacita_volna_covid_negativni,
drop column inf_luzka_upv_kapacita_celkem;
