CREATE TABLE public.fcd_traff_params (
    publication_time timestamp with time zone,
    source_identification varchar(50) NOT NULL,
    measurement_or_calculation_time timestamp with time zone,
    predefined_location varchar(50) NOT NULL,
    version_of_predefined_location int,
    traffic_level varchar(50) NOT NULL,
    queue_exists boolean,
    queue_length int,
    from_point real,
    to_point real,
    data_quality real,
    input_values int,
    average_vehicle_speed int,
    travel_time int,
    free_flow_travel_time int,
    free_flow_speed int,


    -- Audit fields --
    create_batch_id bigint,
    created_at timestamp with time zone,
    created_by varchar(150),
    update_batch_id bigint,
    updated_at timestamp with time zone,
    updated_by varchar(150),

    CONSTRAINT fcd_traff_params_pk PRIMARY KEY (source_identification, measurement_or_calculation_time)
);
