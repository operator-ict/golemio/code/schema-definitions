-- analytic.v_airbnb_occupancy_monthly source

-- analytic.v_airbnb_occupancy_monthly source

CREATE OR REPLACE VIEW analytic.v_airbnb_occupancy_monthly
AS WITH constants AS (
         SELECT 0.55 AS reviewers_ratio,
            3.2 AS avg_accomodation_duration
        ), yearly_occupancy AS (
         SELECT airbnb_occupancy.room_id,
            "left"(airbnb_occupancy.yearmon::text, 4) AS o_year,
            (( SELECT constants.avg_accomodation_duration
                   FROM constants))::double precision * sum(airbnb_occupancy.reviews_count) / (( SELECT constants.reviewers_ratio
                   FROM constants))::double precision AS yearly_estimated_days_occupied
           FROM python.airbnb_occupancy
          GROUP BY airbnb_occupancy.room_id, ("left"(airbnb_occupancy.yearmon::text, 4))
        ), last_12m_occupancy AS (
         SELECT airbnb_occupancy.room_id,
            (( SELECT constants.avg_accomodation_duration
                   FROM constants))::double precision * sum(airbnb_occupancy.reviews_count) / (( SELECT constants.reviewers_ratio
                   FROM constants))::double precision AS yearly_estimated_days_occupied
           FROM python.airbnb_occupancy
          WHERE concat(airbnb_occupancy.yearmon, '-01')::date >= (date_trunc('month'::text, airbnb_occupancy.created_at) - '1 year'::interval) AND concat(airbnb_occupancy.yearmon, '-01')::date < date_trunc('month'::text, airbnb_occupancy.created_at)
          GROUP BY airbnb_occupancy.room_id
        )
 SELECT o.room_id,
    "left"(o.yearmon::text, 4) AS o_year,
    concat(o.yearmon, '-01')::date AS o_month,
    l.nazev_mc,
    l.offer_type,
    l.room_created,
    l.open_closed,
    l.ended_at,
    o.reviews_count / (( SELECT constants.reviewers_ratio
           FROM constants))::double precision AS stays_count,
    (( SELECT constants.avg_accomodation_duration
           FROM constants))::double precision * o.reviews_count / (( SELECT constants.reviewers_ratio
           FROM constants))::double precision AS estimated_days_occupied,
    date_part('day'::text, concat(o.yearmon, '-01')::date + '1 mon'::interval - concat(o.yearmon, '-01')::date::timestamp without time zone) AS days_in_month,
    o.reviews_count,
    o.days_occupied,
    0::numeric AS esimated_price_per_night,
    o.mean_price,
        CASE
            WHEN yo.yearly_estimated_days_occupied > 60::numeric::double precision THEN 'Pravidelné'::text
            ELSE 'Příležitostní'::text
        END AS regularity2,
    l.user_id,
    bf.bytovy_fond,
    l.ended_at AS ukonceno,
        CASE
            WHEN yo.yearly_estimated_days_occupied > 60::numeric::double precision AND l.offer_type = 'Celý dům/byt'::text THEN 'Pravidelné, celý byt/dům'::text
            ELSE 'Sdílená ekonomika'::text
        END AS offer_type_group,
        CASE
            WHEN concat(o.yearmon, '-01')::date >= (date_trunc('month'::text, o.created_at) - '1 year'::interval) AND concat(o.yearmon, '-01')::date < date_trunc('month'::text, o.created_at) THEN 'Y'::text
            ELSE 'N'::text
        END AS last_12m,
    l.latitude,
    l.longitude,
    o.created_at AS record_created_at,
        CASE
            WHEN concat(o.yearmon, '-01')::date >= (date_trunc('month'::text, o.created_at) - '1 year'::interval) AND concat(o.yearmon, '-01')::date < date_trunc('month'::text, o.created_at) THEN true
            ELSE false
        END AS last_12_months,
        CASE
            WHEN lo.yearly_estimated_days_occupied > 60::numeric::double precision THEN 'Pravidelné'::text
            ELSE 'Příležitostní'::text
        END AS regularity_last_12m,
        CASE
            WHEN lo.yearly_estimated_days_occupied > 60::numeric::double precision AND l.offer_type = 'Celý dům/byt'::text THEN 'Pravidelné, celý byt/dům'::text
            ELSE 'Sdílená ekonomika'::text
        END AS offer_type_group_last_12m
   FROM python.airbnb_occupancy o
     JOIN analytic.v_airbnb_listings l ON o.room_id::text = l.room_id::text
     LEFT JOIN yearly_occupancy yo ON yo.room_id::text = o.room_id::text AND yo.o_year = "left"(o.yearmon::text, 4)
     LEFT JOIN python.airbnb_apartment_count bf ON l.nazev_mc::text = bf.nazev_mc::text
     LEFT JOIN last_12m_occupancy lo ON lo.room_id::text = o.room_id::text
  WHERE concat(o.yearmon, '-01')::date >= l.room_created AND concat(o.yearmon, '-01')::date < date_trunc('month'::text, o.created_at) AND concat(o.yearmon, '-01')::date >= '2017-01-01'::date AND l.nazev_mc::text <> ''::text AND l.nazev_mc IS NOT NULL AND o.reviews_count > 0::numeric::double precision;
   
drop materialized view analytic.v_airbnb_occupancy_monthly_materialized;

-- analytic.v_covid19_airbnb source

CREATE OR REPLACE VIEW analytic.v_covid19_airbnb
AS SELECT o.o_month,
    count(DISTINCT o.room_id) AS active_roooms,
    round(sum(o.stays_count)) AS stays_count,
    history.active_roooms AS active_rooms_2019,
    history.stays_count AS stays_count_2019,
    o.nazev_mc,
    NULL::text AS cz_month,
    rank() OVER (ORDER BY o.o_month) AS month_order
   FROM analytic.v_airbnb_occupancy_monthly o
     JOIN ( SELECT v_airbnb_occupancy_monthly.o_month,
            count(DISTINCT v_airbnb_occupancy_monthly.room_id) AS active_roooms,
            round(sum(v_airbnb_occupancy_monthly.stays_count)) AS stays_count,
            concat(date_part('year'::text, v_airbnb_occupancy_monthly.o_month + '1 year'::interval)::text, '-', date_part('month'::text, v_airbnb_occupancy_monthly.o_month)::text, '-', date_part('day'::text, v_airbnb_occupancy_monthly.o_month)::text)::date AS join_month,
            v_airbnb_occupancy_monthly.nazev_mc
           FROM analytic.v_airbnb_occupancy_monthly
          WHERE v_airbnb_occupancy_monthly.o_month >= '2018-09-01'::date
          GROUP BY v_airbnb_occupancy_monthly.o_month, v_airbnb_occupancy_monthly.nazev_mc) history ON history.join_month = o.o_month AND o.nazev_mc::text = history.nazev_mc::text
  WHERE o.o_month >= '2019-09-01'::date
  GROUP BY o.o_month, history.active_roooms, history.stays_count, o.nazev_mc;