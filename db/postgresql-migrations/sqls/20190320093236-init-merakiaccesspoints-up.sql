-- Sequence: public.merakiaccesspoints_observations_id_seq

CREATE SEQUENCE public.merakiaccesspoints_observations_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

-- Table: public.merakiaccesspoints_observations

CREATE TABLE public.merakiaccesspoints_observations
(
  id integer NOT NULL DEFAULT nextval('merakiaccesspoints_observations_id_seq'::regclass),
  ap_mac character varying(255),
  client_mac character varying(255),
  ipv4 character varying(255),
  ipv6 character varying(255),
  lat double precision,
  lng double precision,
  manufacturer character varying(255),
  os character varying(255),
  rssi integer,
  ssid character varying(255),
  "timestamp" timestamp with time zone,
  type character varying(255),
  unc double precision,
  CONSTRAINT merakiaccesspoints_observations_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);


-- Table: public.merakiaccesspoints_tags

CREATE TABLE public.merakiaccesspoints_tags
(
  ap_mac character varying(255) NOT NULL,
  tag character varying(255) NOT NULL,
  CONSTRAINT merakiaccesspoints_tags_pkey PRIMARY KEY (ap_mac, tag)
)
WITH (
  OIDS=FALSE
);
