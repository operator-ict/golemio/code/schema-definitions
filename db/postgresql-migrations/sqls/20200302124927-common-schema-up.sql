/* Replace with your SQL commands */
create schema IF NOT EXISTS common;

CREATE TABLE common.citydistricts (
	geom geometry(POLYGON, 4326) NULL,
	objectid int4 NULL,
	create_date varchar NULL,
	change_date varchar NULL,
	area float8 NULL,
	id int4 NOT NULL,
	zip int4 NULL,
	district_name varchar NULL,
	kod_mo int4 NULL,
	kod_so varchar NULL,
	tid_tmmestckecasti_p int4 NULL,
	provider varchar NULL,
	id_provider int4 NULL,
	change_status varchar NULL,
	district_short_name varchar NULL,
	shape_length float8 NULL,
	shape_area float8 NULL,
	CONSTRAINT citydistricts_pkey PRIMARY KEY (id)
);
CREATE INDEX idx_citydistricts_geom ON common.citydistricts USING gist (geom);

update meta.extract
	set schema_extract = 'common'
	where name_extract = 'citydistricts';


insert into common.citydistricts
	select * from public.citydistricts ;


drop TABLE public.citydistricts;
