drop VIEW if exists analytic.v_airbnb_sreality_offers;
drop VIEW if exists analytic.v_airbnb_rooms_stays;
drop VIEW if exists analytic.v_sreality;
DROP TABLE if exists python.sreality;

CREATE TABLE python.sreality (
	week_label varchar(50) NULL,
	week_number numeric NOT NULL,
	offer_type varchar(50) NULL,
	district varchar(50) NULL,
	category varchar(50) NULL,
	subcategory varchar(50) NULL,
	offers_count numeric NULL,
	mean_price float8 NULL,
	median_price float8 NULL
);

-- analytic.v_covid19_airbnb source

CREATE OR REPLACE VIEW analytic.v_covid19_airbnb
AS SELECT o.o_month,
    count(DISTINCT o.room_id) AS active_roooms,
    round(sum(o.stays_count)) AS stays_count,
    history.active_roooms AS active_rooms_2019,
    history.stays_count AS stays_count_2019,
    o.nazev_mc,
    NULL::text AS cz_month,
    rank() OVER (ORDER BY o.o_month) AS month_order
   FROM analytic.v_airbnb_occupancy_monthly o
     JOIN ( SELECT v_airbnb_occupancy_monthly.o_month,
            count(DISTINCT v_airbnb_occupancy_monthly.room_id) AS active_roooms,
            round(sum(v_airbnb_occupancy_monthly.stays_count)) AS stays_count,
            concat(date_part('year'::text, v_airbnb_occupancy_monthly.o_month + '1 year'::interval)::text, '-', date_part('month'::text, v_airbnb_occupancy_monthly.o_month)::text, '-', date_part('day'::text, v_airbnb_occupancy_monthly.o_month)::text)::date AS join_month,
            v_airbnb_occupancy_monthly.nazev_mc
           FROM analytic.v_airbnb_occupancy_monthly
          WHERE v_airbnb_occupancy_monthly.o_month >= '2018-09-01'::date
          GROUP BY v_airbnb_occupancy_monthly.o_month, v_airbnb_occupancy_monthly.nazev_mc) history ON history.join_month = o.o_month AND o.nazev_mc::text = history.nazev_mc::text
  WHERE o.o_month >= '2019-09-01'::date
  GROUP BY o.o_month, history.active_roooms, history.stays_count, o.nazev_mc;

