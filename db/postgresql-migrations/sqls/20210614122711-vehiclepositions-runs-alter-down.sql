ALTER TABLE public.vehiclepositions_runs_messages RENAME COLUMN actual_stop_asw_id TO actual_stop_aws_id;
ALTER TABLE public.vehiclepositions_runs_messages RENAME COLUMN last_stop_asw_id TO last_stop_aws_id;
ALTER TABLE public.vehiclepositions_runs_messages RENAME COLUMN packet_number TO pkt;
ALTER TABLE public.vehiclepositions_runs_messages RENAME COLUMN msg_timestamp TO tm;

ALTER TABLE public.vehiclepositions_runs DROP COLUMN msg_last_timestamp;
