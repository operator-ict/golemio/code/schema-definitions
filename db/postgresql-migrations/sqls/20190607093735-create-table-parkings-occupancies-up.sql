
CREATE SEQUENCE public.parkings_occupancies_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

CREATE TABLE public.parkings_occupancies
(
  id bigint NOT NULL DEFAULT nextval('parkings_occupancies_id_seq'::regclass),
  capacity integer,
  occupation integer,
  parking_id integer,
  reservedcapacity integer,
  reservedoccupation integer,
  create_batch_id bigint,
  created_at timestamp with time zone,
  created_by character varying(150),
  update_batch_id bigint,
  updated_at timestamp with time zone,
  updated_by character varying(150),
  CONSTRAINT parkings_occupancies_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);