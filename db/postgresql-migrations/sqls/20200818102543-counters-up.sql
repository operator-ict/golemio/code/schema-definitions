CREATE TABLE public.counters_locations
(
  id character varying(255) NOT NULL,
  vendor_id character varying(255) NOT NULL,
  vendor character varying(100),
  lat double precision NOT NULL,
  lng double precision NOT NULL,
  "name" character varying(255),
  "route" character varying(255),

  create_batch_id bigint,
  created_at timestamp with time zone,
  created_by character varying(150),
  update_batch_id bigint,
  updated_at timestamp with time zone,
  updated_by character varying(150),

  CONSTRAINT counters_locations_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);


CREATE TABLE public.counters_directions
(
  id character varying(255) NOT NULL,
  vendor_id character varying(255) NOT NULL,
  locations_id character varying(255) NOT NULL,
  "name" character varying(255),

  create_batch_id bigint,
  created_at timestamp with time zone,
  created_by character varying(150),
  update_batch_id bigint,
  updated_at timestamp with time zone,
  updated_by character varying(150),

  CONSTRAINT counters_directions_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

CREATE INDEX counters_directions_locations_id ON public.counters_directions USING btree (locations_id);


CREATE TABLE public.counters_detections
(
  locations_id character varying(255) NOT NULL,
  directions_id character varying(255) NOT NULL,
  measured_from bigint NOT NULL,
  measured_to bigint NOT NULL,
  category character varying(100),
  "value" integer,

  create_batch_id bigint,
  created_at timestamp with time zone,
  created_by character varying(150),
  update_batch_id bigint,
  updated_at timestamp with time zone,
  updated_by character varying(150),

  CONSTRAINT counters_detections_pkey PRIMARY KEY (locations_id, directions_id, measured_from, category)
)
WITH (
  OIDS=FALSE
);

CREATE INDEX counters_detections_locations_id ON public.counters_detections USING btree (locations_id);

CREATE INDEX counters_detections_directions_id ON public.counters_detections USING btree (directions_id);