-- prague_portal		   
drop VIEW analytic.v_prague_portal_census_average_age;
drop VIEW analytic.v_prague_portal_census;

-- openproject
drop VIEW analytic.v_openproject_mhmp_workpackages;	
drop VIEW analytic.v_openproject_mhmp_topics;	
drop VIEW analytic.v_openproject_mhmp_projects;	
drop VIEW analytic.v_openproject_mhmp_domain;	
-- obis
drop VIEW analytic.v_obis_proposals_all;	
	
drop VIEW analytic.v_obis_submitter_comments;
drop VIEW analytic.v_obis_submitter;

drop VIEW analytic.v_obis_proposals;
drop VIEW analytic.v_obis_processor;
drop VIEW analytic.v_obis_comments;

--
drop VIEW analytic.v_healthy_classroom_mv_data;
drop VIEW analytic.v_healthy_classroom_mv_data_technology_comparison;
drop VIEW analytic.v_healthy_classroom_mv_data_teachers;

drop VIEW analytic.v_internal_reports_golemio_bi;
drop MATERIALIZED VIEW if exists analytic.mv_healthy_classroom_with_timetables;
drop VIEW if exists analytic.v_healthy_classroom_evaluations;
drop VIEW if exists analytic.v_healthy_classroom_5min_intervals;

drop MATERIALIZED VIEW if exists analytic.mv_healthy_classroom_technology_comparison;
drop VIEW if exists analytic.v_healthy_classroom_source_filtered_technology_comparison;

drop VIEW if exists analytic.v_healthy_classroom_source_filtered;

drop VIEW analytic.v_dpp_metro_covid_comparison;
drop VIEW analytic.v_construction_procedure;

drop VIEW analytic.v_airbnb_start_end_monthly;
drop VIEW analytic.v_airbnb_sreality_offers;
drop VIEW analytic.v_airbnb_rooms_stays;
drop VIEW analytic.v_airbnb_room_per_user;
drop VIEW analytic.v_airbnb_overview_12m;

drop VIEW analytic.v_airbnb_dim_regularity;
drop VIEW analytic.v_airbnb_districts_last12m;

drop VIEW analytic.v_airbnb_occupancy_monthly;
drop VIEW analytic.v_airbnb_dim_offertype;
drop VIEW analytic.v_airbnb_listings;

drop VIEW analytic.v_airbnb_dim_district;	
