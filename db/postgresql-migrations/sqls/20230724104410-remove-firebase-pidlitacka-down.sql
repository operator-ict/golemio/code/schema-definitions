-- public.firebase_pidlitacka_applaunch_par definition
CREATE TABLE public.firebase_pidlitacka_applaunch_par (
	event_date date NOT NULL,
	event_count int4 NOT NULL,
	users int4 NOT NULL,
	mails int4 NOT NULL,
	mail_users int4 NOT NULL,
	notis int4 NOT NULL,
	noti_users int4 NOT NULL,
	anonym int4 NOT NULL,
	anonym_users int4 NOT NULL,
	isanonym int4 NOT NULL,
	isanonym_users int4 NOT NULL,
	hasnfc int4 NOT NULL,
	hasnfc_users int4 NOT NULL,
	create_batch_id int8 NULL,
	created_at timestamp NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamp NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT firebase_pidlitacka_applaunch_par_pkey PRIMARY KEY (event_date)
);
CREATE INDEX firebase_pidlitacka_applaunch_par_create_batch ON public.firebase_pidlitacka_applaunch_par USING btree (create_batch_id);
CREATE INDEX firebase_pidlitacka_applaunch_par_update_batch ON public.firebase_pidlitacka_applaunch_par USING btree (update_batch_id);


-- public.firebase_pidlitacka_events definition
CREATE TABLE public.firebase_pidlitacka_events (
	event_date date NOT NULL,
	event_name varchar(255) NOT NULL,
	event_count int4 NOT NULL,
	users int4 NOT NULL,
	create_batch_id int8 NULL,
	created_at timestamp NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamp NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT firebase_pidlitacka_events_pkey PRIMARY KEY (event_date, event_name)
);
CREATE INDEX firebase_pidlitacka_events_create_batch ON public.firebase_pidlitacka_events USING btree (create_batch_id);
CREATE INDEX firebase_pidlitacka_events_update_batch ON public.firebase_pidlitacka_events USING btree (update_batch_id);


-- public.firebase_pidlitacka_route definition
CREATE TABLE public.firebase_pidlitacka_route (
	reference_date date NOT NULL,
	s_from varchar(255) NOT NULL,
	s_to varchar(255) NOT NULL,
	count_case int4 NOT NULL,
	create_batch_id int8 NULL,
	created_at timestamp NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamp NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT firebase_pidlitacka_route_pkey PRIMARY KEY (reference_date, s_from, s_to)
);
CREATE INDEX firebase_pidlitacka_route_create_batch ON public.firebase_pidlitacka_route USING btree (create_batch_id);
CREATE INDEX firebase_pidlitacka_route_update_batch ON public.firebase_pidlitacka_route USING btree (update_batch_id);

-- public.firebase_pidlitacka_web_events definition
CREATE TABLE public.firebase_pidlitacka_web_events (
	id varchar(255) NULL,
	id_profile varchar(255) NULL,
	reference_date date NOT NULL,
	sessions int4 NULL,
	users int4 NULL,
	create_batch_id int8 NULL,
	created_at timestamp NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamp NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT firebase_pidlitacka_web_events_pkey PRIMARY KEY (reference_date)
);
CREATE INDEX firebase_pidlitacka_web_events_create_batch ON public.firebase_pidlitacka_web_events USING btree (create_batch_id);
CREATE INDEX firebase_pidlitacka_web_events_update_batch ON public.firebase_pidlitacka_web_events USING btree (update_batch_id);


-- analytic.v_firebase_pidlitacka_route source
CREATE OR REPLACE VIEW analytic.v_firebase_pidlitacka_route
AS SELECT date_trunc('month'::text, fpr.reference_date::timestamp with time zone) AS reference_date,
    fpr.s_from,
    fpr.s_to,
    sum(fpr.count_case) AS count_case
FROM firebase_pidlitacka_route fpr
WHERE fpr.s_from::text <> '?'::text AND fpr.reference_date >= (now() - '2 years'::interval)
GROUP BY (date_trunc('month'::text, fpr.reference_date::timestamp with time zone)), fpr.s_from, fpr.s_to;


-- analytic.v_ropidbi_firebase_eventy source
CREATE OR REPLACE VIEW analytic.v_ropidbi_firebase_eventy
AS SELECT firebase_pidlitacka_events.event_date AS datum,
    CASE
        WHEN firebase_pidlitacka_events.event_name::text = 'connectionSearch'::text THEN 'Vyhledat spojení'::character varying
        WHEN firebase_pidlitacka_events.event_name::text = 'parkingDetail'::text THEN 'Koupit parkování P+R'::character varying
        WHEN firebase_pidlitacka_events.event_name::text = 'app_remove'::text THEN 'Odinstalování aplikace'::character varying
        WHEN firebase_pidlitacka_events.event_name::text = 'user_engagement'::text THEN 'Počet uživatelů'::character varying
        WHEN firebase_pidlitacka_events.event_name::text = 'departureSearch'::text THEN 'Funkce odjezdy'::character varying
        ELSE firebase_pidlitacka_events.event_name
    END AS event_name
FROM firebase_pidlitacka_events;


-- analytic.v_ropidbi_litacka_functions source
CREATE OR REPLACE VIEW analytic.v_ropidbi_litacka_functions
AS SELECT ue.event_date,
    ue.users,
    a.fce,
    a.users AS n_fce
FROM (
    SELECT firebase_pidlitacka_applaunch_par.event_date,
        firebase_pidlitacka_applaunch_par.mail_users AS users,
        'MAIL_USERS'::text AS fce
    FROM firebase_pidlitacka_applaunch_par
    UNION ALL
    SELECT firebase_pidlitacka_applaunch_par.event_date,
        firebase_pidlitacka_applaunch_par.noti_users AS users,
        'NOTI_USERS'::text AS fce
    FROM firebase_pidlitacka_applaunch_par
    UNION ALL
    SELECT firebase_pidlitacka_applaunch_par.event_date,
        firebase_pidlitacka_applaunch_par.hasnfc_users AS users,
        'HASNFC_USERS'::text AS fce
    FROM firebase_pidlitacka_applaunch_par
    UNION ALL
    SELECT firebase_pidlitacka_events.event_date,
        firebase_pidlitacka_events.users,
        firebase_pidlitacka_events.event_name AS fce
    FROM firebase_pidlitacka_events
    WHERE firebase_pidlitacka_events.event_name::text = 'departureSearch'::text
    UNION ALL
    SELECT firebase_pidlitacka_events.event_date,
        firebase_pidlitacka_events.users,
        firebase_pidlitacka_events.event_name AS fce
    FROM firebase_pidlitacka_events
    WHERE firebase_pidlitacka_events.event_name::text = 'connectionSearch'::text
    UNION ALL
    SELECT firebase_pidlitacka_events.event_date,
        firebase_pidlitacka_events.users,
        firebase_pidlitacka_events.event_name AS fce
    FROM firebase_pidlitacka_events
    WHERE firebase_pidlitacka_events.event_name::text = 'parkingDetail'::text
    UNION ALL
    SELECT firebase_pidlitacka_events.event_date,
        firebase_pidlitacka_events.users,
        firebase_pidlitacka_events.event_name AS fce
    FROM firebase_pidlitacka_events
    WHERE firebase_pidlitacka_events.event_name::text = 'app_remove'::text
    UNION ALL
    SELECT firebase_pidlitacka_events.event_date,
        firebase_pidlitacka_events.users,
        firebase_pidlitacka_events.event_name AS fce
    FROM firebase_pidlitacka_events
    WHERE firebase_pidlitacka_events.event_name::text = 'appLaunch'::text
) a
LEFT JOIN (
    SELECT firebase_pidlitacka_events.event_date,
        firebase_pidlitacka_events.users
    FROM firebase_pidlitacka_events
    WHERE firebase_pidlitacka_events.event_name::text = 'user_engagement'::text
) ue ON ue.event_date = a.event_date
ORDER BY ue.event_date, a.fce;
