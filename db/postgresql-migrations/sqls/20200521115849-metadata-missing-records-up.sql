-- bicyclecounters
insert into meta.dataset (code_dataset ,name_dataset ,description ,created_at )
values ('bicyclecounters','bicyclecounters','Indukční smyčky na cyklotrasách počítající směrové průjezdy cyklistů',now())
on conflict (name_dataset) do nothing ;

insert into  meta."extract" (name_extract ,id_dataset ,id_db ,description ,schema_extract ,created_at )
values 
(
'bicyclecounters_api_logs_failures',
(select id_dataset from meta.dataset where code_dataset ='bicyclecounters'),1,
'Seznam logů failures definovaného testovaného endpointu (neúspěšná volání)','public',now()
),
('bicyclecounters_api_logs_hits',
(select id_dataset from meta.dataset where code_dataset ='bicyclecounters'),1,
'','public',now()
),
('bicyclecounters_detections',
(select id_dataset from meta.dataset where code_dataset ='bicyclecounters'),1,
'průjezdy','public',now()
),
('bicyclecounters_directions',
(select id_dataset from meta.dataset where code_dataset ='bicyclecounters'),1,
'směry','public',now()
),
('bicyclecounters_locations',
(select id_dataset from meta.dataset where code_dataset ='bicyclecounters'),1,
'lokace','public',now()
),
('bicyclecounters_temperatures',
(select id_dataset from meta.dataset where code_dataset ='bicyclecounters'),1,
'měření teplot','public',now()
)
on conflict (name_extract) do nothing ;


-- merakiaccesspoints
/*
insert into meta.dataset (code_dataset ,name_dataset ,description ,created_at )
values ('merakiaccesspoints','merakiaccesspoints','MERAKI accespoint',now())
on conflict (name_dataset) do nothing ;

insert into  meta."extract" (name_extract ,id_dataset ,id_db ,description ,schema_extract ,created_at )
values 
(
'merakiaccesspoints_observations',
(select id_dataset from meta.dataset where code_dataset ='merakiaccesspoints'),1,
'','public',now()
),
(
'merakiaccesspoints_tags',
(select id_dataset from meta.dataset where code_dataset ='merakiaccesspoints'),1,
'','public',now()
)
on conflict (name_extract) do nothing;
*/

-- mobileappstatistics
insert into meta.dataset (code_dataset ,name_dataset ,description ,created_at )
values ('mobileappstatistics','mobileappstatistics','mobileappstatistics',now())
on conflict (name_dataset) do nothing ;

insert into  meta."extract" (name_extract ,id_dataset ,id_db ,description ,schema_extract ,created_at )
values 
(
'mobileappstatistics_appstore',
(select id_dataset from meta.dataset where code_dataset ='mobileappstatistics'),1,
'Data z AppStore','public',now()
),
(
'mobileappstatistics_playstore',
(select id_dataset from meta.dataset where code_dataset ='mobileappstatistics'),1,
'Data z PlayStore','public',now()
)
on conflict (name_extract) do nothing;


-- mos_be
-- dataset již zaregistrován
insert into meta.dataset (code_dataset ,name_dataset ,description ,created_at )
values ('mos_be','mos_be','data MOS',now())
on conflict (code_dataset) do nothing ;

insert into  meta."extract" (name_extract ,id_dataset ,id_db ,description ,schema_extract ,created_at )
values 
(
'mos_be_accounts',
(select id_dataset from meta.dataset where code_dataset ='mos_be'),1,
'Denní přehledy','public',now()
),
(
'mos_be_coupons',
(select id_dataset from meta.dataset where code_dataset ='mos_be'),1,
'Seznam kupónů','public',now()
),
(
'mos_be_customers',
(select id_dataset from meta.dataset where code_dataset ='mos_be'),1,
'Seznam zákazníků','public',now()
),
(
'mos_be_zones',
(select id_dataset from meta.dataset where code_dataset ='mos_be'),1,
'Seznam zón pro kupony','public',now()
)
on conflict (name_extract) do nothing;

-- MOS_MA
insert into meta.dataset (code_dataset ,name_dataset ,description ,created_at )
values ('mos_ma','mos_ma','MOS mobilní aplikace',now())
on conflict (name_dataset) do nothing ;

insert into  meta."extract" (name_extract ,id_dataset ,id_db ,description ,schema_extract ,created_at )
values 
(
'mos_ma_devicemodels',
(select id_dataset from meta.dataset where code_dataset ='mos_ma'),1,
'přehled modelů','public',now()
),
(
'mos_ma_ticketactivations',
(select id_dataset from meta.dataset where code_dataset ='mos_ma'),1,
'aktivace jízdenek','public',now()
),
(
'mos_ma_ticketinspections',
(select id_dataset from meta.dataset where code_dataset ='mos_ma'),1,
'kontroly','public',now()
),
(
'mos_ma_ticketpurchases',
(select id_dataset from meta.dataset where code_dataset ='mos_ma'),1,
'nákupy jízdenek','public',now()
)
on conflict (name_extract) do nothing;

-- parkings
insert into meta.dataset (code_dataset ,name_dataset ,description ,created_at )
values ('parkings','parkings','parkování',now())
on conflict (name_dataset) do nothing ;

insert into  meta."extract" (name_extract ,id_dataset ,id_db ,description ,schema_extract ,created_at )
values 
(
'parkings_occupancies',
(select id_dataset from meta.dataset where code_dataset ='parkings'),1,
'obsazenost','public',now()
)
on conflict (name_extract) do nothing;

-- parkomats
insert into meta.dataset (code_dataset ,name_dataset ,description ,created_at )
values ('parkomats','parkomats','parkování ZPS',now())
on conflict (name_dataset) do nothing ;

insert into  meta."extract" (name_extract ,id_dataset ,id_db ,description ,schema_extract ,created_at )
values 
(
'parkomats',
(select id_dataset from meta.dataset where code_dataset ='parkomats'),1,
'Parkování ZPS','public',now()
)
on conflict (name_extract) do nothing;

-- ropidgtfs
insert into meta.dataset (code_dataset ,name_dataset ,description ,created_at )
values ('ropidgtfs','ropidgtfs','jízdní řády autobusů ROPID',now())
on conflict (name_dataset) do nothing ;

insert into  meta."extract" (name_extract ,id_dataset ,id_db ,description ,schema_extract ,created_at )
values 
(
'ropidgtfs_agency',
(select id_dataset from meta.dataset where code_dataset ='ropidgtfs'),1,
'provozovatel','public',now()
),
(
'ropidgtfs_calendar',
(select id_dataset from meta.dataset where code_dataset ='ropidgtfs'),1,
'kalendář','public',now()
),
(
'ropidgtfs_calendar_dates',
(select id_dataset from meta.dataset where code_dataset ='ropidgtfs'),1,
'kalendář - data','public',now()
),
(
'ropidgtfs_cis_stop_groups',
(select id_dataset from meta.dataset where code_dataset ='ropidgtfs'),1,
'zastávky - skupiny','public',now()
),
(
'ropidgtfs_cis_stops',
(select id_dataset from meta.dataset where code_dataset ='ropidgtfs'),1,
'zastávky','public',now()
),
(
'ropidgtfs_metadata',
(select id_dataset from meta.dataset where code_dataset ='ropidgtfs'),1,
'metadata','public',now()
),
(
'ropidgtfs_routes',
(select id_dataset from meta.dataset where code_dataset ='ropidgtfs'),1,
'trasy','public',now()
),
(
'ropidgtfs_shapes',
(select id_dataset from meta.dataset where code_dataset ='ropidgtfs'),1,
'spoje','public',now()
),
(
'ropidgtfs_stop_times',
(select id_dataset from meta.dataset where code_dataset ='ropidgtfs'),1,
'časy zastávek','public',now()
),
(
'ropidgtfs_stops',
(select id_dataset from meta.dataset where code_dataset ='ropidgtfs'),1,
'zastávek','public',now()
),
(
'ropidgtfs_trips',
(select id_dataset from meta.dataset where code_dataset ='ropidgtfs'),1,
'linky','public',now()
)
on conflict (name_extract) do nothing;

-- vehiclepositions
insert into meta.dataset (code_dataset ,name_dataset ,description ,created_at )
values ('vehiclepositions','vehiclepositions','polohy vozidel',now())
on conflict (name_dataset) do nothing ;

insert into  meta."extract" (name_extract ,id_dataset ,id_db ,description ,schema_extract ,created_at )
values 
(
'vehiclepositions_positions',
(select id_dataset from meta.dataset where code_dataset ='vehiclepositions'),1,
'pozice','public',now()
),
(
'vehiclepositions_stops',
(select id_dataset from meta.dataset where code_dataset ='vehiclepositions'),1,
'zastávky','public',now()
),
(
'vehiclepositions_trips',
(select id_dataset from meta.dataset where code_dataset ='vehiclepositions'),1,
'spoje','public',now()
),
(
'vehiclepositions_vehicle_types',
(select id_dataset from meta.dataset where code_dataset ='vehiclepositions'),1,
'typy vozidel','public',now()
)
on conflict (name_extract) do nothing;

-- wazeccp
insert into meta.dataset (code_dataset ,name_dataset ,description ,created_at )
values ('wazeccp','wazeccp','WAZE data',now())
on conflict (name_dataset) do nothing ;

insert into  meta."extract" (name_extract ,id_dataset ,id_db ,description ,schema_extract ,created_at )
values 
(
'wazeccp_alert_types',
(select id_dataset from meta.dataset where code_dataset ='wazeccp'),1,
'typy výstrah','public',now()
),
(
'wazeccp_alerts',
(select id_dataset from meta.dataset where code_dataset ='wazeccp'),1,
'výstrahy','public',now()
),
(
'wazeccp_irregularities',
(select id_dataset from meta.dataset where code_dataset ='wazeccp'),1,
'dopravní omezení','public',now()
),
(
'wazeccp_jams',
(select id_dataset from meta.dataset where code_dataset ='wazeccp'),1,
'dopravní zácpy','public',now()
),
(
'wazeccp_roads',
(select id_dataset from meta.dataset where code_dataset ='wazeccp'),1,
'cesty','public',now()
)
on conflict (name_extract) do nothing;
