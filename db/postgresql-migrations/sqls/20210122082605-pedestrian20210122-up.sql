/* Replace with your SQL commands */

CREATE OR REPLACE VIEW analytic.v_flow_quality AS 
with timeline AS (
                 SELECT "timestamp"."timestamp"
                   FROM generate_series(( SELECT date_trunc('hour'::text, to_timestamp((min(flow_measurements.start_timestamp) / 1000)::double precision)) AS date_trunc
                   FROM public.flow_measurements), ( SELECT to_timestamp((max(flow_measurements.start_timestamp) / 1000)::double precision) AS to_timestamp
                   FROM public.flow_measurements), '01:00:00'::interval) "timestamp"("timestamp")
                ), 
                category AS (
                 SELECT DISTINCT flow_measurements.cube_id, flow_measurements.sink_id
                   FROM public.flow_measurements
                ), 
                timeline_category AS (
                 SELECT t_1."timestamp",
                    c.cube_id,
                    c.sink_id
                   FROM timeline t_1
                     JOIN category c ON true
                ),
                 real_nrow AS (
                 SELECT date_trunc('hour'::text, to_timestamp((flow_measurements.start_timestamp / 1000)::double precision)) AS date_trunc,
                    flow_measurements.cube_id,
                    flow_measurements.sink_id,
                    count(*) AS n_row
                   FROM public.flow_measurements
                  GROUP BY (date_trunc('hour'::text, to_timestamp((flow_measurements.start_timestamp / 1000)::double precision))), flow_measurements.cube_id,  flow_measurements.sink_id    
                ),
                expected_nrow as (
                select cube_id, 
                        sink_id, 
                        count (distinct category)*12 as expected_nrow
                from public.flow_measurements
                group by cube_id, sink_id
                )
                SELECT t."timestamp",
                        t.cube_id,
                        t.sink_id,
                        r.cube_id as real_cube_id,
                        r.sink_id as real_sink_id,
                        concat(t.cube_id,' - ', t.sink_id) as cube_sink,
                        COALESCE(r.n_row, 0::bigint) AS n_row,
                        er.expected_nrow                
                   FROM timeline_category t
                   LEFT JOIN real_nrow r ON t."timestamp" = r.date_trunc AND t.cube_id = r.cube_id and t.sink_id = r.sink_id
                   left join expected_nrow er on t.cube_id = er.cube_id and t.sink_id = er.sink_id;
