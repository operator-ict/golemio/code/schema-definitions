CREATE TABLE public.containers_ksnko_notifications (
    id SERIAL PRIMARY KEY,
    knsko_id int8 NULL,
    sensor_id int8 NULL,
    mode varchar(50) NULL,
    success bool NULL,
    request json NULL,
    response json NULL,
    create_batch_id int8 NULL,
    created_at timestamptz NULL,
    created_by varchar(150) NULL,
    update_batch_id int8 NULL,
    updated_at timestamptz NULL,
    updated_by varchar(150) NULL
);
