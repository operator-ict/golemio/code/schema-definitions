-- Index: public.vehiclepositions_positions_origin_time

DROP INDEX public.vehiclepositions_positions_origin_time;

-- Index: public.vehiclepositions_positions_trips_id

DROP INDEX public.vehiclepositions_positions_trips_id;

-- Table: public.vehiclepositions_positions

DROP TABLE public.vehiclepositions_positions;

-- Table: public.vehiclepositions_stops

DROP TABLE public.vehiclepositions_stops;

-- Table: public.vehiclepositions_trips

DROP TABLE public.vehiclepositions_trips;
