--drop table if exists analytic.camea_bikecounters_in_contract;
drop view if exists analytic.v_camea_bikecounters_last_update;
drop view if exists analytic.v_camea_bikecounters_data_quality;
drop view if exists analytic.v_camea_bikecounters_data_disbalance;
drop view if exists analytic.v_camea_bikecounters_api_availability_basic;
drop view if exists analytic.v_camea_bikecounters_all_data_today;
drop view if exists analytic.v_camea_bikecounters_all_data_table_today;
drop view if exists analytic.v_camea_bikecounters_all_data_history;
drop view if exists analytic.v_camea_bikecounters_name_table;


-- last update camea
create or replace view analytic.v_camea_bikecounters_last_update
as select max(to_timestamp(measured_to/1000) AT TIME ZONE 'Europe/Prague')
from public.bicyclecounters_detections;

-- name table
CREATE OR REPLACE VIEW analytic.v_camea_bikecounters_name_table
AS SELECT loc.id AS location_id,
    loc.vendor_id AS loc_vendor_id,
    loc.name AS location_name,
    loc.route,
    loc.lat,
    loc.lng,
    dir.id AS direction_id,
    dir.vendor_id AS dir_vendor_id,
    dir.name AS direction_name,
    old.in_camea_contract
   FROM bicyclecounters_locations loc
     JOIN bicyclecounters_directions dir ON loc.id::text = dir.locations_id::text
     JOIN analytic.camea_bikecounters_in_contract old ON loc.vendor_id::text = old.location_id::text;

-- data quality - nová data
create or replace view analytic.v_camea_bikecounters_data_quality
as select to_timestamp(measured_from/1000)::date AS date,
       loc.vendor_id as location_id,
       loc."name" as location_name,
       dir.vendor_id as direction_id,
       dir."name" as direction_name,
       count(det.locations_id) lines,
       count(det.value) as detections,
       count(det.value)/count(det.locations_id)::decimal as ratio,
       old.in_camea_contract
       from public.bicyclecounters_detections det
       left join public.bicyclecounters_locations loc on det.locations_id = loc.id
       left join public.bicyclecounters_directions dir on det.directions_id = dir.id
       left join analytic.camea_bikecounters_in_contract old on loc.vendor_id = old.location_id
group by to_timestamp(measured_from/1000)::date,
       loc.vendor_id,
       loc."name",
       dir.vendor_id,
       dir."name",
       old.in_camea_contract;

-- disbalance
create or replace view analytic.v_camea_bikecounters_data_disbalance
as select
d1.datum,d1.locations_id,d1.location_name,
d1.direction_id dir1id,d1.direction_name dir1_name,
case when d1.dir1det is null then 0 else d1.dir1det end as dir1det,
d1.in_camea_contract,
case when d2.direction_id = d1.direction_id then 'není' else d2.direction_id end as dir2id,
case when d2.direction_id = d1.direction_id then 'není' else d2.direction_name end as dir2_name,
case when d2.direction_id = d1.direction_id then 0
     when dir2det is null then 0 else dir2det end as dir2det
from
        (select
            to_timestamp(measured_from/1000)::date AS datum,
            det.locations_id as locations_id,
            loc."name" as location_name,
            dir.vendor_id as direction_id,
            dir."name" as direction_name,
            old.in_camea_contract,
            sum(det.value) as dir1det
        from public.bicyclecounters_detections det
        left join public.bicyclecounters_locations loc on det.locations_id = loc.id
        left join public.bicyclecounters_directions dir on det.directions_id = dir.id
        left join analytic.camea_bikecounters_in_contract old on loc.vendor_id = old.location_id
        where directions_id in (select max(id) from public.bicyclecounters_directions group by locations_id)
        group by 1,2,3,4,5,6) d1
left join
        (select
            to_timestamp(measured_from/1000)::date AS datum,
            det2.locations_id as locations_id,
            loc."name" as location_name,
            dir.vendor_id as direction_id,
            dir."name" as direction_name,
            sum(det2.value) as dir2det
        from public.bicyclecounters_detections det2
        left join public.bicyclecounters_locations loc on det2.locations_id = loc.id
        left join public.bicyclecounters_directions dir on det2.directions_id = dir.id
        where directions_id in (select min(id) from public.bicyclecounters_directions group by locations_id)
        group by 1,2,3,4,5) d2
        on d1.datum = d2.datum and d1.locations_id = d2.locations_id;


-- view pro veřejný dashboard - aktuální den
create or replace view analytic.v_camea_bikecounters_all_data_today
as select t1.*,t2.temperature
from (select
        to_timestamp(measured_from/1000) AS datum,
        det.locations_id,
        loc."name" as location_name,
        old.in_camea_contract,
        loc.lat,
        loc.lng,
        REPLACE(loc.route, ' ', '' ) as route,
        det.directions_id,
        dir."name" as direction_name,
        case when det.directions_id in (select min(directions_id) from public.bicyclecounters_detections group by locations_id) then 'Směr 1' else 'Směr 2' end as direction_num,
        case when sum(det.value) is null then 0 else sum(det.value) end as detections
        from public.bicyclecounters_detections det
        left join public.bicyclecounters_locations loc on det.locations_id = loc.id
        left join public.bicyclecounters_directions dir on det.directions_id = dir.id
        left join analytic.camea_bikecounters_in_contract old on loc.vendor_id = old.location_id
        where to_timestamp(measured_from/1000)::date in (select max(to_timestamp(measured_from/1000)::date) from public.bicyclecounters_detections) -- zde vyměnit za current_date
        group by 1,2,3,4,5,6,7,8,9,10) t1
left join (select to_timestamp(measured_from/1000) AS datum,
        locations_id, avg(value) as temperature
        from public.bicyclecounters_temperatures
        where to_timestamp(measured_from/1000)::date in (select max(to_timestamp(measured_from/1000)::date) from public.bicyclecounters_temperatures)  -- zde vyměnit za current_date
        group by to_timestamp(measured_from/1000), locations_id) t2
        on t1.datum = t2.datum and t1.locations_id = t2.locations_id;

-- view pro veřejný dashboard - přehledová tabulka - aktuální den
create or replace view analytic.v_camea_bikecounters_all_data_table_today
as select t1.*,
        case when t2.direction_name = t1.direction_name then 'není' else t2.direction_name end as direction2_name,
        t2.dir2det,
        t3.temperature
from (select
            to_timestamp(measured_from/1000) AS datum,
            det.locations_id,
            loc."name" as location_name,
            REPLACE(loc.route, ' ', '' ) as route,
            dir."name" as direction_name,
            old.in_camea_contract,
            case when sum(det.value) is null then 0 else sum(det.value) end as dir1det
        from public.bicyclecounters_detections det
        left join public.bicyclecounters_locations loc on det.locations_id = loc.id
        left join public.bicyclecounters_directions dir on det.directions_id = dir.id
        left join analytic.camea_bikecounters_in_contract old on loc.vendor_id = old.location_id
        where directions_id in (select min(id) from public.bicyclecounters_directions group by locations_id) and
        to_timestamp(measured_from/1000)::date in (select max(to_timestamp(measured_from/1000)::date) from public.bicyclecounters_detections)
        group by 1,2,3,4,5,6) t1
left join
        (select
            to_timestamp(measured_from/1000) AS datum,
            det2.locations_id as locations_id,
            loc."name" as location_name,
            dir.vendor_id as direction_id,
            dir."name" as direction_name,
            case when sum(det2.value) is null then 0 else sum(det2.value) end as dir2det
        from public.bicyclecounters_detections det2
        left join public.bicyclecounters_locations loc on det2.locations_id = loc.id
        left join public.bicyclecounters_directions dir on det2.directions_id = dir.id
        where directions_id in (select max(id) from public.bicyclecounters_directions group by locations_id)  and
        to_timestamp(measured_from/1000)::date in (select max(to_timestamp(measured_from/1000)::date) from public.bicyclecounters_detections)
        group by 1,2,3,4,5) t2
        on t1.datum = t2.datum and t1.locations_id = t2.locations_id
left join (select to_timestamp(measured_from/1000) AS datum,
            locations_id,
            avg(value) as temperature from public.bicyclecounters_temperatures
            where to_timestamp(measured_from/1000)::date in (select max(to_timestamp(measured_from/1000)::date) from public.bicyclecounters_detections)
            group by 1,2) t3
        on t1.datum = t3.datum and t1.locations_id = t3.locations_id;

-- view pro veřejný dashboard - historie
create or replace view analytic.v_camea_bikecounters_all_data_history
as select
            to_timestamp(measured_from/1000)::date AS datum,
            det.locations_id,
            loc."name" as location_name,
            old.in_camea_contract,
            REPLACE(loc.route, ' ', '' ) as route,
            dir."name" as direction_name,
            case when sum(det.value) is null then 0 else sum(det.value) end as detections
        from public.bicyclecounters_detections det
        left join public.bicyclecounters_locations loc on det.locations_id = loc.id
        left join public.bicyclecounters_directions dir on det.directions_id = dir.id
        left join analytic.camea_bikecounters_in_contract old on loc.vendor_id = old.location_id
        group by 1,2,3,4,5,6;

