-- analytic.v_containers_containers source

CREATE OR REPLACE VIEW analytic.v_containers_containers
AS WITH unn AS (
         SELECT containers_containers.code,
            unnest(string_to_array(containers_containers.pick_days::text, ','::text))::integer AS day_num
           FROM containers_containers
        ), translate AS (
         SELECT unn.code,
            unn.day_num,
                CASE
                    WHEN unn.day_num = 1 THEN 'Po'::text
                    WHEN unn.day_num = 2 THEN 'Út'::text
                    WHEN unn.day_num = 3 THEN 'St'::text
                    WHEN unn.day_num = 4 THEN 'Čt'::text
                    WHEN unn.day_num = 5 THEN 'Pá'::text
                    WHEN unn.day_num = 6 THEN 'So'::text
                    WHEN unn.day_num = 7 THEN 'Ne'::text
                    ELSE NULL::text
                END AS day_name
           FROM unn
        ), aa AS (
         SELECT translate.code,
            string_agg(translate.day_name, ', '::text ORDER BY translate.day_num) AS pick_days
           FROM translate
          GROUP BY translate.code
        )
 SELECT cc.code AS container_id,
    cc.total_volume,
    cc.network,
    cc.container_type,
    cc.bin_type,
        CASE
            WHEN cc.trash_type = 1 THEN 'glass coloured'::text
            WHEN cc.trash_type = 2 THEN 'electronics'::text
            WHEN cc.trash_type = 3 THEN 'metal'::text
            WHEN cc.trash_type = 4 THEN 'bevarage cartons'::text
            WHEN cc.trash_type = 5 THEN 'paper'::text
            WHEN cc.trash_type = 6 THEN 'plastic'::text
            WHEN cc.trash_type = 7 THEN 'glass clear'::text
            WHEN cc.trash_type = 8 THEN 'textile'::text
            ELSE NULL::text
        END AS trash_type,
        CASE
            WHEN cc.trash_type = 1 THEN 'Sklo barevné'::text
            WHEN cc.trash_type = 2 THEN 'Elektronika'::text
            WHEN cc.trash_type = 3 THEN 'Kovy'::text
            WHEN cc.trash_type = 4 THEN 'Nápojové kartony'::text
            WHEN cc.trash_type = 5 THEN 'Papír'::text
            WHEN cc.trash_type = 6 THEN 'Plast'::text
            WHEN cc.trash_type = 7 THEN 'Sklo čiré'::text
            WHEN cc.trash_type = 8 THEN 'Textil'::text
            ELSE NULL::text
        END AS typ_odpadu,
        CASE
            WHEN cc.trash_type = 1 THEN 411
            WHEN cc.trash_type = 3 THEN 120
            WHEN cc.trash_type = 4 THEN 55
            WHEN cc.trash_type = 5 THEN 76
            WHEN cc.trash_type = 6 THEN 39
            WHEN cc.trash_type = 7 THEN 411
            ELSE NULL::integer
        END AS koeficient,
    cc.cleaning_frequency_frequency,
    cc.cleaning_frequency_interval,
    cs.latitude,
    cs.longitude,
        CASE
            WHEN cs.address::text = ' Francouzská  240/76'::text THEN "substring"(cs.address::text, 2)::character varying
            ELSE cs.address
        END AS address,
    cd.district_cz,
    concat(1, "right"(split_part(cs.district::text, '-'::text, 2), 1), '000') AS psc,
    cc.sensor_id,
    cc.updated_at,
    aa.pick_days,
    row_number() OVER (PARTITION BY cs.address, cc.trash_type) AS row_number
   FROM containers_containers cc
     LEFT JOIN containers_stations cs ON cc.station_code::text = cs.code::text
     LEFT JOIN analytic.containers_districts cd ON cs.district::text = cd.district::text
     LEFT JOIN aa ON cc.code::text = aa.code::text
  WHERE (cc.code::text IN ( SELECT DISTINCT containers_measurement.container_code
           FROM containers_measurement
          WHERE containers_measurement.percent_calculated IS NOT NULL)) AND cs.address IS NOT NULL;