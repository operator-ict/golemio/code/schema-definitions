-- tabulka na primární data wifi měření, jsou pouze jako csv na sharepointu https://operator.sharepoint.com/:f:/s/Datova_Platforma/EroMekiKaJdOtMPeF_Q8wPEBlZm5tLKHCgpK13H6hlc0Zg?e=4RbCkT
-- file se jmenuje wifi_main.csv
-- 19.3. by měla přijít další várka záznamů, připojím to do mainu (není to jen ctrl+c/v) a dám vědět
create table analytic.pedestrians_wifi(
    measured_from timestamp NULL,
    start_region varchar(50) NULL,
    end_region varchar(50) NULL,
    value int NULL,
    direction_id varchar(50) NULL,
    location_id varchar(50) null
    );
-- tabulka lokací. obsahuje lokace různých druhů a hodně custom dat, takže ji tvoříme ručně. sporadicky probíhají aktualizace
-- tabulka se nachází na https://operator.sharepoint.com/:f:/s/Datova_Platforma/EsE5uSdyLzdImWgyrMY8nFcBzwtH7GOr6TXtzNBn3FLIJw?e=fOov5s a jmenuje se locations_list.csv
CREATE TABLE analytic.pedestrians_locations_list (
	id serial4 NOT NULL,
	location_name_plain varchar(250) NULL,
	location_name varchar(250) NULL,
	lat numeric NULL,
	lng numeric NULL,
	address varchar(250) NULL,
	city_district varchar(250) NULL,
	tech varchar(250) NULL,
	map_picture varchar(2500) NULL,
	gate_picture varchar(2500) NULL,
	measurement_start timestamptz NULL,
	measurement_end timestamptz NULL,
	id_equiptment varchar(250) NULL,
	cube_id varchar(250) NULL,
	lon varbit NULL,
	map_image varchar(55) NULL,
	place_image varchar(59) NULL,
	"table" varchar(19) NULL,
	CONSTRAINT equipment_list_pkey PRIMARY KEY (id)
);
-- tabulka směrů k jednotlivým lokacím, platí to samé co u lokations_list.
-- nachází se na  https://operator.sharepoint.com/:f:/s/Datova_Platforma/EsE5uSdyLzdImWgyrMY8nFcBzwtH7GOr6TXtzNBn3FLIJw?e=fOov5s a jmenuje se directions_list.csv
CREATE TABLE analytic.pedestrians_locations_gates (
    direction_id varchar(250) NOT NULL,
    cube_id varchar(250) NOT NULL,
    location_id int not null,
    "name" varchar(250) NOT NULL,
    direction_type varchar(250) NOT NULL,
    CONSTRAINT equipment_gates_pkey PRIMARY KEY (direction_id, location_id)
);
---------------------------v_pedestrians_detections_daily
-- denní agregace za všechny lokace + podíl zaslaných hodnot vůči očekávanému počtu, bere primární data flow(public), counters(public) a wifi(tabulka analytic) a číselníky (tabulky analytic)
create or replace view analytic.v_pedestrians_detections_daily as
with wifi as (
                select location_id as locations_id,
                        direction_id as directions_id,
                        measured_from::date as measured_from,
                        sum(value) as value,
                        count(value) as count_n,
                        count(value)::decimal / 288 as quality_ratio
                from analytic.pedestrians_wifi
                group by location_id,
                        direction_id,
                        measured_from::date
                ),
    pyro as (
            select locations_id,
                    lg.first_dir as directions_id,
                    to_timestamp(measured_from/1000)::date as measured_from,
                    sum(value) as value,
                    count(value) / count(distinct cd.directions_id) as count_n,
                    count(value)::decimal / 96 / count(distinct cd.directions_id) as quality_ratio
            from public.counters_detections cd
            left join (select direction_id,
                                            direction_type,
                                            min(direction_id) over (partition by cube_id, direction_type) as first_dir
                                    from analytic.pedestrians_locations_gates) lg
                                    on cd.directions_id = lg.direction_id
            where category = 'pedestrian'  and directions_id in (select distinct direction_id
                                                                from analytic.pedestrians_locations_gates)
            group by locations_id,
                     lg.first_dir,
                    to_timestamp(measured_from/1000)::date
            ),
    flow as (
            select cube_id::varchar(50) as location_id,
                    sink_id::varchar(50) as direction_id,
                    to_timestamp(start_timestamp/1000)::date as measured_from,
                    sum(value) as value,
                    count(value) as count_n,
                    count(value)::decimal / 288 as quality_ratio
            from public.flow_measurements
            where (cube_id::varchar(50), sink_id::varchar(50)) in (select distinct cube_id as location_id, direction_id
                                                                    from analytic.pedestrians_locations_gates)
                    and category = 'pedestrian'
            group by cube_id::varchar(50),
                    sink_id::varchar(50),
                    to_timestamp(start_timestamp/1000)::date
            order by measured_from, cube_id::varchar(50) desc,sink_id::varchar(50)
            ),
    measurements as (
            select *
            from pyro
            union all
            select *
            from flow
            union all
            select *
            from wifi
            ),
    calendar as (
            select generate_series(min(measured_from), max(measured_from), '1 day') as calendar
            from measurements
    ),
    classes as (
                select *
                from calendar
                join (select distinct locations_id::varchar(50) as location_id,
                             directions_id::varchar(50) as direction_id
                      from measurements) m on true
                )
        select c.calendar,
                c.location_id,
                c.direction_id,
                concat(c.location_id, '-', c.direction_id) as compound_id,
                coalesce(m.value,0) as value,
                coalesce(count_n, 0) as count_n,
                coalesce(quality_ratio, 0) as quality_ratio,
                case
                    when calendar not between ll.measurement_start and ll.measurement_end then 4
                    when coalesce(quality_ratio, 0) = 0 then 3
                    when coalesce(quality_ratio, 0) < 0.9 then 2
                    WHEN coalesce(quality_ratio, 0) > 0.9 and coalesce(quality_ratio, 0) < 1 then 1
                    when coalesce(quality_ratio, 0) >= 1 then 0
                else null END AS quality_code,
                case
                    when calendar not between ll.measurement_start and ll.measurement_end then 'Nenainstalovaná technologie'
                    when coalesce(quality_ratio, 0) = 0 then 'Bez dat (0%)'
                    when coalesce(quality_ratio, 0) < 0.9 then 'Částečná data (<90%)'
                    WHEN coalesce(quality_ratio, 0) > 0.9 and coalesce(quality_ratio, 0) < 1 then 'Nekompletní data (>90%)'
                    when coalesce(quality_ratio, 0) >= 1 then 'Kompletní data (100%)'
                else null END AS quality_status
        from classes c
        left join measurements m on c.calendar = m.measured_from and c.location_id = m.locations_id and c.direction_id = m.directions_id
        left join analytic.pedestrians_locations_list ll on c.location_id = ll.cube_id;
--15min agregace ze všech lokalit a technologií, bere primární data flow(public), counters(public) a wifi(tabulka analytic) a číselníky (tabulky analytic)
---------------------------v_pedestrians_detections_15min
create or replace view analytic.v_pedestrians_detections_15min as
with wifi as (
            select location_id as locations_id,
                    direction_id as directions_id,
                    date_trunc('hour', measured_from)
                        + date_part('minute', measured_from)::int / 15 * interval '15 min' as measured_from,
                    sum(value) as value,
                    count(value) as count_n
            from analytic.pedestrians_wifi
            group by location_id,
                    direction_id,
                    date_trunc('hour', measured_from)
                        + date_part('minute', measured_from)::int / 15 * interval '15 min'
            ),
    pyro as (
            select locations_id,
                    lg.first_dir as directions_id,
                    to_timestamp(measured_from/1000) as measured_from,
                    sum(value) as value,
                    count(value) as count_n
            from public.counters_detections cd
            left join (select direction_id,
                                direction_type,
                                min(direction_id) over (partition by cube_id, direction_type) as first_dir
                        from analytic.pedestrians_locations_gates) lg
                        on cd.directions_id = lg.direction_id
            where category = 'pedestrian'  and directions_id in (select distinct direction_id
                                                                from analytic.pedestrians_locations_gates)
            group by locations_id,
                     lg.first_dir,
                    to_timestamp(measured_from/1000)
            ),
    flow as (
            select cube_id::varchar(50) as location_id,
                    sink_id::varchar(50) as direction_id,
                    date_trunc('hour', to_timestamp(start_timestamp/1000))
                            + date_part('minute', to_timestamp(start_timestamp/1000))::int / 15 * interval '15 min' as measured_from,
                    sum(value) as value,
                    count(value) as count_n
            from public.flow_measurements
            where (cube_id::varchar(50), sink_id::varchar(50)) in (select distinct cube_id as location_id, direction_id
                                                                    from analytic.pedestrians_locations_gates)
                    and category = 'pedestrian'
            group by cube_id::varchar(50),
                    sink_id::varchar(50),
                    date_trunc('hour', to_timestamp(start_timestamp/1000))
                            + date_part('minute', to_timestamp(start_timestamp/1000))::int / 15 * interval '15 min'
            order by measured_from, cube_id::varchar(50),sink_id::varchar(50)
            ),
    measurements as (
            select *
            from wifi
            union all
            select *
            from pyro
            union all
            select *
            from flow
            ),
    calendar as (
            select generate_series(min(measured_from), max(measured_from), '15min') as calendar
            from measurements
            ),
    classes as (
                select *
                from calendar
                join (select distinct locations_id::varchar(50) as location_id,
                             directions_id::varchar(50) as direction_id
                      from measurements) m on true
                )
                select calendar,
                        date_trunc('hour', calendar) as calendar_hour,
                        calendar::date as calendar_date,
                        ll.measurement_start,
                        coalesce(ll.measurement_end, now()::date) as measurement_end,
                        c.location_id,
                        c.direction_id,
                        concat(c.location_id, '-', c.direction_id) as compound_id,
                        coalesce(m.value,0) as value,
                        coalesce(count_n, 0) as count_n
                from classes c
                left join measurements m on c.calendar = m.measured_from and c.location_id = m.locations_id and c.direction_id = m.directions_id
                left join analytic.pedestrians_locations_list ll on c.location_id = ll.cube_id
                where calendar >= ll.measurement_start and calendar <= coalesce(ll.measurement_end, now()::date);


--servisní dashboard - monitoruje počty zaslaných řádků v rámci flow dat
CREATE OR REPLACE VIEW analytic.v_pedestrians_flow_quality as
with timeline AS (
                 SELECT "timestamp"."timestamp"
                   FROM generate_series(( SELECT date_trunc('hour'::text, to_timestamp((min(flow_measurements.start_timestamp) / 1000)::double precision)) AS date_trunc
                   FROM public.flow_measurements), ( SELECT to_timestamp((max(flow_measurements.start_timestamp) / 1000)::double precision) AS to_timestamp
                   FROM public.flow_measurements), '01:00:00'::interval) "timestamp"("timestamp")
                ),
    category AS (
                 SELECT DISTINCT flow_measurements.cube_id, flow_measurements.sink_id
                   FROM public.flow_measurements
                ),
    timeline_category AS (
                 SELECT t_1."timestamp",
                    c.cube_id,
                    c.sink_id
                   FROM timeline t_1
                     JOIN category c ON true
                ),
    real_nrow AS (
                 SELECT date_trunc('hour'::text, to_timestamp((flow_measurements.start_timestamp / 1000)::double precision)) AS date_trunc,
                    flow_measurements.cube_id,
                    flow_measurements.sink_id,
                    count(*) AS n_row
                   FROM public.flow_measurements
                  GROUP BY (date_trunc('hour'::text, to_timestamp((flow_measurements.start_timestamp / 1000)::double precision))), flow_measurements.cube_id,  flow_measurements.sink_id
                ),
    expected_nrow as (
                select cube_id,
                        sink_id,
                        (count (distinct category))*12 as expected_nrow
                from public.flow_measurements
                group by cube_id, sink_id
                )
                SELECT t."timestamp",
                        t.cube_id,
                        t.sink_id,
                        concat(t.cube_id,' - ', t.sink_id) as cube_sink,
                        COALESCE(r.n_row, 0) AS n_row,
                        er.expected_nrow
                   FROM timeline_category t
                   LEFT JOIN real_nrow r ON t."timestamp" = r.date_trunc AND t.cube_id = r.cube_id and t.sink_id = r.sink_id
                   left join expected_nrow er on t.cube_id = er.cube_id and t.sink_id = er.sink_id;
