CREATE INDEX IF NOT EXISTS vehiclepositions_runs_messages_fkey ON public.vehiclepositions_runs_messages USING btree (runs_id);

ALTER TABLE public.vehiclepositions_positions ADD COLUMN tcp_event VARCHAR(50);
