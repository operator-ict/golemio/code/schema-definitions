drop VIEW public.v_vehiclepositions_last_position_v1;
drop VIEW public.v_vehiclepositions_last_position;

ALTER TABLE public.vehiclepositions_stops DROP asw_stop_id;
ALTER TABLE public.vehiclepositions_trips DROP start_asw_stop_id;
ALTER TABLE public.vehiclepositions_positions DROP asw_last_stop_id;

DROP VIEW IF EXISTS public.v_vehiclepositions_trips_v1;

ALTER TABLE public.vehiclepositions_trips ALTER COLUMN cis_line_id TYPE bigint USING CASE WHEN cis_line_id~E'^\\d+$' THEN cis_line_id::bigint ELSE NULL END;

CREATE OR REPLACE VIEW "public"."v_vehiclepositions_trips_v1"
  AS
    SELECT "v_vehiclepositions_trips_v1"."cis_line_id" AS "cis_id",
      "v_vehiclepositions_trips_v1"."cis_trip_number" AS "cis_number",
      "v_vehiclepositions_trips_v1"."sequence_id" AS "cis_order",
      "v_vehiclepositions_trips_v1"."cis_line_short_name" AS "cis_short_name",
      "v_vehiclepositions_trips_v1"."created_at",
      "v_vehiclepositions_trips_v1"."gtfs_route_id",
      "v_vehiclepositions_trips_v1"."gtfs_route_short_name",
      "v_vehiclepositions_trips_v1"."gtfs_trip_id",
      "v_vehiclepositions_trips_v1"."id",
      "v_vehiclepositions_trips_v1"."updated_at",
      "v_vehiclepositions_trips_v1"."start_cis_stop_id",
      "v_vehiclepositions_trips_v1"."start_cis_stop_platform_code",
      "v_vehiclepositions_trips_v1"."start_time",
      "v_vehiclepositions_trips_v1"."start_timestamp",
      "v_vehiclepositions_trips_v1"."vehicle_type_id" AS "vehicle_type",
      "v_vehiclepositions_trips_v1"."wheelchair_accessible",
      "v_vehiclepositions_trips_v1"."create_batch_id",
      "v_vehiclepositions_trips_v1"."created_by",
      "v_vehiclepositions_trips_v1"."update_batch_id",
      "v_vehiclepositions_trips_v1"."updated_by",
      "v_vehiclepositions_trips_v1"."agency_name_scheduled" AS "cis_agency_name",
      "v_vehiclepositions_trips_v1"."origin_route_name" AS "cis_parent_route_name",
      "v_vehiclepositions_trips_v1"."agency_name_real" AS "cis_real_agency_name",
      "v_vehiclepositions_trips_v1"."vehicle_registration_number" AS "cis_vehicle_registration_number"
    FROM "vehiclepositions_trips" AS "v_vehiclepositions_trips_v1";


-- Function: public.import_vehiclepositions_stops(bigint, json, character varying)

DROP FUNCTION public.import_vehiclepositions_stops(bigint, json, character varying);

-- Function: public.import_vehiclepositions_stops(bigint, json, character varying)

CREATE OR REPLACE FUNCTION public.import_vehiclepositions_stops(
    IN p_batch_id bigint,
    IN p_data json,
    IN p_worker_name character varying,
    OUT x_inserted json,
    OUT x_updated json)
  RETURNS record AS
$BODY$


declare
	p_cas timestamp;
begin
/*
fce provádí merge p_data do tabulky vehicleposition_trips
výstup do dvou polí (záznamy insertované a updatované)
*/
	p_cas = now();

--create temp table mytemp(c varchar);

--with rows as (
update public.vehiclepositions_stops tar
 set
			arrival_time = sor.arrival_time,
			arrival_timestamp = sor.arrival_timestamp,
			cis_stop_id = sor.cis_stop_id,
			cis_stop_platform_code = sor.cis_stop_platform_code,
			--cis_stop_sequence = sor.cis_stop_sequence,  --PK
			delay_arrival = sor.delay_arrival,
			delay_departure = sor.delay_departure,
			delay_type = sor.delay_type,
			departure_time = sor.departure_time,
			departure_timestamp = sor.departure_timestamp,
			--trips_id = sor.trips_id,  --PK

			--
			update_batch_id = p_batch_id,
			updated_at = now(),	--p_cas,
			updated_by = p_worker_name
from
	(
		select
			cast(json_array_elements(data)->>'arrival_time' as time without time zone) arrival_time
			,cast(json_array_elements(data)->>'arrival_timestamp' as bigint) arrival_timestamp
			,cast(json_array_elements(data)->>'cis_stop_id' as integer) cis_stop_id
			,cast(json_array_elements(data)->>'cis_stop_platform_code' as varchar) cis_stop_platform_code
			,cast(json_array_elements(data)->>'cis_stop_sequence' as integer) cis_stop_sequence
			,cast(json_array_elements(data)->>'delay_arrival' as integer) delay_arrival
			,cast(json_array_elements(data)->>'delay_departure' as integer) delay_departure
			,cast(json_array_elements(data)->>'delay_type' as integer) delay_type
			,cast(json_array_elements(data)->>'departure_time' as time without time zone) departure_time
			,cast(json_array_elements(data)->>'departure_timestamp' as bigint) departure_timestamp
			,cast(json_array_elements(data)->>'trips_id' as varchar) trips_id
		from (select p_data as data) a
	) sor
	where
		tar.cis_stop_sequence = sor.cis_stop_sequence
		and tar.trips_id = sor.trips_id

--	RETURNING  tar.cis_stop_id,tar.cis_stop_platform_code
--	)
--INSERT INTO mytemp (c)
--SELECT id
--FROM rows
;

--select array_to_json(array_agg(row)) into x_updated
--from (select c from mytemp) row;

--truncate table mytemp;

--with rows as (
	insert into public.vehiclepositions_stops
	(
		arrival_time,arrival_timestamp,cis_stop_id,cis_stop_platform_code,
		cis_stop_sequence,delay_arrival,delay_departure,delay_type,departure_time,
		departure_timestamp,trips_id,
		--
		create_batch_id, created_at, created_by
		--, update_batch_id, updated_time, updated_by
	)
	select
		c.arrival_time,c.arrival_timestamp,c.cis_stop_id,c.cis_stop_platform_code,
		c.cis_stop_sequence,c.delay_arrival,c.delay_departure,c.delay_type,c.departure_time,
		c.departure_timestamp,c.trips_id,
		--
		c.create_batch_id, c.created_at, c.created_by
	from (
	select b.*
		,row_number() over(partition by b.cis_stop_sequence,b.trips_id order by b.delay_type desc) rn
	from(
		select

			cast(json_array_elements(data)->>'arrival_time' as time without time zone) arrival_time
			,cast(json_array_elements(data)->>'arrival_timestamp' as bigint) arrival_timestamp
			,cast(json_array_elements(data)->>'cis_stop_id' as integer) cis_stop_id
			,cast(json_array_elements(data)->>'cis_stop_platform_code' as varchar) cis_stop_platform_code
			,cast(json_array_elements(data)->>'cis_stop_sequence' as integer) cis_stop_sequence
			,cast(json_array_elements(data)->>'delay_arrival' as integer) delay_arrival
			,cast(json_array_elements(data)->>'delay_departure' as integer) delay_departure
			,cast(json_array_elements(data)->>'delay_type' as integer) delay_type
			,cast(json_array_elements(data)->>'departure_time' as time without time zone) departure_time
			,cast(json_array_elements(data)->>'departure_timestamp' as bigint) departure_timestamp
			,cast(json_array_elements(data)->>'trips_id' as varchar) trips_id
			,p_batch_id create_batch_id,now() created_at,p_worker_name created_by
		from (select p_data as data) a
	) b
	left join public.vehiclepositions_stops bb
		on
		b.cis_stop_sequence = bb.cis_stop_sequence
		and b.trips_id =bb.trips_id
	where bb.cis_stop_id is null
	)c where rn = 1

	--RETURNING  public.vehiclepositions_trips.id
--	)
--INSERT INTO mytemp (c)
--SELECT id
--FROM rows
;

--select array_to_json(array_agg(row)) into x_inserted
--from (select c from mytemp) row;

--drop table mytemp;

end;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;




CREATE OR REPLACE VIEW public.v_vehiclepositions_last_position
as
SELECT DISTINCT ON (vehiclepositions_positions.trips_id) vehiclepositions_positions.created_at,
    vehiclepositions_positions.updated_at,
    vehiclepositions_positions.delay,
    vehiclepositions_positions.delay_stop_arrival,
    vehiclepositions_positions.delay_stop_departure,
    vehiclepositions_positions.next_stop_id,
    vehiclepositions_positions.shape_dist_traveled,
    vehiclepositions_positions.is_canceled,
    vehiclepositions_positions.lat,
    vehiclepositions_positions.lng,
    vehiclepositions_positions.origin_time,
    vehiclepositions_positions.origin_timestamp,
    vehiclepositions_positions.tracking,
    vehiclepositions_positions.trips_id,
    vehiclepositions_positions.create_batch_id,
    vehiclepositions_positions.created_by,
    vehiclepositions_positions.update_batch_id,
    vehiclepositions_positions.updated_by,
    vehiclepositions_positions.id,
    vehiclepositions_positions.bearing,
    vehiclepositions_positions.cis_last_stop_id,
    vehiclepositions_positions.cis_last_stop_sequence,
    vehiclepositions_positions.last_stop_id,
    vehiclepositions_positions.last_stop_sequence,
    vehiclepositions_positions.next_stop_sequence,
    vehiclepositions_positions.speed,
    vehiclepositions_positions.last_stop_arrival_time,
    vehiclepositions_positions.last_stop_departure_time,
    vehiclepositions_positions.next_stop_arrival_time,
    vehiclepositions_positions.next_stop_departure_time,
    vehiclepositions_trips.gtfs_route_id,
    vehiclepositions_trips.gtfs_route_short_name,
    vehiclepositions_trips.gtfs_trip_id,
    vehiclepositions_trips.gtfs_trip_headsign,
    vehiclepositions_trips.vehicle_type_id,
    vehiclepositions_trips.wheelchair_accessible
   FROM vehiclepositions_positions vehiclepositions_positions
     JOIN vehiclepositions_trips vehiclepositions_trips ON vehiclepositions_trips.id::text = vehiclepositions_positions.trips_id::text AND vehiclepositions_trips.gtfs_trip_id IS NOT NULL
  WHERE vehiclepositions_positions.updated_at > (now() - '00:05:00'::interval)
ORDER BY vehiclepositions_positions.trips_id, vehiclepositions_positions.updated_at DESC;

-- public.v_vehiclepositions_last_position_v1 source

CREATE OR REPLACE VIEW public.v_vehiclepositions_last_position_v1
AS SELECT v_vehiclepositions_last_position_v1.created_at,
    v_vehiclepositions_last_position_v1.delay,
    v_vehiclepositions_last_position_v1.delay_stop_arrival,
    v_vehiclepositions_last_position_v1.delay_stop_departure,
    v_vehiclepositions_last_position_v1.next_stop_id AS gtfs_next_stop_id,
    v_vehiclepositions_last_position_v1.shape_dist_traveled AS gtfs_shape_dist_traveled,
    v_vehiclepositions_last_position_v1.is_canceled,
    v_vehiclepositions_last_position_v1.lat,
    v_vehiclepositions_last_position_v1.lng,
    v_vehiclepositions_last_position_v1.origin_time,
    v_vehiclepositions_last_position_v1.origin_timestamp,
    v_vehiclepositions_last_position_v1.tracking,
    v_vehiclepositions_last_position_v1.trips_id,
    v_vehiclepositions_last_position_v1.create_batch_id,
    v_vehiclepositions_last_position_v1.created_by,
    v_vehiclepositions_last_position_v1.update_batch_id,
    v_vehiclepositions_last_position_v1.updated_at,
    v_vehiclepositions_last_position_v1.updated_by,
    v_vehiclepositions_last_position_v1.id,
    v_vehiclepositions_last_position_v1.bearing,
    v_vehiclepositions_last_position_v1.cis_last_stop_id,
    v_vehiclepositions_last_position_v1.cis_last_stop_sequence,
    v_vehiclepositions_last_position_v1.last_stop_id AS gtfs_last_stop_id,
    v_vehiclepositions_last_position_v1.last_stop_sequence AS gtfs_last_stop_sequence,
    v_vehiclepositions_last_position_v1.next_stop_sequence AS gtfs_next_stop_sequence,
    v_vehiclepositions_last_position_v1.speed
FROM v_vehiclepositions_last_position v_vehiclepositions_last_position_v1;