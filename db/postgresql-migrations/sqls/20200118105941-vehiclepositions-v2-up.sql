-- UP

DROP VIEW IF EXISTS v_vehiclepositions_last_position;

ALTER TABLE public.vehiclepositions_trips RENAME COLUMN cis_real_agency_name TO agency_name_real;
ALTER TABLE public.vehiclepositions_trips RENAME COLUMN cis_agency_name TO agency_name_scheduled;
ALTER TABLE public.vehiclepositions_trips RENAME COLUMN cis_id TO cis_line_id;
ALTER TABLE public.vehiclepositions_trips RENAME COLUMN cis_number TO cis_line_number;
ALTER TABLE public.vehiclepositions_trips RENAME COLUMN cis_order TO sequence_id;
ALTER TABLE public.vehiclepositions_trips RENAME COLUMN cis_parent_route_name TO origin_route_name;
ALTER TABLE public.vehiclepositions_trips RENAME COLUMN cis_short_name TO cis_line_short_name;
ALTER TABLE public.vehiclepositions_trips RENAME COLUMN cis_vehicle_registration_number TO vehicle_registration_number;
ALTER TABLE public.vehiclepositions_trips RENAME COLUMN vehicle_type TO vehicle_type_id;

ALTER TABLE public.vehiclepositions_trips ADD COLUMN gtfs_trip_headsign character varying(255);

ALTER TABLE public.vehiclepositions_positions RENAME COLUMN gtfs_last_stop_id TO last_stop_id;
ALTER TABLE public.vehiclepositions_positions RENAME COLUMN gtfs_last_stop_sequence TO last_stop_sequence;
ALTER TABLE public.vehiclepositions_positions RENAME COLUMN gtfs_next_stop_id TO next_stop_id;
ALTER TABLE public.vehiclepositions_positions RENAME COLUMN gtfs_next_stop_sequence TO next_stop_sequence;

ALTER TABLE public.vehiclepositions_positions ADD COLUMN last_stop_arrival_time bigint;
ALTER TABLE public.vehiclepositions_positions ADD COLUMN last_stop_departure_time bigint;
ALTER TABLE public.vehiclepositions_positions ADD COLUMN next_stop_arrival_time bigint;
ALTER TABLE public.vehiclepositions_positions ADD COLUMN next_stop_departure_time bigint;

ALTER TABLE public.vehiclepositions_positions RENAME COLUMN gtfs_shape_dist_traveled TO shape_dist_traveled;

CREATE TABLE public.vehiclepositions_vehicle_types (
    "id" integer NOT NULL,
    "abbreviation" text NOT NULL,
    "description_cs" text NOT NULL,
    "description_en" text NOT NULL,
    
    CONSTRAINT "vehiclepositions_vehicle_types_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

INSERT INTO "vehiclepositions_vehicle_types" ("id", "abbreviation", "description_cs", "description_en") VALUES
    (1, 'Metro', 'metro', 'metro'),
    (2, 'Tram', 'tramvaj', 'tram'),
    (3, 'Bus', 'autobus', 'bus'),
    (4, 'BusReg', 'regionalní autobus', 'regional bus'),
    (5, 'BusNoc', 'noční autobus', 'night bus'),
    (6, 'TramNoc', 'noční tramvaj', 'night tram'),
    (7, 'Nahradni', 'náhradní doprava', 'replacement'),
    (8, 'Lanovka', 'lanová dráha', 'cableway'),
    (9, 'Skolni', 'školní spoj', 'school trip'),
    (10, 'Invalid', 'spoj pro lidi s hendikepem', 'trip for people with disabilities'),
    (11, 'Smluvni', 'smluvní spoj', 'contracted trip'),
    (12, 'Lod', 'loď', 'boat'),
    (13, 'Vlak', 'vlak', 'train'),
    (14, 'VlakNAD', 'náhradní autobus za vlak', 'bus replacement for a train'),
    (15, 'NahrTram', 'náhradní tramvaj', 'replacement tram'),
    (16, 'BusRegNoc', 'noční regionální autobus', 'night regional bus'),
    (17, 'Ostatni', 'ostatní', 'other');


CREATE OR REPLACE VIEW v_vehiclepositions_last_position
  AS
    SELECT
      DISTINCT ON ("vehiclepositions_positions"."trips_id") "vehiclepositions_positions".*,
      "vehiclepositions_trips"."gtfs_route_id", "vehiclepositions_trips"."gtfs_route_short_name", 
      "vehiclepositions_trips"."gtfs_trip_id", "vehiclepositions_trips"."gtfs_trip_headsign",
      "vehiclepositions_trips"."vehicle_type_id", "vehiclepositions_trips"."wheelchair_accessible"
    FROM "vehiclepositions_positions" AS "vehiclepositions_positions"
      JOIN "vehiclepositions_trips" AS "vehiclepositions_trips"
        ON ("vehiclepositions_trips"."id" = "vehiclepositions_positions"."trips_id" AND
            "vehiclepositions_trips"."gtfs_trip_id" IS NOT NULL)
    WHERE "vehiclepositions_positions"."created_at" > (NOW() - INTERVAL '5 min') AND "vehiclepositions_positions"."delay" IS NOT NULL
    ORDER BY "vehiclepositions_positions"."trips_id", "vehiclepositions_positions"."created_at" DESC;

CREATE OR REPLACE VIEW v_vehiclepositions_last_position_v1
  AS
    SELECT "v_vehiclepositions_last_position_v1"."created_at",
      "v_vehiclepositions_last_position_v1"."delay",
      "v_vehiclepositions_last_position_v1"."delay_stop_arrival",
      "v_vehiclepositions_last_position_v1"."delay_stop_departure",
      "v_vehiclepositions_last_position_v1"."next_stop_id" AS "gtfs_next_stop_id",
      "v_vehiclepositions_last_position_v1"."shape_dist_traveled" AS "gtfs_shape_dist_traveled",
      "v_vehiclepositions_last_position_v1"."is_canceled",
      "v_vehiclepositions_last_position_v1"."lat",
      "v_vehiclepositions_last_position_v1"."lng",
      "v_vehiclepositions_last_position_v1"."origin_time",
      "v_vehiclepositions_last_position_v1"."origin_timestamp",
      "v_vehiclepositions_last_position_v1"."tracking",
      "v_vehiclepositions_last_position_v1"."trips_id",
      "v_vehiclepositions_last_position_v1"."create_batch_id",
      "v_vehiclepositions_last_position_v1"."created_by",
      "v_vehiclepositions_last_position_v1"."update_batch_id",
      "v_vehiclepositions_last_position_v1"."updated_at",
      "v_vehiclepositions_last_position_v1"."updated_by",
      "v_vehiclepositions_last_position_v1"."id",
      "v_vehiclepositions_last_position_v1"."bearing",
      "v_vehiclepositions_last_position_v1"."cis_last_stop_id",
      "v_vehiclepositions_last_position_v1"."cis_last_stop_sequence",
      "v_vehiclepositions_last_position_v1"."last_stop_id" AS "gtfs_last_stop_id",
      "v_vehiclepositions_last_position_v1"."last_stop_sequence" AS "gtfs_last_stop_sequence",
      "v_vehiclepositions_last_position_v1"."next_stop_sequence" AS "gtfs_next_stop_sequence",
      "v_vehiclepositions_last_position_v1"."speed"
    FROM "v_vehiclepositions_last_position" AS "v_vehiclepositions_last_position_v1";

CREATE OR REPLACE VIEW v_vehiclepositions_trips_v1
  AS
    SELECT "v_vehiclepositions_trips_v1"."cis_line_id" AS "cis_id",
      "v_vehiclepositions_trips_v1"."cis_line_number" AS "cis_number",
      "v_vehiclepositions_trips_v1"."sequence_id" AS "cis_order",
      "v_vehiclepositions_trips_v1"."cis_line_short_name" AS "cis_short_name",
      "v_vehiclepositions_trips_v1"."created_at",
      "v_vehiclepositions_trips_v1"."gtfs_route_id",
      "v_vehiclepositions_trips_v1"."gtfs_route_short_name",
      "v_vehiclepositions_trips_v1"."gtfs_trip_id",
      "v_vehiclepositions_trips_v1"."id",
      "v_vehiclepositions_trips_v1"."updated_at",
      "v_vehiclepositions_trips_v1"."start_cis_stop_id",
      "v_vehiclepositions_trips_v1"."start_cis_stop_platform_code",
      "v_vehiclepositions_trips_v1"."start_time",
      "v_vehiclepositions_trips_v1"."start_timestamp",
      "v_vehiclepositions_trips_v1"."vehicle_type_id" AS "vehicle_type",
      "v_vehiclepositions_trips_v1"."wheelchair_accessible",
      "v_vehiclepositions_trips_v1"."create_batch_id",
      "v_vehiclepositions_trips_v1"."created_by",
      "v_vehiclepositions_trips_v1"."update_batch_id",
      "v_vehiclepositions_trips_v1"."updated_by",
      "v_vehiclepositions_trips_v1"."agency_name_scheduled" AS "cis_agency_name",
      "v_vehiclepositions_trips_v1"."origin_route_name" AS "cis_parent_route_name",
      "v_vehiclepositions_trips_v1"."agency_name_real" AS "cis_real_agency_name",
      "v_vehiclepositions_trips_v1"."vehicle_registration_number" AS "cis_vehicle_registration_number"
    FROM "vehiclepositions_trips" AS "v_vehiclepositions_trips_v1";

CREATE OR REPLACE VIEW v_vehiclepositions_positions_v1
  AS
    SELECT "v_vehiclepositions_positions_v1"."created_at",
      "v_vehiclepositions_positions_v1"."delay",
      "v_vehiclepositions_positions_v1"."delay_stop_arrival",
      "v_vehiclepositions_positions_v1"."delay_stop_departure",
      "v_vehiclepositions_positions_v1"."next_stop_id" AS "gtfs_next_stop_id",
      "v_vehiclepositions_positions_v1"."shape_dist_traveled" AS "gtfs_shape_dist_traveled",
      "v_vehiclepositions_positions_v1"."is_canceled",
      "v_vehiclepositions_positions_v1"."lat",
      "v_vehiclepositions_positions_v1"."lng",
      "v_vehiclepositions_positions_v1"."origin_time",
      "v_vehiclepositions_positions_v1"."origin_timestamp",
      "v_vehiclepositions_positions_v1"."tracking",
      "v_vehiclepositions_positions_v1"."trips_id",
      "v_vehiclepositions_positions_v1"."create_batch_id",
      "v_vehiclepositions_positions_v1"."created_by",
      "v_vehiclepositions_positions_v1"."update_batch_id",
      "v_vehiclepositions_positions_v1"."updated_at",
      "v_vehiclepositions_positions_v1"."updated_by",
      "v_vehiclepositions_positions_v1"."id",
      "v_vehiclepositions_positions_v1"."bearing",
      "v_vehiclepositions_positions_v1"."cis_last_stop_id",
      "v_vehiclepositions_positions_v1"."cis_last_stop_sequence",
      "v_vehiclepositions_positions_v1"."last_stop_id" AS "gtfs_last_stop_id",
      "v_vehiclepositions_positions_v1"."last_stop_sequence" AS "gtfs_last_stop_sequence",
      "v_vehiclepositions_positions_v1"."next_stop_sequence" AS "gtfs_next_stop_sequence",
      "v_vehiclepositions_positions_v1"."speed"
    FROM "vehiclepositions_positions" AS "v_vehiclepositions_positions_v1";
