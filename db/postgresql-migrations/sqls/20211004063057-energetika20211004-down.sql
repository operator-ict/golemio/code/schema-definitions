drop VIEW analytic.v_vpalac_apartments_daily_consumption;
CREATE OR REPLACE VIEW analytic.v_vpalac_apartments_daily_consumption
AS WITH last_measurement AS (
         SELECT date_part('year'::text, to_timestamp((m.time_measurement / 1000)::double precision)::timestamp without time zone) AS measure_year,
            date_trunc('day'::text, max(to_timestamp((m.time_measurement / 1000)::double precision)::timestamp without time zone)) AS last_measurement,
            m.var_id
           FROM vpalac_measurement m
          GROUP BY (date_part('year'::text, to_timestamp((m.time_measurement / 1000)::double precision)::timestamp without time zone)), m.var_id
        ), daily_status AS (
         SELECT
                CASE
                    WHEN me.me_serial::text = '5640201'::text THEN 'Prostor 109,111'::text
                    WHEN mm.location_name IS NOT NULL THEN mm.location_name::text
                    WHEN me.umisteni::text ~~ '%Byt%'::text THEN "right"(me.umisteni::text, length(me.umisteni::text) - "position"(me.umisteni::text, 'Byt'::text) + 1)
                    ELSE "right"(me.umisteni::text, length(me.umisteni::text) - "position"(me.umisteni::text, 'Prostor'::text) + 1)
                END AS apartment,
                CASE
                    WHEN mt.met_nazev::text = 'Sensor'::text THEN 'Teplo'::character varying
                    ELSE mt.met_nazev
                END AS met_nazev,
            me.mis_nazev,
            me.var_id,
            u.jed_nazev,
            date_trunc('hour'::text, to_timestamp((m.time_measurement / 1000)::double precision)::timestamp without time zone) AS measure_date,
            min(m.value) AS start_value
           FROM vpalac_measuring_equipment me
             JOIN vpalac_measurement m ON me.var_id = m.var_id
             LEFT JOIN vpalac_meter_type mt ON me.met_id = mt.met_id
             LEFT JOIN vpalac_units u ON me.pot_id = u.pot_id
             LEFT JOIN analytic.vpalac_meter_mapping mm ON mm.var_id = me.var_id
          WHERE to_timestamp((m.time_measurement / 1000)::double precision)::timestamp without time zone >= (now() - '3 years'::interval)
          GROUP BY mm.location_name, me.umisteni, me.me_serial, (date_part('month'::text, to_timestamp((m.time_measurement / 1000)::double precision)::timestamp without time zone)), me.var_id, ("right"(me.umisteni::text, 6)), mt.met_nazev, me.mis_nazev, (date_trunc('hour'::text, to_timestamp((m.time_measurement / 1000)::double precision)::timestamp without time zone)), u.jed_nazev
        )
 SELECT ds.apartment,
    ds.met_nazev AS measure_type,
    ds.mis_nazev AS measure_detail,
    ds.jed_nazev,
    ds.measure_date,
    lm.last_measurement,
    lead(ds.start_value) OVER (PARTITION BY ds.var_id ORDER BY ds.measure_date) AS lead,
        CASE
            WHEN ds.met_nazev::text = 'Teplo'::text AND ds.measure_date = lm.last_measurement THEN lead(ds.start_value) OVER (PARTITION BY ds.var_id ORDER BY ds.measure_date)
            ELSE lead(ds.start_value) OVER (PARTITION BY ds.var_id ORDER BY ds.measure_date) - ds.start_value
        END AS consumption,
    ds.start_value,
    ds.var_id
   FROM daily_status ds
     JOIN last_measurement lm ON ds.var_id = lm.var_id AND date_part('year'::text, ds.measure_date) = lm.measure_year;


drop view analytic.v_consumption_gas_reserve ;

CREATE TABLE analytic.consumption_electricity_daily_reserves (
	object_name varchar(255) NULL,
	device_id varchar(255) NULL,
	relevance_year int4 NULL,
	relevance_month int4 NULL,
	daily_reserved_capacity_kwh numeric NULL
);

CREATE TABLE analytic.consumption_gas_daily_reserves (
	object_name varchar(255) NULL,
	device_id varchar(255) NULL,
	relevance_year int4 NULL,
	relevance_month int4 NULL,
	daily_reserved_capacity_m3 numeric NULL
);


CREATE OR REPLACE VIEW analytic.v_consumption_energy_reserve_day_electricity
AS SELECT date_trunc('day'::text, c.time_utc) AS den,
    c.time_utc,
    c.addr,
    c.unit,
    er.object_name,
    er.daily_reserved_capacity_kwh,
    c.value,
        CASE
            WHEN (c.value / er.daily_reserved_capacity_kwh) > 1::numeric THEN '100% a více'::text
            WHEN (c.value / er.daily_reserved_capacity_kwh) >= 0.8 AND (c.value / er.daily_reserved_capacity_kwh) <= 1::numeric THEN '80 - 100%'::text
            WHEN (c.value / er.daily_reserved_capacity_kwh) >= 0.6 AND (c.value / er.daily_reserved_capacity_kwh) <= 0.8 THEN '60 - 80%'::text
            WHEN (c.value / er.daily_reserved_capacity_kwh) >= 0.4 AND (c.value / er.daily_reserved_capacity_kwh) <= 0.6 THEN '40 - 60%'::text
            WHEN (c.value / er.daily_reserved_capacity_kwh) >= 0.2 AND (c.value / er.daily_reserved_capacity_kwh) <= 0.4 THEN '20 - 40%'::text
            WHEN (c.value / er.daily_reserved_capacity_kwh) >= 0::numeric AND (c.value / er.daily_reserved_capacity_kwh) <= 0.2 THEN '0 - 20%'::text
            WHEN c.value = 0::numeric THEN '0 - 20%'::text
            ELSE 'N/A'::text
        END AS ratio_group,
        CASE
            WHEN (c.value / er.daily_reserved_capacity_kwh) > 1::numeric THEN 6
            WHEN (c.value / er.daily_reserved_capacity_kwh) >= 0.8 AND (c.value / er.daily_reserved_capacity_kwh) <= 1::numeric THEN 5
            WHEN (c.value / er.daily_reserved_capacity_kwh) >= 0.6 AND (c.value / er.daily_reserved_capacity_kwh) <= 0.8 THEN 4
            WHEN (c.value / er.daily_reserved_capacity_kwh) >= 0.4 AND (c.value / er.daily_reserved_capacity_kwh) <= 0.6 THEN 3
            WHEN (c.value / er.daily_reserved_capacity_kwh) >= 0.2 AND (c.value / er.daily_reserved_capacity_kwh) <= 0.4 THEN 2
            WHEN (c.value / er.daily_reserved_capacity_kwh) >= 0::numeric AND (c.value / er.daily_reserved_capacity_kwh) <= 0.2 THEN 1
            WHEN c.value = 0::numeric THEN 1
            ELSE 0
        END AS ratio_order
   FROM consumption_energy_consumption c
     JOIN analytic.consumption_electricity_daily_reserves er ON c.addr::text = er.device_id::text AND er.relevance_year::double precision = date_part('year'::text, c.time_utc) AND er.relevance_month::double precision = date_part('month'::text, c.time_utc)
  WHERE (c.addr::text = ANY (ARRAY['/2.3/EF1'::character varying::text, '/2.7/EF1'::character varying::text, '/2.8/EF1'::character varying::text, '/2.9/EF1'::character varying::text, '/2.10.1/EF1'::character varying::text])) AND c.var::text = 'EFwActi'::text;
  

CREATE OR REPLACE VIEW analytic.v_consumption_gas_day
AS WITH kalendar AS (
         SELECT den.den
           FROM generate_series('2019-01-01 00:00:00+01'::timestamp with time zone, now() - '1 day'::interval, '1 day'::interval) den(den)
        ), meraky AS (
         SELECT DISTINCT consumption_energy_consumption.addr
           FROM consumption_energy_consumption
          WHERE consumption_energy_consumption.commodity::text = 'gas'::text AND consumption_energy_consumption.var::text = 'core'::text
        ), prumery AS (
         SELECT b.den,
            b.den2,
            b.addr,
            b.value,
            b.value2,
            (b.value2 - b.value) / (b.den2 - b.den)::numeric AS prumer,
            b.den2 - b.den AS dnu,
            b.value2 - b.value AS rozdil
           FROM ( SELECT a.time_utc::date AS den,
                    lead(a.time_utc::date) OVER (PARTITION BY a.addr ORDER BY a.time_utc) AS den2,
                    a.addr,
                    a.value,
                    lead(a.value) OVER (PARTITION BY a.addr ORDER BY a.time_utc) AS value2
                   FROM ( SELECT row_number() OVER (PARTITION BY (consumption_energy_consumption.time_utc::date), consumption_energy_consumption.addr ORDER BY consumption_energy_consumption.time_utc) AS rn,
                            consumption_energy_consumption.time_utc,
                            consumption_energy_consumption.value,
                            consumption_energy_consumption.addr,
                            consumption_energy_consumption.var,
                            consumption_energy_consumption.type,
                            consumption_energy_consumption.commodity,
                            consumption_energy_consumption.unit,
                            consumption_energy_consumption.meter,
                            consumption_energy_consumption.create_batch_id,
                            consumption_energy_consumption.created_at,
                            consumption_energy_consumption.created_by,
                            consumption_energy_consumption.update_batch_id,
                            consumption_energy_consumption.updated_at,
                            consumption_energy_consumption.updated_by
                           FROM consumption_energy_consumption
                          WHERE consumption_energy_consumption.commodity::text = 'gas'::text AND consumption_energy_consumption.var::text = 'core'::text) a
                  WHERE a.rn = 1) b
        ), prumery2 AS (
         SELECT prumery_1.den,
            prumery_1.den2,
            prumery_1.addr,
            prumery_1.value,
            prumery_1.value2,
            prumery_1.prumer,
            prumery_1.dnu,
            prumery_1.rozdil
           FROM prumery prumery_1
          WHERE prumery_1.dnu > 1
        )
 SELECT kalendar.den,
    meraky.addr,
    prumery.prumer AS delta_value,
    prumery2.prumer AS oprava
   FROM kalendar
     LEFT JOIN meraky ON true
     LEFT JOIN prumery ON prumery.den = kalendar.den::date AND prumery.addr::text = meraky.addr::text
     LEFT JOIN prumery2 ON prumery2.addr::text = meraky.addr::text AND kalendar.den >= prumery2.den AND kalendar.den <= prumery2.den2
  ORDER BY meraky.addr, kalendar.den;	 




CREATE OR REPLACE VIEW analytic.v_consumption_energy_reserve_day_gas
AS WITH consumption AS (
         SELECT v_consumption_gas_day.den AS date,
            v_consumption_gas_day.addr,
            COALESCE(v_consumption_gas_day.delta_value, v_consumption_gas_day.oprava) AS delta_value
           FROM analytic.v_consumption_gas_day
        )
 SELECT consumption.date,
    consumption.addr,
    consumption.delta_value,
    res.daily_reserved_capacity_m3,
    consumption.delta_value / res.daily_reserved_capacity_m3 AS ratio,
        CASE
            WHEN (consumption.delta_value / res.daily_reserved_capacity_m3) > 1::numeric THEN 'nad kapacitou'::text
            WHEN (consumption.delta_value / res.daily_reserved_capacity_m3) >= 0.8 AND (consumption.delta_value / res.daily_reserved_capacity_m3) <= 1::numeric THEN '80 - 100%'::text
            WHEN (consumption.delta_value / res.daily_reserved_capacity_m3) >= 0.6 AND (consumption.delta_value / res.daily_reserved_capacity_m3) <= 0.8 THEN '60 - 80%'::text
            WHEN (consumption.delta_value / res.daily_reserved_capacity_m3) >= 0.4 AND (consumption.delta_value / res.daily_reserved_capacity_m3) <= 0.6 THEN '40 - 60%'::text
            WHEN (consumption.delta_value / res.daily_reserved_capacity_m3) >= 0.2 AND (consumption.delta_value / res.daily_reserved_capacity_m3) <= 0.4 THEN '20 - 40%'::text
            WHEN (consumption.delta_value / res.daily_reserved_capacity_m3) >= 0::numeric AND (consumption.delta_value / res.daily_reserved_capacity_m3) <= 0.2 THEN 'do 20%'::text
            ELSE 'none'::text
        END AS ratio_group
   FROM consumption
     LEFT JOIN analytic.consumption_gas_daily_reserves res ON consumption.addr::text = res.device_id::text AND date_part('year'::text, consumption.date) = res.relevance_year::double precision AND date_part('month'::text, consumption.date) = res.relevance_month::double precision;
	 
  
  
  -- analytic.v_consumption_gas_reserve source

CREATE OR REPLACE VIEW analytic.v_consumption_gas_reserve
AS WITH kalendar AS (
         SELECT den.den
           FROM generate_series('2019-01-01 00:00:00+01'::timestamp with time zone, now() - '1 day'::interval, '1 day'::interval) den(den)
        ), meraky AS (
         SELECT DISTINCT consumption_energy_consumption.addr
           FROM consumption_energy_consumption
          WHERE consumption_energy_consumption.commodity::text = 'gas'::text AND consumption_energy_consumption.var::text = 'core'::text AND (consumption_energy_consumption.addr::text IN ( SELECT DISTINCT consumption_gas_daily_reserves.device_id
                   FROM analytic.consumption_gas_daily_reserves))
        ), daily_delta AS (
         SELECT date_trunc('day'::text, v_consumption_energy_delta.time_utc)::date AS measured_from,
            v_consumption_energy_delta.addr,
            v_consumption_energy_delta.var,
            v_consumption_energy_delta.commodity,
            v_consumption_energy_delta.unit,
            max(v_consumption_energy_delta.value) AS value,
            sum(v_consumption_energy_delta.delta_value) AS delta_value,
            min(date_trunc('day'::text, v_consumption_energy_delta.last_timeutc))::date AS measured_from_before
           FROM analytic.v_consumption_energy_delta
          WHERE v_consumption_energy_delta.commodity::text = 'gas'::text AND date_part('year'::text, v_consumption_energy_delta.time_utc) >= 2019::double precision AND (v_consumption_energy_delta.addr::text IN ( SELECT DISTINCT consumption_gas_daily_reserves.device_id
                   FROM analytic.consumption_gas_daily_reserves))
          GROUP BY (date_trunc('day'::text, v_consumption_energy_delta.time_utc)), v_consumption_energy_delta.addr, v_consumption_energy_delta.var, v_consumption_energy_delta.commodity, v_consumption_energy_delta.unit
          ORDER BY v_consumption_energy_delta.addr, (date_trunc('day'::text, v_consumption_energy_delta.time_utc)) DESC
        ), delta_dates AS (
         SELECT daily_delta.measured_from,
            daily_delta.addr,
            daily_delta.var,
            daily_delta.commodity,
            daily_delta.unit,
            daily_delta.value,
            daily_delta.delta_value,
            daily_delta.measured_from_before,
            lag(daily_delta.measured_from, 1) OVER (PARTITION BY daily_delta.addr ORDER BY daily_delta.measured_from) AS measured_before,
            lead(daily_delta.measured_from, 1) OVER (PARTITION BY daily_delta.addr ORDER BY daily_delta.measured_from) AS measured_after,
            lag(daily_delta.delta_value, 1) OVER (PARTITION BY daily_delta.addr ORDER BY daily_delta.measured_from) AS delta_before,
            lead(daily_delta.delta_value, 1) OVER (PARTITION BY daily_delta.addr ORDER BY daily_delta.measured_from) AS delta_after
           FROM daily_delta
        ), delta_date_diff AS (
         SELECT delta_dates.measured_from,
            delta_dates.addr,
            delta_dates.var,
            delta_dates.commodity,
            delta_dates.unit,
            delta_dates.value,
            delta_dates.delta_value,
            delta_dates.measured_from_before,
            delta_dates.measured_before,
            delta_dates.measured_after,
            delta_dates.delta_before,
            delta_dates.delta_after,
                CASE
                    WHEN (delta_dates.measured_after - delta_dates.measured_from) > 1 THEN 'begin'::text
                    WHEN (delta_dates.measured_from - delta_dates.measured_before) > 1 THEN 'end'::text
                    ELSE 'ok'::text
                END AS value_type,
                CASE
                    WHEN (delta_dates.measured_after - delta_dates.measured_from) > 1 THEN (delta_dates.delta_value + delta_dates.delta_after) / (delta_dates.measured_after - delta_dates.measured_from + 1)::numeric
                    WHEN (delta_dates.measured_from - delta_dates.measured_before) > 1 THEN (delta_dates.delta_value + delta_dates.delta_before) / (delta_dates.measured_from - delta_dates.measured_before + 1)::numeric
                    ELSE NULL::numeric
                END AS fix
           FROM delta_dates
        ), joined_table AS (
         SELECT kalendar.den::date AS measured_from,
            meraky.addr,
            delta_date_diff.delta_value,
            delta_date_diff.fix,
            COALESCE(delta_date_diff.fix, delta_date_diff.delta_value) AS final_fix,
            res.daily_reserved_capacity_m3
           FROM kalendar
             LEFT JOIN meraky ON true
             LEFT JOIN delta_date_diff ON delta_date_diff.measured_from = kalendar.den::date AND delta_date_diff.addr::text = meraky.addr::text
             LEFT JOIN analytic.consumption_gas_daily_reserves res ON meraky.addr::text = res.device_id::text AND date_part('year'::text, kalendar.den) = res.relevance_year::double precision AND date_part('month'::text, kalendar.den) = res.relevance_month::double precision
          ORDER BY meraky.addr, kalendar.den
        ), fixed_table AS (
         SELECT q.measured_from,
            q.addr,
            q.delta_value,
            q.fix,
            q.final_fix,
            q.daily_reserved_capacity_m3,
            q.value_partition,
            first_value(q.final_fix) OVER w AS value
           FROM ( SELECT joined_table.measured_from,
                    joined_table.addr,
                    joined_table.delta_value,
                    joined_table.fix,
                    joined_table.final_fix,
                    joined_table.daily_reserved_capacity_m3,
                    sum(
                        CASE
                            WHEN joined_table.final_fix IS NULL THEN 0
                            ELSE 1
                        END) OVER (PARTITION BY joined_table.addr ORDER BY joined_table.measured_from) AS value_partition
                   FROM joined_table) q
          WINDOW w AS (PARTITION BY q.addr, q.value_partition ORDER BY q.measured_from)
        )
 SELECT fixed_table.measured_from,
    fixed_table.addr,
    fixed_table.value AS delta_value,
    fixed_table.daily_reserved_capacity_m3,
    fixed_table.value / fixed_table.daily_reserved_capacity_m3 AS ratio,
        CASE
            WHEN (fixed_table.value / fixed_table.daily_reserved_capacity_m3) > 1::numeric THEN '100% a více'::text
            WHEN (fixed_table.value / fixed_table.daily_reserved_capacity_m3) >= 0.8 AND (fixed_table.value / fixed_table.daily_reserved_capacity_m3) <= 1::numeric THEN '80 - 100%'::text
            WHEN (fixed_table.value / fixed_table.daily_reserved_capacity_m3) >= 0.6 AND (fixed_table.value / fixed_table.daily_reserved_capacity_m3) <= 0.8 THEN '60 - 80%'::text
            WHEN (fixed_table.value / fixed_table.daily_reserved_capacity_m3) >= 0.4 AND (fixed_table.value / fixed_table.daily_reserved_capacity_m3) <= 0.6 THEN '40 - 60%'::text
            WHEN (fixed_table.value / fixed_table.daily_reserved_capacity_m3) >= 0.2 AND (fixed_table.value / fixed_table.daily_reserved_capacity_m3) <= 0.4 THEN '20 - 40%'::text
            WHEN (fixed_table.value / fixed_table.daily_reserved_capacity_m3) >= 0::numeric AND (fixed_table.value / fixed_table.daily_reserved_capacity_m3) <= 0.2 THEN '0 - 20%'::text
            WHEN fixed_table.value = 0::numeric THEN '0 - 20%'::text
            ELSE 'N/A'::text
        END AS ratio_group
   FROM fixed_table;
   
