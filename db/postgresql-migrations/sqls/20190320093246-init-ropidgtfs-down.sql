-- Table: public.ropidgtfs_agency

DROP TABLE public.ropidgtfs_agency;

-- Table: public.ropidgtfs_calendar

DROP TABLE public.ropidgtfs_calendar;

-- Table: public.ropidgtfs_calendar_dates

DROP TABLE public.ropidgtfs_calendar_dates;

-- Table: public.ropidgtfs_cis_stop_groups

DROP TABLE public.ropidgtfs_cis_stop_groups;

-- Table: public.ropidgtfs_cis_stops

DROP TABLE public.ropidgtfs_cis_stops;

-- Table: public.ropidgtfs_metadata

DROP TABLE public.ropidgtfs_metadata;

-- Sequence: public.ropidgtfs_metadata_id_seq

DROP SEQUENCE public.ropidgtfs_metadata_id_seq;

-- Table: public.ropidgtfs_routes

DROP TABLE public.ropidgtfs_routes;

-- Table: public.ropidgtfs_shapes

DROP TABLE public.ropidgtfs_shapes;

-- Table: public.ropidgtfs_stop_times

DROP TABLE public.ropidgtfs_stop_times;

-- Table: public.ropidgtfs_stops

DROP TABLE public.ropidgtfs_stops;

-- Table: public.ropidgtfs_trips

DROP TABLE public.ropidgtfs_trips;
