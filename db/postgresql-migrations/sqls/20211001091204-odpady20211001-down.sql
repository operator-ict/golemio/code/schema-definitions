CREATE OR REPLACE VIEW analytic.v_containers_pick_dates
AS WITH meas AS (
         SELECT containers_measurement.container_code,
            containers_measurement.measured_at_utc,
            containers_measurement.percent_calculated,
            rank() OVER (PARTITION BY containers_measurement.container_code, (date_trunc('day'::text, containers_measurement.measured_at_utc)) ORDER BY containers_measurement.measured_at_utc) AS rank
           FROM containers_measurement
        ), pick AS (
         SELECT cc.code AS container_id,
            concat(pd.pick_date::date, ' 7:00')::timestamp with time zone AS pick_date,
            'Pražské služby - hlášený svoz'::text AS pick_event,
            2 AS rank
           FROM containers_picks_dates pd
             LEFT JOIN containers_containers cc ON pd.container_id::text = cc.id::text
          WHERE (cc.code::text IN ( SELECT DISTINCT containers_measurement.container_code
                   FROM containers_measurement)) AND pd.pick_date >= (( SELECT min(containers_measurement.measured_at_utc) AS min
                   FROM containers_measurement)) AND pd.pick_date <= now()
        )
 SELECT COALESCE(meas.container_code, pick.container_id) AS container_id,
    COALESCE(meas.measured_at_utc::timestamp with time zone, pick.pick_date) AS measured_at_utc,
    COALESCE(meas.percent_calculated, 0) AS percent_calculated,
    pick.pick_event
   FROM meas
     FULL JOIN pick ON meas.container_code::text = pick.container_id::text AND meas.measured_at_utc::date = pick.pick_date::date AND meas.rank = pick.rank;