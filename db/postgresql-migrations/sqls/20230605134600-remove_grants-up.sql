DROP VIEW analytic.v_grants_data_table;

DROP VIEW analytic.v_grants_last_update;

DROP VIEW analytic.v_grants_projects_applicants;

DROP TABLE python.grants_projects CASCADE;

DROP TABLE python.grants_resolutions CASCADE;

DROP TABLE python.grants_applicants CASCADE;
