-- martinamichlova.v_grants_projects_applicants source

CREATE OR REPLACE VIEW analytic.v_grants_projects_applicants
AS SELECT gp.cislo_projektu,
    gp.nazev_projektu,
    gp.stav,
    gp.nazev_programu,
    gp.typ_dotace,
    gp.nazev_oblasti,
    gp.ucel_dotace,
    gp.rok_od,
    gp.rok_do,
    gp.castka_naklady,
    gp.castka_pozadovana,
    gp.castka_pridelena,
    gp.castka_vycerpana,
    gp.castka_vyuctovana,
    gp.id_projekt,
    gp.id_zadatel,
    ga.nazev,
    ga.pravni_forma
   FROM python.grants_projects gp
     LEFT JOIN python.grants_applicants ga ON gp.id_zadatel = ga.id_zadatel;
	 
-- martinamichlova.v_grants_data_table source

CREATE OR REPLACE VIEW analytic.v_grants_data_table
AS SELECT gr.cislo_usneseni,
    gr.datum_schvaleni,
    gr.schvalil,
    gr.castka_pridelena,
    gr.cislo_smlouvy,
    gp.id_projekt,
    gp.id_zadatel,
    gp.cislo_projektu,
    gp.nazev_projektu,
    gp.nazev_programu,
    gp.nazev_oblasti,
    gp.castka_pozadovana,
    gp.castka_vycerpana,
    gp.castka_vyuctovana,
    gp.stav,
    gp.rok_od,
    gp.rok_do,
    ga.nazev,
    ga.pravni_forma
   FROM python.grants_resolutions gr
     LEFT JOIN python.grants_projects gp ON gr.id_projekt = gp.id_projekt
     LEFT JOIN python.grants_applicants ga ON gp.id_zadatel = ga.id_zadatel;	 
	 
-- martinamichlova.v_grants_last_update source

CREATE OR REPLACE VIEW analytic.v_grants_last_update
AS SELECT gp.updated_at::date AS last_update
   FROM python.grants_projects gp
  ORDER BY gp.updated_at DESC
 LIMIT 1;	 