DROP TABLE python.public_tenders_funded_organisations;

-- pridat dva nove sloupce
alter table python.public_tenders
drop column zadavatel_nazev;
alter table python.public_tenders
drop column zadavatel_ico;

-- prejmenovat anglicky aby to splnovalo jmennou konvenci
alter table python.public_tenders_departments
rename to public_tenders_odbory;
