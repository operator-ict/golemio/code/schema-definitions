DROP TABLE IF EXISTS python.grants_resolutions CASCADE;
DROP TABLE IF EXISTS python.grants_projects CASCADE;
DROP TABLE IF EXISTS python.grants_applicants CASCADE;