-- drop analytic views
drop view analytic.v_containers_24h_missing;
drop view analytic.v_containers_5d_same;
drop view analytic.v_containers_full;
drop view analytic.v_containers_full_days;
drop view analytic.v_containers_measurements;
drop view analytic.v_containers_next_pick;
drop view analytic.v_containers_pick_dates;
drop view analytic.v_containers_picks;
-- drop export views
drop view public.v_containers_picks_export;
drop view public.v_containers_measurement_export;
drop view analytic.v_containers_containers;
-- drop analytic tables
drop table analytic.containers_code_black_list;
drop table analytic.containers_districts;
-- drop tables from python job
drop table python.waste_assigned_pick_dates;
drop table python.waste_assigned_picks;
