/* v_pedestrians_locations_api */

CREATE OR REPLACE VIEW public.v_pedestrians_locations_api
AS SELECT l.location_id::character varying(5) AS location_id,
    l.location_name,
    l.lat,
    l.lng,
    l.address,
    l.city_district,
    l.tech,
    l.map_url,
    l.place_url,
    l.measurement_start,
    l.measurement_end
   FROM analytic.pedestrians_locations_api l
  ORDER BY (l.location_id::character varying(5));

/* v_pedestrians_directions_api */

CREATE OR REPLACE VIEW public.v_pedestrians_directions_api
AS SELECT pedestrians_directions_api.direction_id,
    pedestrians_directions_api.location_id::character varying(5) AS location_id,
    pedestrians_directions_api.direction_name,
    pedestrians_directions_api.direction_type::character varying(5) AS direction_type
FROM analytic.pedestrians_directions_api
ORDER BY (pedestrians_directions_api.location_id::character varying(5));


/* MATERIALIZED VIEW mv_pedestrians_detections_api */

CREATE MATERIALIZED VIEW IF NOT EXISTS mv_pedestrians_detections_api
AS WITH wifi AS (
         SELECT pedestrians_wifi.location_id,
            pedestrians_wifi.direction_id,
            date_trunc('hour'::text, pedestrians_wifi.measured_from) + (date_part('minute'::text, pedestrians_wifi.measured_from)::integer / 15)::double precision * '00:15:00'::interval AS measured_from,
            date_trunc('hour'::text, pedestrians_wifi.measured_from) + (date_part('minute'::text, pedestrians_wifi.measured_from)::integer / 15)::double precision * '00:15:00'::interval + '00:15:00'::interval AS measured_to,
            sum(pedestrians_wifi.value) AS value,
            count(pedestrians_wifi.value) AS count_n,
            3 AS quantity
           FROM analytic.pedestrians_wifi
          GROUP BY pedestrians_wifi.location_id, pedestrians_wifi.direction_id, (date_trunc('hour'::text, pedestrians_wifi.measured_from) + (date_part('minute'::text, pedestrians_wifi.measured_from)::integer / 15)::double precision * '00:15:00'::interval), (date_trunc('hour'::text, pedestrians_wifi.measured_from) + (date_part('minute'::text, pedestrians_wifi.measured_from)::integer / 15)::double precision * '00:15:00'::interval + '00:15:00'::interval)
        ), pyro AS (
         SELECT cd.locations_id AS location_id,
            cd.directions_id AS direction_id,
            to_timestamp((cd.measured_from / 1000)::double precision) AS measured_from,
            to_timestamp((cd.measured_from / 1000)::double precision) + '00:15:00'::interval AS measured_to,
            sum(cd.value) AS value,
            1 AS count_n,
            1 AS quantity
           FROM counters_detections cd
          WHERE cd.category::text = 'pedestrian'::text AND (cd.directions_id::text IN ( SELECT DISTINCT pedestrians_directions_api.direction_id
                   FROM analytic.pedestrians_directions_api))
          GROUP BY cd.locations_id, cd.directions_id, (to_timestamp((cd.measured_from / 1000)::double precision)), (to_timestamp((cd.measured_from / 1000)::double precision) + '00:15:00'::interval)
        ), flow AS (
         SELECT flow_measurements.cube_id::character varying(50) AS location_id,
            flow_measurements.sink_id::character varying(50) AS direction_id,
            date_trunc('hour'::text, to_timestamp((flow_measurements.start_timestamp / 1000)::double precision)) + (date_part('minute'::text, to_timestamp((flow_measurements.start_timestamp / 1000)::double precision))::integer / 15)::double precision * '00:15:00'::interval AS measured_from,
            date_trunc('hour'::text, to_timestamp((flow_measurements.start_timestamp / 1000)::double precision)) + (date_part('minute'::text, to_timestamp((flow_measurements.start_timestamp / 1000)::double precision))::integer / 15)::double precision * '00:15:00'::interval + '00:15:00'::interval AS measured_to,
            sum(flow_measurements.value) AS value,
            count(flow_measurements.value) AS count_n,
            3 AS quantity
           FROM flow_measurements
          WHERE ((flow_measurements.cube_id::character varying(50)::text, flow_measurements.sink_id::character varying(50)::text) IN ( SELECT DISTINCT pedestrians_directions_api.cube_id AS location_id,
                    pedestrians_directions_api.direction_id
                   FROM analytic.pedestrians_directions_api)) AND flow_measurements.category::text = 'pedestrian'::text
          GROUP BY (flow_measurements.cube_id::character varying(50)), (flow_measurements.sink_id::character varying(50)), (date_trunc('hour'::text, to_timestamp((flow_measurements.start_timestamp / 1000)::double precision)) + (date_part('minute'::text, to_timestamp((flow_measurements.start_timestamp / 1000)::double precision))::integer / 15)::double precision * '00:15:00'::interval), (date_trunc('hour'::text, to_timestamp((flow_measurements.start_timestamp / 1000)::double precision)) + (date_part('minute'::text, to_timestamp((flow_measurements.start_timestamp / 1000)::double precision))::integer / 15)::double precision * '00:15:00'::interval + '00:15:00'::interval)
          ORDER BY (date_trunc('hour'::text, to_timestamp((flow_measurements.start_timestamp / 1000)::double precision)) + (date_part('minute'::text, to_timestamp((flow_measurements.start_timestamp / 1000)::double precision))::integer / 15)::double precision * '00:15:00'::interval), (flow_measurements.cube_id::character varying(50)), (flow_measurements.sink_id::character varying(50))
        ), measurements AS (
         SELECT wifi.location_id,
            wifi.direction_id,
            wifi.measured_from,
            wifi.measured_to,
            wifi.value,
            wifi.count_n,
            wifi.quantity
           FROM wifi
        UNION ALL
         SELECT pyro.location_id,
            pyro.direction_id,
            pyro.measured_from,
            pyro.measured_to,
            pyro.value,
            pyro.count_n,
            pyro.quantity
           FROM pyro
        UNION ALL
         SELECT flow.location_id,
            flow.direction_id,
            flow.measured_from,
            flow.measured_to,
            flow.value,
            flow.count_n,
            flow.quantity
           FROM flow
        )
 SELECT measurements.measured_from,
    measurements.measured_to,
    measurements.location_id,
    measurements.direction_id,
    measurements.value,
    measurements.count_n::numeric / measurements.quantity::numeric AS quality
   FROM measurements
  ORDER BY (measurements.count_n::numeric / measurements.quantity::numeric)
WITH NO DATA;


/* Indexes for MATERIALIZED VIEW mv_pedestrians_detections_api */

CREATE INDEX pedestrians_detections_measured_from_index ON public.mv_pedestrians_detections_api USING btree (measured_from);
CREATE INDEX pedestrians_detections_measured_to_index ON public.mv_pedestrians_detections_api USING btree (measured_to);
CREATE INDEX pedestrians_detections_location_id_index ON public.mv_pedestrians_detections_api USING btree (location_id);
CREATE INDEX pedestrians_detections_direction_id_index ON public.mv_pedestrians_detections_api USING btree (direction_id);
