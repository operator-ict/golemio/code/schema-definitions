-- DOWN

DROP VIEW IF EXISTS v_vehiclepositions_last_position_v1;
DROP VIEW IF EXISTS v_vehiclepositions_last_position;
DROP VIEW IF EXISTS v_vehiclepositions_trips_v1;
DROP VIEW IF EXISTS v_vehiclepositions_positions_v1;

DROP TABLE IF EXISTS public.vehiclepositions_vehicle_types;

ALTER TABLE public.vehiclepositions_trips RENAME COLUMN agency_name_real TO cis_real_agency_name;
ALTER TABLE public.vehiclepositions_trips RENAME COLUMN agency_name_scheduled TO cis_agency_name;
ALTER TABLE public.vehiclepositions_trips RENAME COLUMN cis_line_id TO cis_id;
ALTER TABLE public.vehiclepositions_trips RENAME COLUMN cis_line_number TO cis_number;
ALTER TABLE public.vehiclepositions_trips RENAME COLUMN sequence_id TO cis_order;
ALTER TABLE public.vehiclepositions_trips RENAME COLUMN origin_route_name TO cis_parent_route_name;
ALTER TABLE public.vehiclepositions_trips RENAME COLUMN cis_line_short_name TO cis_short_name;
ALTER TABLE public.vehiclepositions_trips RENAME COLUMN vehicle_registration_number TO cis_vehicle_registration_number;
ALTER TABLE public.vehiclepositions_trips RENAME COLUMN vehicle_type_id TO vehicle_type;

ALTER TABLE public.vehiclepositions_trips DROP COLUMN IF EXISTS gtfs_trip_headsign;

ALTER TABLE public.vehiclepositions_positions RENAME COLUMN last_stop_id TO gtfs_last_stop_id;
ALTER TABLE public.vehiclepositions_positions RENAME COLUMN last_stop_sequence TO gtfs_last_stop_sequence;
ALTER TABLE public.vehiclepositions_positions RENAME COLUMN next_stop_id TO gtfs_next_stop_id;
ALTER TABLE public.vehiclepositions_positions RENAME COLUMN next_stop_sequence TO gtfs_next_stop_sequence;

ALTER TABLE public.vehiclepositions_positions DROP COLUMN IF EXISTS last_stop_arrival_time;
ALTER TABLE public.vehiclepositions_positions DROP COLUMN IF EXISTS last_stop_departure_time;
ALTER TABLE public.vehiclepositions_positions DROP COLUMN IF EXISTS next_stop_arrival_time;
ALTER TABLE public.vehiclepositions_positions DROP COLUMN IF EXISTS next_stop_departure_time;

ALTER TABLE public.vehiclepositions_positions RENAME COLUMN shape_dist_traveled TO gtfs_shape_dist_traveled;

CREATE OR REPLACE VIEW v_vehiclepositions_last_position
  AS
    SELECT
      DISTINCT ON ("vehiclepositions_positions"."trips_id") "vehiclepositions_positions".*
    FROM "vehiclepositions_positions" AS "vehiclepositions_positions"
      JOIN "vehiclepositions_trips" AS "vehiclepositions_trips"
        ON ("vehiclepositions_trips"."id" = "vehiclepositions_positions"."trips_id" AND
            "vehiclepositions_trips"."gtfs_trip_id" IS NOT NULL)
    WHERE "vehiclepositions_positions"."created_at" > (NOW() - INTERVAL '5 min') AND "vehiclepositions_positions"."delay" IS NOT NULL
    ORDER BY "vehiclepositions_positions"."trips_id", "vehiclepositions_positions"."created_at" DESC;