create or replace view analytic.v_containers_pick_dates as 
WITH meas AS (
         SELECT containers_measurement.container_code,
            containers_measurement.measured_at_utc,
            containers_measurement.percent_calculated,
            rank() OVER (PARTITION BY containers_measurement.container_code, (date_trunc('day'::text, containers_measurement.measured_at_utc)) ORDER BY containers_measurement.measured_at_utc) AS rank
           FROM containers_measurement
        ), first_meas as ( 
        	select container_code, min(measured_at::date) as min_meas
        	from containers_measurement cm 
        	group by container_code 
        ),
        pick AS (
         SELECT cc.code AS container_id,
            concat(pd.pick_date::date, ' 7:00')::timestamp with time zone AS pick_date,
            'Pražské služby - hlášený svoz'::text AS pick_event,
            2 AS rank
           FROM containers_picks_dates pd
             LEFT JOIN (select id, sensoneo_code as code from containers_containers where sensoneo_code is not null and sensoneo_code != code) cc ON pd.container_id::text = cc.id::text
             left join first_meas fm on fm.container_code = cc.code
          WHERE pd.pick_date >= (( SELECT min(containers_measurement.measured_at_utc) AS min
                   FROM containers_measurement)) AND pd.pick_date <= now() and pd.pick_date >= fm.min_meas                   
        )
 SELECT COALESCE(meas.container_code, pick.container_id) AS container_id,
    COALESCE(meas.measured_at_utc::timestamp with time zone, pick.pick_date) AS measured_at_utc,
    COALESCE(meas.percent_calculated, 0) AS percent_calculated,
    pick.pick_event
   FROM meas
     FULL JOIN pick ON meas.container_code::text = pick.container_id::text AND meas.measured_at_utc::date = pick.pick_date::date AND meas.rank = pick.rank;
