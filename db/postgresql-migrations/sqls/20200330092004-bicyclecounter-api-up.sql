CREATE TABLE IF NOT EXISTS public.bicyclecounters_api_logs_failures
(
  id BIGINT NOT NULL,
  issue TEXT NOT NULL,
  error_code int8 NOT NULL,
  ping DOUBLE PRECISION NOT NULL,
  measured_at TIMESTAMP NOT NULL,
  updated_at TIMESTAMP NOT NULL,
  created_at TIMESTAMP NOT NULL,
  CONSTRAINT bicyclecounters_api_logs_failures_pk PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS public.bicyclecounters_api_logs_hits
(
id BIGINT NOT NULL,
latency BIGINT NOT NULL,
ping_time BIGINT NOT NULL,
measured_at TIMESTAMP NOT NULL,
updated_at TIMESTAMP NOT NULL,
created_at TIMESTAMP NOT NULL,
CONSTRAINT bicyclecounters_api_logs_hits_pk PRIMARY KEY (id)
);

