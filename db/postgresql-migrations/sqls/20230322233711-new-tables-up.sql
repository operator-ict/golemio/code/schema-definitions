DROP TABLE python.ambulance_measurements;

CREATE TABLE python.ambulance_measurements (
	id_call bigint ,
	date_time_call timestamptz NULL,
	date_time_dispatch timestamptz NULL,
	date_time_scene timestamptz NULL,
	dptr_base text NULL,
	indication text NULL,
	dptr_coordinates text NULL,
	lng float NULL,
	lat float NULL,
	hospital text NULL,
	date_time_arvl_hospital timestamptz NULL,
	date_time_handover timestamptz NULL,
	date_time_dptr_hospital timestamptz NULL,
    duration_min float NULL,
    is_valid boolean NULL
);
