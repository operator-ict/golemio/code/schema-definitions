ALTER TABLE "public"."vehiclepositions_trips" DROP COLUMN "last_position_id";

ALTER TABLE "public"."vehiclepositions_positions" DROP COLUMN "state_process";
ALTER TABLE "public"."vehiclepositions_positions" DROP COLUMN "state_position";
ALTER TABLE "public"."vehiclepositions_positions" DROP COLUMN "this_stop_id";