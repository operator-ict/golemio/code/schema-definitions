
drop view analytic.v_ropidbi_coupon_tokens;
drop table public.mos_be_tokens;

alter table public.mos_be_coupons
drop column created_by_id,
DROP COLUMN order_status,
DROP COLUMN order_payment_type,
DROP COLUMN token_id;

delete from meta."extract"
where name_extract = 'mos_be_tokens';

delete from meta.dataset
where code_dataset = 'mos_be';



