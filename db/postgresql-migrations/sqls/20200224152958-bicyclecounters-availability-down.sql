-- drop table if exists analytic.camea_bikecounters_in_contract;

drop view if exists analytic.v_camea_bikecounters_last_update;
drop view if exists analytic.v_camea_bikecounters_data_quality;
drop view if exists analytic.v_camea_bikecounters_data_disbalance;
drop view if exists analytic.v_camea_bikecounters_all_data_today;
drop view if exists analytic.v_camea_bikecounters_all_data_table_today;
drop view if exists analytic.v_camea_bikecounters_all_data_history;
drop view if exists analytic.v_camea_bikecounters_name_table;

