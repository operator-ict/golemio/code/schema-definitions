-- analytic.v_containers_measurements source

CREATE OR REPLACE VIEW analytic.v_containers_measurements
AS SELECT containers_measurement.container_code,
    containers_measurement.station_code,
    containers_measurement.measured_at_utc,
    containers_measurement.measured_at_utc::date AS measured_at_utc_date,
    split_part(containers_measurement.measured_at_utc::character varying(50)::text, ' '::text, 2) AS measured_at_utc_time,
    containers_measurement.percent_calculated,
    COALESCE(containers_measurement.percent_calculated::numeric, 0::numeric) / 100::numeric AS percent_calculated_decimal
   FROM containers_measurement
  WHERE (containers_measurement.container_code::text IN ( SELECT DISTINCT containers_containers.code
           FROM containers_containers));