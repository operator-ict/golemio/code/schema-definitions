-- franz.v_internal_reports_golemio_bi source

CREATE OR REPLACE VIEW analytic.v_internal_reports_golemio_bi
AS SELECT internal_reports_golemio_bi.date,
    internal_reports_golemio_bi.sessions,
    internal_reports_golemio_bi.users,
    internal_reports_golemio_bi.sessionduration AS session_time_total_sec,
    internal_reports_golemio_bi.newusers AS new_users,
    COALESCE(internal_reports_golemio_bi.sessionduration / NULLIF(internal_reports_golemio_bi.sessions, 0)::numeric / 60::numeric, 0::numeric) AS avg_session_time_min,
    COALESCE(internal_reports_golemio_bi.sessionduration / NULLIF(internal_reports_golemio_bi.users, 0)::numeric / 60::numeric, 0::numeric) AS avg_user_time_min
   FROM keboola.internal_reports_golemio_bi;