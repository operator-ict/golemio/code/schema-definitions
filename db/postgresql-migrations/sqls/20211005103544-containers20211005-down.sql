-- analytic.v_containers_24h_missing source

CREATE OR REPLACE VIEW analytic.v_containers_24h_missing
AS SELECT containers_measurement.container_code AS container_id,
    max(containers_measurement.measured_at) AS measured_at
   FROM containers_measurement
  GROUP BY containers_measurement.container_code
 HAVING max(containers_measurement.measured_at) < (now() - '1 day'::interval) OR max(containers_measurement.measured_at) IS NULL;
 
 drop table analytic.containers_code_black_list;