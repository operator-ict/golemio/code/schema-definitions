DROP VIEW analytic.v_uzis_covid19_vaccination_regions_vaccination_points;
DROP VIEW analytic.v_uzis_covid19_vaccination_regions_details;
DROP VIEW analytic.v_uzis_covid19_vaccination_prague_districts;
DROP VIEW analytic.v_uzis_covid19_population_age_regions;
DROP VIEW analytic.v_uzis_covid19_vaccination_logistics;
DROP VIEW analytic.v_uzis_covid19_pcr_requests;
DROP VIEW analytic.v_uzis_covid19_tests_districts;
DROP VIEW analytic.v_uzis_covid19_municipalities;
DROP VIEW analytic.v_uzis_covid19_central_bohemia;
DROP VIEW analytic.v_uzis_covid19_hospital_capacity_technologies;
DROP VIEW analytic.v_uzis_covid19_hospital_capacity_beds;
DROP VIEW analytic.v_uzis_covid19_hospital_capacity;
DROP VIEW analytic.v_uzis_covid19_prague_hospitalized_positive;
DROP VIEW analytic.v_uzis_covid19_prague_districts;
DROP VIEW analytic.v_uzis_covid19_age_groups_prague_ratios;
DROP VIEW analytic.v_uzis_covid19_age_groups_prague;
DROP VIEW analytic.v_uzis_covid19_pes_regional;

DROP VIEW analytic.v_uzis_covid19_moving_averages;
DROP VIEW analytic.v_uzis_covid19_hospitalized_daily;

DROP VIEW analytic.v_uzis_covid19_cz;
DROP VIEW analytic.v_uzis_covid19_age_groups_ratios;
DROP VIEW analytic.v_uzis_covid19_age_groups;


DROP TABLE uzis.population_central_bohemia;
DROP TABLE uzis.population_prague_districts;
DROP TABLE uzis.population_prague_age_districts;
DROP TABLE uzis.population_age_regions;
DROP TABLE uzis.population_regions;
DROP TABLE uzis.population_districts;
DROP TABLE uzis.codebook_regions_orps;
DROP TABLE uzis.codebook_regions;

DROP TABLE uzis.covid19_prague_districts;
DROP TABLE uzis.covid19_municipalities;
DROP TABLE uzis.covid19_vaccination_regional_details;
DROP TABLE uzis.covid19_vaccination_prague_details;
DROP TABLE uzis.covid19_vaccination_usage;
DROP TABLE uzis.covid19_vaccination_points;
DROP TABLE uzis.covid19_vaccination_distribution;
DROP TABLE uzis.covid19_cz_tests_regional;
DROP TABLE uzis.covid19_pes_regional;
DROP TABLE uzis.covid19_pcr_requests;
DROP TABLE uzis.covid19_hospitalized_regional;
drop TABLE uzis.covid19_hospital_capacity_details;
drop TABLE uzis.covid19_cz_daily;
drop TABLE uzis.covid19_cz_details;

drop schema if exists uzis;