drop VIEW analytic.v_data_from_materialized_view_teachers;
drop VIEW analytic.v_data_from_materialized_view;

drop MATERIALIZED VIEW analytic.mv_data_with_timetables;

drop VIEW analytic.v_data_with_evaluations;
drop VIEW analytic.v_data_with_5min_intervals;
drop VIEW analytic.v_source_filtered;

drop TABLE python.values_evaluation;
drop TABLE python.timetables;
drop TABLE python.timetable_parameters;
drop TABLE python.source_csv_data;
drop TABLE python.classrooms ;

