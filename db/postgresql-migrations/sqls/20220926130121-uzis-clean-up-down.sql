ALTER TABLE uzis.history_covid19_vaccination_registrations
  RENAME TO covid19_vaccination_registrations;

ALTER TABLE uzis.history_covid19_vaccination_prague_details
  RENAME TO covid19_vaccination_prague_details;

ALTER TABLE uzis.history_covid19_vaccination_usage
  RENAME TO covid19_vaccination_usage;

ALTER TABLE uzis.history_covid19_vaccination_calendar_capacities
  RENAME TO covid19_vaccination_calendar_capacities;

ALTER TABLE uzis.history_covid19_pes_regional
  RENAME TO covid19_pes_regional;

DROP TABLE uzis.covid19_vaccination_registrations;

DROP TABLE uzis.covid19_vaccination_calendar_capacities;

CREATE OR REPLACE VIEW analytic.v_uzis_covid19_age_groups_prague_ratios
AS WITH age_groups AS (
         SELECT date_trunc('month'::text, covid19_cz_details.datum_hlaseni::timestamp with time zone) AS report_month,
                CASE
                    WHEN covid19_cz_details.vek::numeric <= 5::numeric THEN '0-5'::text
                    WHEN covid19_cz_details.vek::numeric >= 6::numeric AND covid19_cz_details.vek::numeric <= 9::numeric THEN '6-9'::text
                    WHEN covid19_cz_details.vek::numeric >= 10::numeric AND covid19_cz_details.vek::numeric <= 14::numeric THEN '10-14'::text
                    WHEN covid19_cz_details.vek::numeric >= 15::numeric AND covid19_cz_details.vek::numeric <= 19::numeric THEN '15-19'::text
                    WHEN covid19_cz_details.vek::numeric >= 20::numeric AND covid19_cz_details.vek::numeric <= 25::numeric THEN '20-25'::text
                    WHEN covid19_cz_details.vek::numeric >= 26::numeric AND covid19_cz_details.vek::numeric <= 64::numeric THEN '26-64'::text
                    ELSE '65 a víc'::text
                END AS age_group,
            count(*) AS cases_count,
                CASE
                    WHEN covid19_cz_details.kraj = 'CZ010'::text THEN 'Hlavní město Praha'::text
                    ELSE 'Středočeský kraj'::text
                END AS region
           FROM uzis.covid19_cz_details
          WHERE covid19_cz_details.kraj = ANY (ARRAY['CZ010'::text, 'CZ020'::text])
          GROUP BY covid19_cz_details.kraj, (date_trunc('month'::text, covid19_cz_details.datum_hlaseni::timestamp with time zone)), (
                CASE
                    WHEN covid19_cz_details.vek::numeric <= 5::numeric THEN '0-5'::text
                    WHEN covid19_cz_details.vek::numeric >= 6::numeric AND covid19_cz_details.vek::numeric <= 9::numeric THEN '6-9'::text
                    WHEN covid19_cz_details.vek::numeric >= 10::numeric AND covid19_cz_details.vek::numeric <= 14::numeric THEN '10-14'::text
                    WHEN covid19_cz_details.vek::numeric >= 15::numeric AND covid19_cz_details.vek::numeric <= 19::numeric THEN '15-19'::text
                    WHEN covid19_cz_details.vek::numeric >= 20::numeric AND covid19_cz_details.vek::numeric <= 25::numeric THEN '20-25'::text
                    WHEN covid19_cz_details.vek::numeric >= 26::numeric AND covid19_cz_details.vek::numeric <= 64::numeric THEN '26-64'::text
                    ELSE '65 a víc'::text
                END)
        ), totals AS (
         SELECT date_trunc('month'::text, covid19_cz_details.datum_hlaseni::timestamp with time zone) AS report_month,
            count(*) AS cases_count,
                CASE
                    WHEN covid19_cz_details.kraj = 'CZ010'::text THEN 'Hlavní město Praha'::text
                    ELSE 'Středočeský kraj'::text
                END AS region
           FROM uzis.covid19_cz_details
          WHERE covid19_cz_details.kraj = ANY (ARRAY['CZ010'::text, 'CZ020'::text])
          GROUP BY covid19_cz_details.kraj, (date_trunc('month'::text, covid19_cz_details.datum_hlaseni::timestamp with time zone))
        )
 SELECT ag.report_month,
    ag.age_group,
    ag.cases_count::numeric / t.cases_count::numeric AS age_group_ratio,
        CASE
            WHEN ag.age_group = '0-5'::text THEN 1
            WHEN ag.age_group = '6-9'::text THEN 2
            WHEN ag.age_group = '10-14'::text THEN 3
            WHEN ag.age_group = '15-19'::text THEN 4
            WHEN ag.age_group = '20-25'::text THEN 5
            WHEN ag.age_group = '26-64'::text THEN 6
            ELSE 7
        END AS age_group_order,
    ag.region
   FROM age_groups ag
     JOIN totals t ON ag.report_month = t.report_month AND ag.region = t.region;

CREATE OR REPLACE VIEW analytic.v_uzis_covid19_pes_regional
AS SELECT p.datum,
    p.updated_at,
    p.kraj_kod,
    p.incidence14_100,
    p.incidence_65_14_100,
    p.simple_r,
    p.podil_zachycen_hosp,
    k."kraj-nazev" AS kraj_nazev
   FROM uzis.covid19_pes_regional p
     LEFT JOIN uzis.codebook_regions k ON p.kraj_kod = k."kraj-kod"
  WHERE p.datum >= '2021-01-05 00:00:00'::timestamp without time zone;
