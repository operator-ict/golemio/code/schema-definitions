drop VIEW analytic.v_bm_last_update;
drop view analytic.v_bm_data_all;
drop VIEW analytic.v_bm_union2;
drop VIEW analytic.v_bm_trasy_hod;
drop MATERIALIZED VIEW analytic.mv_bm_normal;
drop VIEW analytic.v_bm_trasy_hod_part;
drop VIEW analytic.v_bm_trasy;


DROP VIEW public.v_lkpr_export;
DROP VIEW analytic.v_lkpr_route_details;
DROP VIEW analytic.v_wazett_routes_lines;
DROP VIEW analytic.v_route_travel_times;
DROP VIEW analytic.v_route_live;
DROP VIEW analytic.v_instatntomtom_days;


ALTER TABLE public.wazett_routes  
ALTER COLUMN id TYPE int4;

ALTER TABLE public.wazett_route_lives 
ALTER COLUMN route_id TYPE int4;

ALTER TABLE public.wazett_subroutes  
ALTER COLUMN route_id TYPE int4;

ALTER TABLE public.wazett_subroute_lives 
ALTER COLUMN route_id TYPE int4;


CREATE OR REPLACE VIEW analytic.v_instatntomtom_days
AS SELECT to_char(to_timestamp((wrl.update_time / 1000)::double precision), 'yyyy-mm-dd'::text) AS day_index,
    wrl.route_id,
    round(avg(wrl."time"::numeric / wrl.historic_time::numeric), 2) AS t_index_value,
    count(*) AS t_index_count
   FROM wazett_route_lives wrl
  WHERE to_timestamp((wrl.update_time / 1000)::double precision)::time without time zone >= '05:00:00'::time without time zone AND to_timestamp((wrl.update_time / 1000)::double precision)::time without time zone <= '21:00:00'::time without time zone
  GROUP BY (to_char(to_timestamp((wrl.update_time / 1000)::double precision), 'yyyy-mm-dd'::text)), wrl.route_id;
 
CREATE OR REPLACE VIEW analytic.v_route_live
AS SELECT wrl.route_id,
    wrl.update_time,
    wrl."time",
    wrl.length,
    wrl.historic_time,
    wrl.jam_level,
    wrl.create_batch_id,
    wrl.created_at,
    wrl.created_by,
    wrl.update_batch_id,
    wrl.updated_at,
    wrl.updated_by
   FROM wazett_route_lives wrl
  WHERE (wrl.route_id = ANY (ARRAY[24949, 24953, 24962, 24924, 24925, 24950, 24951, 24952, 24954, 24955, 24956, 24957, 24958, 24960, 24961, 24963, 24964, 24965, 24966, 24967, 24968, 24969])) AND wrl.update_time::double precision >= (( SELECT max(wazett_route_lives.update_time)::double precision - date_part('epoch'::text, '1 year'::interval) * 1000::double precision AS max
           FROM wazett_route_lives));
 
CREATE OR REPLACE VIEW analytic.v_route_travel_times
AS SELECT ts.route_id,
    ts.year,
    ts.month,
    ts.day,
    ts.dow,
    ts.hour,
    ts.quarter,
    ts.hour + (ts.quarter::numeric / 60::numeric)::double precision AS hour_quarter,
    ((((((((ts.year || '-'::text) || ts.month) || '-'::text) || ts.day) || ' '::text) || ts.hour) || ':'::text) || ts.quarter)::timestamp without time zone AS date,
    avg(ts.travel_time)::integer AS travel_time
   FROM ( SELECT raw.route_id,
            raw.update_time,
            raw.travel_time,
            date_part('year'::text, raw.update_time) AS year,
            date_part('month'::text, raw.update_time) AS month,
            date_part('day'::text, raw.update_time) AS day,
            date_part('dow'::text, raw.update_time) AS dow,
            date_part('hour'::text, raw.update_time) AS hour,
            date_part('minute'::text, raw.update_time) AS minute,
            date_part('minute'::text, raw.update_time)::integer / 15 * 15 AS quarter
           FROM ( SELECT wazett_route_lives.route_id,
                    timezone('Europe/Prague'::text, to_timestamp((wazett_route_lives.update_time / 1000)::double precision)::timestamp without time zone) AS update_time,
                    wazett_route_lives."time" AS travel_time
                   FROM wazett_route_lives
                     JOIN analytic.waze_dashboard_route_name wdrn ON wdrn.route_id = wazett_route_lives.route_id) raw) ts
  WHERE ts.year > 2000::double precision
  GROUP BY ts.route_id, ts.year, ts.month, ts.day, ts.dow, ts.hour, ts.quarter;
  
CREATE OR REPLACE VIEW analytic.v_wazett_routes_lines
AS SELECT wazett_routes.id,
    wazett_routes.name,
    st_astext(wazett_routes.line) AS line,
    concat('#', "left"(lpad(to_hex((wazett_routes.id::double precision * character_length(wazett_routes.name)::double precision / (( SELECT max(wazett_routes_1.id * character_length(wazett_routes_1.name)) AS max
           FROM wazett_routes wazett_routes_1))::double precision * 10000000::double precision)::bigint), 6, '0'::text), 6)) AS color
   FROM wazett_routes;

CREATE OR REPLACE VIEW analytic.v_lkpr_route_details
AS SELECT re.id,
    re.name,
    re.route_code,
    re.route_name,
    re.from_name,
    re.to_name,
    re.route_type,
    re.route_num,
    re.route_variant,
        CASE
            WHEN re.route_variant = 'A1'::text THEN 'Na letiště'::text
            WHEN re.route_variant = 'B1'::text THEN 'Z letiště'::text
            ELSE 'Ostatní'::text
        END AS direction,
    drn.route_group_name,
    drn.order_idx
   FROM ( SELECT rc.id,
            rc.name,
            rc.route_code,
            rc.route_name,
            rc.from_name,
            rc.to_name,
            split_part(rc.route_code, '-'::text, 1) AS route_type,
            split_part(rc.route_code, '-'::text, 2) AS route_num,
            split_part(rc.route_code, '-'::text, 3) AS route_variant
           FROM ( SELECT r.id,
                    r.name,
                    split_part(r.name, ' '::text, 1) AS route_code,
                    "substring"(r.name, "position"(r.name, ' '::text) + 1) AS route_name,
                    r.from_name,
                    r.to_name
                   FROM wazett_routes r
                  WHERE r.feed_id = 1) rc) re
     LEFT JOIN analytic.lkpr_dashboard_route_names drn ON re.route_type = drn.route_type AND re.route_num = drn.route_num
  ORDER BY drn.order_idx;
 
CREATE OR REPLACE VIEW public.v_lkpr_export
AS SELECT COALESCE(vrd.id, wrl.route_id) AS route_id,
    vrd.name,
    vrd.route_name,
    vrd.from_name,
    vrd.to_name,
    vrd.route_num,
    vrd.direction,
    vrd.order_idx,
    to_timestamp((wrl.update_time / 1000)::double precision) AS update_time,
    wrl."time" AS actual_time,
    round(wrl.length::numeric * 3.6 / wrl."time"::numeric) AS actual_speed,
    wrl.historic_time,
    round(wrl.length::numeric * 3.6 / wrl.historic_time::numeric) AS historic_speed,
    wrl.length
   FROM analytic.v_lkpr_route_details vrd
     FULL JOIN wazett_route_lives wrl ON vrd.id = wrl.route_id
  WHERE vrd.name ~~ 'TN-%'::text OR vrd.route_code ~~ 'TN-%'::text;
