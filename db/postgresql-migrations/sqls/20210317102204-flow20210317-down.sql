/* Replace with your SQL commands */

drop view analytic.v_pedestrians_flow_quality;
drop view analytic.v_pedestrians_detections_15min;
drop view analytic.v_pedestrians_detections_daily;

drop TABLE analytic.pedestrians_locations_gates;
drop TABLE analytic.pedestrians_locations_list;
drop table analytic.pedestrians_wifi;