CREATE TABLE python.grants_applicants (
	nazev text NULL,
	ico text NULL,
	pravni_forma text NULL,
	ulice text NULL,
	cislo_popisne text NULL,
	cislo_orientacni text NULL,
	mestska_cast text NULL,
	psc text NULL,
	obec text NULL,
	id_zadatel text NULL PRIMARY KEY,  
	updated_at timestamptz NULL
);

CREATE TABLE python.grants_projects (
	cislo_projektu text NULL,
	nazev_projektu text NULL,
	popis text NULL,
	stav text NULL,
	nazev_programu text NULL,
	typ_dotace text NULL,
	nazev_oblasti text NULL,
	ucel_dotace text NULL,
	rok_od int4 NULL,
	rok_do int4 NULL,
	castka_naklady float4 NULL,
	castka_pozadovana float4 NULL,
	castka_pridelena float4 NULL,
	castka_vycerpana float4 NULL,
	castka_vyuctovana float4 NULL,
	id_projekt text NULL PRIMARY KEY,
	id_zadatel text NULL REFERENCES python.grants_applicants (id_zadatel),
	updated_at timestamptz NULL
);

CREATE TABLE python.grants_resolutions (
	cislo_usneseni text NULL,
	datum_schvaleni date NULL,
	schvalil text NULL,
	cislo_smlouvy text NULL,
	castka_pridelena float4 NULL,
	id_projekt text NULL REFERENCES python.grants_projects (id_projekt),
	id_zadatel text NULL REFERENCES python.grants_applicants (id_zadatel),
	updated_at timestamptz NULL
);
