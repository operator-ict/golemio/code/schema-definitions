CREATE SEQUENCE public.parkings_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

CREATE TABLE public.parkings
(
    "id" bigint NOT NULL UNIQUE DEFAULT nextval('parkings_id_seq'::regclass),
    "source" character varying(255) NOT NULL,
    "sourceId" character varying(255) NOT NULL,
    "dataProvider" text,
    "name" character varying(255) NOT NULL,
    "usageScenario" json,
    "dateModified" bigint,
    "address" json,
    "location" geometry,
    "areaServed" character varying(255),
    "paymentUrl" text,
    -- averageOccupancy // TODO
    "totalSpotNumber" INTEGER,

    create_batch_id bigint,
    created_at timestamp with time zone,
    created_by character varying(150),
    update_batch_id bigint,
    updated_at timestamp with time zone,
    updated_by character varying(150),

    CONSTRAINT parkings_pk PRIMARY KEY ("source", "sourceId")
);

CREATE SEQUENCE public.parkings_measurements_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

CREATE TABLE public.parkings_measurements
(
    "id" bigint NOT NULL DEFAULT nextval('parkings_measurements_id_seq'::regclass),
    "source" character varying(255) NOT NULL,
    "sourceId" character varying(255) NOT NULL,
    "availableSpotNumber" integer,
    "closedSpotNumber" integer,
    "occupiedSpotNumber" integer,
    "totalSpotNumber" integer,
    "dateModified" bigint,

    create_batch_id bigint,
    created_at timestamp with time zone,
    created_by character varying(150),
    update_batch_id bigint,
    updated_at timestamp with time zone,
    updated_by character varying(150),

    CONSTRAINT parkings_measurements_pk PRIMARY KEY ("id")
);
