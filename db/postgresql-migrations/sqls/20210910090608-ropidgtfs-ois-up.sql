CREATE TABLE public.ropidgtfs_ois
(
  ois integer NOT NULL,
  node integer NOT NULL,
  name varchar(255) NOT NULL,

  create_batch_id bigint,
  created_at timestamptz,
  created_by varchar(150),
  update_batch_id bigint,
  updated_at timestamptz,
  updated_by varchar(150),
  CONSTRAINT ropidgtfs_ois_pkey PRIMARY KEY (ois)
);
