CREATE TABLE public.flow_cubes (
    "id" integer NOT NULL, --
    "name" varchar(150), --

    "create_batch_id" bigint,
    "created_at" timestamp with time zone,
    "created_by" character varying(150),
    "update_batch_id" bigint,
    "updated_at" timestamp with time zone,
    "updated_by" character varying(150),
    CONSTRAINT flow_cubespkey PRIMARY KEY (id)
);

CREATE TABLE public.flow_analytics (
    "id" integer NOT NULL, --
    "name" varchar(150), --
    "sequence_number" integer NOT NULL, --

    "create_batch_id" bigint,
    "created_at" timestamp with time zone,
    "created_by" character varying(150),
    "update_batch_id" bigint,
    "updated_at" timestamp with time zone,
    "updated_by" character varying(150),
    CONSTRAINT flow_analyticspkey PRIMARY KEY (id, sequence_number)
);

CREATE TABLE public.flow_sinks (
    "id" integer NOT NULL, --
    "name" varchar(150), --
    "history_start_timestamp" bigint NOT NULL, --
    "output_value_type" varchar(150), --

    "create_batch_id" bigint,
    "created_at" timestamp with time zone,
    "created_by" character varying(150),
    "update_batch_id" bigint,
    "updated_at" timestamp with time zone,
    "updated_by" character varying(150),
    CONSTRAINT flow_sinkspkey PRIMARY KEY (id, output_value_type)
);

CREATE TABLE public.flow_measurements (
    "sink_id" integer NOT NULL, --
    "cube_id" integer NOT NULL, --
    "sequence_number" integer NOT NULL, --
    "analytic_id" integer NOT NULL, --
    "start_timestamp" bigint NOT NULL, --
    "end_timestamp" bigint NOT NULL, --
    "category" varchar(150), --
    "value" integer NOT NULL, --

    "create_batch_id" bigint,
    "created_at" timestamp with time zone,
    "created_by" character varying(150),
    "update_batch_id" bigint,
    "updated_at" timestamp with time zone,
    "updated_by" character varying(150),
    CONSTRAINT flow_measurementspkey PRIMARY KEY (sink_id, start_timestamp, end_timestamp, category, sequence_number)
);
