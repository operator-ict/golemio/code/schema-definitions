drop VIEW public.v_waze_potholes_last_days;

drop VIEW public.v_vehiclepositions_trips_v1;
drop VIEW public.v_vehiclepositions_positions_v1;
drop VIEW public.v_vehiclepositions_last_position_v1;
drop VIEW public.v_vehiclepositions_last_position;



drop VIEW public.v_ropidgtfs_trips_stop_times_view;
drop VIEW public.v_ropidgtfs_trips_shapes_view;
drop MATERIALIZED VIEW public.v_ropidgtfs_departures;
drop MATERIALIZED VIEW public.v_ropidgtfs_services_first14days;
drop MATERIALIZED VIEW public.v_ropidgtfs_trips_minmaxsequences;


--waze
DROP TABLE public.wazeccp_roads;
DROP TABLE public.wazeccp_jams;
DROP TABLE public.wazeccp_irregularities;
DROP TABLE public.wazeccp_alerts;
DROP TABLE public.wazeccp_alert_types;
-- vehicle
DROP TABLE public.vehiclepositions_vehicle_types;
DROP TABLE public.vehiclepositions_trips;
DROP TABLE public.vehiclepositions_stops;
DROP TABLE public.vehiclepositions_runs_messages;
DROP TABLE public.vehiclepositions_runs;
DROP TABLE public.vehiclepositions_positions;

DROP TABLE public.ropidvymi_events_stops;
DROP TABLE public.ropidvymi_events_routes;
DROP TABLE public.ropidvymi_events;
DROP TABLE public.ropidgtfs_trips;
DROP TABLE public.ropidgtfs_stops;
DROP TABLE public.ropidgtfs_stop_times;
DROP TABLE public.ropidgtfs_shapes;
DROP TABLE public.ropidgtfs_run_numbers;
DROP TABLE public.ropidgtfs_routes;
DROP TABLE public.ropidgtfs_ois;
DROP TABLE public.ropidgtfs_metadata;
DROP TABLE public.ropidgtfs_cis_stops;
DROP TABLE public.ropidgtfs_cis_stop_groups;
DROP TABLE public.ropidgtfs_calendar_dates;

DROP TABLE public.ropidgtfs_calendar;
DROP TABLE public.ropidgtfs_agency;
DROP TABLE public.ropid_departures_presets;
DROP TABLE public.ropid_departures_directions;

DROP TABLE public.ndic_traffic_restrictions;
DROP TABLE public.ndic_traffic_info;
DROP TABLE public.fcd_traff_params_part;

DROP TABLE public.airqualitystations_measurements;
DROP TABLE public.airqualitystations_indexes;
DROP TABLE public.airqualitystations_index_types;
DROP TABLE public.airqualitystations_component_types;
DROP TABLE public.airqualitystations;