-- analytic.v_uzis_covid19_municipalities source
drop VIEW analytic.v_uzis_covid19_municipalities;
CREATE OR REPLACE VIEW analytic.v_uzis_covid19_municipalities
AS SELECT eo.datum,
    eo.kraj_kod,
    eo.kraj_nazev,
    eo.okres_kod,
    eo.okres_nazev,
    eo.obec_kod,
    eo.obec_nazev,
    population_regions.population AS pocet_obyvatel_kraj,
    population_districts.population AS pocet_obyvatel_okres,
    eo.kumulativni_pocet_pozitivnich_osob,
    eo.kumulativni_pocet_zemrelych,
    eo.kumulativni_pocet_vylecenych,
        CASE
            WHEN eo.kumulativni_pocet_zemrelych IS NULL THEN 0::numeric
            ELSE eo.kumulativni_pocet_zemrelych::numeric
        END -
        CASE
            WHEN lag(eo.kumulativni_pocet_zemrelych) OVER (PARTITION BY eo.obec_kod ORDER BY eo.datum) IS NULL THEN 0::numeric
            ELSE lag(eo.kumulativni_pocet_zemrelych) OVER (PARTITION BY eo.obec_kod ORDER BY eo.datum)::numeric
        END AS pocet_zemrelych_den,
    eo.incidence AS pocet_pozitivnich_osob_den,
        CASE
            WHEN eo.prevalence IS NULL THEN 0::numeric
            ELSE eo.prevalence::numeric
        END -
        CASE
            WHEN lag(eo.prevalence) OVER (PARTITION BY eo.obec_kod ORDER BY eo.datum) IS NULL THEN 0::numeric
            ELSE lag(eo.prevalence) OVER (PARTITION BY eo.obec_kod ORDER BY eo.datum)::numeric
        END AS pocet_aktivnich_pripadu_den,
    eo.prevalence AS aktualni_pocet_aktivnich_pripadu,
    eo.aktualni_pocet_hospitalizovanych_osob,
        CASE
            WHEN eo.aktualni_pocet_hospitalizovanych_osob IS NULL THEN 0::numeric::double precision
            ELSE eo.aktualni_pocet_hospitalizovanych_osob
        END -
        CASE
            WHEN lag(eo.aktualni_pocet_hospitalizovanych_osob) OVER (PARTITION BY eo.obec_kod ORDER BY eo.datum) IS NULL THEN 0::numeric::double precision
            ELSE lag(eo.aktualni_pocet_hospitalizovanych_osob) OVER (PARTITION BY eo.obec_kod ORDER BY eo.datum)
        END AS pocet_hospitalizovanych_osob,
    eo.casova_znamka AS posledni_aktualizace,
        CASE
            WHEN eo.datum = (date_trunc('day'::text, now()) - '1 day'::interval) THEN true
            ELSE false
        END AS vcerejsi_den,
    eo.kumulativni_pocet_hospitalizovanych_osob,
    eo.kumulativni_pocet_zemrelych + eo.kumulativni_pocet_vylecenych AS aktualni_pocet_neaktivnich_pripadu,
        CASE
            WHEN eo.datum >= (CURRENT_DATE - '7 days'::interval) THEN true
            ELSE false
        END AS last_7days,
        CASE
            WHEN eo.datum >= (CURRENT_DATE - '14 days'::interval) THEN true
            ELSE false
        END AS last_14days,
        CASE
            WHEN eo.datum >= (CURRENT_DATE - '7 days'::interval) THEN 'Posledních 7 dní'::text
            ELSE NULL::text
        END AS last_7days_desc,
        CASE
            WHEN eo.datum >= (CURRENT_DATE - '14 days'::interval) THEN 'Posledních 14 dní'::text
            ELSE NULL::text
        END AS last_14days_desc
   FROM uzis.covid19_municipalities eo
     LEFT JOIN uzis.population_regions ON eo.kraj_kod = population_regions.region_code
     LEFT JOIN uzis.population_districts ON eo.okres_kod = population_districts.district_code
  WHERE eo.datum < CURRENT_DATE;