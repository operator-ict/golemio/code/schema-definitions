-- drop views
DROP VIEW analytic.v_obis_proposals_all;
DROP VIEW analytic.v_obis_submitter_comments;
DROP VIEW analytic.v_obis_comments;
DROP VIEW analytic.v_obis_proposals;
DROP VIEW analytic.v_obis_processor;
DROP VIEW analytic.v_obis_submitter;

--drop tables
DROP TABLE python.obis_proposals;
DROP TABLE python.obis_proposals_departments;
DROP TABLE python.obis_proposals_employees;