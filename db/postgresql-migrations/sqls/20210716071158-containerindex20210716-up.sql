CREATE INDEX if not exists containers_containers_trash_type
ON public.containers_containers 
USING hash (trash_type);

CREATE INDEX if not exists containers_containers_station_code
ON public.containers_containers 
USING hash (station_code);
