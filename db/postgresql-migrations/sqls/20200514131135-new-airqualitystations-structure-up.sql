
CREATE TABLE public.airqualitystations_index_types (
    "id" integer NOT NULL,
    "index_code" varchar(50) NOT NULL,
    "limit_gte" double precision,
    "limit_lt" double precision,
    "color" varchar(50) NOT NULL,
    "color_text" varchar(50) NOT NULL,
    "description_cs" text NOT NULL,
    "description_en" text NOT NULL,

    CONSTRAINT "airqualitystations_index_types_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

INSERT INTO public.airqualitystations_index_types ("id", "index_code", "limit_gte", "limit_lt", "color", "color_text", "description_cs", "description_en") VALUES
    (1, '1A', '0.00', '0.34', '009900', '000000', 'velmi dobrá až dobrá', 'very good to good'),
    (2, '1B', '0.34', '0.67', '00CC00', '000000', 'velmi dobrá až dobrá', 'very good to good'),
    (3, '2A', '0.67', '1.00', 'FFF200', '000000', 'přijatelná', 'acceptable'),
    (4, '2B', '1.00', '1.50', 'FAA61A', 'FFFFFF', 'přijatelná', 'acceptable'),
    (5, '3A', '1.50', '2.00', 'ED1C24', 'FFFFFF', 'zhoršená až špatná', 'aggravated to bad'),
    (6, '3B', '2.00', NULL, '671F20', '000000', 'zhoršená až špatná', 'aggravated to bad'),
    (7, '0', NULL, NULL, 'FFFFFF', '000000', 'neúplná data', 'incomplete data'),
    (8, '-1', NULL, NULL, 'CFCFCF', '000000', 'index nestanoven', 'index not determined');

CREATE TABLE public.airqualitystations_component_types (
    "id" integer NOT NULL,
    "component_code" varchar(50) NOT NULL,
    "unit" varchar(50) NOT NULL,
    "description_cs" text NOT NULL,
    "description_en" text NOT NULL,

    CONSTRAINT "airqualitystations_component_types_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

INSERT INTO public.airqualitystations_component_types ("id", "component_code", "unit", "description_cs", "description_en") VALUES
    (1, 'SO2', 'µg/m³', 'oxid siřičitý', 'sulphur dioxide'),
    (2, 'NO2', 'µg/m³', 'oxid dusičitý', 'nitrogen dioxide'),
    (3, 'PM10', 'µg/m³', 'částice PM10', 'particles PM10'),
    (4, 'O3', 'µg/m³', 'ozón', 'ozone'),
    (5, 'O3_Model', 'µg/m³', 'ozón (model)', 'ozone (model)'),
    (6, 'PM2_5', 'µg/m³', 'částice PM2,5', 'fine particles PM2.5'),
    (7, 'CO', 'µg/m³', 'oxid uhelnatý', 'carbon monoxide');


CREATE TABLE public.airqualitystations (
    "id" varchar(255) NOT NULL, -- States[].Code + "_" + States[].Regions[].Code + "_" + States[].Regions[].Stations[].Code, e.g. CZ_A_AKALA
    "state_code" varchar(50) NOT NULL, -- States[].Code, e.g. CZ
    "state_name" varchar(255) NOT NULL, -- States[].Name, e.g. Česká republika
    "region_code" varchar(50) NOT NULL, -- States[].Regions[].Code, e.g. A
    "region_name" varchar(255) NOT NULL, -- States[].Regions[].Name, e.g. Praha
    "station_vendor_id" varchar(255) NOT NULL, -- States[].Regions[].Stations[].Code, e.g. AKALA
    "station_name" varchar(255) NOT NULL, -- States[].Regions[].Stations[].Name, e.g. Praha 8-Karlín
    "owner" varchar(255), -- States[].Regions[].Stations[].Owner, e.g. ČHMÚ
    "classification" TEXT, -- States[].Regions[].Stations[].Classif, e.g. dopravní
    "latitude" double precision, -- States[].Regions[].Stations[].Lat, e.g. 50.094238
    "longitude" double precision, -- States[].Regions[].Stations[].Lon, e.g. 14.442049

    "create_batch_id" bigint,
    "created_at" timestamp with time zone,
    "created_by" character varying(150),
    "update_batch_id" bigint,
    "updated_at" timestamp with time zone,
    "updated_by" character varying(150),
    CONSTRAINT airqualitystations_pkey PRIMARY KEY (id)
);

CREATE TABLE public.airqualitystations_measurements (
    "station_id" varchar(255) NOT NULL, -- airqualitystations.id, e.g. CZ_A_AKALA
    "measured_from" bigint NOT NULL, -- States[].DateFromUTC, e.g. 1592429492940
    "measured_to" bigint NOT NULL, -- States[].DateToUTC, e.g. 1592449492940
    "component_code" varchar(50) NOT NULL, -- airqualitystations_metadata_components.component_code, e.g. SO2
    "aggregation_interval" varchar(50) NOT NULL, -- States[].Regions[].Stations[].Components[].Int, e.g. 3h
    "value" double precision, -- States[].Regions[].Stations[].Components[].Val, e.g. 50.3

    "create_batch_id" bigint,
    "created_at" timestamp with time zone,
    "created_by" character varying(150),
    "update_batch_id" bigint,
    "updated_at" timestamp with time zone,
    "updated_by" character varying(150),
    CONSTRAINT airqualitystations_measurements_pkey PRIMARY KEY (station_id, measured_from, measured_to, component_code, aggregation_interval)
);

CREATE INDEX airqualitystations_measurements_component_code
    ON public.airqualitystations_measurements USING btree
    (component_code COLLATE pg_catalog."default" ASC NULLS LAST)
    TABLESPACE pg_default;

CREATE INDEX airqualitystations_measurements_aggregation_interval
    ON public.airqualitystations_measurements USING btree
    (aggregation_interval COLLATE pg_catalog."default" ASC NULLS LAST)
    TABLESPACE pg_default;

CREATE TABLE public.airqualitystations_indexes (
    "station_id" varchar(255) NOT NULL, -- airqualitystations.id, e.g. CZ_A_AKALA
    "measured_from" bigint NOT NULL, -- States[].DateFromUTC, e.g. 159242949294
    "measured_to" bigint NOT NULL, -- States[].DateToUTC, e.g. 159244949294
    "index_code" varchar(50) NOT NULL, -- airqualitystations_metadata_indexes.index_code, e.g. 1A

    "create_batch_id" bigint,
    "created_at" timestamp with time zone,
    "created_by" character varying(150),
    "update_batch_id" bigint,
    "updated_at" timestamp with time zone,
    "updated_by" character varying(150),
    CONSTRAINT airqualitystations_indexes_pkey PRIMARY KEY (station_id, measured_from, measured_to)
);
