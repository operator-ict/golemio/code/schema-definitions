CREATE TABLE if not exists uzis.history_covid19_hospital_capacity_regional (
	datum timestamp NULL,
	kraj_nuts_kod text NULL,
	ecmo_kapacita_volna int8 NULL,
	ecmo_kapacita_celkem int8 NULL,
	upv_kapacita_volna int8 NULL,
	upv_kapacita_celkem int8 NULL,
	crrt_kapacita_volna int8 NULL,
	crrt_kapacita_celkem int8 NULL,
	ihd_kapacita_volna int8 NULL,
	ihd_kapacita_celkem int8 NULL,
	luzka_aro_jip_kapacita_celkem int8 NULL,
	luzka_aro_jip_kapacita_volna_covid_pozitivni int8 NULL,
	luzka_aro_jip_kapacita_volna_covid_negativni int8 NULL,
	luzka_standard_kyslik_kapacita_celkem int8 NULL,
	luzka_standard_kyslik_kapacita_volna_covid_pozitivni int8 NULL,
	luzka_standard_kyslik_kapacita_volna_covid_negativni int8 NULL,
	ventilatory_prenosne_kapacita_volna int8 NULL,
	ventilatory_prenosne_kapacita_celkem int8 NULL,
	ventilatory_operacni_sal_kapacita_volna int8 NULL,
	ventilatory_operacni_sal_kapacita_celkem int8 NULL,
	reprofilizovana_kapacita_luzka_aro_jip_kapacita_volna float8 NULL,
	reprofilizovana_kapacita_luzka_aro_jip_kapacita_celkem float8 NULL,
	reprofilizovana_kapacita_luzka_aro_jip_kapacita_planovana float8 NULL,
	reprofilizovana_kapacita_luzka_standard_kyslik_kapacita_volna float8 NULL,
	reprofilizovana_kapacita_luzka_standard_kyslik_kapacita_celkem float8 NULL,
	reprofilizovana_kapacita_luzka_standard_kyslik_kapacita_planova float8 NULL,
	updated_at timestamp NULL,
	updated_by text NULL,
	created_at timestamp NULL,
	created_by text NULL
);


--SET search_path TO uzis,public;

--ALTER TABLE uzis.covid19_hospital_capacity_regional
--RENAME TO history_covid19_hospital_capacity_regional;

CREATE TABLE if not exists uzis.covid19_regions (
	casova_znamka timestamp NULL,
	"Datum" timestamp NULL,
	"Kraj" text NULL,
	kumulativni_pocet_pozitivnich_osob int8 NULL,
	kumulativni_pocet_hospitalizovanych_osob float8 NULL,
	aktualni_pocet_hospitalizovanych_osob float8 NULL,
	zemreli_za_hospitalizace float8 NULL,
	incidence int8 NULL,
	kumulativni_pocet_vylecenych int8 NULL,
	kumulativni_pocet_zemrelych int8 NULL,
	prevalence int8 NULL,
	podil_65__na_kumulativnim_poctu_pozitivnich float8 NULL,
	podil_65__na_kumulativnim_poctu_hospitalizovanych float8 NULL,
	podil_65__na_aktualnim_poctu_hospitalizovanych float8 NULL,
	podil_65__na_incidenci float8 NULL,
	podil_65__na_kumulativnim_poctu_vylecenych float8 NULL,
	podil_65__na_kumulativnim_poctu_zemrelych float8 NULL,
	podil_65__na_prevalenci float8 NULL,
	update_batch_id int8 NULL,
	created_at timestamptz(0) NULL,
	created_by varchar(150) NULL,
	create_batch_id int8 NULL,
	updated_at timestamptz(0) NULL,
	updated_by varchar(150) NULL
);

ALTER TABLE uzis.covid19_regions 
RENAME COLUMN "Datum" TO datum;

ALTER TABLE uzis.covid19_regions 
RENAME COLUMN "Kraj" TO kraj;


-- franz.v_uzis_covid19_vaccinated_split source

CREATE VIEW analytic.v_uzis_covid19_vaccinated_split
AS WITH ockovani AS (
         SELECT v_uzis_covid19_vaccination_regions_details.datum_vakcinace,
            v_uzis_covid19_vaccination_regions_details.kraj_bydliste,
            sum(sum(v_uzis_covid19_vaccination_regions_details.pocet_davek)) OVER (PARTITION BY v_uzis_covid19_vaccination_regions_details.kraj_bydliste ORDER BY v_uzis_covid19_vaccination_regions_details.datum_vakcinace) AS pocet_uzavrenych_ockovani,
            sum(sum(v_uzis_covid19_vaccination_regions_details.pocet_davek) FILTER (WHERE v_uzis_covid19_vaccination_regions_details.vekova_skupina = ANY (ARRAY['65-69'::text, '70-74'::text, '75-79'::text, '80+'::text]))) OVER (PARTITION BY v_uzis_covid19_vaccination_regions_details.kraj_bydliste ORDER BY v_uzis_covid19_vaccination_regions_details.datum_vakcinace) AS pocet_uzavrenych_ockovani65,
            sum(sum(
                CASE
                    WHEN v_uzis_covid19_vaccination_regions_details.vekova_skupina = '12-17'::text THEN 15
                    WHEN v_uzis_covid19_vaccination_regions_details.vekova_skupina = '18-24'::text THEN 21
                    WHEN v_uzis_covid19_vaccination_regions_details.vekova_skupina = '25-29'::text THEN 27
                    WHEN v_uzis_covid19_vaccination_regions_details.vekova_skupina = '30-34'::text THEN 32
                    WHEN v_uzis_covid19_vaccination_regions_details.vekova_skupina = '35-39'::text THEN 37
                    WHEN v_uzis_covid19_vaccination_regions_details.vekova_skupina = '40-44'::text THEN 42
                    WHEN v_uzis_covid19_vaccination_regions_details.vekova_skupina = '45-49'::text THEN 47
                    WHEN v_uzis_covid19_vaccination_regions_details.vekova_skupina = '50-54'::text THEN 52
                    WHEN v_uzis_covid19_vaccination_regions_details.vekova_skupina = '55-59'::text THEN 57
                    WHEN v_uzis_covid19_vaccination_regions_details.vekova_skupina = '60-64'::text THEN 62
                    WHEN v_uzis_covid19_vaccination_regions_details.vekova_skupina = '65-69'::text THEN 67
                    WHEN v_uzis_covid19_vaccination_regions_details.vekova_skupina = '70-74'::text THEN 72
                    WHEN v_uzis_covid19_vaccination_regions_details.vekova_skupina = '75-79'::text THEN 77
                    WHEN v_uzis_covid19_vaccination_regions_details.vekova_skupina = '80+'::text THEN 85
                    ELSE NULL::integer
                END * v_uzis_covid19_vaccination_regions_details.pocet_davek)) OVER (PARTITION BY v_uzis_covid19_vaccination_regions_details.kraj_bydliste ORDER BY v_uzis_covid19_vaccination_regions_details.datum_vakcinace) / sum(sum(v_uzis_covid19_vaccination_regions_details.pocet_davek)) OVER (PARTITION BY v_uzis_covid19_vaccination_regions_details.kraj_bydliste ORDER BY v_uzis_covid19_vaccination_regions_details.datum_vakcinace) AS vek_prumer
           FROM analytic.v_uzis_covid19_vaccination_regions_details
          WHERE v_uzis_covid19_vaccination_regions_details.poradi_davky = 2
          GROUP BY v_uzis_covid19_vaccination_regions_details.datum_vakcinace, v_uzis_covid19_vaccination_regions_details.kraj_bydliste
        )
 SELECT i.datum,
    date_trunc('week'::text, i.datum) AS tyden,
    date_trunc('month'::text, i.datum) AS mesic,
        CASE
            WHEN i.datum = (( SELECT max(covid19_vaccinated_split_incidence.datum) AS max
               FROM uzis.covid19_vaccinated_split_incidence)) THEN 'poslední den'::text
            ELSE NULL::text
        END AS last_day,
        CASE
            WHEN i.datum > ((( SELECT max(covid19_vaccinated_split_incidence.datum) AS max
               FROM uzis.covid19_vaccinated_split_incidence)) - '7 days'::interval) THEN 'posledních 7 dnů'::text
            ELSE NULL::text
        END AS last_7_days,
        CASE
            WHEN i.datum > ((( SELECT max(covid19_vaccinated_split_incidence.datum) AS max
               FROM uzis.covid19_vaccinated_split_incidence)) - '14 days'::interval) THEN 'posledních 14 dnů'::text
            ELSE NULL::text
        END AS last_14_days,
        CASE
            WHEN i.datum < date_trunc('week'::text, CURRENT_DATE::timestamp with time zone) THEN false
            ELSE true
        END AS unfinished_calendar_week,
    i.krajkod,
    cr."kraj-nazev" AS kraj_nazev,
    pr.population,
    par.population65,
    ockovani.pocet_uzavrenych_ockovani,
    ockovani.pocet_uzavrenych_ockovani65,
    ockovani.vek_prumer,
    i.poz_bez_ockovani + i.poz_po_prvni AS incidence_neockovani,
    (100000 * (i.poz_bez_ockovani + i.poz_po_prvni))::numeric / (pr.population::numeric - ockovani.pocet_uzavrenych_ockovani) AS incidence_neockovani_100tis,
    i.poz_po_ukonceni + i.poz_po_ukonceni_s_posilujici AS incidence_ockovani,
    (100000 * (i.poz_po_ukonceni + i.poz_po_ukonceni_s_posilujici))::numeric / ockovani.pocet_uzavrenych_ockovani AS incidence_ockovani_100tis,
    i.poz_vek_bez_ockovani_vek AS incidence_prumerny_vek_neockovani,
    i.poz_vek_po_ukonceni AS incidence_prumerny_vek_ockovani,
    i65.p65_bez_ockovani + i65.p65_po_prvni AS incidence65_neockovani,
    (100000 * (i65.p65_bez_ockovani + i65.p65_po_prvni))::numeric / (par.population65 - ockovani.pocet_uzavrenych_ockovani65) AS incidence65_neockovani_100tis,
    i65.p65_po_ukonceni + i65.p65_po_ukonceni_s_posilujici AS incidence65_ockovani,
    (100000 * (i65.p65_po_ukonceni + i65.p65_po_ukonceni_s_posilujici))::numeric / ockovani.pocet_uzavrenych_ockovani65 AS incidence65_ockovani_100tis,
    i65.p65_vek_bez_ockovani_vek AS incidence65_prumerny_vek_neockovani,
    i65.p65_vek_po_ukonceni AS incidence65_prumerny_vek_ockovani,
    h.hos_bez_ockovani + h.hos_po_prvni AS hospitalizace_neockovani,
    (100000 * (h.hos_bez_ockovani + h.hos_po_prvni))::numeric / (pr.population::numeric - ockovani.pocet_uzavrenych_ockovani) AS hospitalizace_neockovani_100tis,
    h.hos_po_ukonceni + h.hos_po_ukonceni_s_posilujici AS hospitalizace_ockovani,
    (100000 * (h.hos_po_ukonceni + h.hos_po_ukonceni_s_posilujici))::numeric / ockovani.pocet_uzavrenych_ockovani AS hospitalizace_ockovani_100tis,
    h.hos_vek_bez_ockovani_vek AS hospitalizace_prumerny_vek_neockovani,
    h.hos_vek_po_ukonceni1 AS hospitalizace_prumerny_vek_ockovani,
    icu.jip_bez_ockovani + icu.jip_po_prvni AS jip_neockovani,
    (100000 * (icu.jip_bez_ockovani + icu.jip_po_prvni))::numeric / (pr.population::numeric - ockovani.pocet_uzavrenych_ockovani) AS jip_neockovani_100tis,
    icu.jip_po_ukonceni + icu.jip_po_ukonceni_s_posilujici AS jip_ockovani,
    (100000 * (icu.jip_po_ukonceni + icu.jip_po_ukonceni_s_posilujici))::numeric / ockovani.pocet_uzavrenych_ockovani AS jip_ockovani_100tis,
    icu.jip_vek_bez_ockovani_vek AS jip_prumerny_vek_neockovani,
    icu.jip_vek_po_ukonceni AS jip_prumerny_vek_ockovani,
    d.zem_bez_ockovani + d.zem_po_prvni AS zemreli_neockovani,
    (100000 * (d.zem_bez_ockovani + d.zem_po_prvni))::numeric / (pr.population::numeric - ockovani.pocet_uzavrenych_ockovani) AS zemreli_neockovani_100tis,
    d.zem_po_ukonceni + d.zem_po_ukonceni_s_posilujici AS zemreli_ockovani,
    (100000 * (d.zem_po_ukonceni + d.zem_po_ukonceni_s_posilujici))::numeric / ockovani.pocet_uzavrenych_ockovani AS zemreli_ockovani_100tis,
    d.zem_vek_bez_ockovani_vek AS zemreli_prumerny_vek_neockovani,
    d.zem_vek_po_ukonceni AS zemreli_prumerny_vek_ockovani
   FROM uzis.covid19_vaccinated_split_incidence i
     JOIN uzis.covid19_vaccinated_split_incidence65 i65 ON i.datum = i65.datum AND i.krajkod = i65.krajkod
     JOIN uzis.covid19_vaccinated_split_hospitalized h ON i.datum = h.datum AND i.krajkod = h.krajkod
     JOIN uzis.covid19_vaccinated_split_icu icu ON i.datum = icu.datum AND i.krajkod = icu.krajkod
     JOIN uzis.covid19_vaccinated_split_deaths d ON i.datum = d.datum AND i.krajkod = d.krajkod
     LEFT JOIN uzis.population_regions pr ON i.krajkod = pr.region_code
     LEFT JOIN ( SELECT sum(population_age_regions.population) AS population65,
            population_age_regions.region_code
           FROM uzis.population_age_regions
          WHERE population_age_regions.age >= 65
          GROUP BY population_age_regions.region_code) par ON i.krajkod = par.region_code
     LEFT JOIN ockovani ON ockovani.datum_vakcinace = i.datum AND ockovani.kraj_bydliste = pr.region_name
     LEFT JOIN uzis.codebook_regions cr ON i.krajkod = cr."kraj-kod"
  WHERE i.datum >= '2021-02-01 00:00:00'::timestamp without time zone;


-- franz.v_pedestrians_flow_quality source
drop view analytic.v_pedestrians_flow_quality;
CREATE view analytic.v_pedestrians_flow_quality
AS WITH timeline_category AS (
         SELECT DISTINCT fs.cube_id,
            fs.id AS sink_id,
            timeline."timestamp"
           FROM flow_sinks fs
             LEFT JOIN ( SELECT generate_series(date_trunc('month'::text, CURRENT_DATE - '180 days'::interval)::timestamp with time zone, date_trunc('hour'::text, CURRENT_DATE::timestamp with time zone), '01:00:00'::interval) AS "timestamp") timeline ON true
        ), real_nrow AS (
         SELECT date_trunc('hour'::text, to_timestamp((flow_measurements.start_timestamp / 1000)::double precision)) AS date_trunc,
            flow_measurements.cube_id,
            flow_measurements.sink_id,
            count(*) AS n_row,
            count(*) FILTER (WHERE flow_measurements.category::text = 'pedestrian'::text) AS n_row_pedestrian
           FROM flow_measurements
          WHERE flow_measurements.start_timestamp::double precision >= (1000::double precision * date_part('epoch'::text, CURRENT_DATE - '220 days'::interval))
          GROUP BY (date_trunc('hour'::text, to_timestamp((flow_measurements.start_timestamp / 1000)::double precision))), flow_measurements.cube_id, flow_measurements.sink_id
        ), expected_nrow AS (
         SELECT flow_measurements.cube_id,
            flow_measurements.sink_id,
            count(DISTINCT flow_measurements.category) * 12 AS expected_nrow,
            12 AS expected_nrow_pedestrian
           FROM flow_measurements
          WHERE flow_measurements.start_timestamp::double precision >= (1000::double precision * date_part('epoch'::text, CURRENT_DATE - '220 days'::interval))
          GROUP BY flow_measurements.cube_id, flow_measurements.sink_id
        )
 SELECT t."timestamp",
    date_trunc('month'::text, t."timestamp")::date AS month,
    t.cube_id,
    t.sink_id,
    concat(t.cube_id, ' - ', t.sink_id) AS cube_sink,
    COALESCE(r.n_row, 0::bigint) AS n_row,
    COALESCE(r.n_row_pedestrian, 0::bigint) AS n_row_pedestrian,
    er.expected_nrow,
    er.expected_nrow_pedestrian
   FROM timeline_category t
     LEFT JOIN real_nrow r ON t."timestamp" = r.date_trunc AND t.cube_id = r.cube_id AND t.sink_id = r.sink_id
     LEFT JOIN expected_nrow er ON t.cube_id = er.cube_id AND t.sink_id = er.sink_id;