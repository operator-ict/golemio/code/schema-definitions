
-- vehiclepositions_trips

ALTER TABLE vehiclepositions_trips DROP cis_agency_name;
ALTER TABLE vehiclepositions_trips DROP cis_parent_route_name;
ALTER TABLE vehiclepositions_trips DROP cis_real_agency_name;
ALTER TABLE vehiclepositions_trips DROP cis_vehicle_registration_number;

-- vehiclepositions_positions

DROP VIEW IF EXISTS v_vehiclepositions_last_position;

ALTER TABLE vehiclepositions_positions DROP bearing;
ALTER TABLE vehiclepositions_positions DROP cis_last_stop_id;
ALTER TABLE vehiclepositions_positions DROP cis_last_stop_sequence;
ALTER TABLE vehiclepositions_positions DROP gtfs_last_stop_id;
ALTER TABLE vehiclepositions_positions DROP gtfs_last_stop_sequence;
ALTER TABLE vehiclepositions_positions DROP gtfs_next_stop_sequence;
ALTER TABLE vehiclepositions_positions DROP speed;

CREATE OR REPLACE VIEW v_vehiclepositions_last_position
  AS
    SELECT
      DISTINCT ON ("vehiclepositions_positions"."trips_id") "vehiclepositions_positions".*
    FROM "vehiclepositions_positions" AS "vehiclepositions_positions"
      JOIN "vehiclepositions_trips" AS "vehiclepositions_trips"
        ON ("vehiclepositions_trips"."id" = "vehiclepositions_positions"."trips_id" AND
            "vehiclepositions_trips"."gtfs_trip_id" IS NOT NULL)
    WHERE "vehiclepositions_positions"."created_at" > (NOW() - INTERVAL '5 min') AND "vehiclepositions_positions"."delay" IS NOT NULL
    ORDER BY "vehiclepositions_positions"."trips_id", "vehiclepositions_positions"."created_at" DESC;

-- vehiclepoitions_stops

ALTER TABLE vehiclepositions_stops DROP arrival_delay_type;
ALTER TABLE vehiclepositions_stops DROP CONSTRAINT vehiclepositions_stops_pkey;
ALTER TABLE vehiclepositions_stops ADD CONSTRAINT vehiclepositions_stops_pkey PRIMARY KEY (cis_stop_id, cis_stop_platform_code, cis_stop_sequence, trips_id);
