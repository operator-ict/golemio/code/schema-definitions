alter table demographics.census_gender_average_age
drop constraint census_gender_average_age_pkey;

alter table demographics.census_gender_average_age
add primary key (uzemi_kod, uroven, pohlavi_txt);
