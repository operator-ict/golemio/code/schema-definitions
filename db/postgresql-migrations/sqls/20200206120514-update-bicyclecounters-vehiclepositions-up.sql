-- VEHICLEPOSITIONS_TRIPS

ALTER TABLE "public"."vehiclepositions_trips" RENAME COLUMN "cis_line_number" TO "cis_trip_number";

CREATE OR REPLACE VIEW "public"."v_vehiclepositions_last_position"
  AS
    SELECT
      DISTINCT ON ("vehiclepositions_positions"."trips_id") "vehiclepositions_positions".*,
      "vehiclepositions_trips"."gtfs_route_id", "vehiclepositions_trips"."gtfs_route_short_name",
      "vehiclepositions_trips"."gtfs_trip_id", "vehiclepositions_trips"."gtfs_trip_headsign",
      "vehiclepositions_trips"."vehicle_type_id", "vehiclepositions_trips"."wheelchair_accessible"
    FROM "vehiclepositions_positions" AS "vehiclepositions_positions"
      JOIN "vehiclepositions_trips" AS "vehiclepositions_trips"
        ON ("vehiclepositions_trips"."id" = "vehiclepositions_positions"."trips_id" AND
            "vehiclepositions_trips"."gtfs_trip_id" IS NOT NULL)
    WHERE "vehiclepositions_positions"."created_at" > (NOW() - INTERVAL '5 min') AND "vehiclepositions_positions"."delay" IS NOT NULL
    ORDER BY "vehiclepositions_positions"."trips_id", "vehiclepositions_positions"."created_at" DESC;

CREATE OR REPLACE VIEW "public"."v_vehiclepositions_trips_v1"
  AS
    SELECT "v_vehiclepositions_trips_v1"."cis_line_id" AS "cis_id",
      "v_vehiclepositions_trips_v1"."cis_trip_number" AS "cis_number",
      "v_vehiclepositions_trips_v1"."sequence_id" AS "cis_order",
      "v_vehiclepositions_trips_v1"."cis_line_short_name" AS "cis_short_name",
      "v_vehiclepositions_trips_v1"."created_at",
      "v_vehiclepositions_trips_v1"."gtfs_route_id",
      "v_vehiclepositions_trips_v1"."gtfs_route_short_name",
      "v_vehiclepositions_trips_v1"."gtfs_trip_id",
      "v_vehiclepositions_trips_v1"."id",
      "v_vehiclepositions_trips_v1"."updated_at",
      "v_vehiclepositions_trips_v1"."start_cis_stop_id",
      "v_vehiclepositions_trips_v1"."start_cis_stop_platform_code",
      "v_vehiclepositions_trips_v1"."start_time",
      "v_vehiclepositions_trips_v1"."start_timestamp",
      "v_vehiclepositions_trips_v1"."vehicle_type_id" AS "vehicle_type",
      "v_vehiclepositions_trips_v1"."wheelchair_accessible",
      "v_vehiclepositions_trips_v1"."create_batch_id",
      "v_vehiclepositions_trips_v1"."created_by",
      "v_vehiclepositions_trips_v1"."update_batch_id",
      "v_vehiclepositions_trips_v1"."updated_by",
      "v_vehiclepositions_trips_v1"."agency_name_scheduled" AS "cis_agency_name",
      "v_vehiclepositions_trips_v1"."origin_route_name" AS "cis_parent_route_name",
      "v_vehiclepositions_trips_v1"."agency_name_real" AS "cis_real_agency_name",
      "v_vehiclepositions_trips_v1"."vehicle_registration_number" AS "cis_vehicle_registration_number"
    FROM "vehiclepositions_trips" AS "v_vehiclepositions_trips_v1";

UPDATE "public"."vehiclepositions_vehicle_types" SET
    "id" = '4',
    "abbreviation" = 'BusReg',
    "description_cs" = 'regionální autobus',
    "description_en" = 'regional bus'
WHERE "id" = '4';

-- BICYCLECOUNTERS

-- tabulka pro staré locations - kvůli in_camea_contract
create table analytic.camea_bikecounters_in_contract
(id int,location_id varchar(50),location_name varchar(50),route_name varchar(50),lat dec,lng dec,in_camea_contract boolean, old_id varchar(50));

-- last update camea
create or replace view analytic.v_camea_last_update
as select max(to_timestamp(measured_to/1000) AT TIME ZONE 'Europe/Prague')
from public.bicyclecounters_detections;

-- data quality - nová data
create or replace view analytic.v_camea_data_quality
as select to_timestamp(measured_from/1000)::date AS date,
       loc.vendor_id as location_id,
       loc."name" as location_name,
       dir.vendor_id as direction_id,
       dir."name" as direction_name,
       count(det.locations_id) lines,
       count(det.value) as detections,
       count(det.value)/count(det.locations_id)::decimal as ratio,
       old.in_camea_contract
       from public.bicyclecounters_detections det
       left join public.bicyclecounters_locations loc on det.locations_id = loc.id
       left join public.bicyclecounters_directions dir on det.directions_id = dir.id
       left join analytic.camea_bikecounters_in_contract old on loc.vendor_id = old.location_id
group by to_timestamp(measured_from/1000)::date,
       loc.vendor_id,
       loc."name",
       dir.vendor_id,
       dir."name",
       old.in_camea_contract;

-- disbalance
create or replace view analytic.v_camea_data_disbalance
as select
d1.datum,d1.locations_id,d1.location_name,
d1.direction_id dir1id,d1.direction_name dir1_name,
case when d1.dir1det is null then 0 else d1.dir1det end as dir1det,
d1.in_camea_contract,
case when d2.direction_id = d1.direction_id then 'není' else d2.direction_id end as dir2id,
case when d2.direction_id = d1.direction_id then 'není' else d2.direction_name end as dir2_name,
case when d2.direction_id = d1.direction_id then 0
     when dir2det is null then 0 else dir2det end as dir2det
from
        (select
            to_timestamp(measured_from/1000)::date AS datum,
            det.locations_id as locations_id,
            loc."name" as location_name,
            dir.vendor_id as direction_id,
            dir."name" as direction_name,
            old.in_camea_contract,
            sum(det.value) as dir1det
        from public.bicyclecounters_detections det
        left join public.bicyclecounters_locations loc on det.locations_id = loc.id
        left join public.bicyclecounters_directions dir on det.directions_id = dir.id
        left join analytic.camea_bikecounters_in_contract old on loc.vendor_id = old.location_id
        where directions_id in (select max(id) from public.bicyclecounters_directions group by locations_id)
        group by 1,2,3,4,5,6) d1
left join
        (select
            to_timestamp(measured_from/1000)::date AS datum,
            det2.locations_id as locations_id,
            loc."name" as location_name,
            dir.vendor_id as direction_id,
            dir."name" as direction_name,
            sum(det2.value) as dir2det
        from public.bicyclecounters_detections det2
        left join public.bicyclecounters_locations loc on det2.locations_id = loc.id
        left join public.bicyclecounters_directions dir on det2.directions_id = dir.id
        where directions_id in (select min(id) from public.bicyclecounters_directions group by locations_id)
        group by 1,2,3,4,5) d2
        on d1.datum = d2.datum and d1.locations_id = d2.locations_id;

-- view pro Valerii - aktuální den
create or replace view analytic.v_camea_bikecounters_all_data_today
as select t1.*,t2.temperature
from (select
			date_trunc('hour', to_timestamp(measured_from/1000)) AS datum,
			det.locations_id,
			loc."name" as location_name,
			old.in_camea_contract,
			loc.lat,
			loc.lng,
			loc.route,
			dir."name" as direction_name,
			case when sum(det.value) is null then 0 else sum(det.value) end as detections
		from public.bicyclecounters_detections det
		left join public.bicyclecounters_locations loc on det.locations_id = loc.id
		left join public.bicyclecounters_directions dir on det.directions_id = dir.id
		left join analytic.camea_bikecounters_in_contract old on loc.vendor_id = old.location_id
		where to_timestamp(measured_from/1000)::date = current_date
		group by 1,2,3,4,5,6,7,8) t1
left join (select date_trunc('hour', to_timestamp(measured_from/1000)) AS datum,
			locations_id, avg(value) as temperature from public.bicyclecounters_temperatures
			where to_timestamp(measured_from/1000)::date = current_date
			group by date_trunc('hour', to_timestamp(measured_from/1000)),
			locations_id) t2
			on t1.datum = t2.datum and t1.locations_id = t2.locations_id;

-- view pro Valerii - přehledová tabulka - aktuální den
create or replace view analytic.v_camea_bikecounters_all_data_table_today
as select t1.*,
		case when t2.direction_name = t1.direction_name then 'není' else t2.direction_name end as direction2_name,
		t2.dir2det,
		t3.temperature
from (select
			date_trunc('hour', to_timestamp(measured_from/1000)) AS datum,
			det.locations_id,
			loc."name" as location_name,
			loc.route,
			dir."name" as direction_name,
			case when sum(det.value) is null then 0 else sum(det.value) end as dir1det
		from public.bicyclecounters_detections det
		left join public.bicyclecounters_locations loc on det.locations_id = loc.id
		left join public.bicyclecounters_directions dir on det.directions_id = dir.id
		left join analytic.camea_bikecounters_in_contract old on loc.vendor_id = old.location_id
		where old.in_camea_contract = 'true' and directions_id in (select max(id) from public.bicyclecounters_directions group by locations_id) and
		to_timestamp(measured_from/1000)::date = current_date
		group by 1,2,3,4,5) t1
left join
		(select
			date_trunc('hour', to_timestamp(measured_from/1000)) AS datum,
			det2.locations_id as locations_id,
			loc."name" as location_name,
			dir.vendor_id as direction_id,
			dir."name" as direction_name,
			case when sum(det2.value) is null then 0 else sum(det2.value) end as dir2det
		from public.bicyclecounters_detections det2
		left join public.bicyclecounters_locations loc on det2.locations_id = loc.id
		left join public.bicyclecounters_directions dir on det2.directions_id = dir.id
		where directions_id in (select min(id) from public.bicyclecounters_directions group by locations_id)  and
		to_timestamp(measured_from/1000)::date = current_date
		group by 1,2,3,4,5) t2
		on t1.datum = t2.datum and t1.locations_id = t2.locations_id
left join (select date_trunc('hour', to_timestamp(measured_from/1000)) AS datum,
			locations_id,
			avg(value) as temperature from public.bicyclecounters_temperatures
			where to_timestamp(measured_from/1000)::date = current_date
			group by 1,2) t3
		on t1.datum = t3.datum and t1.locations_id = t3.locations_id;

-- view pro Valerii - historie
create or replace view analytic.v_camea_bikecounters_all_data_history
as select
            to_timestamp(measured_from/1000)::date AS datum,
            det.locations_id,
            loc."name" as location_name,
            old.in_camea_contract,
            loc.route,
            dir."name" as direction_name,
            case when sum(det.value) is null then 0 else sum(det.value) end as detections
        from public.bicyclecounters_detections det
        left join public.bicyclecounters_locations loc on det.locations_id = loc.id
        left join public.bicyclecounters_directions dir on det.directions_id = dir.id
        left join analytic.camea_bikecounters_in_contract old on loc.vendor_id = old.location_id
        group by 1,2,3,4,5,6;
