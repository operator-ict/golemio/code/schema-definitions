DROP TABLE IF EXISTS public.ropidvymi_events_routes;
DROP TABLE IF EXISTS public.ropidvymi_events_stops;
DROP TABLE IF EXISTS public.ropidvymi_events;

DROP SEQUENCE IF EXISTS public.ropidvymi_events_id_seq;
DROP SEQUENCE IF EXISTS public.ropidvymi_events_routes_id_seq;
DROP SEQUENCE IF EXISTS public.ropidvymi_events_stops_id_seq;
