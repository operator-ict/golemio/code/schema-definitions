/*
Lokalizační tabulky
Pavel Procházka, 15.12.2020
Odborný konzultant: Přemek Derbek
*/

INSERT INTO meta.dataset(	code_dataset,name_dataset,	description, 	retention_days, 	h_retention_days, 	created_at	)
	VALUES (	'rsd_tmc',		--code_dataset,
	'Lokalizační tabulky',	--name_dataset,
	'Lokalizační tabulky, celá ČR',	--descr,
	null,			--retention_days,
	null,			--h_retention_days,
	now() 			--created_at
	);

-- zaevidování do meta.extract
INSERT INTO meta."extract"(	name_extract, 	id_dataset, 	id_db, 	description, 	schema_extract, 	retention_days, 	h_retention_days, 	created_at)
VALUES (
	'rsd_tmc_admins',		-- name_extract,
	(select id_dataset from meta.dataset where code_dataset = 'rsd_tmc'),		--;id_dataset,
	1,		--	1 - postgres, 2 mongodb_db	id_db,
	'admins',	--description,
	'public',	--schema_extract,
	null,	--retention_days,
	null,	--h_retention_days,
	now())
on conflict (name_extract) do nothing;
--------------------------------------------------------------------------------------


CREATE TABLE public.rsd_tmc_admins (
	cid int4 NULL,
	tabcd int4 NULL,
	lcd int4 primary key,
	"class" varchar(1024) NULL,
	tcd int4 NULL,
	stcd int4 NULL,
	firstname varchar(1024) NULL,
	area_ref int4 NULL,
	area_name varchar(1024) NULL
);

-- zaevidování do meta.extract
INSERT INTO meta."extract"(	name_extract, 	id_dataset, 	id_db, 	description, 	schema_extract, 	retention_days, 	h_retention_days, 	created_at)
VALUES (
	'rsd_tmc_points',		-- name_extract,
	(select id_dataset from meta.dataset where code_dataset = 'rsd_tmc'),		--;id_dataset,
	1,		--	1 - postgres, 2 mongodb_db	id_db,
	'points',	--description,
	'public',	--schema_extract,
	null,	--retention_days,
	null,	--h_retention_days,
	now())
on conflict (name_extract) do nothing;
--------------------------------------------------------------------------------------

CREATE TABLE public.rsd_tmc_points (
	cid int4 NULL,
	"table" int4 NULL,
	lcd int4 primary key,
	"class" varchar(1024) NULL,
	tcd int4 NULL,
	stcd int4 NULL,
	jnumber int4 NULL,
	roadnumber varchar(1024) NULL,
	roadname varchar(1024) NULL,
	firstname varchar(1024) NULL,
	secondname varchar(1024) NULL,
	area_ref int4 NULL,
	area_name varchar(1024) NULL,
	roa_lcd int4 NULL,
	seg_lcd int4 NULL,
	roa_type varchar(1024) NULL,
	inpos int4 NULL,
	outpos int4 NULL,
	inneg int4 NULL,
	outneg int4 NULL,
	presentpos int4 NULL,
	presentneg int4 NULL,
	interrupt int4 NULL,
	urban int4 NULL,
	int_lcd varchar(1024) NULL,
	neg_off int4 NULL,
	pos_off int4 NULL,
	wgs84_x varchar(1024) NULL,
	wgs84_y varchar(1024) NULL,
	sjtsk_x varchar(1024) NULL,
	sjtsk_y varchar(1024) NULL,
	isolated int4 NULL
);

-- zaevidování do meta.extract
INSERT INTO meta."extract"(	name_extract, 	id_dataset, 	id_db, 	description, 	schema_extract, 	retention_days, 	h_retention_days, 	created_at)
VALUES (
	'rsd_tmc_roads',		-- name_extract,
	(select id_dataset from meta.dataset where code_dataset = 'rsd_tmc'),		--;id_dataset,
	1,		--	1 - postgres, 2 mongodb_db	id_db,
	'roads',	--description,
	'public',	--schema_extract,
	null,	--retention_days,
	null,	--h_retention_days,
	now())
on conflict (name_extract) do nothing;
--------------------------------------------------------------------------------------


CREATE TABLE public.rsd_tmc_roads (
	cid int4 NULL,
	tabcd int4 NULL,
	lcd int4 primary key,
	"class" varchar(1024) NULL,
	tcd int4 NULL,
	stcd int4 NULL,
	roadnumber varchar(1024) NULL,
	roadname varchar(1024) NULL,
	firstname varchar(1024) NULL,
	secondname varchar(1024) NULL,
	area_ref int4 NULL,
	area_name varchar(1024) NULL
);

-- zaevidování do meta.extract
INSERT INTO meta."extract"(	name_extract, 	id_dataset, 	id_db, 	description, 	schema_extract, 	retention_days, 	h_retention_days, 	created_at)
VALUES (
	'rsd_tmc_segments',		-- name_extract,
	(select id_dataset from meta.dataset where code_dataset = 'rsd_tmc'),		--;id_dataset,
	1,		--	1 - postgres, 2 mongodb_db	id_db,
	'segments',	--description,
	'public',	--schema_extract,
	null,	--retention_days,
	null,	--h_retention_days,
	now())
on conflict (name_extract) do nothing;
--------------------------------------------------------------------------------------

CREATE TABLE public.rsd_tmc_segments (
	cid int4 NULL,
	tabcd int4 NULL,
	lcd int4 primary key,
	"class" varchar(1024) NULL,
	tcd int4 NULL,
	stcd int4 NULL,
	roadnumber varchar(1024) NULL,
	roadname varchar(1024) NULL,
	firstname varchar(1024) NULL,
	secondname varchar(1024) NULL,
	area_ref int4 NULL,
	area_name varchar(1024) NULL,
	roa_lcd int4 NULL,
	roa_type varchar(1024) NULL,
	neg_off int4 NULL,
	pos_off int4 NULL
);

