-- Obecná fce na import json

-- FUNCTION: meta.import_from_json(bigint, json, character varying, character varying, json, json, character varying)

CREATE OR REPLACE FUNCTION meta.import_from_json(
    p_batch_id bigint,
    p_data json,
    p_table_schema character varying,
    p_table_name character varying,
    p_pk json,
    p_sort json,
    p_worker_name character varying,
    OUT x_inserted json,
    OUT x_updated json)
    RETURNS record
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE
AS $BODY$


declare
    pole_seznam varchar;
    pole_sort varchar;
    pole_seznam_datatype varchar;
    pole_pk varchar;
    pole_pk_tar varchar;
    query_rec record;
    x_sql varchar;
    x_sql_sor varchar;
    x_pole varchar;
    x_data_typ varchar;
    x_lenght integer;
    x_sql_where varchar;
    prvni_pole varchar;
--
    p_cas timestamp;
begin
-- vzor P_pk: [{"pole": "pole1"},{"pole": "sloupec2"},{"pole": "dasi-sloupec"}]

-- kontrola pokud je vyplněn p_sort, musí být i p_pk
if p_sort is not null and p_pk is null then
    raise EXCEPTION 'Nelze vyplnit p_sort(%) a nemí vyplněn PK', p_sort;
end if;

-- kontrola existence tabulky
    if not exists
    (
        select table_name
        from information_schema.tables
        where upper(table_name) = upper(p_table_name)
        and upper(table_schema) = upper(p_table_schema)
    )   then
        raise EXCEPTION 'Tabulka (%.%) neexistuje', p_table_schema,p_table_name;
    end if;
-- /kontrola existence tabulky
drop table if exists pp_data;

create temp table pp_data as
select p_data  as data;

--execute 'select p_data as data';
-- pole z json
FOR  query_rec IN
    select json_object_keys(data->0) pole
    from pp_data
LOOP
    if not exists
    (
        select column_name
        from information_schema.columns
        where upper(table_schema) = upper(p_table_schema)
        and upper(table_name) = upper(p_table_name)
        and upper (column_name) = upper(query_rec.pole)
    )   then
        raise EXCEPTION 'Tabulka %.% neobsahuje pole %', p_table_schema,p_table_name,query_rec.pole;
    else
        if pole_seznam is null then
            pole_seznam := query_rec.pole;
        else
            pole_seznam := pole_seznam ||', '||query_rec.pole;
        end if;
    end if;
end loop;
-- /pole z json

-- Kontrola PK
FOR  query_rec IN
    select cast(json_array_elements(data)->>'pole' as varchar) pole
    from (select p_pk as data) a
LOOP
    if not exists
    (
        select column_name
        from information_schema.columns
        where upper(table_schema) = upper(p_table_schema)
        and upper(table_name) = upper(p_table_name)
        and upper (column_name) = upper(query_rec.pole)
    )   then
        raise EXCEPTION 'Tabulka %.% neobsahuje PK pole %', p_table_schema,p_table_name,query_rec.pole;
    else
        if pole_pk is null then
            pole_pk := query_rec.pole;
            pole_pk_tar := 'tar.'||query_rec.pole;
            x_sql_where := 'tar.'||query_rec.pole||' = sor.'||query_rec.pole;
        else
            pole_pk := pole_pk ||', '||query_rec.pole;
            pole_pk_tar := pole_pk_tar ||', tar.'||query_rec.pole;
            x_sql_where := x_sql_where||' and tar.'||query_rec.pole||' = sor.'||query_rec.pole;
        end if;
    end if;
end loop;
-- /Kontrola PK

-- kontrola sort
FOR  query_rec IN
    select cast(json_array_elements(data)->>'pole' as varchar) pole
    from (select p_sort as data) a
LOOP
    if not exists
    (
        select column_name
        from information_schema.columns
        where upper(table_schema) = upper(p_table_schema)
        and upper(table_name) = upper(p_table_name)
        and upper (column_name) = upper(query_rec.pole)
    )   then
        raise EXCEPTION 'Tabulka %.% neobsahuje sort pole %', p_table_schema,p_table_name,query_rec.pole;
    else
        if pole_sort is null then
            pole_sort := query_rec.pole;
        else
            pole_sort := pole_sort ||', '||query_rec.pole;
        end if;
    end if;
end loop;
-- /kontrol sort

/*
fce provádí merge p_data do tabulky p_table_name
výstup do dvou polí (záznamy insertované a updatované)
*/

drop table if exists mytemp;
x_sql := 'create temp table mytemp (';
FOR  query_rec IN
select b.pole, data_type, character_maximum_length
from (
    select
        cast(json_array_elements(data)->>'pole' as varchar) pole
        from (select p_pk as data) a
    )b
    join information_schema.columns inf
    on inf.table_schema = p_table_schema
        and inf.table_name = p_table_name
        and inf.column_name = b.pole
LOOP
    if not right(x_sql,1)='(' then
        x_sql := x_sql||',';
    end if;
    x_sql := x_sql||query_rec.pole||' '||query_rec.data_type;
    if query_rec.character_maximum_length is not null
    then
        x_sql := x_sql||'('||query_rec.character_maximum_length||')';
    end if;
end loop;
x_sql := x_sql||')';

execute x_sql;

--  update
    x_sql := 'with rows as (update '||p_table_schema||'.'||p_table_name||' tar set ';
    x_sql_sor:= 'select '||chr(10);
    -- příklad:             cis_id = sor.cis_id::bigint,
    FOR  query_rec IN
        select b.pole
        ,data_type ,character_maximum_length
        from (
            select json_object_keys(data->0) pole
            from pp_data
        )b
        join information_schema.columns inf
        on inf.table_schema = p_table_schema
            and inf.table_name = p_table_name
            and inf.column_name = b.pole
    LOOP
        if not right(x_sql,5)=' set 'then
            x_sql := x_sql||', ';
            x_sql_sor:= x_sql_sor||',';
        end if;
        x_sql := x_sql||query_rec.pole||'=sor.'||query_rec.pole;
        x_sql_sor:= x_sql_sor||'cast(json_array_elements(data)->>'''||query_rec.pole||''' as '||query_rec.data_type||') '||query_rec.pole||chr(10);
    end loop;
    x_sql_sor:= x_sql_sor||' from pp_data a';
    -- auditní pole
    x_sql := x_sql||',update_batch_id = $1';
    x_sql := x_sql||',updated_at = now()';
    x_sql := x_sql||',updated_by = $2 ';
    -- from
    x_sql := x_sql||' from ('||x_sql_sor||') sor where '||x_sql_where;
    x_sql := x_sql||' RETURNING '||pole_pk_tar||') INSERT INTO mytemp ('||pole_pk||') SELECT '||pole_pk||' FROM rows';

if p_pk is not null
then
    execute x_sql using p_batch_id,p_worker_name;

    x_sql:='select array_to_json(array_agg(row)) from (select '||pole_pk||' from mytemp) row';
    execute x_sql into x_updated;
end if;

truncate table mytemp;

-- insert
if p_pk is not null then
    x_sql:='with rows as (';
else
    x_sql:='';
end if;
x_sql:=x_sql||'insert into '||p_table_schema||'.'||p_table_name||chr(10)||' ('||pole_seznam||',create_batch_id, created_at, created_by)'||chr(10);
x_sql:=x_sql||' select ';
FOR  query_rec IN
    select pole,isch.data_type
    from(
        select json_object_keys(data->0) pole
        from pp_data a
    ) aa
    join information_schema.columns isch
    on
         isch.table_name = p_table_name and table_schema =p_table_schema and isch.column_name =aa.pole
LOOP
    if pole_seznam_datatype is null then
        pole_seznam_datatype := chr(10)||'aa.'||query_rec.pole||'::'||query_rec.data_type;
    else
        pole_seznam_datatype := pole_seznam_datatype ||chr(10)||', aa.'||query_rec.pole||'::'||query_rec.data_type;
    end if;
end loop;
-- / pole z json a datový typ
x_sql:=x_sql||pole_seznam_datatype||chr(10);
-- auditní pole
    x_sql := x_sql||', $1 create_batch_id'||chr(10);
    x_sql := x_sql||',now() created_at'||chr(10);
    x_sql := x_sql||',$2 created_by'||chr(10);
x_sql := x_sql||' from ( select tar.*';
if p_sort is not null then
    x_sql:=x_sql||', row_number() over(partition by '||pole_pk_tar||' order by tar.'||pole_sort||') rn';
end if;
x_sql:=x_sql||' from ('||x_sql_sor||') tar'||chr(10);
if p_pk is not null then
    x_sql:=x_sql||' left join '||p_table_schema||'.'||p_table_name||' bb on '||chr(10);

    prvni_pole := null;
    FOR  query_rec IN
    select pole
    from (
        select
            cast(json_array_elements(data)->>'pole' as varchar) pole
            from (select p_pk as data) a
        )b
    LOOP
        if prvni_pole is null then
            prvni_pole := query_rec.pole;
        end if;
        if not right(x_sql,5)=' on '||chr(10) then
            x_sql := x_sql||' and ';
        end if;
        x_sql := x_sql||'tar.'||query_rec.pole||'= bb.'||query_rec.pole||chr(10);
    end loop;
end if;
if p_pk is not null then
    x_sql:=x_sql||' where bb.'||prvni_pole||' is null';
end if;
x_sql:=x_sql||') aa';
if p_sort is not null then
    x_sql:=x_sql||' where rn=1 '||chr(10);
end if;
if p_pk is not null then
    x_sql:=x_sql||' returning '||pole_pk||')'||chr(10);
    x_sql:=x_sql||' insert into mytemp ('||pole_pk||') select '||pole_pk||' from rows '||chr(10);
end if;

execute x_sql using p_batch_id,p_worker_name;
if p_pk is not null then
    x_sql:='select array_to_json(array_agg(row)) from (select '||pole_pk||' from mytemp) row';
    execute x_sql into x_inserted;
end if;

drop table mytemp;

end;

$BODY$;

--ALTER FUNCTION meta.import_from_json(bigint, json, character varying, character varying, json, json, character varying) OWNER TO postgres;

COMMENT ON FUNCTION meta.import_from_json(bigint, json, character varying, character varying, json, json, character varying) IS 'update/insert
parametry:
p_batch_id (bigint) --> číslo dávky
p_data (json) --> vstupní data (název_pole:hodnota)
p_table_schema (varchar) --> schéma cílové tabulky
p_table_name (varchar) --> název cílové tabulky
p_pk (json) --> primární klíč pro update a výběr insertu ("pole":nazev_pole)
p_sort (json) --> třídění při výběru ("pole":nazev_pole)
p_worker_name (varchar) --> název workeru (procesu/uživatele)
';
