ALTER TABLE public.ropidgtfs_cis_stops RENAME COLUMN "altIdosName" TO alt_idos_name;
ALTER TABLE public.ropidgtfs_cis_stops RENAME COLUMN "jtskX" TO jtsk_x;
ALTER TABLE public.ropidgtfs_cis_stops RENAME COLUMN "jtskY" TO jtsk_y;
ALTER TABLE public.ropidgtfs_cis_stops RENAME COLUMN "wheelchairAccess" TO wheelchair_access;

ALTER TABLE public.ropidgtfs_cis_stop_groups RENAME COLUMN "avgJtskX" TO avg_jtsk_x;
ALTER TABLE public.ropidgtfs_cis_stop_groups RENAME COLUMN "avgJtskY" TO avg_jtsk_y;
ALTER TABLE public.ropidgtfs_cis_stop_groups RENAME COLUMN "avgLat" TO avg_lat;
ALTER TABLE public.ropidgtfs_cis_stop_groups RENAME COLUMN "avgLon" TO avg_lon;
ALTER TABLE public.ropidgtfs_cis_stop_groups RENAME COLUMN "districtCode" TO district_code;
ALTER TABLE public.ropidgtfs_cis_stop_groups RENAME COLUMN "fullName" TO full_name;
ALTER TABLE public.ropidgtfs_cis_stop_groups RENAME COLUMN "idosCategory" TO idos_category;
ALTER TABLE public.ropidgtfs_cis_stop_groups RENAME COLUMN "idosName" TO idos_name;
ALTER TABLE public.ropidgtfs_cis_stop_groups RENAME COLUMN "uniqueName" TO unique_name;
