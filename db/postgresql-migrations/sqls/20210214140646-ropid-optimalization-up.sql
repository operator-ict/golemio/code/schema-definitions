ALTER TABLE "public"."vehiclepositions_trips" ADD COLUMN "last_position_id" bigint;

ALTER TABLE "public"."vehiclepositions_positions" ADD COLUMN "state_process" character varying(50);
ALTER TABLE "public"."vehiclepositions_positions" ADD COLUMN "state_position" character varying(50);
ALTER TABLE "public"."vehiclepositions_positions" ADD COLUMN "this_stop_id" character varying(50);