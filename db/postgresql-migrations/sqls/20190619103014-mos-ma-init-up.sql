
CREATE SEQUENCE public.mos_ma_devicemodels_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

CREATE TABLE public.mos_ma_devicemodels
(
  id bigint NOT NULL DEFAULT nextval('mos_ma_devicemodels_id_seq'::regclass),
  "count" integer,
  model character varying(255),

  create_batch_id bigint,
  created_at timestamp with time zone,
  created_by character varying(150),
  update_batch_id bigint,
  updated_at timestamp with time zone,
  updated_by character varying(150),
  CONSTRAINT mos_ma_devicemodels_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);


CREATE TABLE public.mos_ma_ticketactivations
(
  "date" timestamp with time zone,
  lat numeric,
  lon numeric,
  ticket_id integer NOT NULL,
  "type" character varying(255),
  zones character varying(255),

  create_batch_id bigint,
  created_at timestamp with time zone,
  created_by character varying(150),
  update_batch_id bigint,
  updated_at timestamp with time zone,
  updated_by character varying(150),
  CONSTRAINT mos_ma_ticketactivations_pkey PRIMARY KEY (ticket_id)
)
WITH (
  OIDS=FALSE
);


CREATE SEQUENCE public.mos_ma_ticketinspections_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

CREATE TABLE public.mos_ma_ticketinspections
(
  "date" timestamp with time zone,
  id bigint NOT NULL DEFAULT nextval('mos_ma_ticketinspections_id_seq'::regclass),
  lat numeric,
  lon numeric,
  reason character varying(255),
  result boolean,
  "user_id" character varying(255),

  create_batch_id bigint,
  created_at timestamp with time zone,
  created_by character varying(150),
  update_batch_id bigint,
  updated_at timestamp with time zone,
  updated_by character varying(150),
  CONSTRAINT mos_ma_ticketinspections_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);


CREATE TABLE public.mos_ma_ticketpurchases
(
  account_id character varying(255),
  cptp integer,
  "date" timestamp with time zone,
  duration integer,
  lat numeric,
  lon numeric,
  tariff_id integer,
  tariff_name character varying(255),
  ticket_id integer NOT NULL,
  zone_count integer,

  create_batch_id bigint,
  created_at timestamp with time zone,
  created_by character varying(150),
  update_batch_id bigint,
  updated_at timestamp with time zone,
  updated_by character varying(150),
  CONSTRAINT mos_ma_ticketpurchases_pkey PRIMARY KEY (ticket_id)
)
WITH (
  OIDS=FALSE
);
