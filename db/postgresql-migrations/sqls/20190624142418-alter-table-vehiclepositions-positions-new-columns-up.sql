
-- vehiclepositions_trips

ALTER TABLE vehiclepositions_trips ADD COLUMN cis_agency_name character varying(255);
ALTER TABLE vehiclepositions_trips ADD COLUMN cis_parent_route_name character varying(255);
ALTER TABLE vehiclepositions_trips ADD COLUMN cis_real_agency_name character varying(255);
ALTER TABLE vehiclepositions_trips ADD COLUMN cis_vehicle_registration_number integer;

-- vehiclepositions_positions

DROP VIEW IF EXISTS v_vehiclepositions_last_position;

ALTER TABLE vehiclepositions_positions ADD COLUMN bearing integer;
ALTER TABLE vehiclepositions_positions ADD COLUMN cis_last_stop_id integer;
ALTER TABLE vehiclepositions_positions ADD COLUMN cis_last_stop_sequence integer;
ALTER TABLE vehiclepositions_positions ADD COLUMN gtfs_last_stop_id character varying(255);
ALTER TABLE vehiclepositions_positions ADD COLUMN gtfs_last_stop_sequence integer;
ALTER TABLE vehiclepositions_positions ADD COLUMN gtfs_next_stop_sequence integer;
ALTER TABLE vehiclepositions_positions ADD COLUMN speed integer;

CREATE OR REPLACE VIEW v_vehiclepositions_last_position
  AS
    SELECT
      DISTINCT ON ("vehiclepositions_positions"."trips_id") "vehiclepositions_positions".*
    FROM "vehiclepositions_positions" AS "vehiclepositions_positions"
      JOIN "vehiclepositions_trips" AS "vehiclepositions_trips"
        ON ("vehiclepositions_trips"."id" = "vehiclepositions_positions"."trips_id" AND
            "vehiclepositions_trips"."gtfs_trip_id" IS NOT NULL)
    WHERE "vehiclepositions_positions"."created_at" > (NOW() - INTERVAL '5 min') AND "vehiclepositions_positions"."delay" IS NOT NULL
    ORDER BY "vehiclepositions_positions"."trips_id", "vehiclepositions_positions"."created_at" DESC;

-- vehiclepoitions_stops

ALTER TABLE vehiclepositions_stops ADD COLUMN arrival_delay_type integer;
ALTER TABLE vehiclepositions_stops DROP CONSTRAINT vehiclepositions_stops_pkey;
ALTER TABLE vehiclepositions_stops ADD CONSTRAINT vehiclepositions_stops_pkey PRIMARY KEY (cis_stop_sequence, trips_id);
