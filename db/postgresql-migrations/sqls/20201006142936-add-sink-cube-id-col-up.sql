TRUNCATE TABLE public.flow_sinks;
ALTER TABLE public.flow_sinks ADD COLUMN cube_id integer;
alter table public.flow_sinks drop constraint flow_sinkspkey;
alter table public.flow_sinks add constraint flow_sinkspkey primary key (id, cube_id, output_value_type) ;