CREATE MATERIALIZED VIEW "v_ropidgtfs_trips_minmaxsequences" AS
SELECT ropidgtfs_stop_times.trip_id,
    max(ropidgtfs_stop_times.stop_sequence) AS max_stop_sequence,
    min(ropidgtfs_stop_times.stop_sequence) AS min_stop_sequence
   FROM ropidgtfs_stop_times
  GROUP BY ropidgtfs_stop_times.trip_id;

REFRESH MATERIALIZED VIEW "v_ropidgtfs_trips_minmaxsequences";
