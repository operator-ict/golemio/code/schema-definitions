drop VIEW analytic.v_public_tenders;

drop table python.public_tenders_mmr_commodity_groups;
drop TABLE python.public_tenders_cpv_groups;
drop TABLE python.public_tenders;

drop schema python;