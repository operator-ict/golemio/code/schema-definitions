/* Replace with your SQL commands */

-- nové tabulky pro ROPID - frirbase
-- upravil: Pavel Procházka dne: 20.1.2020
-- změny názvů
-- dataset: litacka_fb --> firebase_pidlitacka

select setval('meta.dataset_id_dataset_seq',(SELECT last_value FROM meta.dataset_id_dataset_seq),true);

INSERT INTO meta.dataset(
	--	id_dataset, --nevyplňovat - autoincrement
	code_dataset, 
	name_dataset, 
	description, 
	retention_days, 
	h_retention_days, 
	created_at
	)
	VALUES (
	--id_dataset, 
	'firebase_pidlitacka',		--code_dataset, 
	'FireBase litacka - agregovaná data',	--name_dataset, 
	'Data z FireBase o litacce',	--descr, 
	null,			--retention_days, 
	null,			--h_retention_days, 
	now() 			--created_at
	);


--změna názvu litacka_fb_events --> firebase_pidlitacka_events
CREATE TABLE public.firebase_pidlitacka_events (
	event_date date NOT NULL,
	event_name varchar(255) NOT NULL,
	event_count int4 NOT NULL,
	users int4 NOT NULL,
/*	create_batch_id int8 NULL,
	created_at timestamp NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamp NULL,
	updated_by varchar(150) NULL,*/
	CONSTRAINT firebase_pidlitacka_events_pkey PRIMARY KEY (event_date,event_name)
);

SELECT meta.add_audit_fields('public','firebase_pidlitacka_events');

-- zaevidování do meta.extract
INSERT INTO meta."extract"(
    name_extract, 
    id_dataset, 
    id_db, 
    description, 
    schema_extract, 
    retention_days, 
    h_retention_days, 
    created_at)
    
    VALUES (
    'firebase_pidlitacka_events',        -- name_extract, 
    (select id_dataset from meta.dataset where code_dataset = 'firebase_pidlitacka'),        --;id_dataset, 
    1,        --    1 - postgres, 2 mongodb_db    id_db, 
    'Přehledy jednotlivýhc událostí - fireBase',    --description, 
    'public',    --schema_extract, 
    null,    --retention_days, 
    null,    --h_retention_days, 
    now());
--------------------------------------------------------------------------------------

--změna názvu: litacka_fb_applaunch_par --> firebase_pidlitacka_applaunch_par
CREATE TABLE public.firebase_pidlitacka_applaunch_par (
	event_date date NOT NULL,
	event_count int4 NOT NULL,
	users int4 NOT NULL,
	mails int4 NOT NULL, -- MAILS
	mail_users int4 NOT NULL, -- MAIL_USERS
	notis int4 NOT NULL, -- NOTIS
	noti_users int4 NOT NULL,  -- NOTI_USERS
	anonym int4 NOT NULL, -- ANONYM
	anonym_users int4 NOT NULL,  -- ANONYM_USERS
	isanonym int4 NOT NULL,  -- ISANONYM
	isanonym_users int4 NOT NULL,  -- ISANONYM_USERS
	hasnfc int4 NOT NULL,  -- HASNFC
	hasnfc_users int4 NOT NULL,	-- HASNFC_USERS
/*	create_batch_id int8 NULL,
	created_at timestamp NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamp NULL,
	updated_by varchar(150) NULL,*/
	CONSTRAINT firebase_pidlitacka_applaunch_par_pkey PRIMARY KEY (event_date)
);

SELECT meta.add_audit_fields('public','firebase_pidlitacka_applaunch_par');

-- zaevidování do meta.extract
INSERT INTO meta."extract"(
    name_extract, 
    id_dataset, 
    id_db, 
    description, 
    schema_extract, 
    retention_days, 
    h_retention_days, 
    created_at)
    
    VALUES (
    'firebase_pidlitacka_applaunch_par',        -- name_extract, 
    (select id_dataset from meta.dataset where code_dataset = 'firebase_pidlitacka'),        --;id_dataset, 
    1,        --    1 - postgres, 2 mongodb_db    id_db, 
    'PARAMETRY fireBase',    --description, 
    'public',    --schema_extract, 
    null,    --retention_days, 
    null,    --h_retention_days, 
    now());
--------------------------------------------------------------------------------------

DROP TABLE if exists public.firebase_trasy;

-- změna názvu: litacka_fb_trasy --> firebase_pidlitacka_route
CREATE TABLE public.firebase_pidlitacka_route (
	reference_date date NOT NULL, 	-- datum
	s_from varchar(255) NOT NULL, 	-- search from
	s_to varchar(255) NOT NULL,		-- search to
	count_case int4 NOT NULL,		-- pocet
	/*create_batch_id int8 NULL,
	created_at timestamp NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamp NULL,
	updated_by varchar(150) NULL,*/
	CONSTRAINT firebase_pidlitacka_route_pkey PRIMARY KEY (reference_date, s_from, s_to)
);

SELECT meta.add_audit_fields('public','firebase_pidlitacka_route');

-- zaevidování do meta.extract
INSERT INTO meta."extract"(
    name_extract, 
    id_dataset, 
    id_db, 
    description, 
    schema_extract, 
    retention_days, 
    h_retention_days, 
    created_at)
    
    VALUES (
    'firebase_pidlitacka_route',        -- name_extract, 
    (select id_dataset from meta.dataset where code_dataset = 'firebase_pidlitacka'),        --;id_dataset, 
    1,        --    1 - postgres, 2 mongodb_db    id_db, 
    'Vyhledávnaé trasy - fireBase',    --description, 
    'public',    --schema_extract, 
    null,    --retention_days, 
    null,    --h_retention_days, 
    now());
--------------------------------------------------------------------------------------


--změna názvu: litacka_fb_web_events -->firebase_pidlitacka_web_events 
CREATE TABLE public.firebase_pidlitacka_web_events (
	id varchar(255) NULL,
	id_profile varchar(255) NULL,	-- idProfile
	reference_date date NOT NULL,		-- date
	sessions int4 NULL,
	users int4 NULL,
	CONSTRAINT firebase_pidlitacka_web_events_pkey PRIMARY KEY (reference_date)
);

SELECT meta.add_audit_fields('public','firebase_pidlitacka_web_events');
-- zaevidování do meta.extract
INSERT INTO meta."extract"(
    name_extract, 
    id_dataset, 
    id_db, 
    description, 
    schema_extract, 
    retention_days, 
    h_retention_days, 
    created_at)
    
    VALUES (
    'firebase_pidlitacka_web_events',        -- name_extract, 
    (select id_dataset from meta.dataset where code_dataset = 'firebase_pidlitacka'),        --;id_dataset, 
    1,        --    1 - postgres, 2 mongodb_db    id_db, 
    'používání webové aplikace lítačka - Googel analitics',    --description, 
    'public',    --schema_extract, 
    null,    --retention_days, 
    null,    --h_retention_days, 
    now());
--------------------------------------------------------------------------------------



-- /nové tabulky pro ROPID - frirbase

drop view if exists analytic.v_ropidbi_ticket_activation_types;
drop view if exists analytic.v_ropidbi_ticket_activation_times;
drop view if exists analytic.v_ropidbi_ticket_activation_location;
drop view if exists analytic.v_ropidbi_ticket_sales;
drop view if exists analytic.v_ropidbi_device_counts;
drop view if exists analytic.v_ropidbi_customer_age;
drop view if exists analytic.v_ropidbi_ticket_zones;
drop view if exists analytic.v_ropidbi_soubeh_jizdenek;
--drop table if exists analytic.pid_cenik;
--drop table if exists public.firebase_trasy;

drop materialized view if exists analytic.v_ropidbi_ticket;

-- ropidbi_ticket

CREATE materialized VIEW analytic.v_ropidbi_ticket
AS SELECT 
	a.date AS activation_date,
	a.date::date AS act_date,
	date_trunc('hour'::text, a.date)::time AS act_time,
    a.lat AS act_lat,
    a.lon AS act_lon,
    a.ticket_id,
    CASE
            WHEN a.type::text = 'manualNow'::text THEN 'Ruční aktivace ihned'::text
            WHEN a.type::text = 'manualTime'::text THEN 'Ruční aktivace na daný čas'::text
            WHEN a.type::text = 'purchaseNow'::text THEN 'Nákup s aktivací ihned'::text
            WHEN a.type::text = 'purchaseTime'::text THEN 'Nákup s aktivací na daný čas'::text
            ELSE 'Zatím neaktivováno'::text
        END AS activation_type,
    a.zones,
    p.cptp,
    p.date AS purchase_date,
    p.duration AS duration_min,
    p.tariff_name,
    p.zone_count,
    case
    	when p.tariff_name like '%Praha' then 'Praha'
    	else 'Region'
    	end as oblast
   FROM public.mos_ma_ticketactivations a
     RIGHT JOIN mos_ma_ticketpurchases p ON a.ticket_id = p.ticket_id;


-- activation_types

CREATE OR REPLACE VIEW analytic.v_ropidbi_ticket_activation_types
AS SELECT 
		act_date,
        activation_type,
        oblast,
    	v_ropidbi_ticket.tariff_name,
    	count(*) AS count
  FROM analytic.v_ropidbi_ticket
  WHERE v_ropidbi_ticket.activation_type::text != 'Zatím neaktivováno'::text AND v_ropidbi_ticket.tariff_name::text <> ''::text
  GROUP BY act_date,
        activation_type,
        oblast,
    	v_ropidbi_ticket.tariff_name;

-- activation_times

CREATE OR REPLACE VIEW analytic.v_ropidbi_ticket_activation_times
AS SELECT 
		  act_date,
    	  act_time,
    	  v_ropidbi_ticket.tariff_name,
    	  oblast,
          count(*) AS count
   FROM analytic.v_ropidbi_ticket
   GROUP BY act_date,
    	  act_time,
    	  v_ropidbi_ticket.tariff_name,
    	  oblast;


-- location

CREATE OR REPLACE VIEW analytic.v_ropidbi_ticket_activation_location
AS SELECT 
		  act_date,
	      round (v_ropidbi_ticket.act_lat, 4) as act_lat,
	      round (v_ropidbi_ticket.act_lon, 4) as act_lon,
	      oblast,
	      count(*) AS count
   FROM analytic.v_ropidbi_ticket
  WHERE act_date IS NOT NULL 
  		AND v_ropidbi_ticket.act_lat between 49.4 and 50.7
  		AND v_ropidbi_ticket.act_lon between 13 and 15.6
  GROUP BY act_date,
	      round (v_ropidbi_ticket.act_lat, 4),
	      round (v_ropidbi_ticket.act_lon, 4),
	      oblast;

-- sales

CREATE OR REPLACE VIEW analytic.v_ropidbi_ticket_sales
AS SELECT v_ropidbi_ticket.purchase_date::date AS purchase_date,
		  tariff_name,
		  oblast,
		  count (*)
          FROM analytic.v_ropidbi_ticket
  WHERE purchase_date::date IS NOT NULL 
  		AND tariff_name IS NOT NULL
  GROUP BY v_ropidbi_ticket.purchase_date::date,
		  tariff_name,
		  oblast;

-- devices

CREATE OR REPLACE VIEW analytic.v_ropidbi_device_counts
as SELECT
        trim(SUBSTR(replace(model,'_',' '),1, POSITION(' ' IN replace(model,'_',' ')))) brand,
        replace(model,'_',' ') as model,
        case when left(model, 1) = 'i' then 'iOS'
        	 else 'Android' end as operation_system,
        sum (count)
        from public.mos_ma_devicemodels
    where model is not null
   group by trim(SUBSTR(replace(model,'_',' '),1, POSITION(' ' IN replace(model,'_',' ')))),
        replace(model,'_',' '),
        case when left(model, 1) = 'i' then 'iOS'
        	 else 'Android' end;
        	
-- customer age

CREATE OR REPLACE VIEW analytic.v_ropidbi_customer_age
as select date_part('year', age(date_of_birth::date)) as age,
		  count (*)
    from mos_be_customers
   where date_part('year', age(date_of_birth::date)) between 5 and 100
  group by date_part('year', age(date_of_birth::date));

-- coupons_zones

CREATE OR REPLACE VIEW analytic.v_ropidbi_ticket_zones
as select c.created::date as valid_from,
		  c.customer_profile_name as customer,
		  c.tariff_profile_name as tarif,
		  case when z.zone_name in ('Praha', '0+B') then 'P+0+B'
		  	   else z.zone_name end as zona,
		  case when z.zone_name in ('Praha', '0+B') then '0'
		  	   else z.zone_name end as zone_order,
		  count (*)
    from mos_be_coupons as c
    left join mos_be_zones as z on z.coupon_id = c.coupon_id  
   where c.created::date is not null
   		 and z.zone_name is not null
  group by c.created::date,
		  c.customer_profile_name,
		  c.tariff_profile_name,
		  case when z.zone_name in ('Praha', '0+B') then 'P+0+B'
		  	   else z.zone_name end,
		  case when z.zone_name in ('Praha', '0+B') then '0'
		  	   else z.zone_name end
  order by c.created::date asc;
 
-- soubeh jízdenek

CREATE OR REPLACE VIEW analytic.v_ropidbi_soubeh_jizdenek
AS SELECT celkem.datum,
    celkem.count_acc AS acc_all,
    celkem.count_glob AS ticket_all,
    dupl.count_acc AS acc_dupl,
    dupl.count_glob AS ticket_dupl
   FROM ( SELECT mos_ma_ticketpurchases.date::date AS datum,
            count(DISTINCT mos_ma_ticketpurchases.account_id) AS count_acc,
            count(*) AS count_glob
           FROM mos_ma_ticketpurchases
          GROUP BY (mos_ma_ticketpurchases.date::date)) celkem
     LEFT JOIN ( SELECT b.valid_from::date AS datum,
            count(DISTINCT b.account_id) AS count_acc,
            count(*) AS count_glob
           FROM ( SELECT a.account_id,
                    a.valid_from,
                    a.valid_to,
                    a.xx
                   FROM ( SELECT mos_ma_ticketpurchases.account_id,
                            mos_ma_ticketpurchases.date AS valid_from,
                            mos_ma_ticketpurchases.date + ((mos_ma_ticketpurchases.duration || ' minutes'::text)::interval) AS valid_to,
                            lead(mos_ma_ticketpurchases.date) OVER (PARTITION BY mos_ma_ticketpurchases.account_id ORDER BY mos_ma_ticketpurchases.date) AS xx
                           FROM mos_ma_ticketpurchases) a
                  WHERE a.xx >= a.valid_from AND a.xx <= a.valid_to) b
          GROUP BY (b.valid_from::date)) dupl ON celkem.datum = dupl.datum;

-- Firebase_applaunch

create or replace view analytic.v_ropidbi_firebase_eventy
as select 
	event_date as datum,
	case
		when event_name = 'connectionSearch' then 'Vyhledat spojení'
		when event_name = 'parkingDetail' then 'Koupit parkování P+R'
		when event_name = 'app_remove' then 'Odinstalování aplikace'
		when event_name = 'user_engagement' then 'Počet uživatelů'
		when event_name = 'departureSearch' then 'Funkce odjezdy'
		else event_name end
	from public.firebase_pidlitacka_events;		
--	from public.firebase_agr
	
-- litacka events translation table, data jsou Rabin: analytic.firebase_pidlitacka_events_table

--změna názvu: 	litacka_fb_events_table --> firebase_pidlitacka_events_table
create table analytic.firebase_pidlitacka_events_table  (
	event_name varchar(255) NOT NULL,
	event_name_cz varchar(255),
	description varchar(5000),
	description_cz varchar(5000)
	);
	
-- import data
INSERT INTO analytic.firebase_pidlitacka_events_table
(event_name, event_name_cz, description, description_cz)
VALUES('--', '--', '--', 'Začněte výběrem eventu, který chcete sledovat');
INSERT INTO analytic.firebase_pidlitacka_events_table
(event_name, event_name_cz, description, description_cz)
VALUES('app_clear_data', 'Smazání osobních dat', 'when the user resets/clears the app data, removing all settings and sign-in data', 'Případy, kdy uživatel vymaže data aplikace, čímž odstraní také nastavení a přihlašovací data');
INSERT INTO analytic.firebase_pidlitacka_events_table
(event_name, event_name_cz, description, description_cz)
VALUES('app_exception', 'Chyba/výjimka aplikace', 'when the app crashes or throws an exception', 'Případy selhání nebo ohlášení výjimky aplikace');
INSERT INTO analytic.firebase_pidlitacka_events_table
(event_name, event_name_cz, description, description_cz)
VALUES('app_remove', 'Odstranění aplikace', 'When an application package is removed or "uninstalled" from an Android device. This event is different from the Daily uninstalls by device and Daily uninstalls by user metrics, which are both reported by Google Play Developer Console. The app_remove event counts the removal of application packages, regardless of the installation source, and the count changes depending on the date range you are using for the report. The Daily uninstalls by device and Daily uninstalls by user metrics count the removal of application packages only when they were installed from Google Play, and are reported on a daily basis.', 'Uživatelé, kteří v daný den odstranili aplikaci');
INSERT INTO analytic.firebase_pidlitacka_events_table
(event_name, event_name_cz, description, description_cz)
VALUES('app_update', 'Update aplikace', 'when the app is updated to a new version and launched again 
The previous app version id is passed as a parameter.
This event is conceptually different from the Daily upgrades by device metric, which is reported by Google Play Developer Console.  An upgrade refers to the updating of the application binary, whereas an app_update event is triggered upon the subsequent launch of the upgraded app.', 'Případy spuštění aplikace poté, co proběhla aktualizace');
INSERT INTO analytic.firebase_pidlitacka_events_table
(event_name, event_name_cz, description, description_cz)
VALUES('appLaunch', 'Přihlášení uživatelé', 'Při vstupu do app z backgroundu', 'Počet uživatelů, kteří se daný den přihlášili do aplikace');
INSERT INTO analytic.firebase_pidlitacka_events_table
(event_name, event_name_cz, description, description_cz)
VALUES('connectionSearch', 'Vyhledání spojení', 'po klepnutí na tlačítko "vyhledat spojení"', 'Počet využití funkce "vyhledat spojení"');
INSERT INTO analytic.firebase_pidlitacka_events_table
(event_name, event_name_cz, description, description_cz)
VALUES('departureSearch', 'Vyhledání odjezdu', 'Při zobrazení fce odjezdy při změně zastávky od minula', 'Případy použití "funkce odjezdy" při změně zastávky od minula');
INSERT INTO analytic.firebase_pidlitacka_events_table
(event_name, event_name_cz, description, description_cz)
VALUES('firebase_campaign', 'Kampaň Firebase', NULL, NULL);
INSERT INTO analytic.firebase_pidlitacka_events_table
(event_name, event_name_cz, description, description_cz)
VALUES('first_open', 'První spuštění', 'The first time a user launches an app after installing or re-installing it
This event is not triggered when a user downloads the app onto a device, but instead when he or she first uses it. To see raw download numbers, look in Google Play Developer Console or in iTunesConnect.', 'Případy prvního spuštění aplikace po instalaci nebo re-instalaci. Na rozdíl od počtu stažení se počítá až po spuštění aplikace');
INSERT INTO analytic.firebase_pidlitacka_events_table
(event_name, event_name_cz, description, description_cz)
VALUES('notification_dismiss', 'Odstranění notifikace', 'when a user dismisses a notification sent by FCM. Android apps only.', 'Případy, kdy uživatel zruší notifikaci. Platí pouze pro uživatele Android');
INSERT INTO analytic.firebase_pidlitacka_events_table
(event_name, event_name_cz, description, description_cz)
VALUES('notification_foreground', 'Obdržení notifikace během užívání aplikace', 'when a notification sent by FCM is received while the app is in the foreground.', 'Přijetí notifikace během aktivního využívání aplikace, kdy je aplikace v popředí zařízení');
INSERT INTO analytic.firebase_pidlitacka_events_table
(event_name, event_name_cz, description, description_cz)
VALUES('notification_open', 'Otevření notifikace', 'when a user opens a notification sent by FCM.', 'Otevření přijaté notifikace');
INSERT INTO analytic.firebase_pidlitacka_events_table
(event_name, event_name_cz, description, description_cz)
VALUES('notification_receive', 'Obdržení notifikace na zařízení Android', 'when a notification sent by FCM is received by a device. Android apps only.', 'Přijetí notifikace ve chvíli, kdy aplikace není aktivně využívána v popředí. Pouze pro uživatele Android');
INSERT INTO analytic.firebase_pidlitacka_events_table
(event_name, event_name_cz, description, description_cz)
VALUES('os_update', 'Aktualizace systému', 'when the device operating system is updated to a new version. The previous operating system version id is passed as a parameter.', 'Aktualizace operačního systému používaného zařízení.');
INSERT INTO analytic.firebase_pidlitacka_events_table
(event_name, event_name_cz, description, description_cz)
VALUES('parkingDetail', 'Zakoupené parkování', 'Při otevření browseru z detailu parkoviště nebo zóny', 'Při otevření browseru z detailu parkoviště nebo zóny');
INSERT INTO analytic.firebase_pidlitacka_events_table
(event_name, event_name_cz, description, description_cz)
VALUES('screen_view', 'Zobrazení aplikace', 'when a screen transition occurs and any of the following criteria are met:
No screen was previously set
The new screen name differs from the previous screen name
The new screen-class name differs from the previous screen-class name
The new screen id differs from the previous screen id', 'Zobrazení aplikace v popředí');
INSERT INTO analytic.firebase_pidlitacka_events_table
(event_name, event_name_cz, description, description_cz)
VALUES('session_start', 'Opětovné využití aplikace po neaktivitě', 'when a user engages the app for more than the minimum session duration after a period of inactivity that exceeds the session timeout duration.', 'Využití aplikace po uplynutí minimální doby nutné pro "spuštění neakivity"');
INSERT INTO analytic.firebase_pidlitacka_events_table
(event_name, event_name_cz, description, description_cz)
VALUES('ticketActivation', 'Aktivace jízdenky', NULL, 'V případě, že uživatel aktivuje jízdenku, a to v jakémkoli režimu');
INSERT INTO analytic.firebase_pidlitacka_events_table
(event_name, event_name_cz, description, description_cz)
VALUES('user_engagement', 'Počet uživatelů', 'periodically, while the app is in the foreground.', 'Využití aplikace i v případě nepřihlášených uživatelů');
INSERT INTO analytic.firebase_pidlitacka_events_table
(event_name, event_name_cz, description, description_cz)
VALUES('HASNFC_USERS', 'Zapnuté NFC', NULL, 'Uživatelé, kteří mají na svém zařízení povolenou funkci NFC');
INSERT INTO analytic.firebase_pidlitacka_events_table
(event_name, event_name_cz, description, description_cz)
VALUES('NOTI_USERS', 'Zapnuté notifikace', NULL, 'Uživatelé, kteří mají povolenou funkci "Zasílat notifikace".');
INSERT INTO analytic.firebase_pidlitacka_events_table
(event_name, event_name_cz, description, description_cz)
VALUES('MAIL_USERS', 'Zasílání dokladu na email', NULL, 'Uživatelé, kteří mají na svém zařízení povolenou funkci "Zasílat daňový doklad na email".');
	
	

-- analytic.v_ropidbi_litacka_functions

create or replace view analytic.v_ropidbi_litacka_functions as
select 
ue.event_date, 
ue.users as users,
fce,
a.users n_fce
from
(select  
event_date,
mail_users users,
'MAIL_USERS' fce
from public.firebase_pidlitacka_applaunch_par
 union all
select  
event_date, noti_users users, 'NOTI_USERS' fce
from public.firebase_pidlitacka_applaunch_par 
union all
select  
event_date, hasnfc_users users, 'HASNFC_USERS' fce
from public.firebase_pidlitacka_applaunch_par 
union all
select  
event_date, users users, event_name fce
from public.firebase_pidlitacka_events
where event_name = 'departureSearch' 
union all
select  
event_date, users users, event_name fce
from public.firebase_pidlitacka_events
where event_name = 'connectionSearch' 
union all
select  
event_date, users users, event_name fce
from public.firebase_pidlitacka_events
where event_name = 'parkingDetail' 
union all
select  
event_date, users users, event_name fce
from public.firebase_pidlitacka_events
where event_name = 'app_remove' 
union all
select  
event_date, users users, event_name fce
from public.firebase_pidlitacka_events
where event_name = 'appLaunch' 
) a
left join
(select  
event_date, users as users
from public.firebase_pidlitacka_events
where event_name = 'user_engagement' 
) ue
on ue.event_date = a.event_date
order by 1,3;



-- naplnění tabluky
INSERT INTO analytic.pid_cenik
(tarif, cestující, platnost, zóny, "Číslo_tarifu_cptp", cena_s_dph)
VALUES('Zvýhodněná, 15 minut, 2 pásma', 'Zvýhodněná', '15 minut', '2 pásma', 3002, 3);
INSERT INTO analytic.pid_cenik
(tarif, cestující, platnost, zóny, "Číslo_tarifu_cptp", cena_s_dph)
VALUES('Zvýhodněná, 30 minut, 2 pásma', 'Zvýhodněná', '30 minut', '2 pásma', 3001, 4);
INSERT INTO analytic.pid_cenik
(tarif, cestující, platnost, zóny, "Číslo_tarifu_cptp", cena_s_dph)
VALUES('Zvýhodněná, 60 minut, 3 pásma', 'Zvýhodněná', '60 minut', '3 pásma', 3001, 6);
INSERT INTO analytic.pid_cenik
(tarif, cestující, platnost, zóny, "Číslo_tarifu_cptp", cena_s_dph)
VALUES('Zvýhodněná, 90 minut, 4 pásma', 'Zvýhodněná', '90 minut', '4 pásma', 3001, 8);
INSERT INTO analytic.pid_cenik
(tarif, cestující, platnost, zóny, "Číslo_tarifu_cptp", cena_s_dph)
VALUES('Zvýhodněná, 120 minut, 5 pásem', 'Zvýhodněná', '120 minut', '5 pásem', 3001, 10);
INSERT INTO analytic.pid_cenik
(tarif, cestující, platnost, zóny, "Číslo_tarifu_cptp", cena_s_dph)
VALUES('Zvýhodněná, 150 minut, 6 pásem', 'Zvýhodněná', '150 minut', '6 pásem', 3001, 11);
INSERT INTO analytic.pid_cenik
(tarif, cestující, platnost, zóny, "Číslo_tarifu_cptp", cena_s_dph)
VALUES('Dospělý, 15 minut, 2 pásma', 'Dospělý', '15 minut', '2 pásma', 102, 12);
INSERT INTO analytic.pid_cenik
(tarif, cestující, platnost, zóny, "Číslo_tarifu_cptp", cena_s_dph)
VALUES('Dítě, 30 minut, Praha', 'Dítě', '30 minut', 'Praha', 601, 12);
INSERT INTO analytic.pid_cenik
(tarif, cestující, platnost, zóny, "Číslo_tarifu_cptp", cena_s_dph)
VALUES('Senior, 30 minut, Praha', 'Senior', '30 minut', 'Praha', 601, 12);
INSERT INTO analytic.pid_cenik
(tarif, cestující, platnost, zóny, "Číslo_tarifu_cptp", cena_s_dph)
VALUES('Zvýhodněná, 180 minut, 7 pásem', 'Zvýhodněná', '180 minut', '7 pásem', 3001, 13);
INSERT INTO analytic.pid_cenik
(tarif, cestující, platnost, zóny, "Číslo_tarifu_cptp", cena_s_dph)
VALUES('Zvýhodněná, 210 minut, 8 pásem', 'Zvýhodněná', '210 minut', '8 pásem', 3001, 15);
INSERT INTO analytic.pid_cenik
(tarif, cestující, platnost, zóny, "Číslo_tarifu_cptp", cena_s_dph)
VALUES('Senior, 90 minut, Praha', 'Senior', '90 minut', 'Praha', 601, 16);
INSERT INTO analytic.pid_cenik
(tarif, cestující, platnost, zóny, "Číslo_tarifu_cptp", cena_s_dph)
VALUES('Dítě, 90 minut, Praha', 'Dítě', '90 minut', 'Praha', 601, 16);
INSERT INTO analytic.pid_cenik
(tarif, cestující, platnost, zóny, "Číslo_tarifu_cptp", cena_s_dph)
VALUES('Zvýhodněná, 240 minut, 9 pásem', 'Zvýhodněná', '240 minut', '9 pásem', 3001, 17);
INSERT INTO analytic.pid_cenik
(tarif, cestující, platnost, zóny, "Číslo_tarifu_cptp", cena_s_dph)
VALUES('Dospělý, 30 minut, 2 pásma', 'Dospělý', '30 minut', '2 pásma', 101, 18);
INSERT INTO analytic.pid_cenik
(tarif, cestující, platnost, zóny, "Číslo_tarifu_cptp", cena_s_dph)
VALUES('Zvýhodněná, 4,5 hodiny, 10 pásem', 'Zvýhodněná', '4,5 hodiny', '10 pásem', 3001, 19);
INSERT INTO analytic.pid_cenik
(tarif, cestující, platnost, zóny, "Číslo_tarifu_cptp", cena_s_dph)
VALUES('Zvýhodněná, 5 hodin, 11 pásem', 'Zvýhodněná', '5 hodin', '11 pásem', 3001, 21);
INSERT INTO analytic.pid_cenik
(tarif, cestující, platnost, zóny, "Číslo_tarifu_cptp", cena_s_dph)
VALUES('Dospělý, 30 minut, Praha', 'Dospělý', '30 minut', 'Praha', 101, 24);
INSERT INTO analytic.pid_cenik
(tarif, cestující, platnost, zóny, "Číslo_tarifu_cptp", cena_s_dph)
VALUES('Dospělý, 60 minut, 3 pásma', 'Dospělý', '60 minut', '3 pásma', 101, 24);
INSERT INTO analytic.pid_cenik
(tarif, cestující, platnost, zóny, "Číslo_tarifu_cptp", cena_s_dph)
VALUES('Dospělý, 90 minut, Praha', 'Dospělý', '90 minut', 'Praha', 101, 32);
INSERT INTO analytic.pid_cenik
(tarif, cestující, platnost, zóny, "Číslo_tarifu_cptp", cena_s_dph)
VALUES('Dospělý, 90 minut, 4 pásma', 'Dospělý', '90 minut', '4 pásma', 101, 32);
INSERT INTO analytic.pid_cenik
(tarif, cestující, platnost, zóny, "Číslo_tarifu_cptp", cena_s_dph)
VALUES('Zvýhodněná, 24 hodin, vnější pásma', 'Zvýhodněná', '24 hodin', 'vnější pásma', 3010, 37);
INSERT INTO analytic.pid_cenik
(tarif, cestující, platnost, zóny, "Číslo_tarifu_cptp", cena_s_dph)
VALUES('Dospělý, 2 hodiny, 5 pásem', 'Dospělý', '2 hodiny', '5 pásem', 101, 40);
INSERT INTO analytic.pid_cenik
(tarif, cestující, platnost, zóny, "Číslo_tarifu_cptp", cena_s_dph)
VALUES('Dospělý, 2,5 hodiny, 6 pásem', 'Dospělý', '2,5 hodiny', '6 pásem', 101, 46);
INSERT INTO analytic.pid_cenik
(tarif, cestující, platnost, zóny, "Číslo_tarifu_cptp", cena_s_dph)
VALUES('Dospělý, 3 hodiny, 7 pásem', 'Dospělý', '3 hodiny', '7 pásem', 101, 54);
INSERT INTO analytic.pid_cenik
(tarif, cestující, platnost, zóny, "Číslo_tarifu_cptp", cena_s_dph)
VALUES('Senior, 24 hodin, Praha', 'Senior', '24 hodin', 'Praha', 607, 55);
INSERT INTO analytic.pid_cenik
(tarif, cestující, platnost, zóny, "Číslo_tarifu_cptp", cena_s_dph)
VALUES('Dítě, 24 hodin, Praha', 'Dítě', '24 hodin', 'Praha', 607, 55);
INSERT INTO analytic.pid_cenik
(tarif, cestující, platnost, zóny, "Číslo_tarifu_cptp", cena_s_dph)
VALUES('Dospělý, 3,5 hodiny, 8 pásem', 'Dospělý', '3,5 hodiny', '8 pásem', 101, 62);
INSERT INTO analytic.pid_cenik
(tarif, cestující, platnost, zóny, "Číslo_tarifu_cptp", cena_s_dph)
VALUES('Dospělý, 4 hodiny, 9 pásem', 'Dospělý', '4 hodiny', '9 pásem', 101, 68);
INSERT INTO analytic.pid_cenik
(tarif, cestující, platnost, zóny, "Číslo_tarifu_cptp", cena_s_dph)
VALUES('Dospělý, 4,5 hodiny, 10 pásem', 'Dospělý', '4,5 hodiny', '10 pásem', 101, 76);
INSERT INTO analytic.pid_cenik
(tarif, cestující, platnost, zóny, "Číslo_tarifu_cptp", cena_s_dph)
VALUES('Dospělý, 5 hodin, 11 pásem', 'Dospělý', '5 hodin', '11 pásem', 101, 84);
INSERT INTO analytic.pid_cenik
(tarif, cestující, platnost, zóny, "Číslo_tarifu_cptp", cena_s_dph)
VALUES('Dospělý, 5,5 hodiny, 12 pásem', 'Dospělý', '5,5 hodiny', '12 pásem', 101, 92);
INSERT INTO analytic.pid_cenik
(tarif, cestující, platnost, zóny, "Číslo_tarifu_cptp", cena_s_dph)
VALUES('Dospělý, 6 hodin, 13 pásem', 'Dospělý', '6 hodin', '13 pásem', 101, 100);
INSERT INTO analytic.pid_cenik
(tarif, cestující, platnost, zóny, "Číslo_tarifu_cptp", cena_s_dph)
VALUES('Dospělý, 24 hodin, Praha', 'Dospělý', '24 hodin', 'Praha', 107, 110);
INSERT INTO analytic.pid_cenik
(tarif, cestující, platnost, zóny, "Číslo_tarifu_cptp", cena_s_dph)
VALUES('Dospělý, 24 hodin, vnější pásma', 'Dospělý', '24 hodin', 'vnější pásma', 110, 150);
INSERT INTO analytic.pid_cenik
(tarif, cestující, platnost, zóny, "Číslo_tarifu_cptp", cena_s_dph)
VALUES('Dospělý, 24 hodin, Praha XXL', 'Dospělý', '24 hodin', 'Praha XXL', 109, 160);
INSERT INTO analytic.pid_cenik
(tarif, cestující, platnost, zóny, "Číslo_tarifu_cptp", cena_s_dph)
VALUES('Dospělý, 24 hodin, celosíťová', 'Dospělý', '24 hodin', 'celosíťová', 105, 240);
INSERT INTO analytic.pid_cenik
(tarif, cestující, platnost, zóny, "Číslo_tarifu_cptp", cena_s_dph)
VALUES('Dospělý, 72 hodin, Praha', 'Dospělý', '72 hodin', 'Praha', 104, 310);
INSERT INTO analytic.pid_cenik
(tarif, cestující, platnost, zóny, "Číslo_tarifu_cptp", cena_s_dph)
VALUES('Přepravné zavazadlo, pes (pes)', NULL, NULL, NULL, 1801, 16);
INSERT INTO analytic.pid_cenik
(tarif, cestující, platnost, zóny, "Číslo_tarifu_cptp", cena_s_dph)
VALUES('Přepravné zavazadlo, pes (zavazadlo)', NULL, NULL, NULL, 1801, 16);

