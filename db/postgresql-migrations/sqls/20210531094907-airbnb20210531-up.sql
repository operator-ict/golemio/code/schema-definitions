-- python.airbnb_apartment_count definition
CREATE TABLE if not exists python.airbnb_apartment_count (
	nazev_mc varchar(500) NULL,
	bytovy_fond numeric NULL,
	valid_year date NULL
);

-- python.airbnb_listings definition
CREATE TABLE if not exists python.airbnb_listings (
	room_id int8 NULL,
	"name" text NULL,
	propertytype text NULL,
	roomtype text NULL,
	bathrooms float8 NULL,
	bedrooms float8 NULL,
	capacity float8 NULL,
	cleaningfee float8 NULL,
	latitude float8 NULL,
	longitude float8 NULL,
	minnights float8 NULL,
	picturecount float8 NULL,
	overall_rating float8 NULL,
	user_id float8 NULL,
	user_name text NULL,
	user_superhost bool NULL,
	reviews_total float8 NULL,
	room_created timestamp NULL,
	room_lastupdate timestamp NULL,
	created_by text NULL,
	created_at timestamp NULL
);

-- python.airbnb_occupancy definition
CREATE TABLE if not exists python.airbnb_occupancy (
	room_id int8 NULL,
	yearmon timestamp NULL,
	days_occupied float8 NULL,
	occupancy_rate float8 NULL,
	mean_price float8 NULL,
	reviews_count float8 NULL,
	created_by text NULL,
	created_at timestamp NULL
);

-- python.sreality definition
CREATE TABLE if not exists python.sreality (
	week_label varchar(50) NULL,
	week_number numeric NOT NULL,
	offer_type varchar(50) NULL,
	district varchar(50) NULL,
	category varchar(50) NULL,
	subcategory varchar(50) NULL,
	offers_count numeric NULL,
	mean_price float8 NULL,
	median_price float8 NULL
);

-- analytic.v_airbnb_dim_district source
drop VIEW if exists analytic.v_airbnb_dim_district;
CREATE OR REPLACE VIEW analytic.v_airbnb_dim_district
AS SELECT DISTINCT d.district_name::character varying(500) AS nazev_mc,
    ltrim("right"(d.district_name::text, 2))::numeric AS order_by
   FROM common.citydistricts d
  WHERE d.district_name::text <> ''::text AND (ltrim("right"(d.district_name::text, 2)) ~ '^[0-9]+$'::text) = true
UNION ALL
 SELECT d.district_name::character varying(500) AS nazev_mc,
    100 + row_number() OVER (ORDER BY d.district_name) AS order_by
   FROM ( SELECT DISTINCT citydistricts.district_name
           FROM common.citydistricts
          WHERE (ltrim("right"(citydistricts.district_name::text, 2)) ~ '^[0-9]+$'::text) = false AND (citydistricts.district_name::text <> ALL (ARRAY[''::character varying::text, 'nazev_mc'::character varying::text]))) d;



drop view if exists analytic.v_airbnb_dim_offertype;
drop VIEW if exists analytic.v_airbnb_dim_regularity;
-- analytic.v_airbnb_districts_last12m source
drop VIEW analytic.v_airbnb_districts_last12m; 	-- vytvoření za v_airbnb_occupancy_monthly_materialized
drop VIEW analytic.v_airbnb_start_end_monthly;
-- analytic.v_airbnb_occupancy_monthly_materialized source
drop VIEW analytic.v_airbnb_overview_12m;		-- vytvoření za v_airbnb_occupancy_monthly_materialized
drop VIEW analytic.v_airbnb_room_per_user;		-- vytvoření za v_airbnb_occupancy_monthly_materialized
drop view analytic.v_airbnb_occupancy_monthly;
drop MATERIALIZED VIEW analytic.v_airbnb_occupancy_monthly_materialized;

drop VIEW analytic.v_airbnb_listings;
-- analytic.v_airbnb_listings source
CREATE OR REPLACE VIEW analytic.v_airbnb_listings
AS WITH last_activity AS (
         SELECT airbnb_occupancy.room_id,
            max(concat(airbnb_occupancy.yearmon, '-01')::date) AS last_activity
           FROM python.airbnb_occupancy
          WHERE airbnb_occupancy.reviews_count > 0::numeric::double precision
          GROUP BY airbnb_occupancy.room_id
        )
 SELECT l.room_id,
    l.name,
        CASE
            WHEN l.roomtype = 'Entire home/apt'::text THEN 'Celý dům/byt'::text
            WHEN l.roomtype = 'Private room'::text THEN 'Celý pokoj'::text
            WHEN l.roomtype = 'Shared room'::text THEN 'Sdílený pokoj'::text
            WHEN l.roomtype = 'Hotel room'::text THEN 'Hotelový pokoj'::text
            ELSE NULL::text
        END AS offer_type,
    l.capacity,
    l.cleaningfee,
    l.overall_rating,
    l.reviews_total,
    l.room_created,
    citydistricts.district_name::character varying(500) AS nazev_mc,
        CASE
            WHEN la.last_activity >= date_trunc('month'::text, now() - '3 mons'::interval)::date THEN 'Otevřená'::text
            ELSE 'Ukončená'::text
        END AS open_closed,
        CASE
            WHEN la.last_activity >= date_trunc('month'::text, now() - '3 mons'::interval)::date THEN NULL::date::timestamp without time zone
            WHEN la.last_activity IS NULL THEN l.room_created + '1 mon'::interval
            ELSE la.last_activity + '1 mon'::interval
        END::date AS ended_at,
    l.user_id,
        CASE
            WHEN l.longitude::text <> ''::text THEN l.longitude::numeric
            ELSE NULL::numeric
        END AS longitude,
        CASE
            WHEN l.latitude::text <> ''::text THEN l.latitude::numeric
            ELSE NULL::numeric
        END AS latitude,
    l.created_at AS record_created_at
   FROM python.airbnb_listings l
     JOIN common.citydistricts ON st_within(st_setsrid(st_point(l.longitude, l.latitude), 4326), citydistricts.geom)
     LEFT JOIN last_activity la ON l.room_id::text = la.room_id::text
  WHERE l.roomtype IS NOT NULL AND l.roomtype <> ''::text AND l.latitude::text <> ''::text AND l.longitude::text <> ''::text;
  
CREATE MATERIALIZED VIEW analytic.v_airbnb_occupancy_monthly_materialized
TABLESPACE pg_default
AS WITH constants AS (
         SELECT 0.55 AS reviewers_ratio,
            3.2 AS avg_accomodation_duration
        ), yearly_occupancy AS (
         SELECT airbnb_occupancy.room_id,
            "left"(airbnb_occupancy.yearmon::text, 4) AS o_year,
            (( SELECT constants.avg_accomodation_duration
                   FROM constants))::double precision * sum(airbnb_occupancy.reviews_count) / (( SELECT constants.reviewers_ratio
                   FROM constants))::double precision AS yearly_estimated_days_occupied
           FROM python.airbnb_occupancy
          GROUP BY airbnb_occupancy.room_id, ("left"(airbnb_occupancy.yearmon::text, 4))
        ), last_12m_occupancy AS (
         SELECT airbnb_occupancy.room_id,
            (( SELECT constants.avg_accomodation_duration
                   FROM constants))::double precision * sum(airbnb_occupancy.reviews_count) / (( SELECT constants.reviewers_ratio
                   FROM constants))::double precision AS yearly_estimated_days_occupied
           FROM python.airbnb_occupancy
          WHERE concat(airbnb_occupancy.yearmon, '-01')::date >= (date_trunc('month'::text, airbnb_occupancy.created_at) - '1 year'::interval) AND concat(airbnb_occupancy.yearmon, '-01')::date < date_trunc('month'::text, airbnb_occupancy.created_at)
          GROUP BY airbnb_occupancy.room_id
        )
 SELECT o.room_id,
    "left"(o.yearmon::text, 4) AS o_year,
    concat(o.yearmon, '-01')::date AS o_month,
    l.nazev_mc,
    l.offer_type,
    l.room_created,
    l.open_closed,
    l.ended_at,
    o.reviews_count / (( SELECT constants.reviewers_ratio
           FROM constants))::double precision AS stays_count,
    (( SELECT constants.avg_accomodation_duration
           FROM constants))::double precision * o.reviews_count / (( SELECT constants.reviewers_ratio
           FROM constants))::double precision AS estimated_days_occupied,
    date_part('day'::text, concat(o.yearmon, '-01')::date + '1 mon'::interval - concat(o.yearmon, '-01')::date::timestamp without time zone) AS days_in_month,
    o.reviews_count,
    o.days_occupied,
    0::numeric AS esimated_price_per_night,
    o.mean_price,
        CASE
            WHEN yo.yearly_estimated_days_occupied > 60::numeric::double precision THEN 'Pravidelné'::text
            ELSE 'Příležitostní'::text
        END AS regularity2,
    l.user_id,
    bf.bytovy_fond,
    l.ended_at AS ukonceno,
        CASE
            WHEN yo.yearly_estimated_days_occupied > 60::numeric::double precision AND l.offer_type = 'Celý dům/byt'::text THEN 'Pravidelné, celý byt/dům'::text
            ELSE 'Sdílená ekonomika'::text
        END AS offer_type_group,
        CASE
            WHEN concat(o.yearmon, '-01')::date >= (date_trunc('month'::text, o.created_at) - '1 year'::interval) AND concat(o.yearmon, '-01')::date < date_trunc('month'::text, o.created_at) THEN 'Y'::text
            ELSE 'N'::text
        END AS last_12m,
    l.latitude,
    l.longitude,
    o.created_at AS record_created_at,
        CASE
            WHEN concat(o.yearmon, '-01')::date >= (date_trunc('month'::text, o.created_at) - '1 year'::interval) AND concat(o.yearmon, '-01')::date < date_trunc('month'::text, o.created_at) THEN true
            ELSE false
        END AS last_12_months,
        CASE
            WHEN lo.yearly_estimated_days_occupied > 60::numeric::double precision THEN 'Pravidelné'::text
            ELSE 'Příležitostní'::text
        END AS regularity_last_12m,
        CASE
            WHEN lo.yearly_estimated_days_occupied > 60::numeric::double precision AND l.offer_type = 'Celý dům/byt'::text THEN 'Pravidelné, celý byt/dům'::text
            ELSE 'Sdílená ekonomika'::text
        END AS offer_type_group_last_12m
   FROM python.airbnb_occupancy o
     JOIN analytic.v_airbnb_listings l ON o.room_id::text = l.room_id::text
     LEFT JOIN yearly_occupancy yo ON yo.room_id::text = o.room_id::text AND yo.o_year = "left"(o.yearmon::text, 4)
     LEFT JOIN python.airbnb_apartment_count bf ON l.nazev_mc::text = bf.nazev_mc::text
     LEFT JOIN last_12m_occupancy lo ON lo.room_id::text = o.room_id::text
  WHERE concat(o.yearmon, '-01')::date >= l.room_created AND concat(o.yearmon, '-01')::date < date_trunc('month'::text, o.created_at) AND concat(o.yearmon, '-01')::date >= '2017-01-01'::date AND l.nazev_mc::text <> ''::text AND l.nazev_mc IS NOT NULL AND o.reviews_count > 0::numeric::double precision
WITH DATA;

-- analytic.v_airbnb_occupancy_monthly source
CREATE OR REPLACE VIEW analytic.v_airbnb_occupancy_monthly
AS SELECT v_airbnb_occupancy_monthly.room_id,
    v_airbnb_occupancy_monthly.o_year,
    v_airbnb_occupancy_monthly.o_month,
    v_airbnb_occupancy_monthly.nazev_mc,
    v_airbnb_occupancy_monthly.offer_type,
    v_airbnb_occupancy_monthly.room_created,
    v_airbnb_occupancy_monthly.open_closed,
    v_airbnb_occupancy_monthly.ended_at,
    v_airbnb_occupancy_monthly.stays_count,
    v_airbnb_occupancy_monthly.estimated_days_occupied,
    v_airbnb_occupancy_monthly.days_in_month,
    v_airbnb_occupancy_monthly.reviews_count,
    v_airbnb_occupancy_monthly.days_occupied,
    v_airbnb_occupancy_monthly.esimated_price_per_night,
    v_airbnb_occupancy_monthly.mean_price,
    v_airbnb_occupancy_monthly.regularity2,
    v_airbnb_occupancy_monthly.user_id,
    v_airbnb_occupancy_monthly.bytovy_fond,
    v_airbnb_occupancy_monthly.ukonceno,
    v_airbnb_occupancy_monthly.offer_type_group,
    v_airbnb_occupancy_monthly.last_12m,
    v_airbnb_occupancy_monthly.latitude,
    v_airbnb_occupancy_monthly.longitude,
    v_airbnb_occupancy_monthly.record_created_at,
    v_airbnb_occupancy_monthly.last_12_months,
    v_airbnb_occupancy_monthly.regularity_last_12m,
    v_airbnb_occupancy_monthly.offer_type_group_last_12m
   FROM analytic.v_airbnb_occupancy_monthly_materialized v_airbnb_occupancy_monthly;	 
   
CREATE OR REPLACE VIEW analytic.v_airbnb_districts_last12m
AS WITH districts_list AS (
         SELECT v_airbnb_occupancy_monthly.nazev_mc,
            count(DISTINCT v_airbnb_occupancy_monthly.room_id) AS active_count
           FROM analytic.v_airbnb_occupancy_monthly
          WHERE v_airbnb_occupancy_monthly.last_12_months = true AND v_airbnb_occupancy_monthly.reviews_count > 0::numeric::double precision
          GROUP BY v_airbnb_occupancy_monthly.nazev_mc
         HAVING count(DISTINCT v_airbnb_occupancy_monthly.room_id) >= 10
        )
 SELECT el.nazev_mc,
    'Ukončené nabídky'::text AS metric,
    count(el.room_id) AS metric_value
   FROM analytic.v_airbnb_listings el
     JOIN districts_list dl ON el.nazev_mc::text = dl.nazev_mc::text
  WHERE el.ended_at IS NOT NULL AND el.nazev_mc IS NOT NULL AND date_trunc('month'::text, el.ended_at + '3 mons'::interval) >= (date_trunc('month'::text, el.record_created_at) - '1 year'::interval) AND date_trunc('month'::text, el.ended_at + '3 mons'::interval) < date_trunc('month'::text, el.record_created_at)
  GROUP BY el.nazev_mc
UNION ALL
 SELECT nl.nazev_mc,
    'Nové nabídky'::text AS metric,
    count(nl.room_id) AS metric_value
   FROM analytic.v_airbnb_listings nl
     JOIN districts_list dl ON nl.nazev_mc::text = dl.nazev_mc::text
  WHERE nl.nazev_mc IS NOT NULL AND nl.room_created >= (date_trunc('month'::text, nl.record_created_at) - '1 year'::interval) AND nl.room_created < date_trunc('month'::text, nl.record_created_at)
  GROUP BY nl.nazev_mc
UNION ALL
 SELECT districts_list.nazev_mc,
    'Počet aktivních nabídek'::text AS metric,
    districts_list.active_count AS metric_value
   FROM districts_list
UNION ALL
 SELECT o1.nazev_mc,
    'Medián ceny za celý byt/dům'::text AS metric,
    round(percentile_disc(0.5::double precision) WITHIN GROUP (ORDER BY o1.mean_price)) AS metric_value
   FROM analytic.v_airbnb_occupancy_monthly o1
     JOIN districts_list dl ON o1.nazev_mc::text = dl.nazev_mc::text
  WHERE o1.last_12_months = true AND o1.nazev_mc IS NOT NULL AND o1.offer_type = 'Celý dům/byt'::text AND o1.mean_price < 5250::numeric::double precision AND o1.mean_price > 588::numeric::double precision AND o1.reviews_count > 0::numeric::double precision
  GROUP BY o1.nazev_mc
UNION ALL
 SELECT o2.nazev_mc,
    'Medián ceny za pokoj'::text AS metric,
    round(percentile_disc(0.5::double precision) WITHIN GROUP (ORDER BY o2.mean_price)) AS metric_value
   FROM analytic.v_airbnb_occupancy_monthly o2
     JOIN districts_list dl ON o2.nazev_mc::text = dl.nazev_mc::text
  WHERE o2.last_12_months = true AND o2.nazev_mc IS NOT NULL AND o2.offer_type <> 'Celý dům/byt'::text AND o2.mean_price < 5250::numeric::double precision AND o2.mean_price > 588::numeric::double precision AND o2.reviews_count > 0::numeric::double precision
  GROUP BY o2.nazev_mc
UNION ALL
 SELECT o3.nazev_mc,
    'Počet pobytů'::text AS metric,
    round(sum(o3.stays_count)) AS metric_value
   FROM analytic.v_airbnb_occupancy_monthly o3
     JOIN districts_list dl ON o3.nazev_mc::text = dl.nazev_mc::text
  WHERE o3.last_12_months = true AND o3.nazev_mc IS NOT NULL AND o3.reviews_count > 0::numeric::double precision
  GROUP BY o3.nazev_mc;  
  
-- analytic.v_airbnb_room_per_user source
CREATE OR REPLACE VIEW analytic.v_airbnb_room_per_user
AS SELECT date_trunc('year'::text, v_airbnb_occupancy_monthly.o_month::timestamp with time zone) AS o_year,
    v_airbnb_occupancy_monthly.user_id,
    count(DISTINCT v_airbnb_occupancy_monthly.room_id) AS room_count
   FROM analytic.v_airbnb_occupancy_monthly
  WHERE v_airbnb_occupancy_monthly.user_id::text <> ''::text
  GROUP BY (date_trunc('year'::text, v_airbnb_occupancy_monthly.o_month::timestamp with time zone)), v_airbnb_occupancy_monthly.user_id;  
  
-- analytic.v_airbnb_overview_12m source
CREATE OR REPLACE VIEW analytic.v_airbnb_overview_12m
AS WITH pravidelny AS (
         SELECT o.nazev_mc,
            count(DISTINCT o.room_id) AS pravidelny_count
           FROM analytic.v_airbnb_occupancy_monthly o
          WHERE o.offer_type_group_last_12m = 'Pravidelné, celý byt/dům'::text AND o.last_12_months = true
          GROUP BY o.nazev_mc
        ), prilezitostni AS (
         SELECT o.nazev_mc,
            count(DISTINCT o.room_id) AS prilezitostni_count
           FROM analytic.v_airbnb_occupancy_monthly o
          WHERE o.offer_type_group_last_12m = 'Sdílená ekonomika'::text AND o.last_12_months = true
          GROUP BY o.nazev_mc
        )
 SELECT bf.nazev_mc,
    pra.pravidelny_count,
    pri.prilezitostni_count,
    bf.bytovy_fond,
    pra.pravidelny_count + pri.prilezitostni_count AS airbnb_rented,
    bf.bytovy_fond - (pra.pravidelny_count + pri.prilezitostni_count)::numeric AS airbnb_not_rented,
    bf.bytovy_fond - pra.pravidelny_count::numeric AS airbnb_not_rented_or_irregular
   FROM python.airbnb_apartment_count bf
     JOIN pravidelny pra ON bf.nazev_mc::text = pra.nazev_mc::text
     JOIN prilezitostni pri ON bf.nazev_mc::text = pri.nazev_mc::text;  
  
   
-- analytic.v_airbnb_start_end_monthly source
CREATE OR REPLACE VIEW analytic.v_airbnb_start_end_monthly
AS WITH ended AS (
         SELECT date_trunc('month'::text, v_airbnb_listings.ended_at + '3 mons'::interval) AS ended_month,
            v_airbnb_listings.nazev_mc,
            v_airbnb_listings.offer_type,
            count(v_airbnb_listings.room_id) AS ended_count
           FROM analytic.v_airbnb_listings
          WHERE v_airbnb_listings.ended_at IS NOT NULL
          GROUP BY v_airbnb_listings.nazev_mc, v_airbnb_listings.offer_type, (date_trunc('month'::text, v_airbnb_listings.ended_at + '3 mons'::interval))
        )
 SELECT date_trunc('month'::text, created.room_created::timestamp with time zone) AS months,
    count(DISTINCT created.room_id) AS created_count,
    ended.ended_count,
    created.nazev_mc,
    created.offer_type,
        CASE
            WHEN date_trunc('month'::text, created.room_created::timestamp with time zone) >= (date_trunc('month'::text, created.record_created_at) - '1 year'::interval) THEN 'Y'::text
            ELSE 'N'::text
        END AS last_12m
   FROM analytic.v_airbnb_listings created
     LEFT JOIN ended ON ended.ended_month = date_trunc('month'::text, created.room_created::timestamp with time zone) AND ended.nazev_mc::text = created.nazev_mc::text AND ended.offer_type = created.offer_type
  WHERE created.room_created >= '2017-01-01'::date AND created.room_created < date_trunc('month'::text, created.record_created_at)
  GROUP BY created.nazev_mc, created.offer_type, (date_trunc('month'::text, created.room_created::timestamp with time zone)), ended.ended_count, (date_trunc('month'::text, created.record_created_at));






   
   
-- analytic.v_airbnb_dim_regularity source
CREATE OR REPLACE VIEW analytic.v_airbnb_dim_regularity
AS SELECT DISTINCT v_airbnb_occupancy_monthly.regularity2
   FROM analytic.v_airbnb_occupancy_monthly
  WHERE v_airbnb_occupancy_monthly.regularity2 <> ''::text AND v_airbnb_occupancy_monthly.regularity2 IS NOT NULL;  
  

-- analytic.v_airbnb_overview_12m source
drop VIEW analytic.v_airbnb_overview_12m;
CREATE OR REPLACE VIEW analytic.v_airbnb_overview_12m
AS WITH pravidelny AS (
         SELECT o.nazev_mc,
            count(DISTINCT o.room_id) AS pravidelny_count
           FROM analytic.v_airbnb_occupancy_monthly o
          WHERE o.offer_type_group_last_12m = 'Pravidelné, celý byt/dům'::text AND o.last_12_months = true
          GROUP BY o.nazev_mc
        ), prilezitostni AS (
         SELECT o.nazev_mc,
            count(DISTINCT o.room_id) AS prilezitostni_count
           FROM analytic.v_airbnb_occupancy_monthly o
          WHERE o.offer_type_group_last_12m = 'Sdílená ekonomika'::text AND o.last_12_months = true
          GROUP BY o.nazev_mc
        )
 SELECT bf.nazev_mc,
    pra.pravidelny_count,
    pri.prilezitostni_count,
    bf.bytovy_fond,
    pra.pravidelny_count + pri.prilezitostni_count AS airbnb_rented,
    bf.bytovy_fond - (pra.pravidelny_count + pri.prilezitostni_count)::numeric AS airbnb_not_rented,
    bf.bytovy_fond - pra.pravidelny_count::numeric AS airbnb_not_rented_or_irregular
   FROM python.airbnb_apartment_count bf
     JOIN pravidelny pra ON bf.nazev_mc::text = pra.nazev_mc::text
     JOIN prilezitostni pri ON bf.nazev_mc::text = pri.nazev_mc::text;
   

-- analytic.v_airbnb_dim_offertype source
CREATE OR REPLACE VIEW analytic.v_airbnb_dim_offertype
AS SELECT DISTINCT v_airbnb_listings.offer_type
   FROM analytic.v_airbnb_listings
  WHERE v_airbnb_listings.offer_type <> ''::text AND v_airbnb_listings.offer_type IS NOT NULL;


-- drop tables from public

drop table public.airbnb_apartment_count;
drop table public.airbnb_listings;
drop table public.airbnb_occupancy;




