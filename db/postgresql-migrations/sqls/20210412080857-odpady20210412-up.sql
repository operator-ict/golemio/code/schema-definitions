-- kontejnery - nahradit existující view analytic.v_containers_containers 
-- nové view přidává svozové dny a datum posledního updatu - to je důležité pro filtrování zrušených kontejnerů

create or replace view analytic.v_containers_containers as
with unn as (
			select code, 
					unnest(string_to_array(pick_days::text,','::text))::int as day_num
			from containers_containers
			),
translate as (
			select code,
					day_num,
					case when day_num = 1 then 'Po'
						 when day_num = 2 then 'Út'
						 when day_num = 3 then 'St'
						 when day_num = 4 then 'Čt'
						 when day_num = 5 then 'Pá'
						 when day_num = 6 then 'So'
						 when day_num = 7 then 'Ne'
					end as day_name
			from unn
			),
		aa as (
			select code,
					string_agg(day_name,', ' order by day_num) pick_days
			from translate 
			group by code
			)
			select cc.code as container_id,
					cc.total_volume,
					cc.network,
					cc.container_type,
					cc.bin_type,
					case when cc.trash_type = 1 then 'glass coloured'
						when cc.trash_type = 2 then 'electronics'
						when cc.trash_type = 3 then 'metal'
						when cc.trash_type = 4 then 'bevarage cartons'
						when cc.trash_type = 5 then 'paper'
						when cc.trash_type = 6 then 'plastic'
						when cc.trash_type = 7 then 'glass clear'
						when cc.trash_type = 8 then 'textile'
						else null end as trash_type,
					case when cc.trash_type = 1 then 'Sklo barevné'
						when cc.trash_type = 2 then 'Elektronika'
						when cc.trash_type = 3 then 'Kovy'
						when cc.trash_type = 4 then 'Nápojové kartony'
						when cc.trash_type = 5 then 'Papír'
						when cc.trash_type = 6 then 'Plast'
						when cc.trash_type = 7 then 'Sklo čiré'
						when cc.trash_type = 8 then 'Textil'
						else null end as typ_odpadu,
					case when cc.trash_type = 1 then 411
						when cc.trash_type = 3 then 120
						when cc.trash_type = 4 then 55
						when cc.trash_type = 5 then 76
						when cc.trash_type = 6 then 39
						when cc.trash_type = 7 then 411
						else null end as koeficient,
					cc.cleaning_frequency_frequency,
					cc.cleaning_frequency_interval,
					cs.latitude,
					cs.longitude,
					case when cs.address = ' Francouzská  240/76' then substring(cs.address from 2)
						else cs.address end as address,
					cd.district_cz,
					concat(1,right(split_part(cs.district, '-', 2),1),'000') as psc,
					cc.sensor_id,
					cc.updated_at,
					aa.pick_days,
					row_number() over (partition by address, trash_type)
			from public.containers_containers as cc
			left join public.containers_stations as cs on cc.station_code = cs.code
			left join analytic.containers_districts as cd on cs.district = cd.district
			left join aa on cc.code = aa.code
			where cc.code in (select distinct container_code from containers_measurement where percent_calculated is not null)
					and cs.address is not null;

comment on column analytic.v_containers_containers.district_cz is 'Jde o přidruženou tabulku s českým přepisem názvů městských částí (napsat funkci, která správně opraví interpunkci, nejde). Zdrojové csv se nachází na: https://operator.sharepoint.com/:x:/s/Datova_Platforma/EVIvMiZGvm1Ep4d5naJ5x3UBLrZctzUMuxt7RGgMn2VD9A?e=H3bDWz';


-- nahrazuje public.v_containers_full_days, uvádá intervaly plnosti >95% kont. ohraničené každým dnem. tzn když je plný od 1.1.2021 16:00 do 2.1.2021 8:00, vznikne 1.1.2021 16:00-23:59 a 2.1.2021 00:00 - 8:00		
drop view if exists analytic.v_containers_full_days;
create or replace view analytic.v_containers_full_days as 
	with base as (
				select container_code as container_id, measured_at, percent_calculated
				from public.containers_measurement
				),
status_tbl as (
		 		select *,				
					   case when percent_calculated > 95 then
					   			case when lag(percent_calculated, 1) over (partition by container_id order by measured_at asc) < 96
					   						and measured_at::date - lead(measured_at::date, 1) over (partition by container_id order by measured_at asc) <> 0 then 'begin_till_midnight'
					   				 when measured_at::date - lag(measured_at::date, 1) over (partition by container_id order by measured_at asc) <> 0
					   				 		and lag(percent_calculated, 1) over (partition by container_id order by measured_at asc) > 95 then 'begin_from_midnight'
					   				 when lag(percent_calculated, 1) over (partition by container_id order by measured_at asc) < 96 then 'begin'
									 when measured_at::date - lead(measured_at::date, 1) over (partition by container_id order by measured_at asc) <> 0 then 'end_at_midnight'
									 else 'body' end
							when lag(percent_calculated, 1) over (partition by container_id order by measured_at asc) > 95 then 
								 case when measured_at::date - lag(measured_at::date, 1) over (partition by container_id order by measured_at asc) <> 0 then 'end_from_midnight'
									  else 'end' end
							else 'ok' end as status
				from base
				order by container_id, measured_at
			),
start_end as (
				select container_id,
						measured_at,
						percent_calculated,
						status,
						case when status = 'end_from_midnight' then date_trunc('day', measured_at)
							 when status = 'begin' then measured_at
							 when status = 'begin_till_midnight' then measured_at
							 when status = 'begin_from_midnight' then date_trunc('day', measured_at)
							 else null end as start_time,
						case when status like 'begin%' and measured_at::date - lead(measured_at::date, 1) over (partition by container_id order by measured_at asc) <> 0 then date_trunc('day', measured_at) + interval '1 day'
							 when status like 'begin%' and lead(status, 1) over (partition by container_id order by measured_at asc) = 'end_at_midnight' then date_trunc('day', measured_at) + interval '1 day'
							 when status like 'begin%' and lead(status, 1) over (partition by container_id order by measured_at asc) = 'end' then lead(measured_at, 1) over (partition by container_id order by measured_at)
							 when status = 'end_from_midnight' then measured_at
							 else null end as end_time
				from status_tbl
				where status in ('begin_till_midnight', 'begin_from_midnight','begin', 'end_at_midnight', 'end_from_midnight', 'end') 
				order by container_id, measured_at
				)
				select container_id,
						date_trunc('day', start_time) as date,
						start_time,
						end_time
				from start_end
				where status = 'end_from_midnight' or status like 'begin%';

-- celé intervaly plnosti > 95% i přes půlnoc
-- příklad: koš:0001/ 001C00101	2020-11-25 18:01:39	2020-12-01 10:01:39

drop view if exists analytic.v_containers_full;
create or replace view analytic.v_containers_full as
	with base as (
				select container_code as container_id, measured_at, percent_calculated
				from public.containers_measurement
				),
status_tbl as (
		 		select *,				
					   case when percent_calculated > 95 then
					   		case when lag(percent_calculated, 1) over (partition by container_id order by measured_at asc) < 96 then 'begin'
								else 'body' end
							when lag(percent_calculated, 1) over (partition by container_id order by measured_at asc) > 95 then 'end'
							else 'ok' end as status
				from base
				order by container_id, measured_at
			),
start_end as (
				select container_id,
						measured_at,
						percent_calculated,
						status,
						case when status = 'begin' then measured_at
							 else null end as start_time,
						case when status = 'begin' then coalesce((lead(measured_at, 1) over (partition by container_id order by measured_at)), (date_trunc('day', measured_at) + interval '1 day'))::timestamptz
							 else null end as end_time
				from status_tbl
				where status in ('begin','end') 
				order by container_id, measured_at
				)
				select container_id,
						date_trunc('day', start_time) as date,
						start_time,
						end_time
				from start_end
				where status = 'begin';

-- pick_dates - svozy kontejnerů podle harmonogramu v Heliosu
drop VIEW if exists analytic.v_containers_pick_dates;
CREATE OR REPLACE VIEW analytic.v_containers_pick_dates
AS WITH meas AS (
         SELECT containers_measurement.container_code,
            containers_measurement.measured_at_utc,
            containers_measurement.percent_calculated,
            rank() OVER (PARTITION BY containers_measurement.container_code, (date_trunc('day'::text, containers_measurement.measured_at_utc)) ORDER BY containers_measurement.measured_at_utc) AS rank
           FROM containers_measurement
        ), pick AS (
         SELECT cc.code AS container_id,
            concat(pd.pick_date::date, ' 7:00')::timestamp with time zone AS pick_date,
            'Pražské služby - hlášený svoz'::text AS pick_event,
            2 AS rank
           FROM containers_picks_dates pd
             LEFT JOIN containers_containers cc ON pd.container_id::text = cc.id::text
          WHERE (cc.code::text IN ( SELECT DISTINCT containers_measurement.container_code
                   FROM containers_measurement)) AND pd.pick_date >= (( SELECT min(containers_measurement.measured_at_utc) AS min
                   FROM containers_measurement)) AND pd.pick_date <= now()
        )
 SELECT COALESCE(meas.container_code, pick.container_id) AS container_id,
    COALESCE(meas.measured_at_utc::timestamp with time zone, pick.pick_date) AS measured_at_utc,
    COALESCE(meas.percent_calculated, 0) AS percent_calculated,
    pick.pick_event
   FROM meas
     FULL JOIN pick ON meas.container_code::text = pick.container_id::text AND meas.measured_at_utc::date = pick.pick_date::date AND meas.rank = pick.rank;
    
    
  --dále smazat na Golemovi:
  -- analytic.containers_blockege_hour
  drop table if exists analytic.containers_blockege_hour;
  
  -- analytic.v_containers_full_hour
  
  
  -- analytic.v_containers_full_share
  drop VIEW if exists analytic.v_containers_full_share;
  -- public.containers_full_hour
  DROP TABLE public.containers_full_hour;  
  DROP TABLE analytic.containers_full_hour;  
  -- analytic.set_containers_full_hour (procedura)
  drop FUNCTION analytic.set_containers_full_hour();
 