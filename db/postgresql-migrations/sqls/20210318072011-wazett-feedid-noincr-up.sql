ALTER TABLE public.wazett_feeds ALTER COLUMN id DROP DEFAULT;

DROP SEQUENCE IF EXISTS public.wazett_feeds_id_seq;