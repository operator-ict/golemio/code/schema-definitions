ALTER TABLE rsd_tmc_admins ADD version_id varchar(50);

update rsd_tmc_admins 
set version_id = 'v8.0'
where true;

ALTER TABLE rsd_tmc_admins drop CONSTRAINT rsd_tmc_admins_pkey;
ALTER TABLE rsd_tmc_admins ADD CONSTRAINT rsd_tmc_admins_pkey PRIMARY KEY (lcd,version_id);

--
ALTER TABLE rsd_tmc_points ADD version_id varchar(50);

update rsd_tmc_points 
set version_id = 'v8.0'
where true;

ALTER TABLE rsd_tmc_points drop CONSTRAINT rsd_tmc_points_pkey;
ALTER TABLE rsd_tmc_points ADD CONSTRAINT rsd_tmc_points_pkey PRIMARY KEY (lcd,version_id);

ALTER TABLE rsd_tmc_points
ALTER COLUMN wgs84_x TYPE numeric USING replace(wgs84_x,',','.')::numeric,
ALTER COLUMN wgs84_y TYPE numeric USING replace(wgs84_y,',','.')::numeric,
ALTER COLUMN sjtsk_x TYPE numeric USING replace(sjtsk_x,',','.')::numeric,
ALTER COLUMN sjtsk_y TYPE numeric USING replace(sjtsk_y,',','.')::numeric
;

ALTER TABLE rsd_tmc_points ADD wgs84_point geography(POINT,4326);
update rsd_tmc_points
set wgs84_point = ST_Point( wgs84_x, wgs84_y)::geography
where true;


--
ALTER TABLE rsd_tmc_roads ADD version_id varchar(50);

update rsd_tmc_roads 
set version_id = 'v8.0'
where true;

ALTER TABLE rsd_tmc_roads drop CONSTRAINT rsd_tmc_roads_pkey;
ALTER TABLE rsd_tmc_roads ADD CONSTRAINT rsd_tmc_roads_pkey PRIMARY KEY (lcd,version_id);

--
ALTER TABLE rsd_tmc_segments ADD version_id varchar(50);

update rsd_tmc_segments 
set version_id = 'v8.0'
where true;

ALTER TABLE rsd_tmc_segments drop CONSTRAINT rsd_tmc_segments_pkey;
ALTER TABLE rsd_tmc_segments ADD CONSTRAINT rsd_tmc_segments_pkey PRIMARY KEY (lcd,version_id);



