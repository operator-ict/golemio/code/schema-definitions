/* Replace with your SQL commands */
CREATE OR REPLACE VIEW analytic.v_containers_24h_missing
AS 
select container_code as container_id,
		MAX(measured_at)  measured_at 
from  public.containers_measurement
group by container_code
having max(measured_at)  < (now() - interval '1d') or max(measured_at) is null;
