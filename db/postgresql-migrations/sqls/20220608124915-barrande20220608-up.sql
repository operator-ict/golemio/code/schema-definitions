-- franz.v_barrandov_bridge_route_details source

CREATE OR REPLACE VIEW analytic.v_barrandov_bridge_route_details
AS SELECT re.id,
    re.name,
    re.section,
    re.reverse_direction,
    re.from_name,
    re.to_name,
    re.poradi
   FROM ( SELECT r.id,
            r.name,
                CASE
                    WHEN r.name ~~ '% ÚS %'::text THEN true
                    WHEN r.name ~~ '% TN %'::text THEN false
                    ELSE NULL::boolean
                END AS section,
                CASE
                    WHEN r.name ~~ '% OS%'::text THEN true
                    ELSE false
                END AS reverse_direction,
            r.from_name,
            r.to_name,
            row_number() OVER (PARTITION BY r.name ORDER BY r.id DESC) AS poradi
           FROM wazett_routes r
          WHERE "left"(r.name, 2) = 'BM'::text) re;
		  
-- franz.v_barrandov_bridge_route_live source

CREATE OR REPLACE VIEW analytic.v_barrandov_bridge_route_live
AS SELECT wrl.route_id,
    wrl.update_time,
    wrl."time",
    wrl.length,
    wrl.historic_time,
    wrl.jam_level,
    wrl.create_batch_id,
    wrl.created_at,
    wrl.created_by,
    wrl.update_batch_id,
    wrl.updated_at,
    wrl.updated_by
   FROM wazett_route_lives wrl
  WHERE (wrl.route_id IN ( SELECT DISTINCT wazett_routes.id
           FROM wazett_routes
          WHERE wazett_routes.name ~~ 'BM %'::text)) AND wrl.update_time::double precision >= (date_part('epoch'::text, CURRENT_DATE - '7 days'::interval) * 1000::double precision);
		  
