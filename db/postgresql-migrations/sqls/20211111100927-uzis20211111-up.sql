CREATE TABLE uzis.covid19_vaccinated_split_deaths (
	datum timestamp NULL,
	krajkod text NULL,
	zem_celkem int8 NULL,
	zem_bez_ockovani int8 NULL,
	proc_zem_bez_ockovani float8 NULL,
	zem_vek_bez_ockovani_vek float8 NULL,
	zem_po_prvni int8 NULL,
	proc_zem_po_prvni float8 NULL,
	zem_vek_po_prvni float8 NULL,
	zem_po_ukonceni int8 NULL,
	proc_zem_po_ukonceni float8 NULL,
	zem_vek_po_ukonceni float8 NULL,
	zem_po_ukonceni_s_posilujici int8 NULL,
	proc_zem_po_ukonceni_s_posilujici float8 NULL,
	zem_vek_po_ukonceni_s_posilujici float8 NULL,
	updated_at timestamp NULL,
	updated_by text NULL,
	created_at timestamp NULL,
	created_by text NULL
);


CREATE TABLE uzis.covid19_vaccinated_split_hospitalized (
	datum timestamp NULL,
	krajkod text NULL,
	hos_celkem int8 NULL,
	hos_bez_ockovani int8 NULL,
	proc_hos_bez_ockovani float8 NULL,
	hos_vek_bez_ockovani_vek float8 NULL,
	hos_po_prvni int8 NULL,
	proc_hos_po_prvni float8 NULL,
	hos_vek_po_prvni float8 NULL,
	hos_po_ukonceni int8 NULL,
	proc_hos_po_ukonceni float8 NULL,
	hos_vek_po_ukonceni1 float8 NULL,
	hos_po_ukonceni_s_posilujici int8 NULL,
	proc_hos_po_ukonceni_s_posilujici float8 NULL,
	hos_vek_po_ukonceni_s_posilujici float8 NULL,
	updated_at timestamp NULL,
	updated_by text NULL,
	created_at timestamp NULL,
	created_by text NULL
);


CREATE TABLE uzis.covid19_vaccinated_split_icu (
	datum timestamp NULL,
	krajkod text NULL,
	jip_celkem int8 NULL,
	jip_bez_ockovani int8 NULL,
	proc_jip_bez_ockovani float8 NULL,
	jip_vek_bez_ockovani_vek float8 NULL,
	jip_po_prvni int8 NULL,
	proc_jip_po_prvni float8 NULL,
	jip_vek_po_prvni float8 NULL,
	jip_po_ukonceni int8 NULL,
	proc_jip_po_ukonceni float8 NULL,
	jip_vek_po_ukonceni float8 NULL,
	jip_po_ukonceni_s_posilujici int8 NULL,
	proc_jip_po_ukonceni_s_posilujici float8 NULL,
	jip_vek_po_ukonceni_s_posilujici float8 NULL,
	updated_at timestamp NULL,
	updated_by text NULL,
	created_at timestamp NULL,
	created_by text NULL
);


CREATE TABLE uzis.covid19_vaccinated_split_incidence (
	datum timestamp NULL,
	krajkod text NULL,
	poz_celkem int8 NULL,
	poz_bez_ockovani int8 NULL,
	proc_poz_bez_ockovani float8 NULL,
	poz_vek_bez_ockovani_vek float8 NULL,
	poz_po_prvni int8 NULL,
	proc_poz_po_prvni float8 NULL,
	poz_vek_po_prvni float8 NULL,
	poz_po_ukonceni int8 NULL,
	proc_poz_po_ukonceni float8 NULL,
	poz_vek_po_ukonceni float8 NULL,
	poz_po_ukonceni_s_posilujici int8 NULL,
	proc_poz_po_ukonceni_s_posilujici float8 NULL,
	poz_vek_po_ukonceni_s_posilujici float8 NULL,
	updated_at timestamp NULL,
	updated_by text NULL,
	created_at timestamp NULL,
	created_by text NULL
);


CREATE TABLE uzis.covid19_vaccinated_split_incidence65 (
	datum timestamp NULL,
	krajkod text NULL,
	p65_celkem int8 NULL,
	p65_bez_ockovani int8 NULL,
	proc_p65_bez_ockovani float8 NULL,
	p65_vek_bez_ockovani_vek float8 NULL,
	p65_po_prvni int8 NULL,
	proc_p65_po_prvni float8 NULL,
	p65_vek_po_prvni float8 NULL,
	p65_po_ukonceni int8 NULL,
	proc_p65_po_ukonceni float8 NULL,
	p65_vek_po_ukonceni float8 NULL,
	p65_po_ukonceni_s_posilujici int8 NULL,
	proc_p65_po_ukonceni_s_posilujici float8 NULL,
	p65_vek_po_ukonceni_s_posilujici float8 NULL,
	updated_at timestamp NULL,
	updated_by text NULL,
	created_at timestamp NULL,
	created_by text NULL
);