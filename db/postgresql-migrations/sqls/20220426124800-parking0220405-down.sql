ALTER TABLE public.parkings DROP COLUMN parking_type;
ALTER TABLE public.parkings DROP COLUMN zone_type;
ALTER TABLE public.parkings DROP COLUMN centroid;
ALTER TABLE public.parkings DROP COLUMN android_app_payment_url;
ALTER TABLE public.parkings DROP COLUMN ios_app_payment_url;
ALTER TABLE public.parkings RENAME COLUMN web_app_payment_url TO payment_url;

ALTER TABLE public.parkings_tariffs DROP COLUMN accepts_litacka;

DROP TABLE IF EXISTS parkings_measurements_actual;

