DROP TABLE IF EXISTS  public.bicyclecounters_api_logs_failures;
DROP SEQUENCE IF EXISTS public.bicyclecounters_api_logs_failures_pk;

DROP TABLE IF EXISTS  public.bicyclecounters_api_logs_hits;
DROP SEQUENCE IF EXISTS public.bicyclecounters_api_logs_hits_pk;
