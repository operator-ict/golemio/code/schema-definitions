/* Replace with your SQL commands */

drop TABLE public.citydistricts;

delete from  meta."extract"
where id_dataset = (select id_dataset from meta.dataset where code_dataset = 'common_tables');

delete from meta.dataset
where code_dataset = 'common_tables';