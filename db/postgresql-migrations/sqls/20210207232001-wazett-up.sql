CREATE SEQUENCE public.wazett_feeds_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 2147483647
  START 1
  CACHE 1;

CREATE TABLE public.wazett_feeds (
    "id" integer DEFAULT nextval('public.wazett_feeds_id_seq'),
    "area_name" text,
    "broadcaster_id" text,
    "name" text,
    "bbox" jsonb,
    "ismetric" boolean,

    create_batch_id bigint,
    created_at timestamp with time zone,
    created_by character varying(150),
    update_batch_id bigint,
    updated_at timestamp with time zone,
    updated_by character varying(150),
    CONSTRAINT "wazett_feeds_pkey" PRIMARY KEY ("id")
) WITH (oids = false);


CREATE TABLE public.wazett_routes (
    "id" integer NOT NULL,
    "feed_id" integer,
    "from_name" text,
    "to_name" text,
    "name" text,
    "bbox" jsonb,
    "last_update" bigint,
    "line" geometry(LINESTRING, 4326),

    create_batch_id bigint,
    created_at timestamp with time zone,
    created_by character varying(150),
    update_batch_id bigint,
    updated_at timestamp with time zone,
    updated_by character varying(150),
    CONSTRAINT "wazett_routes_pkey" PRIMARY KEY ("id"),
    CONSTRAINT "wazett_routes_to_feed_id" FOREIGN KEY ("feed_id")
        REFERENCES public.wazett_feeds (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
) WITH (oids = false);


CREATE TABLE public.wazett_jams_stats (
    "feed_id" integer,
    "update_time" bigint NOT NULL,
    "jam_level" integer,
    "wazers_count" integer,
    "length_of_jams" integer,
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    CONSTRAINT "wazett_jams_stats_pkey" PRIMARY KEY (feed_id,update_time,jam_level),
    CONSTRAINT "wazett_jams_stats_to_feed_id" FOREIGN KEY ("feed_id")
        REFERENCES public.wazett_feeds (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
) WITH (oids = false);


CREATE SEQUENCE public.wazett_subroutes_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 2147483647
  START 1
  CACHE 1;

CREATE TABLE public.wazett_subroutes (
    "id" integer DEFAULT nextval('public.wazett_subroutes_id_seq') NOT NULL,
    "route_id" integer,
    "line" geometry(LINESTRING, 4326),
    "from_name" text,
    "to_name" text,

    create_batch_id bigint,
    created_at timestamp with time zone,
    created_by character varying(150),
    update_batch_id bigint,
    updated_at timestamp with time zone,
    updated_by character varying(150),
    CONSTRAINT "wazett_subroutes_pkey" PRIMARY KEY ("id"),
    CONSTRAINT "wazett_subroutes_to_route_id" FOREIGN KEY ("route_id")
        REFERENCES public.wazett_routes (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
) WITH (oids = false);


CREATE TABLE public.wazett_route_lives (
    "route_id" integer,
    "update_time" bigint NOT NULL,
    "time" integer,
    "length" integer,
    "historic_time" integer,
    "jam_level" integer,

    create_batch_id bigint,
    created_at timestamp with time zone,
    created_by character varying(150),
    update_batch_id bigint,
    updated_at timestamp with time zone,
    updated_by character varying(150),
    CONSTRAINT "wazett_route_lives_pkey" PRIMARY KEY (update_time,route_id),
    CONSTRAINT "wazett_route_lives_to_route_id" FOREIGN KEY ("route_id")
        REFERENCES public.wazett_routes (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
) WITH (oids = false);


CREATE TABLE public.wazett_subroute_lives (
    "route_id" integer,
    "update_time" bigint NOT NULL,
    "subroute_id" integer,
    "time" integer,
    "length" integer,
    "historic_time" integer,
    "jam_level" integer,

    create_batch_id bigint,
    created_at timestamp with time zone,
    created_by character varying(150),
    update_batch_id bigint,
    updated_at timestamp with time zone,
    updated_by character varying(150),
    CONSTRAINT "wazett_subroute_lives_pkey" PRIMARY KEY (update_time,route_id,subroute_id),
    CONSTRAINT "wazett_subroute_lives_to_route_id" FOREIGN KEY ("route_id")
        REFERENCES public.wazett_routes (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT "wazett_subroute_lives_to_subroute_id" FOREIGN KEY ("subroute_id")
        REFERENCES public.wazett_subroutes (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
) WITH (oids = false);

