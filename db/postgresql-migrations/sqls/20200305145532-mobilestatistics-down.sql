delete from meta."extract"
where name_extract = 'mobileappstatistics_appstore';
delete from meta."extract"
where name_extract = 'mobileappstatistics_playstore';

drop table if exists public.mobileappstatistics_appstore;
drop table if exists public.mobileappstatistics_playstore;

delete from meta.dataset
where code_dataset = 'mobileappstatistics';