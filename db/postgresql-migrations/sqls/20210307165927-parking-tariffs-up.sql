ALTER TABLE public.parkings RENAME COLUMN usage_scenario TO category;
ALTER TABLE public.parkings ALTER COLUMN category TYPE varchar(100);

ALTER TABLE public.parkings ADD COLUMN tariff_id varchar(50) NULL;
ALTER TABLE public.parkings ADD COLUMN valid_from timestamptz NULL;
ALTER TABLE public.parkings ADD COLUMN valid_to timestamptz NULL;

CREATE SEQUENCE public.parkings_tariffs_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

CREATE TABLE public.parkings_tariffs
(
    id int NOT NULL UNIQUE DEFAULT nextval('parkings_tariffs_id_seq'::regclass),
    tariff_id varchar(50) NOT NULL,
    source character varying(255) NOT NULL,
    last_updated timestamptz NOT NULL,
    payment_mode varchar(255) NOT NULL,
    payment_additional_description text,
    free_of_charge boolean NOT NULL,
    url_link_address text,
    charge_band_name varchar(50) NOT NULL, -- tariff name
    accepts_payment_card boolean,
    accepts_cash boolean,
    accepts_mobile_payment boolean,
    charge_currency varchar(50) NOT NULL,
    charge numeric NOT NULL,
    charge_type varchar(50),
    charge_order_index smallint NOT NULL,
    charge_interval int,
    max_iterations_of_charge int,
    min_iterations_of_charge int,
    start_time_of_period varchar(50),
    end_time_of_period varchar(50),
    allowed_vehicle_type varchar(255),
    allowed_fuel_type varchar(255),

    create_batch_id bigint,
    created_at timestamptz,
    created_by varchar(150),
    update_batch_id bigint,
    updated_at timestamptz,
    updated_by varchar(150),

    CONSTRAINT parkings_tariffs_pk PRIMARY KEY (tariff_id, charge_order_index)
);

