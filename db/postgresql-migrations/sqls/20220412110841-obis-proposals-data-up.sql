CREATE TABLE python.obis_proposals (
	tt_usneseni_id int8 NULL,
	usnkoho text NULL,
	cislotisku text NULL,
	cislousneseni int8 NULL,
	datumjednani timestamp NULL,
	nazevtisku text NULL,
	stavmaterialu text NULL,
	zpracovatel text NULL,
	zpr_id_utv text NULL,
	zpr_zkr_utv text NULL,
	zpr_nazev_utv text NULL,
	predkladatel text NULL,
	id_predkl_osoba text NULL,
	zalozil text NULL,
	tc_stadok_id int8 NULL,
	varianta text NULL,
	vh_spolecnost text NULL,
	kdy_ins timestamp NULL,
	kdy_upd timestamp NULL,
	vliv_na_rozpocet float8 NULL,
	predkl_zast int8 NULL,
	cislojednani int8 NULL,
	datum_zah_1faze timestamp NULL,
	datum_uko_pfaze timestamp NULL,
	pripdnucelk int8 NULL,
	tc_pripfaze_id int8 NULL,
	tc_multicis_id int8 NULL,
	pripmist_popis text NULL,
	pocet_pripom int8 NULL,
	pripmist_stav text NULL,
	start_faze_od timestamp NULL,
	datum_podpisu timestamp NULL,
	dnu_pripom int8 NULL,
	updated_at timestamp NULL
);

CREATE TABLE python.obis_proposals_departments (
	id int8 NULL,
	platnost int8 NULL,
	utvar_nazev text NULL,
	utvar_cislo int8 NULL,
	utvar_nadrizeny_cislo int8 NULL,
	utvar_kod text NULL,
	updated_at timestamp NULL
);

CREATE TABLE python.obis_proposals_employees (
	id int8 NULL,
	platnost int8 NULL,
	titul_pred text NULL,
	jmeno text NULL,
	jmeno2 text NULL,
	prijmeni text NULL,
	prijmeni2 text NULL,
	titul_za text NULL,
	misto_id int8 NULL,
	misto_jmeno text NULL,
	odbor_id int8 NULL,
	odbor_jmeno text NULL,
	oddeleni_id int8 NULL,
	oddeleni_jmeno text NULL,
	updated_at timestamp NULL
);

CREATE OR REPLACE VIEW analytic.v_obis_proposals
AS SELECT obis_proposals.cislotisku AS cislo_tisku,
    obis_proposals.nazevtisku AS nazev_tisku,
    obis_proposals.tt_usneseni_id AS usneseni_id,
    obis_proposals.cislousneseni AS cislo_usneseni,
    obis_proposals.usnkoho AS usneseni_koho,
    obis_proposals.cislojednani AS cislo_jednani,
    obis_proposals.datumjednani::date AS datum_jednani,
    obis_proposals.tc_stadok_id AS tisk_status_id,
    obis_proposals.stavmaterialu AS tisk_status,
    obis_proposals.zalozil,
    obis_proposals.kdy_ins::date AS zalozil_date,
    obis_proposals.kdy_upd::date AS updated_date,
        CASE
            WHEN date_part('year'::text, obis_proposals.datum_zah_1faze::date) < 2000::double precision OR obis_proposals.datum_zah_1faze::date IS NULL THEN COALESCE(obis_proposals.datum_uko_pfaze::date, obis_proposals.datumjednani::date) - '2 days'::interval
            ELSE obis_proposals.datum_zah_1faze::date::timestamp without time zone
        END AS prvni_faze_start,
    COALESCE(obis_proposals.datum_uko_pfaze::date, obis_proposals.datumjednani::date) AS ukonceni_datum,
    obis_proposals.pripdnucelk AS prip_pocet_dnu
   FROM python.obis_proposals
  GROUP BY obis_proposals.cislotisku, obis_proposals.nazevtisku, obis_proposals.tt_usneseni_id, obis_proposals.cislousneseni, obis_proposals.usnkoho, obis_proposals.cislojednani, (obis_proposals.datumjednani::date), obis_proposals.tc_stadok_id, obis_proposals.stavmaterialu, obis_proposals.zalozil, (obis_proposals.kdy_ins::date), (obis_proposals.kdy_upd::date), (
        CASE
            WHEN date_part('year'::text, obis_proposals.datum_zah_1faze::date) < 2000::double precision OR obis_proposals.datum_zah_1faze::date IS NULL THEN COALESCE(obis_proposals.datum_uko_pfaze::date, obis_proposals.datumjednani::date) - '2 days'::interval
            ELSE obis_proposals.datum_zah_1faze::date::timestamp without time zone
        END), (COALESCE(obis_proposals.datum_uko_pfaze::date, obis_proposals.datumjednani::date)), obis_proposals.pripdnucelk;

CREATE OR REPLACE VIEW analytic.v_obis_comments
AS SELECT t.tc_multicis_id AS prip_id,
    COALESCE((ozn.jmeno || ' '::text) || ozn.prijmeni, oun.utvar_nazev, t.pripmist_popis) AS prip_jmeno,
    t.pocet_pripom AS prip_pocet,
    t.tc_pripfaze_id AS prip_faze,
    t.pripmist_stav AS prip_stav,
    t.start_faze_od AS prip_start_faze,
    COALESCE(t.datum_podpisu, a.end_date::timestamp without time zone) AS prip_datum_podpisu,
        CASE
            WHEN t.dnu_pripom = 0 THEN 1::bigint
            ELSE abs(t.dnu_pripom)
        END AS dnu_pripom,
    t.cislotisku AS tisk_id
   FROM python.obis_proposals t
     LEFT JOIN ( SELECT obis_proposals_employees.id,
            obis_proposals_employees.platnost,
            obis_proposals_employees.titul_pred,
            obis_proposals_employees.jmeno,
            obis_proposals_employees.jmeno2,
            obis_proposals_employees.prijmeni,
            obis_proposals_employees.prijmeni2,
            obis_proposals_employees.titul_za,
            obis_proposals_employees.misto_id,
            obis_proposals_employees.misto_jmeno,
            obis_proposals_employees.odbor_id,
            obis_proposals_employees.odbor_jmeno,
            obis_proposals_employees.oddeleni_id,
            obis_proposals_employees.oddeleni_jmeno,
            obis_proposals_employees.updated_at
           FROM python.obis_proposals_employees
          WHERE obis_proposals_employees.platnost = 65) ozn ON "right"(t.tc_multicis_id::character varying(50)::text, 5)::integer = ozn.id
     LEFT JOIN ( SELECT obis_proposals_departments.id,
            obis_proposals_departments.platnost,
            obis_proposals_departments.utvar_nazev,
            obis_proposals_departments.utvar_cislo,
            obis_proposals_departments.utvar_nadrizeny_cislo,
            obis_proposals_departments.utvar_kod,
            obis_proposals_departments.updated_at
           FROM python.obis_proposals_departments
          WHERE obis_proposals_departments.platnost = 65) oun ON "right"(t.tc_multicis_id::character varying(50)::text, 5)::integer = oun.id
     JOIN ( SELECT obis_proposals.cislotisku AS tisk_id,
            COALESCE(obis_proposals.datum_uko_pfaze::date, obis_proposals.datumjednani::date) AS end_date
           FROM python.obis_proposals
          GROUP BY obis_proposals.cislotisku, (COALESCE(obis_proposals.datum_uko_pfaze::date, obis_proposals.datumjednani::date))) a ON t.cislotisku = a.tisk_id
  WHERE char_length(t.tc_multicis_id::character varying(50)::text) = 6
UNION ALL
 SELECT t.tc_multicis_id AS prip_id,
    COALESCE((ozn.jmeno || ' '::text) || ozn.prijmeni, t.pripmist_popis) AS prip_jmeno,
    t.pocet_pripom AS prip_pocet,
    t.tc_pripfaze_id AS prip_faze,
    t.pripmist_stav AS prip_stav,
    t.start_faze_od AS prip_start_faze,
    COALESCE(t.datum_podpisu, a.end_date::timestamp without time zone) AS prip_datum_podpisu,
        CASE
            WHEN t.dnu_pripom = 0 THEN 1::bigint
            ELSE abs(t.dnu_pripom)
        END AS dnu_pripom,
    t.cislotisku AS tisk_id
   FROM python.obis_proposals t
     LEFT JOIN ( SELECT obis_proposals_employees.id,
            obis_proposals_employees.platnost,
            obis_proposals_employees.titul_pred,
            obis_proposals_employees.jmeno,
            obis_proposals_employees.jmeno2,
            obis_proposals_employees.prijmeni,
            obis_proposals_employees.prijmeni2,
            obis_proposals_employees.titul_za,
            obis_proposals_employees.misto_id,
            obis_proposals_employees.misto_jmeno,
            obis_proposals_employees.odbor_id,
            obis_proposals_employees.odbor_jmeno,
            obis_proposals_employees.oddeleni_id,
            obis_proposals_employees.oddeleni_jmeno,
            obis_proposals_employees.updated_at
           FROM python.obis_proposals_employees
          WHERE obis_proposals_employees.platnost = 65) ozn ON t.tc_multicis_id = ozn.misto_id
     JOIN ( SELECT obis_proposals.cislotisku AS tisk_id,
            COALESCE(obis_proposals.datum_uko_pfaze::date, obis_proposals.datumjednani::date) AS end_date
           FROM python.obis_proposals
          GROUP BY obis_proposals.cislotisku, (COALESCE(obis_proposals.datum_uko_pfaze::date, obis_proposals.datumjednani::date))) a ON t.cislotisku = a.tisk_id
  WHERE char_length(t.tc_multicis_id::character varying(50)::text) < 6;

CREATE OR REPLACE VIEW analytic.v_obis_processor
AS WITH base AS (
         SELECT obis_proposals.zpr_id_utv,
            obis_proposals.cislotisku AS tisk_id
           FROM python.obis_proposals
          GROUP BY obis_proposals.zpr_id_utv, obis_proposals.cislotisku
        ), unnest AS (
         SELECT unnest(string_to_array(base.zpr_id_utv, ';'::text)) AS zpracovatel_id,
            base.tisk_id
           FROM base
        )
 SELECT unnest.zpracovatel_id,
    unnest.tisk_id,
    u.utvar_kod,
    u.utvar_nazev
   FROM unnest
     LEFT JOIN ( SELECT obis_proposals_departments.id::text AS id,
            obis_proposals_departments.platnost,
            obis_proposals_departments.utvar_nazev,
            obis_proposals_departments.utvar_cislo,
            obis_proposals_departments.utvar_nadrizeny_cislo,
            obis_proposals_departments.utvar_kod,
            obis_proposals_departments.updated_at
           FROM python.obis_proposals_departments) u ON unnest.zpracovatel_id = u.id;

CREATE OR REPLACE VIEW analytic.v_obis_submitter
AS WITH base AS (
         SELECT obis_proposals.id_predkl_osoba,
            obis_proposals.cislotisku AS tisk_id
           FROM python.obis_proposals
          GROUP BY obis_proposals.id_predkl_osoba, obis_proposals.cislotisku
        ), unnest AS (
         SELECT unnest(string_to_array(base.id_predkl_osoba, ';'::text)) AS predkladatel_id,
            base.tisk_id
           FROM base
        )
 SELECT unnest.predkladatel_id,
    unnest.tisk_id,
    z.id,
    z.platnost,
    z.title_prefix AS titul_pred,
    z.name AS jmeno,
    z.mid_name AS jmeno2,
    z.surname AS prijmeni,
    z.surname2 AS prijmeni2,
    z.title_suffix AS titul_za,
    z.position_code AS misto_id,
    z.position_name AS misto_jmeno,
    z.utvar_id AS odbor_id,
    z.utvar_name AS odbor_jmeno,
    z.oddeleni_id,
    z.oddeleni_name AS oddeleni_jmeno,
    z.updated_at
   FROM unnest
     LEFT JOIN ( SELECT obis_proposals_employees.id::text AS id,
            obis_proposals_employees.platnost,
            obis_proposals_employees.titul_pred AS title_prefix,
            obis_proposals_employees.jmeno AS name,
            obis_proposals_employees.jmeno2 AS mid_name,
            obis_proposals_employees.prijmeni AS surname,
            obis_proposals_employees.prijmeni2 AS surname2,
            obis_proposals_employees.titul_za AS title_suffix,
            obis_proposals_employees.misto_id AS position_code,
            obis_proposals_employees.misto_jmeno AS position_name,
            obis_proposals_employees.odbor_id AS utvar_id,
            obis_proposals_employees.odbor_jmeno AS utvar_name,
            obis_proposals_employees.oddeleni_id,
            obis_proposals_employees.oddeleni_jmeno AS oddeleni_name,
            obis_proposals_employees.updated_at
           FROM python.obis_proposals_employees) z ON unnest.predkladatel_id = z.id;

CREATE OR REPLACE VIEW analytic.v_obis_submitter_comments
AS SELECT e.predkladatel_id,
    concat(e.jmeno, ' ', e.prijmeni) AS jmeno_predkladatel,
    i.tisk_id,
    i.prip_id,
    i.prip_jmeno,
        CASE
            WHEN i.dnu_pripom = 0 THEN 1::bigint
            ELSE abs(i.dnu_pripom)
        END AS prip_pocet_dnu
   FROM analytic.v_obis_submitter e
     JOIN analytic.v_obis_comments i ON e.tisk_id = i.tisk_id;

CREATE OR REPLACE VIEW analytic.v_obis_proposals_all
AS SELECT e.id,
    concat(e.jmeno, ' ', e.prijmeni) AS predkladatel_jmeno,
    z.zpracovatel_id,
    z.utvar_kod AS zpracovatel_kod,
    z.utvar_nazev AS zpracovatel_jmeno,
    t.cislo_tisku AS tisk_cislo,
    t.nazev_tisku AS tisk_nazev,
    t.usneseni_id,
    t.cislo_usneseni AS usneseni_cislo,
    t.usneseni_koho,
    t.cislo_jednani AS jednani_cislo,
    t.datum_jednani AS jednani_datum,
    t.tisk_status_id,
    t.tisk_status,
    t.zalozil,
    t.zalozil_date,
    t.updated_date,
    t.prvni_faze_start,
    t.ukonceni_datum,
    t.prip_pocet_dnu,
    e.updated_at,
    i.prip_id,
    i.prip_jmeno,
    i.prip_stav,
    i.prip_faze,
    i.prip_pocet,
    i.prip_start_faze AS prip_start,
    i.prip_datum_podpisu,
    i.dnu_pripom
   FROM analytic.v_obis_submitter e
     LEFT JOIN analytic.v_obis_processor z ON e.tisk_id = z.tisk_id
     LEFT JOIN analytic.v_obis_proposals t ON e.tisk_id = t.cislo_tisku
     LEFT JOIN analytic.v_obis_comments i ON e.tisk_id = i.tisk_id;
