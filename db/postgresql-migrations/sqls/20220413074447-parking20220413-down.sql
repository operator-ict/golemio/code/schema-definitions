-- public.parkings_measurements definition

-- Drop table

-- DROP TABLE public.parkings_measurements;

CREATE TABLE public.parkings_measurements (
	id bigserial NOT NULL,
	"source" varchar(255) NOT NULL,
	source_id varchar(255) NOT NULL,
	available_spot_number int4 NULL,
	closed_spot_number int4 NULL,
	occupied_spot_number int4 NULL,
	total_spot_number int4 NULL,
	date_modified int8 NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT parkings_measurements_pk PRIMARY KEY (id)
);