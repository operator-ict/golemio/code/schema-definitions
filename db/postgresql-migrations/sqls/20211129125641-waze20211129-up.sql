
CREATE TABLE analytic.waze_dashboard_route_name (
	route_id int4 NULL primary key,
	idx int4 NULL,
	"from" varchar(100) NULL,
	"to" varchar(100) NULL,
	db_name varchar(100) NULL,
	"name" varchar(500) NULL
);

CREATE VIEW analytic.v_route_travel_times
AS SELECT ts.route_id,
    ts.year,
    ts.month,
    ts.day,
    ts.dow,
    ts.hour,
    ts.quarter,
    ts.hour + (ts.quarter::numeric / 60::numeric)::double precision AS hour_quarter,
    ((((((((ts.year || '-'::text) || ts.month) || '-'::text) || ts.day) || ' '::text) || ts.hour) || ':'::text) || ts.quarter)::timestamp without time zone AS date,
    avg(ts.travel_time)::integer AS travel_time
   FROM ( SELECT raw.route_id,
            raw.update_time,
            raw.travel_time,
            date_part('year'::text, raw.update_time) AS year,
            date_part('month'::text, raw.update_time) AS month,
            date_part('day'::text, raw.update_time) AS day,
            date_part('dow'::text, raw.update_time) AS dow,
            date_part('hour'::text, raw.update_time) AS hour,
            date_part('minute'::text, raw.update_time) AS minute,
            date_part('minute'::text, raw.update_time)::integer / 15 * 15 AS quarter
           FROM ( SELECT wazett_route_lives.route_id,
                    timezone('Europe/Prague'::text, to_timestamp((wazett_route_lives.update_time / 1000)::double precision)::timestamp without time zone) AS update_time,
                    wazett_route_lives."time" AS travel_time
                   FROM wazett_route_lives
				   join analytic.waze_dashboard_route_name wdrn on wdrn.route_id = wazett_route_lives.route_id
                  /*WHERE wazett_route_lives.route_id = ANY (ARRAY[22690, 22754, 22732, 22697, 22703, 22716, 22720, 
             22715, 22722, 22723, 22724, 22460, 22725, 24260, 24261, 22728, 22729, 22730, 22731, 22733, 22691, 
             22693, 22694, 22695, 22696, 22701, 22702, 22704, 22705, 22706, 22709, 22710, 22711, 22712, 22713,
             22714, 22717, 22718])*/
			 ) raw) ts
  WHERE ts.year > 2000::double precision
  GROUP BY ts.route_id, ts.year, ts.month, ts.day, ts.dow, ts.hour, ts.quarter;
