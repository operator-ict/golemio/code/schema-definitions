TRUNCATE TABLE public.flow_sinks;
alter table public.flow_sinks drop constraint flow_sinkspkey;
ALTER TABLE public.flow_sinks DROP COLUMN cube_id;
alter table public.flow_sinks add constraint flow_sinkspkey primary key (id, output_value_type);