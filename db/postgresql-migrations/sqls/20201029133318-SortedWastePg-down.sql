/* Replace with your SQL commands */

-- drop FUNCTION analytic.set_containers_blocked_hour;

-- drop VIEW analytic.v_containers_full;
-- drop VIEW analytic.v_containers_full_share;
-- drop VIEW analytic.v_containers_missig_data_24;
-- drop VIEW analytic.v_containers_network_reliability;

drop TABLE IF EXISTS public.containers_opening_monitor;
drop TABLE IF EXISTS public.containers_measurement;
drop table IF EXISTS public.containers_containers;
drop TABLE IF EXISTS public.containers_picks;
drop TABLE IF EXISTS public.containers_scenar_items;
drop TABLE IF EXISTS public.containers_stations;
drop TABLE IF EXISTS public.containers_picks_dates;

drop TABLE IF EXISTS common.psc;

drop TABLE IF EXISTS analytic.containers_full_hour;
drop TABLE IF EXISTS analytic.containers_blockege_hour;

-- containers
delete from meta."extract"
where id_dataset = (select id_dataset from meta.dataset where code_dataset ='containers');

delete from meta.dataset
where code_dataset ='containers';
