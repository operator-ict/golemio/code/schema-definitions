/* Replace with your SQL commands */

alter table uzis.covid19_hospital_capacity_details
add column inf_luzka_kyslik_kapacita_volna_covid_pozitivni float8 NULL,
add column inf_luzka_kyslik_kapacita_volna_covid_negativni float8 NULL,
add column inf_luzka_kyslik_kapacita_celkem float8 NULL,
add column inf_luzka_hfno_kapacita_volna_covid_pozitivni float8 NULL,
add column inf_luzka_hfno_kapacita_volna_covid_negativni float8 NULL,
add column inf_luzka_hfno_kapacita_celkem float8 NULL,
add column inf_luzka_upv_kapacita_volna_covid_pozitivni float8 NULL,
add column inf_luzka_upv_kapacita_volna_covid_negativni float8 NULL,
add column inf_luzka_upv_kapacita_celkem float8 NULL;

-- franz.v_uzis_covid19_vaccination_regions_details source

CREATE OR REPLACE VIEW analytic.v_uzis_covid19_vaccination_regions_details
AS SELECT count(*) AS pocet_davek,
    v.datum_vakcinace,
        CASE
            WHEN v.datum_vakcinace >= (CURRENT_DATE - '7 days'::interval) AND v.datum_vakcinace < CURRENT_DATE THEN 'posledních 7 dnů'::text
            WHEN v.datum_vakcinace >= (CURRENT_DATE - '14 days'::interval) AND v.datum_vakcinace < (CURRENT_DATE - '7 days'::interval) THEN 'predchozích 7 dnů'::text
            ELSE NULL::text
        END AS "7_14_dnu",
    date_trunc('week'::text, v.datum_vakcinace)::date AS tyden,
        CASE
            WHEN date_trunc('week'::text, v.datum_vakcinace)::date = date_trunc('week'::text, CURRENT_DATE::timestamp with time zone)::date THEN 'posledni nedokonceny tyden'::text
            ELSE 'cely tyden'::text
        END AS tyden_filter_posledni,
    v.updated_at AS datum_aktualizace,
    v.vakcina,
        CASE
            WHEN v.vekova_skupina = ANY (ARRAY['12-15'::text, '16-17'::text]) THEN '12-17'::text
            ELSE v.vekova_skupina
        END AS vekova_skupina,
        CASE
            WHEN v.vekova_skupina = ANY (ARRAY['12-15'::text, '16-17'::text]) THEN 1
            WHEN v.vekova_skupina = '18-24'::text THEN 2
            WHEN v.vekova_skupina = '25-29'::text THEN 3
            WHEN v.vekova_skupina = '30-34'::text THEN 4
            WHEN v.vekova_skupina = '35-39'::text THEN 5
            WHEN v.vekova_skupina = '40-44'::text THEN 6
            WHEN v.vekova_skupina = '45-49'::text THEN 7
            WHEN v.vekova_skupina = '50-54'::text THEN 8
            WHEN v.vekova_skupina = '55-59'::text THEN 9
            WHEN v.vekova_skupina = '60-64'::text THEN 10
            WHEN v.vekova_skupina = '65-69'::text THEN 11
            WHEN v.vekova_skupina = '70-74'::text THEN 12
            WHEN v.vekova_skupina = '75-79'::text THEN 13
            WHEN v.vekova_skupina = '80+'::text THEN 14
            WHEN v.vekova_skupina = 'Neznámá'::text THEN 15
            ELSE 15
        END AS vekova_skupina_order,
        CASE
            WHEN v.indikace_zdravotnik = 1 THEN 'Zdravotníci'::text
            WHEN v.indikace_socialni_sluzby = 1 THEN 'Socilání služby'::text
            WHEN v.indikace_pedagog = 1 THEN 'Pedagogové'::text
            WHEN v.indikace_skolstvi_ostatni = 1 THEN 'Školství ostatní'::text
            WHEN v.chronicke_onemocneni = 1 THEN 'Chronicky nemocní'::text
            WHEN v.bezpecnostni_infrastruktura = 1 THEN 'Bezpečnostní infrastruktura'::text
            ELSE NULL::text
        END AS indikacni_skupina,
    v.kraj_nazev AS kraj_ockovani,
        CASE
            WHEN c.kraj_nazev IS NULL THEN 'Neznámý'::text
            ELSE c.kraj_nazev
        END AS kraj_bydliste,
    v.pohlavi,
    v.poradi_davky,
    v.zarizeni_nazev,
    concat(v.kraj_kod, '_',
        CASE
            WHEN v.vekova_skupina = ANY (ARRAY['12-15'::text, '16-17'::text]) THEN '12-17'::text
            ELSE v.vekova_skupina
        END) AS kraj_ockovani_vek_id,
    concat(c.kraj_kod, '_',
        CASE
            WHEN v.vekova_skupina = ANY (ARRAY['12-15'::text, '16-17'::text]) THEN '12-17'::text
            ELSE v.vekova_skupina
        END) AS kraj_bydliste_vek_id
   FROM uzis.covid19_vaccination_regional_details v
     LEFT JOIN uzis.codebook_regions_orps c ON v.orp_bydliste = c.orp_nazev
  GROUP BY v.datum_vakcinace, v.updated_at, v.vakcina, v.vekova_skupina, v.kraj_nazev, c.kraj_nazev, v.pohlavi, v.poradi_davky, v.zarizeni_nazev, v.zrizovatel, v.kraj_kod, c.kraj_kod, (
        CASE
            WHEN v.indikace_zdravotnik = 1 THEN 'Zdravotníci'::text
            WHEN v.indikace_socialni_sluzby = 1 THEN 'Socilání služby'::text
            WHEN v.indikace_pedagog = 1 THEN 'Pedagogové'::text
            WHEN v.indikace_skolstvi_ostatni = 1 THEN 'Školství ostatní'::text
            WHEN v.chronicke_onemocneni = 1 THEN 'Chronicky nemocní'::text
            WHEN v.bezpecnostni_infrastruktura = 1 THEN 'Bezpečnostní infrastruktura'::text
            ELSE NULL::text
        END);
		

-- analytic.v_uzis_covid19_hospital_capacity source
drop VIEW analytic.v_uzis_covid19_hospital_capacity;
CREATE OR REPLACE VIEW analytic.v_uzis_covid19_hospital_capacity
AS SELECT covid19_hospital_capacity_details.datum,
        CASE
            WHEN covid19_hospital_capacity_details.datum = max(covid19_hospital_capacity_details.datum) OVER (PARTITION BY covid19_hospital_capacity_details.zz_nazev) THEN true
            ELSE false
        END AS posledni_den,
    covid19_hospital_capacity_details.zz_nazev,
    covid19_hospital_capacity_details.kraj_nazev,
    covid19_hospital_capacity_details.luzka_standard_kyslik_kapacita_volna_covid_pozitivni,
    covid19_hospital_capacity_details.luzka_standard_kyslik_kapacita_volna_covid_negativni,
    covid19_hospital_capacity_details.luzka_standard_kyslik_kapacita_celkem,
    covid19_hospital_capacity_details.luzka_hfno_cpap_kapacita_volna_covid_pozitivni,
    covid19_hospital_capacity_details.luzka_hfno_cpap_kapacita_volna_covid_negativni,
    covid19_hospital_capacity_details.luzka_hfno_cpap_kapacita_celkem,
    covid19_hospital_capacity_details.luzka_upv_niv_kapacita_volna_covid_pozitivni,
    covid19_hospital_capacity_details.luzka_upv_niv_kapacita_volna_covid_negativni,
    covid19_hospital_capacity_details.luzka_upv_niv_kapacita_celkem,
    covid19_hospital_capacity_details.inf_luzka_kyslik_kapacita_volna_covid_pozitivni,
    covid19_hospital_capacity_details.inf_luzka_kyslik_kapacita_volna_covid_negativni,
    covid19_hospital_capacity_details.inf_luzka_kyslik_kapacita_celkem,
    covid19_hospital_capacity_details.inf_luzka_hfno_kapacita_volna_covid_pozitivni,
    covid19_hospital_capacity_details.inf_luzka_hfno_kapacita_volna_covid_negativni,
    covid19_hospital_capacity_details.inf_luzka_hfno_kapacita_celkem,
    covid19_hospital_capacity_details.inf_luzka_upv_kapacita_volna_covid_pozitivni,
    covid19_hospital_capacity_details.inf_luzka_upv_kapacita_volna_covid_negativni,
    covid19_hospital_capacity_details.inf_luzka_upv_kapacita_celkem,
    covid19_hospital_capacity_details.ecmo_kapacita_volna,
    covid19_hospital_capacity_details.ecmo_kapacita_celkem,
    covid19_hospital_capacity_details.cvvhd_kapacita_volna,
    covid19_hospital_capacity_details.cvvhd_kapacita_celkem,
    covid19_hospital_capacity_details.ventilatory_prenosne_kapacita_volna,
    covid19_hospital_capacity_details.ventilatory_prenosne_kapacita_celkem,
    covid19_hospital_capacity_details.ventilatory_operacni_sal_kapacita_volna,
    covid19_hospital_capacity_details.ventilatory_operacni_sal_kapacita_celkem
   FROM uzis.covid19_hospital_capacity_details
  WHERE covid19_hospital_capacity_details.datum >= '2021-04-01 00:00:00'::timestamp without time zone;
  
  -- analytic.v_uzis_covid19_hospital_capacity_beds source

CREATE OR REPLACE VIEW analytic.v_uzis_covid19_hospital_capacity_beds
AS SELECT covid19_hospital_capacity_details.datum,
        CASE
            WHEN covid19_hospital_capacity_details.datum = max(covid19_hospital_capacity_details.datum) OVER (PARTITION BY covid19_hospital_capacity_details.zz_nazev) THEN true
            ELSE false
        END AS posledni_den,
    covid19_hospital_capacity_details.zz_nazev,
    covid19_hospital_capacity_details.kraj_nazev,
    covid19_hospital_capacity_details.luzka_standard_kyslik_kapacita_volna_covid_pozitivni AS kapacita_volna_covid_pozitivni_luzka,
    covid19_hospital_capacity_details.luzka_standard_kyslik_kapacita_volna_covid_negativni AS kapacita_volna_covid_negativni_luzka,
    covid19_hospital_capacity_details.luzka_standard_kyslik_kapacita_celkem AS kapacita_celkem_luzka,
    covid19_hospital_capacity_details.luzka_standard_kyslik_kapacita_celkem - covid19_hospital_capacity_details.luzka_standard_kyslik_kapacita_volna_covid_pozitivni - covid19_hospital_capacity_details.luzka_standard_kyslik_kapacita_volna_covid_negativni AS kapacita_zaplnena_luzka,
    'luzka_standard_kyslik'::text AS typ,
    'Běžné'::text AS oddeleni
   FROM uzis.covid19_hospital_capacity_details
  WHERE covid19_hospital_capacity_details.datum >= '2021-04-01 00:00:00'::timestamp without time zone
UNION ALL
 SELECT covid19_hospital_capacity_details.datum,
        CASE
            WHEN covid19_hospital_capacity_details.datum = max(covid19_hospital_capacity_details.datum) OVER (PARTITION BY covid19_hospital_capacity_details.zz_nazev) THEN true
            ELSE false
        END AS posledni_den,
    covid19_hospital_capacity_details.zz_nazev,
    covid19_hospital_capacity_details.kraj_nazev,
    covid19_hospital_capacity_details.luzka_hfno_cpap_kapacita_volna_covid_pozitivni AS kapacita_volna_covid_pozitivni_luzka,
    covid19_hospital_capacity_details.luzka_hfno_cpap_kapacita_volna_covid_negativni AS kapacita_volna_covid_negativni_luzka,
    covid19_hospital_capacity_details.luzka_hfno_cpap_kapacita_celkem AS kapacita_celkem_luzka,
    covid19_hospital_capacity_details.luzka_hfno_cpap_kapacita_celkem - covid19_hospital_capacity_details.luzka_hfno_cpap_kapacita_volna_covid_pozitivni - covid19_hospital_capacity_details.luzka_hfno_cpap_kapacita_volna_covid_negativni AS kapacita_zaplnena_luzka,
    'luzka_hfno_cpap'::text AS typ,
    'Běžné'::text AS oddeleni
   FROM uzis.covid19_hospital_capacity_details
  WHERE covid19_hospital_capacity_details.datum >= '2021-04-01 00:00:00'::timestamp without time zone
UNION ALL
 SELECT covid19_hospital_capacity_details.datum,
        CASE
            WHEN covid19_hospital_capacity_details.datum = max(covid19_hospital_capacity_details.datum) OVER (PARTITION BY covid19_hospital_capacity_details.zz_nazev) THEN true
            ELSE false
        END AS posledni_den,
    covid19_hospital_capacity_details.zz_nazev,
    covid19_hospital_capacity_details.kraj_nazev,
    covid19_hospital_capacity_details.luzka_upv_niv_kapacita_volna_covid_pozitivni AS kapacita_volna_covid_pozitivni_luzka,
    covid19_hospital_capacity_details.luzka_upv_niv_kapacita_volna_covid_negativni AS kapacita_volna_covid_negativni_luzka,
    covid19_hospital_capacity_details.luzka_upv_niv_kapacita_celkem AS kapacita_celkem_luzka,
    covid19_hospital_capacity_details.luzka_upv_niv_kapacita_celkem - covid19_hospital_capacity_details.luzka_upv_niv_kapacita_volna_covid_pozitivni - covid19_hospital_capacity_details.luzka_upv_niv_kapacita_volna_covid_negativni AS kapacita_zaplnena_luzka,
    'luzka_upv_niv'::text AS typ,
    'Běžné'::text AS oddeleni
   FROM uzis.covid19_hospital_capacity_details
  WHERE covid19_hospital_capacity_details.datum >= '2021-04-01 00:00:00'::timestamp without time zone
UNION ALL
 SELECT covid19_hospital_capacity_details.datum,
        CASE
            WHEN covid19_hospital_capacity_details.datum = max(covid19_hospital_capacity_details.datum) OVER (PARTITION BY covid19_hospital_capacity_details.zz_nazev) THEN true
            ELSE false
        END AS posledni_den,
    covid19_hospital_capacity_details.zz_nazev,
    covid19_hospital_capacity_details.kraj_nazev,
    covid19_hospital_capacity_details.inf_luzka_kyslik_kapacita_volna_covid_pozitivni AS kapacita_volna_covid_pozitivni_luzka,
    covid19_hospital_capacity_details.inf_luzka_kyslik_kapacita_volna_covid_negativni AS kapacita_volna_covid_negativni_luzka,
    covid19_hospital_capacity_details.inf_luzka_kyslik_kapacita_celkem AS kapacita_celkem_luzka,
    covid19_hospital_capacity_details.inf_luzka_kyslik_kapacita_celkem - covid19_hospital_capacity_details.inf_luzka_kyslik_kapacita_volna_covid_pozitivni - covid19_hospital_capacity_details.inf_luzka_kyslik_kapacita_volna_covid_negativni AS kapacita_zaplnena_luzka,
    'luzka_standard_kyslik'::text AS typ,
    'Infekční'::text AS oddeleni
   FROM uzis.covid19_hospital_capacity_details
  WHERE covid19_hospital_capacity_details.datum >= '2021-04-01 00:00:00'::timestamp without time zone
UNION ALL
 SELECT covid19_hospital_capacity_details.datum,
        CASE
            WHEN covid19_hospital_capacity_details.datum = max(covid19_hospital_capacity_details.datum) OVER (PARTITION BY covid19_hospital_capacity_details.zz_nazev) THEN true
            ELSE false
        END AS posledni_den,
    covid19_hospital_capacity_details.zz_nazev,
    covid19_hospital_capacity_details.kraj_nazev,
    covid19_hospital_capacity_details.inf_luzka_hfno_kapacita_volna_covid_pozitivni AS kapacita_volna_covid_pozitivni_luzka,
    covid19_hospital_capacity_details.inf_luzka_hfno_kapacita_volna_covid_negativni AS kapacita_volna_covid_negativni_luzka,
    covid19_hospital_capacity_details.inf_luzka_hfno_kapacita_celkem AS kapacita_celkem_luzka,
    covid19_hospital_capacity_details.inf_luzka_hfno_kapacita_celkem - covid19_hospital_capacity_details.inf_luzka_hfno_kapacita_volna_covid_pozitivni - covid19_hospital_capacity_details.inf_luzka_hfno_kapacita_volna_covid_negativni AS kapacita_zaplnena_luzka,
    'luzka_hfno_cpap'::text AS typ,
    'Infekční'::text AS oddeleni
   FROM uzis.covid19_hospital_capacity_details
  WHERE covid19_hospital_capacity_details.datum >= '2021-04-01 00:00:00'::timestamp without time zone
UNION ALL
 SELECT covid19_hospital_capacity_details.datum,
        CASE
            WHEN covid19_hospital_capacity_details.datum = max(covid19_hospital_capacity_details.datum) OVER (PARTITION BY covid19_hospital_capacity_details.zz_nazev) THEN true
            ELSE false
        END AS posledni_den,
    covid19_hospital_capacity_details.zz_nazev,
    covid19_hospital_capacity_details.kraj_nazev,
    covid19_hospital_capacity_details.inf_luzka_upv_kapacita_volna_covid_pozitivni AS kapacita_volna_covid_pozitivni_luzka,
    covid19_hospital_capacity_details.inf_luzka_upv_kapacita_volna_covid_negativni AS kapacita_volna_covid_negativni_luzka,
    covid19_hospital_capacity_details.inf_luzka_upv_kapacita_celkem AS kapacita_celkem_luzka,
    covid19_hospital_capacity_details.inf_luzka_upv_kapacita_celkem - covid19_hospital_capacity_details.inf_luzka_upv_kapacita_volna_covid_pozitivni - covid19_hospital_capacity_details.inf_luzka_upv_kapacita_volna_covid_negativni AS kapacita_zaplnena_luzka,
    'luzka_upv_niv'::text AS typ,
    'Infekční'::text AS oddeleni
   FROM uzis.covid19_hospital_capacity_details
  WHERE covid19_hospital_capacity_details.datum >= '2021-04-01 00:00:00'::timestamp without time zone;
  
drop VIEW analytic.v_uzis_covid19_hospital_capacity_technologies;  