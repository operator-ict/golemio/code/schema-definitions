

DROP TABLE IF EXISTS public.wazett_subroute_lives;
DROP TABLE IF EXISTS public.wazett_route_lives;


DROP TABLE IF EXISTS public.wazett_subroutes;
DROP SEQUENCE IF EXISTS public.wazett_subroutes_id_seq;

DROP TABLE IF EXISTS public.wazett_routes;
DROP SEQUENCE IF EXISTS public.wazett_routes_id_seq;

DROP TABLE IF EXISTS public.wazett_jams_stats;

DROP TABLE IF EXISTS public.wazett_feeds;
DROP SEQUENCE IF EXISTS public.wazett_feeds_id_seq;






