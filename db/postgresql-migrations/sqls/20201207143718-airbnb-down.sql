DROP VIEW analytic.v_airbnb_start_end_monthly;
DROP VIEW analytic.v_airbnb_room_per_user;
DROP VIEW analytic.v_airbnb_overview_12m;
DROP VIEW analytic.v_airbnb_districts_last12m;
DROP VIEW analytic.v_airbnb_dim_regularity;
DROP VIEW analytic.v_airbnb_occupancy_monthly;
DROP VIEW analytic.v_airbnb_dim_offertype;

DROP MATERIALIZED VIEW analytic.v_airbnb_occupancy_monthly_materialized;

DROP VIEW analytic.v_airbnb_listings;
DROP VIEW analytic.v_airbnb_dim_district;

Drop TABLE public.airbnb_occupancy;
Drop TABLE public.airbnb_listings;
Drop TABLE public.airbnb_apartment_count;