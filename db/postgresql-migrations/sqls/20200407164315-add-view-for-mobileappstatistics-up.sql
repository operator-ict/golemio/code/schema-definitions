CREATE OR REPLACE VIEW analytic.v_ropidbi_litacka_installs
AS SELECT t1.begin_day,
    t1.appstore_count,
    t2.gplay_count,
    t1.appstore_count + t2.gplay_count AS total_count
FROM (
    SELECT
        mobileappstatistics_appstore.begin_day,
        sum(mobileappstatistics_appstore.event_count) AS appstore_count
    FROM mobileappstatistics_appstore
    WHERE mobileappstatistics_appstore.app_id::text = 'mhd-en'::text AND mobileappstatistics_appstore.event_type::text = '1F'::text
    GROUP BY mobileappstatistics_appstore.begin_day
) t1
LEFT JOIN (
    SELECT
        mobileappstatistics_playstore.begin_day,
    sum(mobileappstatistics_playstore.install_events) AS gplay_count
    FROM mobileappstatistics_playstore
    WHERE mobileappstatistics_playstore.package_name::text = 'cz.dpp.praguepublictransport'::text
        AND mobileappstatistics_playstore.begin_day >= (( SELECT min(mobileappstatistics_appstore.begin_day) AS min
    FROM mobileappstatistics_appstore))
    GROUP BY mobileappstatistics_playstore.begin_day
) t2
ON t1.begin_day = t2.begin_day;