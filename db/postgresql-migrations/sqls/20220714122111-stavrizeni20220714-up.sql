-- stavebni rizeni
CREATE TABLE python.construction_procedure (
	uuid varchar(1024) NOT NULL,
	dat_zah date NULL,
	typ varchar(1024) NULL,
	prerus varchar(1024) NULL,
	dat_lhuty_nebezi date NULL,
	dat_prerus date NULL,
	dat_zast date NULL,
	dat_stan_do date NULL,
	dat_termin date NULL,
	dat_ks int4 NULL,
	dat_zp date NULL,
	dat_zp_doba date NULL,
	dat_roz_vyh date NULL,
	dat_roz_vyp date NULL,
	dat_roz_dor date NULL,
	odv varchar(1024) NULL,
	dat_odv date NULL,
	dat_moc date NULL,
	novy varchar(1024) NULL,
	pocet_bytu int4 NULL,
	plocha int4 NULL,
	dat_komplet date NULL,
	mestska_cast varchar(1024) NULL,
	created_at timestamptz NULL default now(),
	updated_at timestamptz NULL,
	CONSTRAINT construction_procedure_pkey PRIMARY KEY (uuid)
);

comment on table python.construction_procedure is 'Základní tabulka - stavební řízení';

-- stavebni_rizeni.stavebni_rizeni_cis_uct_jed definition

-- Drop table

-- DROP TABLE stavebni_rizeni.stavebni_rizeni_cis_uct_jed;

CREATE TABLE python.construction_procedure_account_unit (
	zc_ucjed varchar(50) NULL,
	dateto varchar(50) NULL,
	datefrom varchar(50) NULL,
	zc_ico varchar(50) NULL,
	zc_dic varchar(50) NULL,
	street60 varchar(250) NULL,
	house_num1 varchar(20) NULL,
	city_1 varchar(150) NULL,
	postalcode varchar(10) NULL,
	zc_ujzri varchar(50) NULL,
	zc_nace varchar(50) NULL,
	zc_nuts varchar(20) NULL,
	zc_cofog varchar(50) NULL,
	zc_ujkat varchar(50) NULL,
	zc_isekt varchar(50) NULL,
	fm_area varchar(50) NULL,
	zc_duif varchar(50) NULL,
	zc_uradf varchar(50) NULL,
	zc_forma varchar(50) NULL,
	zc_pocob varchar(50) NULL,
	zc_ad020 varchar(250) NULL,
	zc_ad021 varchar(250) NULL,
	zc_ad022 varchar(250) NULL,
	zc_ad023 varchar(250) NULL,
	zc_ad024 varchar(250) NULL,
	zc_expdat varchar(50) NULL,
	zc_credat varchar(50) NULL,
	zc_publst varchar(10) NULL,
	zc_veduc varchar(50) NULL,
	zc_ujzm4 varchar(50) NULL,
	zc_zpodm varchar(50) NULL,
	zc_dri varchar(50) NULL,
	zc_statni varchar(50) NULL,
	zc_toss varchar(50) NULL,
	zc_ujzfo varchar(50) NULL,
	zc_zuj varchar(50) NULL,
	zc_cispou varchar(50) NULL,
	zc_cisorp varchar(50) NULL,
	zc_aktdat varchar(50) NULL,
	created_at timestamptz NULL default now(),
	updated_at timestamptz NULL
);
comment on table python.construction_procedure_account_unit is 'číselník - stavebni_rizeni_cis_uct_jed';

--cadastral

CREATE TABLE python.construction_procedure_cadastral (
	uuid varchar(1024) NULL,
--	ku_order int4 NULL,
	ku int4 NULL,
	created_at timestamptz NULL default now(),
	updated_at timestamptz NULL
);

comment on table python.construction_procedure_cadastral is 'stavební řízení dotčené katastrální území - stavebni_rizeni_ku';

CREATE TABLE python.construction_procedure_cadastral_list (
	ku_kod varchar(50) NULL,
	ku_prac varchar(50) NULL,
	ku_nazev varchar(50) NULL,
	platnost_od varchar(20) NULL,
	platnost_do varchar(20) NULL,
	obec_kod varchar(50) NULL,
	obec_nazev varchar(150) NULL,
	prares_kod varchar(50) NULL,
	prares_nazev varchar(250) NULL,
	poznamka varchar(500) NULL,
	created_at timestamptz NULL default now(),
	updated_at timestamptz NULL
);

comment on table python.construction_procedure_cadastral is 'stavební řízení - číselník katastrálních území (stavebni_rizeni_ku_seznam)';

-- stavebni_rizeni.stavebni_rizeni_stan definition

CREATE TABLE python.construction_procedure_statement (
	uuid varchar(1024) NULL,
--	ku_order int4 NULL,
	ico varchar(1024) NULL,
	dat_pod varchar(1024) NULL,
	stan varchar(1024) NULL,
	created_at timestamptz NULL default now(),
	updated_at timestamptz NULL
);

comment on table python.construction_procedure_statement is 'stavební řízení - stanovsika (stavebni_rizeni_stan)';

-- stavebni_rizeni.stavebni_rizeni_typ_rizeni definition

CREATE TABLE python.construction_procedure_type (
	typ_rizeni varchar(3) NULL,
	popis_rizeni varchar(65) NULL,
	created_at timestamptz NULL default now(),
	updated_at timestamptz NULL
);

comment on table python.construction_procedure_type is 'stavební řízení - typy řízení (stavebni_rizeni_typ_rizeni)';

-- stavebni_rizeni.stavebni_rizeni_typ_stanoviska definition

CREATE TABLE python.construction_procedure_type_statemnt (
	sta_kod varchar(1) NULL,
	sta_popis varchar(18) NULL,
	created_at timestamptz NULL default now(),
	updated_at timestamptz NULL
);

comment on table python.construction_procedure_type_statemnt is 'stavební řízení - typy stanovisek';

-- stavebni_rizeni.v_stavebni_rizeni source

CREATE OR REPLACE VIEW analytic.v_construction_procedure
AS SELECT srtr.popis_rizeni,
    sr.uuid,
    sr.dat_zah,
    sr.typ,
    sr.prerus,
    sr.dat_lhuty_nebezi,
    sr.dat_prerus,
    sr.dat_zast,
    sr.dat_stan_do,
    sr.dat_termin,
    sr.dat_ks,
    sr.dat_zp,
    sr.dat_zp_doba,
    sr.dat_roz_vyh,
    sr.dat_roz_vyp,
    sr.dat_roz_dor,
    sr.odv,
    sr.dat_odv,
    sr.dat_moc,
    sr.novy,
    sr.pocet_bytu,
    sr.plocha,
    sr.dat_komplet,
    sr.mestska_cast,
    sr.created_at,
        CASE
            WHEN
            CASE
                WHEN sr.dat_moc IS NOT NULL THEN sr.dat_moc - sr.dat_zah
                WHEN sr.prerus::text = ANY (ARRAY['O'::character varying, 'Z'::character varying, 'S'::character varying]::text[]) THEN sr.dat_roz_vyp - sr.dat_zah
                ELSE NULL::integer
            END < 0 THEN NULL::integer
            ELSE
            CASE
                WHEN sr.dat_moc IS NOT NULL THEN sr.dat_moc - sr.dat_zah
                WHEN sr.prerus::text = ANY (ARRAY['O'::character varying, 'Z'::character varying, 'S'::character varying]::text[]) THEN sr.dat_roz_vyp - sr.dat_zah
                ELSE NULL::integer
            END
        END AS zpracovano_dnu,
        CASE
            WHEN sr.dat_moc IS NULL AND (sr.prerus::text <> ALL (ARRAY['O'::character varying, 'Z'::character varying, 'S'::character varying]::text[])) AND (CURRENT_DATE - sr.dat_zah) > 182 THEN 1
            ELSE NULL::integer
        END AS prekroceno_pul_roku,
        CASE
            WHEN sr.dat_moc IS NULL AND (sr.prerus::text <> ALL (ARRAY['O'::character varying, 'Z'::character varying, 'S'::character varying]::text[])) AND (CURRENT_DATE - sr.dat_zah) > 365 THEN 1
            ELSE NULL::integer
        END AS prekrocen_rok,
        CASE
            WHEN sr.dat_moc IS NOT NULL THEN 0
            WHEN sr.prerus::text = ANY (ARRAY['O'::character varying, 'Z'::character varying, 'S'::character varying]::text[]) THEN 0
            ELSE 1
        END AS neukoncene
   FROM python.construction_procedure sr
     LEFT JOIN python.construction_procedure_type srtr ON sr.typ::text = srtr.typ_rizeni::text
  ORDER BY sr.dat_zah;