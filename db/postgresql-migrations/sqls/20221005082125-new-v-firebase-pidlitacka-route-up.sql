CREATE OR REPLACE VIEW analytic.v_firebase_pidlitacka_route
AS SELECT date_trunc('month'::text, fpr.reference_date::timestamp with time zone) AS reference_date,
    fpr.s_from,
    fpr.s_to,
    sum(fpr.count_case) AS count_case
   FROM public.firebase_pidlitacka_route fpr
  WHERE fpr.s_from::text <> '?'::text AND fpr.reference_date >= (now() - '2 years'::interval)
  GROUP BY (date_trunc('month'::text, fpr.reference_date::timestamp with time zone)), fpr.s_from, fpr.s_to;