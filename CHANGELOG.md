# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

### Changed

-   Mark as deprecated ([schema-definitions#55](https://gitlab.com/operator-ict/golemio/code/modules/schema-definitions/-/issues/55)

## [2.4.2] - 2023-10-25

-   No changelog

## [2.4.1] - 2023-10-18

### Removed

-   odstrnění fce z analytic

## [2.4.0] - 2023-10-18

### Changed

-   Migration of RSD tables from the public schema([schema-definitions#55](https://gitlab.com/operator-ict/golemio/code/modules/schema-definitions/-/issues/55))

## [2.3.10] - 2023-10-16

### Changed

-   NodeJS 18.17.0
-   odstrnaění části pohledů analytic (úklid, část 1)
-   odstrnaění části pohledů analytic (úklid, část 2)

## [2.3.9] - 2023-07-26

### Changed

-   mos schema migration [mos#4](https://gitlab.com/operator-ict/golemio/code/modules/mos/-/issues/4)

### Added

-   add table: python.gitlab [issue](https://gitlab.com/operator-ict/golemio/projekty/general/-/issues/1793)

### Removed

-   firebase pid litacka from public after schema separation

## [2.3.8] - 2023-07-24

### Removed

-   mobile app statistics after schema separation

## [2.3.7] - 2023-07-17

### Removed

-   unused `analytics.v_ropidbi_litacka_installs` view ([#8](https://gitlab.com/operator-ict/golemio/code/modules/mobile-app-statistics/-/issues/8))

## [2.3.6] - 2023-06-12

### Changed

-   Remove tables and views for grants, [issue](https://gitlab.com/operator-ict/golemio/projekty/general/-/issues/174)
-   Move intermodal migration to new traffic-common module

## [2.3.5] - 2023-05-10

### Changed

-   change table: python.public_tenders_sck, [issue](https://gitlab.com/operator-ict/golemio/projekty/sck/verejne-zakazky-sck/-/issues/3)

### Added

-   add views: analytic.v_public_tenders_sck, analytic.v_public_tenders_sck_internal and analytic.v_public_tenders_sck_internal_frequent_suppliers, [issue](https://gitlab.com/operator-ict/golemio/projekty/sck/verejne-zakazky-sck/-/issues/3)

## [2.3.4] - 2023-05-03

### Removed

-   Mongo removed
-   Drop the `public.error_log` table ([core#61](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/61))

## [2.3.3] - 2023-04-19

### Added

-   add tables: python.public_tenders_sck and python.public_tenders_sck_funded_organisations, [issue](https://gitlab.com/operator-ict/golemio/projekty/sck/verejne-zakazky-sck/-/issues/2)

## [2.3.2] - 2023-04-17

### Removed

-   remove table: public.wazett\_\* (https://gitlab.com/operator-ict/golemio/projekty/oict/d0207.-waze-dojezdove-doby/-/issues/44#note_1343579783)

### Changed

-   Modify table analytic.pid_pricelist by setting correct date types and removing unused columns. Adjust dependent views.

### Fixed

-   fix: add PK into table uzis.covid19_cz_daily
-   analytic.v_ropidbi_ticket_sales_monthly_by_zones correct wrong string->int conversion

## [2.3.1] - 2023-03-27

### Changed

-   New structure of the table python.ambulance_measurements, [issue](https://gitlab.com/operator-ict/golemio/projekty/mhmp/p0240.-zachranka/-/issues/9).

## [2.3.0] - 2023-02-27

### Added

-   Adding two new tables into database, specifically, python.ambulance_measurements and python.ambulance_indication. [Issue](https://gitlab.com/operator-ict/golemio/projekty/mhmp/p0240.-zachranka/-/issues/6)

### Changed

-   Update Node.js to v18.14.0 ([core#50](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/50))

### Removed

-   remove table: python.consumption_electricity_oict_api2 (https://gitlab.com/operator-ict/golemio/projekty/energetika/spotreby-energii-mestskych-budov/-/issues/72)
-   `@golemio/utils` dependency ([utils#2](https://gitlab.com/operator-ict/golemio/code/utils/-/issues/2))
-   `mongoose-to-jsonschema.js` unused util script
-   remove table: python.consumption_electricity_oict_api2 (https://gitlab.com/operator-ict/golemio/projekty/energetika/spotreby-energii-mestskych-budov/-/issues/72)

## [2.2.1] - 2023-02-20

### Removed

-   Waste collection: views and tables for old integration. [Issue](https://gitlab.com/operator-ict/golemio/projekty/oict/p0149-chytry-svoz-odpadu/-/issues/269)

### Changed

-   Modification of three views, [issue](https://gitlab.com/operator-ict/golemio/projekty/ropid/p0100-bi-pro-ropid/-/issues/77).

## [2.2.0] - 2023-01-25

### Changed

-   Remove constraint public_tenders_funded_organisations_name for table `python.public_tenders_funded_organisations`

## [2.1.0] - 2023-01-23

### Changed

-   Migrate to npm

## [2.0.51] - 2023-01-04

-   No changelog

## [2.0.50] - 2022-12-07

-   No changelog

## [2.0.49] - 2022-11-29

### Added

-   Add changelog
